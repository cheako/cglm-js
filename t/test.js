try { chai.exists; } catch(e) { chai = require('chai') ; }

require('tap').mochaGlobals();

should = chai.should(),
  expect = chai.expect;

// note: scaffolding for testing with node CLI
if (typeof cglmCreate !== 'function' || !cglmCreate) {
   try { _ENV = require('../xjs._ENV') || _ENV; /* for cscript / old engine testing */ } catch(e) { }
   try { console.exists } catch(e) { console = _ENV.console; }
   IMPLEMENTATIONS = {
      'cglm': function() { cglmCreate = require('../build/cglm.js'); },
      'cglm-min': function() { cglmCreate = require('../build/cglm.min.js'); },
   };
   try { var G = process.env.GLM; } catch(e) { G = environment.GLM; }
   if (!(G in IMPLEMENTATIONS)) {
      console.error('example invocation:\n\tenv GLM=<'+Object.keys(IMPLEMENTATIONS).join("|")+"> tap -J -c -Rspec --cov --coverage-report=text ${TESTDIR:-t}/*.js\n");
      process.exit(-2);
   }
   console.warn('IMPLEMENTATIONS['+G+']()');
    try {
        IMPLEMENTATIONS[G]();
    } catch(e) {
        console.error(e);
    }
   if (!cglmCreate) {
       console.error('could not load implementation ' + G);
       process.exit(-3);
   }
}

glm = new cglmCreate();

describe('glm', function(){
	it('add', function() {
		expect(glm.add(2,2)).to.eql(4);
	});
});

