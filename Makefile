
.SUFFIXES: .js.in .js

M4       = m4
M4FLAGS  =
M4SCRIPT =

%.js: %.js.in
	${M4} ${M4FLAGS} ${M4SCRIPT} $< > $*.js

all: build/cglm.min.js

build/cglm.js: $(wildcard src/*.js) $(wildcard src/*.js.in) $(wildcard lib/*.js) $(wildcard lib/*.js.in) upstream/libcglm.js tools/libfoot.awk

clean:
	rm -f build/cglm.min.js

dist-clean: clean
	rm -f build/cglm.js

.PHONY: all clean dist-clean

build/%.min.js: build/%.js
	java -jar extern/closure-compiler.jar --language_in ECMASCRIPT6 --js_output_file $@ --js $<

