__newType__(mat4, 4 * 4);

/**
 * @brief Set mat4 to value
 */
mat4.prototype.set = function $mat4$set() {
        if (arguments.length < 1) {
                for (var i = 0; i < 4 * 4; i++)
                        this.storage[i] = 0;
                return;
        }
        if (arguments[0] instanceof _vec) {
                this.storage[0] = arguments[0].storage[0];
                this.storage[1] = this.storage[2] = this.storage[3] = 0;
                this.storage[5] = arguments[0].storage[1];
                this.storage[4] = this.storage[6] = this.storage[7] = 0;
                this.storage[10] = arguments[0].storage[2];
                this.storage[8] = this.storage[9] = this.storage[11] = 0;
                this.storage[12] = this.storage[13] = this.storage[14] = this.storage[15] = 0;
        } else if (arguments[0] instanceof _vec4) {
                this.storage[0] = arguments[0].storage[0];
                this.storage[1] = this.storage[2] = this.storage[3] = 0;
                this.storage[5] = arguments[0].storage[1];
                this.storage[4] = this.storage[6] = this.storage[7] = 0;
                this.storage[10] = arguments[0].storage[2];
                this.storage[8] = this.storage[9] = this.storage[11] = 0;
                this.storage[15] = arguments[0].storage[3];
                this.storage[12] = this.storage[13] = this.storage[14] = 0;
        } else if (arguments[0] instanceof _mat3) {
                for (var i = 0; i < 3; i++)
                        this.storage[i] = arguments[0].storage[i];
                this.storage[3] = 0;
                for (var i = 3; i < 6; i++)
                        this.storage[i + 1] = arguments[0].storage[i];
                this.storage[7] = 0;
                for (var i = 6; i < 9; i++)
                        this.storage[i + 2] = arguments[0].storage[i];
                this.storage[11] = 0;
                this.storage[15] = 1;
                this.storage[12] = this.storage[13] = this.storage[14] = 0;
        } else if (arguments[0] instanceof _mat4) {
                for (var i = 0; i < 4 * 4; i++)
                        this.storage[i] = arguments[0].storage[i];
        } else {
                switch (arguments.length) {
                        case 0:
                                this.storage[0] = 1;
                                this.storage[1] = this.storage[2] = this.storage[3] = 0;
                                this.storage[5] = 1;
                                this.storage[4] = this.storage[6] = this.storage[7] = 0;
                                this.storage[10] = 1;
                                this.storage[8] = this.storage[9] = this.storage[11] = 0;
                                this.storage[15] = 1;
                                this.storage[12] = this.storage[13] = this.storage[14] = 0;
                                break;
                        case 1:
                                this.storage[0] = this.storage[5] = this.storage[10] = arguments[0];
                                this.storage[1] = this.storage[2] = this.storage[3] = 0;
                                this.storage[4] = this.storage[6] = this.storage[7] = 0;
                                this.storage[8] = this.storage[9] = this.storage[11] = 0;
                                this.storage[15] = 1;
                                this.storage[12] = this.storage[13] = this.storage[14] = 0;
                                break;
                        case 3:
                                this.storage[0] = arguments[0];
                                this.storage[1] = this.storage[2] = this.storage[3] = 0;
                                this.storage[5] = arguments[1];
                                this.storage[4] = this.storage[6] = this.storage[7] = 0;
                                this.storage[10] = arguments[2];
                                this.storage[8] = this.storage[9] = this.storage[11] = 0;
                                this.storage[15] = 1;
                                this.storage[12] = this.storage[13] = this.storage[14] = 0;
                                break;
                        case 4:
                                this.storage[0] = arguments[0];
                                this.storage[1] = this.storage[2] = this.storage[3] = 0;
                                this.storage[5] = arguments[1];
                                this.storage[4] = this.storage[6] = this.storage[7] = 0;
                                this.storage[10] = arguments[2];
                                this.storage[8] = this.storage[9] = this.storage[11] = 0;
                                this.storage[15] = arguments[3];
                                this.storage[12] = this.storage[13] = this.storage[14] = 0;
                                break;
                        case 16:
                                for (var i = 0; i < 4 * 4; i++)
                                        this.storage[i] = arguments[i];
                                break;
                        default:
                                assert(0 && "Not implemented.");
                }
        }
};
