__newType__(vec,3);

/**
 * @brief Set vec to value
 */
vec.prototype.set = function $vec$set() {
        if (arguments.length < 1) {
                for (var i = 0; i < 3; i++) this.storage[i] = 0;
                return;
        }
        if (arguments[0] instanceof vec || arguments[0] instanceof vec4) {
                for (var i = 0; i < 3; i++)
                        this.storage[i] = arguments[0].storage[i];
        } else if (arguments[0] instanceof vec2) {
                for (var i = 0; i < 2; i++)
                        this.storage[i] = arguments[0].storage[i];
                this[2] = 0;
        } else {
                switch (arguments.length) {
                        case 1:
                                this.storage[0] = this.storage[1] =
                                        this.storage[2] = arguments[0];
                                break;
                        case 2:
                                for (var i = 0; i < 2; i++)
                                        this.storage[i] = arguments[i];
                                this[2] = 0;
                                break;
                        case 3:
                        case 4:
                                for (var i = 0; i < 3; i++)
                                        this.storage[i] = arguments[i];
                                break;
                        default:
                                for (var i = 0; i < 3; i++) this.storage[i] = 0;
                }
        }
};
