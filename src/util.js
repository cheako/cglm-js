
var getTypeSize = function (type) {
        var size = Module.getNativeTypeSize(type)|0;
        if (!size) {
                var floatSize = Module.getNativeTypeSize('float')|0;
                switch (type) {
                        case 'vec2':
                                return floatSize * 2;
                        case 'vec':
                        case 'vec3':
                                return floatSize * 3;
                        case 'vec4':
                                return floatSize * 4;
                        case 'mat3':
                                return floatSize * 3 * 3;
                        case 'mat4':
                                return floatSize * 4 * 4;
                }
        } else
                return size;
        assert(0 && "Type size unknown.");
};

var typeMalloc = function (type) {
        return Module.ccall('malloc', 'number', ['number'], [getTypeSize(type)]);
};
