#!/bin/sh

export CFLAGS="-g -O3"
make &&
emcc $CFLAGS -s EXPORTED_FUNCTIONS="[$(
grep -h -o '^glmc_[^(]*' src/*.c |
    awk 'BEGIN { ORS=""; };
         /_print$/ { next; };
         NR==1 { print "'\''_" $_ "'\''"; };
         NR!=1 { print ", '\''_" $_ "'\''"; };'
)]" --memory-init-file 0 .libs/libcglm.so.0.0.1 -o libcglm.js
