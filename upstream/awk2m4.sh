#!/bin/sh
awk -F'[[:space:](]+' -- '
	BEGIN {
		print "ifelse(`"
		print "	First letters:"
		print "		r Function has return"
		print "		d last param to function is destination, delete this"
        	print "		v function uses first param as destination, return this"
		print "'\'')dnl";
	};
	_brief && ( \
		/ \*[[:space:]]*$/ || \
	       	/^CGLM_INLINE/ || \
		$2 ~ /^@/ \
	) { brief=_brief; _brief=""; };
	_brief { for(i=2;i<=NF;++i)_brief=_brief" "$i; };
	/^ \* @brief/ { _brief=$4; for(i=5;i<=NF;++i)_brief=_brief" "$i; };
	cglm_inline == 2 && /);$/ { cglm_inline=0; return_number=0; brief=""; };
	cglm_inline == 2 && /^glm_/ {
		funcname=$1;
		sub(/^glm_/,"",funcname);
		firsttype=$2;
		for(i=2;$i != "{" && i<=NF;i+=2)types=types":"$i;
	};
	function makemodulefunctionname(n) {
		if(modulefunctions[n]) {
			return makemodulefunctionname(n "_extra");
		}
		modulefunctions[n]=1;
		return n;
	}
	function makeobjectfunctionname(t, n) {
		if(objectfunctions[t][n]) {
			return makeobjectfunctionname(t, n "_extra");
		}
		objectfunctions[t][n]=1;
		return n;
	}
	function output(r) {
		typematch="^" (firsttype == "vec3" ? "vec" : firsttype) "_";
		partname=funcname;
		sub(typematch, "", partname);
		print "deffunc(" r types ", " funcname ", " \
			makemodulefunctionname(funcname) ", " \
			makeobjectfunctionname(firsttype, partname) ", ``" \
				brief "'\'\'')dnl";
		funcname=""; firsttype=""; types="";
		cglm_inline=0; return_number=0;
	};
	cglm_inline == 2 && /dest[\[0-9\]]*) {/ {
		if(!match($1, /^glm_/))
			for(i=2;$i != "{" && i<=NF;i+=2)types=types":"$i;
		output(return_number ? "r*" : "d");
		next;
	};
	cglm_inline == 2 && /) {/ {
		if(!match($1, /^glm_/))
			for(i=2;$i != "{" && i<=NF;i+=2)types=types":"$i;
		output(return_number ? "r" : "v");
		next;
	};
	cglm_inline == 2 {
		if(!match($1, /^glm_/))
			for(i=2;$i != "{" && i<=NF;i+=2)types=types":"$i;
	};
	cglm_inline == 1 && /void/ { cglm_inline=2; };
	cglm_inline == 1 { return_number=1; cglm_inline=2; };
	/^CGLM_INLINE/ { cglm_inline=1; };
' include/cglm/*.h
