// The Module object: Our interface to the outside world. We import
// and export values on it. There are various ways Module can be used:
// 1. Not defined. We create it here
// 2. A function parameter, function(Module) { ..generated code.. }
// 3. pre-run appended it, var Module = {}; ..generated code..
// 4. External script tag defines var Module.
// We need to check if Module already exists (e.g. case 3 above).
// Substitution will be replaced with actual code on later stage of the build,
// this way Closure Compiler will not mangle it (e.g. case 4. above).
// Note that if you want to run closure, and also to use Module
// after the generated code, you will need to define   var Module = {};
// before the code. Then that object will be used in the code, and you
// can continue to use Module afterwards as well.
var Module = typeof Module !== 'undefined' ? Module : {};

// --pre-jses are emitted after the Module integration code, so that they can
// refer to Module (if they choose; they can also define Module)
// {{PRE_JSES}}

// Sometimes an existing Module object exists with properties
// meant to overwrite the default module functionality. Here
// we collect those properties and reapply _after_ we configure
// the current environment's defaults to avoid having to be so
// defensive during initialization.
var moduleOverrides = {};
var key;
for (key in Module) {
  if (Module.hasOwnProperty(key)) {
    moduleOverrides[key] = Module[key];
  }
}

Module['arguments'] = [];
Module['thisProgram'] = './this.program';
Module['quit'] = function(status, toThrow) {
  throw toThrow;
};
Module['preRun'] = [];
Module['postRun'] = [];

// The environment setup code below is customized to use Module.
// *** Environment setup code ***
var ENVIRONMENT_IS_WEB = false;
var ENVIRONMENT_IS_WORKER = false;
var ENVIRONMENT_IS_NODE = false;
var ENVIRONMENT_IS_SHELL = false;

// Three configurations we can be running in:
// 1) We could be the application main() thread running in the main JS UI thread. (ENVIRONMENT_IS_WORKER == false and ENVIRONMENT_IS_PTHREAD == false)
// 2) We could be the application main() thread proxied to worker. (with Emscripten -s PROXY_TO_WORKER=1) (ENVIRONMENT_IS_WORKER == true, ENVIRONMENT_IS_PTHREAD == false)
// 3) We could be an application pthread running in a worker. (ENVIRONMENT_IS_WORKER == true and ENVIRONMENT_IS_PTHREAD == true)

if (Module['ENVIRONMENT']) {
  if (Module['ENVIRONMENT'] === 'WEB') {
    ENVIRONMENT_IS_WEB = true;
  } else if (Module['ENVIRONMENT'] === 'WORKER') {
    ENVIRONMENT_IS_WORKER = true;
  } else if (Module['ENVIRONMENT'] === 'NODE') {
    ENVIRONMENT_IS_NODE = true;
  } else if (Module['ENVIRONMENT'] === 'SHELL') {
    ENVIRONMENT_IS_SHELL = true;
  } else {
    throw new Error('Module[\'ENVIRONMENT\'] value is not valid. must be one of: WEB|WORKER|NODE|SHELL.');
  }
} else {
  ENVIRONMENT_IS_WEB = typeof window === 'object';
  ENVIRONMENT_IS_WORKER = typeof importScripts === 'function';
  ENVIRONMENT_IS_NODE = typeof process === 'object' && typeof require === 'function' && !ENVIRONMENT_IS_WEB && !ENVIRONMENT_IS_WORKER;
  ENVIRONMENT_IS_SHELL = !ENVIRONMENT_IS_WEB && !ENVIRONMENT_IS_NODE && !ENVIRONMENT_IS_WORKER;
}


if (ENVIRONMENT_IS_NODE) {
  // Expose functionality in the same simple way that the shells work
  // Note that we pollute the global namespace here, otherwise we break in node
  var nodeFS;
  var nodePath;

  Module['read'] = function shell_read(filename, binary) {
    var ret;
    ret = tryParseAsDataURI(filename);
    if (!ret) {
      if (!nodeFS) nodeFS = require('fs');
      if (!nodePath) nodePath = require('path');
      filename = nodePath['normalize'](filename);
      ret = nodeFS['readFileSync'](filename);
    }
    return binary ? ret : ret.toString();
  };

  Module['readBinary'] = function readBinary(filename) {
    var ret = Module['read'](filename, true);
    if (!ret.buffer) {
      ret = new Uint8Array(ret);
    }
    assert(ret.buffer);
    return ret;
  };

  if (process['argv'].length > 1) {
    Module['thisProgram'] = process['argv'][1].replace(/\\/g, '/');
  }

  Module['arguments'] = process['argv'].slice(2);

  if (typeof module !== 'undefined') {
    module['exports'] = Module;
  }

  process['on']('uncaughtException', function(ex) {
    // suppress ExitStatus exceptions from showing an error
    if (!(ex instanceof ExitStatus)) {
      throw ex;
    }
  });
  // Currently node will swallow unhandled rejections, but this behavior is
  // deprecated, and in the future it will exit with error status.
  process['on']('unhandledRejection', function(reason, p) {
    process['exit'](1);
  });

  Module['inspect'] = function () { return '[Emscripten Module object]'; };
}
else if (ENVIRONMENT_IS_SHELL) {
  if (typeof read != 'undefined') {
    Module['read'] = function shell_read(f) {
      var data = tryParseAsDataURI(f);
      if (data) {
        return intArrayToString(data);
      }
      return read(f);
    };
  }

  Module['readBinary'] = function readBinary(f) {
    var data;
    data = tryParseAsDataURI(f);
    if (data) {
      return data;
    }
    if (typeof readbuffer === 'function') {
      return new Uint8Array(readbuffer(f));
    }
    data = read(f, 'binary');
    assert(typeof data === 'object');
    return data;
  };

  if (typeof scriptArgs != 'undefined') {
    Module['arguments'] = scriptArgs;
  } else if (typeof arguments != 'undefined') {
    Module['arguments'] = arguments;
  }

  if (typeof quit === 'function') {
    Module['quit'] = function(status, toThrow) {
      quit(status);
    }
  }
}
else if (ENVIRONMENT_IS_WEB || ENVIRONMENT_IS_WORKER) {
  Module['read'] = function shell_read(url) {
    try {
      var xhr = new XMLHttpRequest();
      xhr.open('GET', url, false);
      xhr.send(null);
      return xhr.responseText;
    } catch (err) {
      var data = tryParseAsDataURI(url);
      if (data) {
        return intArrayToString(data);
      }
      throw err;
    }
  };

  if (ENVIRONMENT_IS_WORKER) {
    Module['readBinary'] = function readBinary(url) {
      try {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', url, false);
        xhr.responseType = 'arraybuffer';
        xhr.send(null);
        return new Uint8Array(xhr.response);
      } catch (err) {
        var data = tryParseAsDataURI(url);
        if (data) {
          return data;
        }
        throw err;
      }
    };
  }

  Module['readAsync'] = function readAsync(url, onload, onerror) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.responseType = 'arraybuffer';
    xhr.onload = function xhr_onload() {
      if (xhr.status == 200 || (xhr.status == 0 && xhr.response)) { // file URLs can return 0
        onload(xhr.response);
        return;
      }
      var data = tryParseAsDataURI(url);
      if (data) {
        onload(data.buffer);
        return;
      }
      onerror();
    };
    xhr.onerror = onerror;
    xhr.send(null);
  };

  Module['setWindowTitle'] = function(title) { document.title = title };
}

// console.log is checked first, as 'print' on the web will open a print dialogue
// printErr is preferable to console.warn (works better in shells)
// bind(console) is necessary to fix IE/Edge closed dev tools panel behavior.
Module['print'] = typeof console !== 'undefined' ? console.log.bind(console) : (typeof print !== 'undefined' ? print : null);
Module['printErr'] = typeof printErr !== 'undefined' ? printErr : ((typeof console !== 'undefined' && console.warn.bind(console)) || Module['print']);

// *** Environment setup code ***

// Closure helpers
Module.print = Module['print'];
Module.printErr = Module['printErr'];

// Merge back in the overrides
for (key in moduleOverrides) {
  if (moduleOverrides.hasOwnProperty(key)) {
    Module[key] = moduleOverrides[key];
  }
}
// Free the object hierarchy contained in the overrides, this lets the GC
// reclaim data used e.g. in memoryInitializerRequest, which is a large typed array.
moduleOverrides = undefined;



// {{PREAMBLE_ADDITIONS}}

var STACK_ALIGN = 16;


function staticAlloc(size) {
  assert(!staticSealed);
  var ret = STATICTOP;
  STATICTOP = (STATICTOP + size + 15) & -16;
  return ret;
}

function dynamicAlloc(size) {
  assert(DYNAMICTOP_PTR);
  var ret = HEAP32[DYNAMICTOP_PTR>>2];
  var end = (ret + size + 15) & -16;
  HEAP32[DYNAMICTOP_PTR>>2] = end;
  if (end >= TOTAL_MEMORY) {
    var success = enlargeMemory();
    if (!success) {
      HEAP32[DYNAMICTOP_PTR>>2] = ret;
      return 0;
    }
  }
  return ret;
}

function alignMemory(size, factor) {
  if (!factor) factor = STACK_ALIGN; // stack alignment (16-byte) by default
  var ret = size = Math.ceil(size / factor) * factor;
  return ret;
}

function getNativeTypeSize(type) {
  switch (type) {
    case 'i1': case 'i8': return 1;
    case 'i16': return 2;
    case 'i32': return 4;
    case 'i64': return 8;
    case 'float': return 4;
    case 'double': return 8;
    default: {
      if (type[type.length-1] === '*') {
        return 4; // A pointer
      } else if (type[0] === 'i') {
        var bits = parseInt(type.substr(1));
        assert(bits % 8 === 0);
        return bits / 8;
      } else {
        return 0;
      }
    }
  }
}

function warnOnce(text) {
  if (!warnOnce.shown) warnOnce.shown = {};
  if (!warnOnce.shown[text]) {
    warnOnce.shown[text] = 1;
    Module.printErr(text);
  }
}



var jsCallStartIndex = 1;
var functionPointers = new Array(0);

// 'sig' parameter is only used on LLVM wasm backend
function addFunction(func, sig) {
  var base = 0;
  for (var i = base; i < base + 0; i++) {
    if (!functionPointers[i]) {
      functionPointers[i] = func;
      return jsCallStartIndex + i;
    }
  }
  throw 'Finished up all reserved function pointers. Use a higher value for RESERVED_FUNCTION_POINTERS.';
}

function removeFunction(index) {
  functionPointers[index-jsCallStartIndex] = null;
}

var funcWrappers = {};

function getFuncWrapper(func, sig) {
  if (!func) return; // on null pointer, return undefined
  assert(sig);
  if (!funcWrappers[sig]) {
    funcWrappers[sig] = {};
  }
  var sigCache = funcWrappers[sig];
  if (!sigCache[func]) {
    // optimize away arguments usage in common cases
    if (sig.length === 1) {
      sigCache[func] = function dynCall_wrapper() {
        return dynCall(sig, func);
      };
    } else if (sig.length === 2) {
      sigCache[func] = function dynCall_wrapper(arg) {
        return dynCall(sig, func, [arg]);
      };
    } else {
      // general case
      sigCache[func] = function dynCall_wrapper() {
        return dynCall(sig, func, Array.prototype.slice.call(arguments));
      };
    }
  }
  return sigCache[func];
}


function makeBigInt(low, high, unsigned) {
  return unsigned ? ((+((low>>>0)))+((+((high>>>0)))*4294967296.0)) : ((+((low>>>0)))+((+((high|0)))*4294967296.0));
}

function dynCall(sig, ptr, args) {
  if (args && args.length) {
    return Module['dynCall_' + sig].apply(null, [ptr].concat(args));
  } else {
    return Module['dynCall_' + sig].call(null, ptr);
  }
}



var Runtime = {
  // FIXME backwards compatibility layer for ports. Support some Runtime.*
  //       for now, fix it there, then remove it from here. That way we
  //       can minimize any period of breakage.
  dynCall: dynCall, // for SDL2 port
};

// The address globals begin at. Very low in memory, for code size and optimization opportunities.
// Above 0 is static memory, starting with globals.
// Then the stack.
// Then 'dynamic' memory for sbrk.
var GLOBAL_BASE = 8;



// === Preamble library stuff ===

// Documentation for the public APIs defined in this file must be updated in:
//    site/source/docs/api_reference/preamble.js.rst
// A prebuilt local version of the documentation is available at:
//    site/build/text/docs/api_reference/preamble.js.txt
// You can also build docs locally as HTML or other formats in site/
// An online HTML version (which may be of a different version of Emscripten)
//    is up at http://kripken.github.io/emscripten-site/docs/api_reference/preamble.js.html



//========================================
// Runtime essentials
//========================================

var ABORT = 0; // whether we are quitting the application. no code should run after this. set in exit() and abort()
var EXITSTATUS = 0;

/** @type {function(*, string=)} */
function assert(condition, text) {
  if (!condition) {
    abort('Assertion failed: ' + text);
  }
}

var globalScope = this;

// Returns the C function with a specified identifier (for C++, you need to do manual name mangling)
function getCFunc(ident) {
  var func = Module['_' + ident]; // closure exported function
  assert(func, 'Cannot call unknown function ' + ident + ', make sure it is exported');
  return func;
}

var JSfuncs = {
  // Helpers for cwrap -- it can't refer to Runtime directly because it might
  // be renamed by closure, instead it calls JSfuncs['stackSave'].body to find
  // out what the minified function name is.
  'stackSave': function() {
    stackSave()
  },
  'stackRestore': function() {
    stackRestore()
  },
  // type conversion from js to c
  'arrayToC' : function(arr) {
    var ret = stackAlloc(arr.length);
    writeArrayToMemory(arr, ret);
    return ret;
  },
  'stringToC' : function(str) {
    var ret = 0;
    if (str !== null && str !== undefined && str !== 0) { // null string
      // at most 4 bytes per UTF-8 code point, +1 for the trailing '\0'
      var len = (str.length << 2) + 1;
      ret = stackAlloc(len);
      stringToUTF8(str, ret, len);
    }
    return ret;
  }
};

// For fast lookup of conversion functions
var toC = {
  'string': JSfuncs['stringToC'], 'array': JSfuncs['arrayToC']
};

// C calling interface.
function ccall (ident, returnType, argTypes, args, opts) {
  var func = getCFunc(ident);
  var cArgs = [];
  var stack = 0;
  if (args) {
    for (var i = 0; i < args.length; i++) {
      var converter = toC[argTypes[i]];
      if (converter) {
        if (stack === 0) stack = stackSave();
        cArgs[i] = converter(args[i]);
      } else {
        cArgs[i] = args[i];
      }
    }
  }
  var ret = func.apply(null, cArgs);
  if (returnType === 'string') ret = Pointer_stringify(ret);
  else if (returnType === 'boolean') ret = Boolean(ret);
  if (stack !== 0) {
    stackRestore(stack);
  }
  return ret;
}

function cwrap (ident, returnType, argTypes) {
  argTypes = argTypes || [];
  var cfunc = getCFunc(ident);
  // When the function takes numbers and returns a number, we can just return
  // the original function
  var numericArgs = argTypes.every(function(type){ return type === 'number'});
  var numericRet = returnType !== 'string';
  if (numericRet && numericArgs) {
    return cfunc;
  }
  return function() {
    return ccall(ident, returnType, argTypes, arguments);
  }
}

/** @type {function(number, number, string, boolean=)} */
function setValue(ptr, value, type, noSafe) {
  type = type || 'i8';
  if (type.charAt(type.length-1) === '*') type = 'i32'; // pointers are 32-bit
    switch(type) {
      case 'i1': HEAP8[((ptr)>>0)]=value; break;
      case 'i8': HEAP8[((ptr)>>0)]=value; break;
      case 'i16': HEAP16[((ptr)>>1)]=value; break;
      case 'i32': HEAP32[((ptr)>>2)]=value; break;
      case 'i64': (tempI64 = [value>>>0,(tempDouble=value,(+(Math_abs(tempDouble))) >= (+1) ? (tempDouble > (+0) ? ((Math_min((+(Math_floor((tempDouble)/(+4294967296)))), (+4294967295)))|0)>>>0 : (~~((+(Math_ceil((tempDouble - +(((~~(tempDouble)))>>>0))/(+4294967296))))))>>>0) : 0)],HEAP32[((ptr)>>2)]=tempI64[0],HEAP32[(((ptr)+(4))>>2)]=tempI64[1]); break;
      case 'float': HEAPF32[((ptr)>>2)]=value; break;
      case 'double': HEAPF64[((ptr)>>3)]=value; break;
      default: abort('invalid type for setValue: ' + type);
    }
}

/** @type {function(number, string, boolean=)} */
function getValue(ptr, type, noSafe) {
  type = type || 'i8';
  if (type.charAt(type.length-1) === '*') type = 'i32'; // pointers are 32-bit
    switch(type) {
      case 'i1': return HEAP8[((ptr)>>0)];
      case 'i8': return HEAP8[((ptr)>>0)];
      case 'i16': return HEAP16[((ptr)>>1)];
      case 'i32': return HEAP32[((ptr)>>2)];
      case 'i64': return HEAP32[((ptr)>>2)];
      case 'float': return HEAPF32[((ptr)>>2)];
      case 'double': return HEAPF64[((ptr)>>3)];
      default: abort('invalid type for getValue: ' + type);
    }
  return null;
}

var ALLOC_NORMAL = 0; // Tries to use _malloc()
var ALLOC_STACK = 1; // Lives for the duration of the current function call
var ALLOC_STATIC = 2; // Cannot be freed
var ALLOC_DYNAMIC = 3; // Cannot be freed except through sbrk
var ALLOC_NONE = 4; // Do not allocate

// allocate(): This is for internal use. You can use it yourself as well, but the interface
//             is a little tricky (see docs right below). The reason is that it is optimized
//             for multiple syntaxes to save space in generated code. So you should
//             normally not use allocate(), and instead allocate memory using _malloc(),
//             initialize it with setValue(), and so forth.
// @slab: An array of data, or a number. If a number, then the size of the block to allocate,
//        in *bytes* (note that this is sometimes confusing: the next parameter does not
//        affect this!)
// @types: Either an array of types, one for each byte (or 0 if no type at that position),
//         or a single type which is used for the entire block. This only matters if there
//         is initial data - if @slab is a number, then this does not matter at all and is
//         ignored.
// @allocator: How to allocate memory, see ALLOC_*
/** @type {function((TypedArray|Array<number>|number), string, number, number=)} */
function allocate(slab, types, allocator, ptr) {
  var zeroinit, size;
  if (typeof slab === 'number') {
    zeroinit = true;
    size = slab;
  } else {
    zeroinit = false;
    size = slab.length;
  }

  var singleType = typeof types === 'string' ? types : null;

  var ret;
  if (allocator == ALLOC_NONE) {
    ret = ptr;
  } else {
    ret = [typeof _malloc === 'function' ? _malloc : staticAlloc, stackAlloc, staticAlloc, dynamicAlloc][allocator === undefined ? ALLOC_STATIC : allocator](Math.max(size, singleType ? 1 : types.length));
  }

  if (zeroinit) {
    var stop;
    ptr = ret;
    assert((ret & 3) == 0);
    stop = ret + (size & ~3);
    for (; ptr < stop; ptr += 4) {
      HEAP32[((ptr)>>2)]=0;
    }
    stop = ret + size;
    while (ptr < stop) {
      HEAP8[((ptr++)>>0)]=0;
    }
    return ret;
  }

  if (singleType === 'i8') {
    if (slab.subarray || slab.slice) {
      HEAPU8.set(/** @type {!Uint8Array} */ (slab), ret);
    } else {
      HEAPU8.set(new Uint8Array(slab), ret);
    }
    return ret;
  }

  var i = 0, type, typeSize, previousType;
  while (i < size) {
    var curr = slab[i];

    type = singleType || types[i];
    if (type === 0) {
      i++;
      continue;
    }

    if (type == 'i64') type = 'i32'; // special case: we have one i32 here, and one i32 later

    setValue(ret+i, curr, type);

    // no need to look up size unless type changes, so cache it
    if (previousType !== type) {
      typeSize = getNativeTypeSize(type);
      previousType = type;
    }
    i += typeSize;
  }

  return ret;
}

// Allocate memory during any stage of startup - static memory early on, dynamic memory later, malloc when ready
function getMemory(size) {
  if (!staticSealed) return staticAlloc(size);
  if (!runtimeInitialized) return dynamicAlloc(size);
  return _malloc(size);
}

/** @type {function(number, number=)} */
function Pointer_stringify(ptr, length) {
  if (length === 0 || !ptr) return '';
  // TODO: use TextDecoder
  // Find the length, and check for UTF while doing so
  var hasUtf = 0;
  var t;
  var i = 0;
  while (1) {
    t = HEAPU8[(((ptr)+(i))>>0)];
    hasUtf |= t;
    if (t == 0 && !length) break;
    i++;
    if (length && i == length) break;
  }
  if (!length) length = i;

  var ret = '';

  if (hasUtf < 128) {
    var MAX_CHUNK = 1024; // split up into chunks, because .apply on a huge string can overflow the stack
    var curr;
    while (length > 0) {
      curr = String.fromCharCode.apply(String, HEAPU8.subarray(ptr, ptr + Math.min(length, MAX_CHUNK)));
      ret = ret ? ret + curr : curr;
      ptr += MAX_CHUNK;
      length -= MAX_CHUNK;
    }
    return ret;
  }
  return UTF8ToString(ptr);
}

// Given a pointer 'ptr' to a null-terminated ASCII-encoded string in the emscripten HEAP, returns
// a copy of that string as a Javascript String object.

function AsciiToString(ptr) {
  var str = '';
  while (1) {
    var ch = HEAP8[((ptr++)>>0)];
    if (!ch) return str;
    str += String.fromCharCode(ch);
  }
}

// Copies the given Javascript String object 'str' to the emscripten HEAP at address 'outPtr',
// null-terminated and encoded in ASCII form. The copy will require at most str.length+1 bytes of space in the HEAP.

function stringToAscii(str, outPtr) {
  return writeAsciiToMemory(str, outPtr, false);
}

// Given a pointer 'ptr' to a null-terminated UTF8-encoded string in the given array that contains uint8 values, returns
// a copy of that string as a Javascript String object.

var UTF8Decoder = typeof TextDecoder !== 'undefined' ? new TextDecoder('utf8') : undefined;
function UTF8ArrayToString(u8Array, idx) {
  var endPtr = idx;
  // TextDecoder needs to know the byte length in advance, it doesn't stop on null terminator by itself.
  // Also, use the length info to avoid running tiny strings through TextDecoder, since .subarray() allocates garbage.
  while (u8Array[endPtr]) ++endPtr;

  if (endPtr - idx > 16 && u8Array.subarray && UTF8Decoder) {
    return UTF8Decoder.decode(u8Array.subarray(idx, endPtr));
  } else {
    var u0, u1, u2, u3, u4, u5;

    var str = '';
    while (1) {
      // For UTF8 byte structure, see http://en.wikipedia.org/wiki/UTF-8#Description and https://www.ietf.org/rfc/rfc2279.txt and https://tools.ietf.org/html/rfc3629
      u0 = u8Array[idx++];
      if (!u0) return str;
      if (!(u0 & 0x80)) { str += String.fromCharCode(u0); continue; }
      u1 = u8Array[idx++] & 63;
      if ((u0 & 0xE0) == 0xC0) { str += String.fromCharCode(((u0 & 31) << 6) | u1); continue; }
      u2 = u8Array[idx++] & 63;
      if ((u0 & 0xF0) == 0xE0) {
        u0 = ((u0 & 15) << 12) | (u1 << 6) | u2;
      } else {
        u3 = u8Array[idx++] & 63;
        if ((u0 & 0xF8) == 0xF0) {
          u0 = ((u0 & 7) << 18) | (u1 << 12) | (u2 << 6) | u3;
        } else {
          u4 = u8Array[idx++] & 63;
          if ((u0 & 0xFC) == 0xF8) {
            u0 = ((u0 & 3) << 24) | (u1 << 18) | (u2 << 12) | (u3 << 6) | u4;
          } else {
            u5 = u8Array[idx++] & 63;
            u0 = ((u0 & 1) << 30) | (u1 << 24) | (u2 << 18) | (u3 << 12) | (u4 << 6) | u5;
          }
        }
      }
      if (u0 < 0x10000) {
        str += String.fromCharCode(u0);
      } else {
        var ch = u0 - 0x10000;
        str += String.fromCharCode(0xD800 | (ch >> 10), 0xDC00 | (ch & 0x3FF));
      }
    }
  }
}

// Given a pointer 'ptr' to a null-terminated UTF8-encoded string in the emscripten HEAP, returns
// a copy of that string as a Javascript String object.

function UTF8ToString(ptr) {
  return UTF8ArrayToString(HEAPU8,ptr);
}

// Copies the given Javascript String object 'str' to the given byte array at address 'outIdx',
// encoded in UTF8 form and null-terminated. The copy will require at most str.length*4+1 bytes of space in the HEAP.
// Use the function lengthBytesUTF8 to compute the exact number of bytes (excluding null terminator) that this function will write.
// Parameters:
//   str: the Javascript string to copy.
//   outU8Array: the array to copy to. Each index in this array is assumed to be one 8-byte element.
//   outIdx: The starting offset in the array to begin the copying.
//   maxBytesToWrite: The maximum number of bytes this function can write to the array. This count should include the null
//                    terminator, i.e. if maxBytesToWrite=1, only the null terminator will be written and nothing else.
//                    maxBytesToWrite=0 does not write any bytes to the output, not even the null terminator.
// Returns the number of bytes written, EXCLUDING the null terminator.

function stringToUTF8Array(str, outU8Array, outIdx, maxBytesToWrite) {
  if (!(maxBytesToWrite > 0)) // Parameter maxBytesToWrite is not optional. Negative values, 0, null, undefined and false each don't write out any bytes.
    return 0;

  var startIdx = outIdx;
  var endIdx = outIdx + maxBytesToWrite - 1; // -1 for string null terminator.
  for (var i = 0; i < str.length; ++i) {
    // Gotcha: charCodeAt returns a 16-bit word that is a UTF-16 encoded code unit, not a Unicode code point of the character! So decode UTF16->UTF32->UTF8.
    // See http://unicode.org/faq/utf_bom.html#utf16-3
    // For UTF8 byte structure, see http://en.wikipedia.org/wiki/UTF-8#Description and https://www.ietf.org/rfc/rfc2279.txt and https://tools.ietf.org/html/rfc3629
    var u = str.charCodeAt(i); // possibly a lead surrogate
    if (u >= 0xD800 && u <= 0xDFFF) u = 0x10000 + ((u & 0x3FF) << 10) | (str.charCodeAt(++i) & 0x3FF);
    if (u <= 0x7F) {
      if (outIdx >= endIdx) break;
      outU8Array[outIdx++] = u;
    } else if (u <= 0x7FF) {
      if (outIdx + 1 >= endIdx) break;
      outU8Array[outIdx++] = 0xC0 | (u >> 6);
      outU8Array[outIdx++] = 0x80 | (u & 63);
    } else if (u <= 0xFFFF) {
      if (outIdx + 2 >= endIdx) break;
      outU8Array[outIdx++] = 0xE0 | (u >> 12);
      outU8Array[outIdx++] = 0x80 | ((u >> 6) & 63);
      outU8Array[outIdx++] = 0x80 | (u & 63);
    } else if (u <= 0x1FFFFF) {
      if (outIdx + 3 >= endIdx) break;
      outU8Array[outIdx++] = 0xF0 | (u >> 18);
      outU8Array[outIdx++] = 0x80 | ((u >> 12) & 63);
      outU8Array[outIdx++] = 0x80 | ((u >> 6) & 63);
      outU8Array[outIdx++] = 0x80 | (u & 63);
    } else if (u <= 0x3FFFFFF) {
      if (outIdx + 4 >= endIdx) break;
      outU8Array[outIdx++] = 0xF8 | (u >> 24);
      outU8Array[outIdx++] = 0x80 | ((u >> 18) & 63);
      outU8Array[outIdx++] = 0x80 | ((u >> 12) & 63);
      outU8Array[outIdx++] = 0x80 | ((u >> 6) & 63);
      outU8Array[outIdx++] = 0x80 | (u & 63);
    } else {
      if (outIdx + 5 >= endIdx) break;
      outU8Array[outIdx++] = 0xFC | (u >> 30);
      outU8Array[outIdx++] = 0x80 | ((u >> 24) & 63);
      outU8Array[outIdx++] = 0x80 | ((u >> 18) & 63);
      outU8Array[outIdx++] = 0x80 | ((u >> 12) & 63);
      outU8Array[outIdx++] = 0x80 | ((u >> 6) & 63);
      outU8Array[outIdx++] = 0x80 | (u & 63);
    }
  }
  // Null-terminate the pointer to the buffer.
  outU8Array[outIdx] = 0;
  return outIdx - startIdx;
}

// Copies the given Javascript String object 'str' to the emscripten HEAP at address 'outPtr',
// null-terminated and encoded in UTF8 form. The copy will require at most str.length*4+1 bytes of space in the HEAP.
// Use the function lengthBytesUTF8 to compute the exact number of bytes (excluding null terminator) that this function will write.
// Returns the number of bytes written, EXCLUDING the null terminator.

function stringToUTF8(str, outPtr, maxBytesToWrite) {
  return stringToUTF8Array(str, HEAPU8,outPtr, maxBytesToWrite);
}

// Returns the number of bytes the given Javascript string takes if encoded as a UTF8 byte array, EXCLUDING the null terminator byte.

function lengthBytesUTF8(str) {
  var len = 0;
  for (var i = 0; i < str.length; ++i) {
    // Gotcha: charCodeAt returns a 16-bit word that is a UTF-16 encoded code unit, not a Unicode code point of the character! So decode UTF16->UTF32->UTF8.
    // See http://unicode.org/faq/utf_bom.html#utf16-3
    var u = str.charCodeAt(i); // possibly a lead surrogate
    if (u >= 0xD800 && u <= 0xDFFF) u = 0x10000 + ((u & 0x3FF) << 10) | (str.charCodeAt(++i) & 0x3FF);
    if (u <= 0x7F) {
      ++len;
    } else if (u <= 0x7FF) {
      len += 2;
    } else if (u <= 0xFFFF) {
      len += 3;
    } else if (u <= 0x1FFFFF) {
      len += 4;
    } else if (u <= 0x3FFFFFF) {
      len += 5;
    } else {
      len += 6;
    }
  }
  return len;
}

// Given a pointer 'ptr' to a null-terminated UTF16LE-encoded string in the emscripten HEAP, returns
// a copy of that string as a Javascript String object.

var UTF16Decoder = typeof TextDecoder !== 'undefined' ? new TextDecoder('utf-16le') : undefined;
function UTF16ToString(ptr) {
  var endPtr = ptr;
  // TextDecoder needs to know the byte length in advance, it doesn't stop on null terminator by itself.
  // Also, use the length info to avoid running tiny strings through TextDecoder, since .subarray() allocates garbage.
  var idx = endPtr >> 1;
  while (HEAP16[idx]) ++idx;
  endPtr = idx << 1;

  if (endPtr - ptr > 32 && UTF16Decoder) {
    return UTF16Decoder.decode(HEAPU8.subarray(ptr, endPtr));
  } else {
    var i = 0;

    var str = '';
    while (1) {
      var codeUnit = HEAP16[(((ptr)+(i*2))>>1)];
      if (codeUnit == 0) return str;
      ++i;
      // fromCharCode constructs a character from a UTF-16 code unit, so we can pass the UTF16 string right through.
      str += String.fromCharCode(codeUnit);
    }
  }
}

// Copies the given Javascript String object 'str' to the emscripten HEAP at address 'outPtr',
// null-terminated and encoded in UTF16 form. The copy will require at most str.length*4+2 bytes of space in the HEAP.
// Use the function lengthBytesUTF16() to compute the exact number of bytes (excluding null terminator) that this function will write.
// Parameters:
//   str: the Javascript string to copy.
//   outPtr: Byte address in Emscripten HEAP where to write the string to.
//   maxBytesToWrite: The maximum number of bytes this function can write to the array. This count should include the null
//                    terminator, i.e. if maxBytesToWrite=2, only the null terminator will be written and nothing else.
//                    maxBytesToWrite<2 does not write any bytes to the output, not even the null terminator.
// Returns the number of bytes written, EXCLUDING the null terminator.

function stringToUTF16(str, outPtr, maxBytesToWrite) {
  // Backwards compatibility: if max bytes is not specified, assume unsafe unbounded write is allowed.
  if (maxBytesToWrite === undefined) {
    maxBytesToWrite = 0x7FFFFFFF;
  }
  if (maxBytesToWrite < 2) return 0;
  maxBytesToWrite -= 2; // Null terminator.
  var startPtr = outPtr;
  var numCharsToWrite = (maxBytesToWrite < str.length*2) ? (maxBytesToWrite / 2) : str.length;
  for (var i = 0; i < numCharsToWrite; ++i) {
    // charCodeAt returns a UTF-16 encoded code unit, so it can be directly written to the HEAP.
    var codeUnit = str.charCodeAt(i); // possibly a lead surrogate
    HEAP16[((outPtr)>>1)]=codeUnit;
    outPtr += 2;
  }
  // Null-terminate the pointer to the HEAP.
  HEAP16[((outPtr)>>1)]=0;
  return outPtr - startPtr;
}

// Returns the number of bytes the given Javascript string takes if encoded as a UTF16 byte array, EXCLUDING the null terminator byte.

function lengthBytesUTF16(str) {
  return str.length*2;
}

function UTF32ToString(ptr) {
  var i = 0;

  var str = '';
  while (1) {
    var utf32 = HEAP32[(((ptr)+(i*4))>>2)];
    if (utf32 == 0)
      return str;
    ++i;
    // Gotcha: fromCharCode constructs a character from a UTF-16 encoded code (pair), not from a Unicode code point! So encode the code point to UTF-16 for constructing.
    // See http://unicode.org/faq/utf_bom.html#utf16-3
    if (utf32 >= 0x10000) {
      var ch = utf32 - 0x10000;
      str += String.fromCharCode(0xD800 | (ch >> 10), 0xDC00 | (ch & 0x3FF));
    } else {
      str += String.fromCharCode(utf32);
    }
  }
}

// Copies the given Javascript String object 'str' to the emscripten HEAP at address 'outPtr',
// null-terminated and encoded in UTF32 form. The copy will require at most str.length*4+4 bytes of space in the HEAP.
// Use the function lengthBytesUTF32() to compute the exact number of bytes (excluding null terminator) that this function will write.
// Parameters:
//   str: the Javascript string to copy.
//   outPtr: Byte address in Emscripten HEAP where to write the string to.
//   maxBytesToWrite: The maximum number of bytes this function can write to the array. This count should include the null
//                    terminator, i.e. if maxBytesToWrite=4, only the null terminator will be written and nothing else.
//                    maxBytesToWrite<4 does not write any bytes to the output, not even the null terminator.
// Returns the number of bytes written, EXCLUDING the null terminator.

function stringToUTF32(str, outPtr, maxBytesToWrite) {
  // Backwards compatibility: if max bytes is not specified, assume unsafe unbounded write is allowed.
  if (maxBytesToWrite === undefined) {
    maxBytesToWrite = 0x7FFFFFFF;
  }
  if (maxBytesToWrite < 4) return 0;
  var startPtr = outPtr;
  var endPtr = startPtr + maxBytesToWrite - 4;
  for (var i = 0; i < str.length; ++i) {
    // Gotcha: charCodeAt returns a 16-bit word that is a UTF-16 encoded code unit, not a Unicode code point of the character! We must decode the string to UTF-32 to the heap.
    // See http://unicode.org/faq/utf_bom.html#utf16-3
    var codeUnit = str.charCodeAt(i); // possibly a lead surrogate
    if (codeUnit >= 0xD800 && codeUnit <= 0xDFFF) {
      var trailSurrogate = str.charCodeAt(++i);
      codeUnit = 0x10000 + ((codeUnit & 0x3FF) << 10) | (trailSurrogate & 0x3FF);
    }
    HEAP32[((outPtr)>>2)]=codeUnit;
    outPtr += 4;
    if (outPtr + 4 > endPtr) break;
  }
  // Null-terminate the pointer to the HEAP.
  HEAP32[((outPtr)>>2)]=0;
  return outPtr - startPtr;
}

// Returns the number of bytes the given Javascript string takes if encoded as a UTF16 byte array, EXCLUDING the null terminator byte.

function lengthBytesUTF32(str) {
  var len = 0;
  for (var i = 0; i < str.length; ++i) {
    // Gotcha: charCodeAt returns a 16-bit word that is a UTF-16 encoded code unit, not a Unicode code point of the character! We must decode the string to UTF-32 to the heap.
    // See http://unicode.org/faq/utf_bom.html#utf16-3
    var codeUnit = str.charCodeAt(i);
    if (codeUnit >= 0xD800 && codeUnit <= 0xDFFF) ++i; // possibly a lead surrogate, so skip over the tail surrogate.
    len += 4;
  }

  return len;
}

// Allocate heap space for a JS string, and write it there.
// It is the responsibility of the caller to free() that memory.
function allocateUTF8(str) {
  var size = lengthBytesUTF8(str) + 1;
  var ret = _malloc(size);
  if (ret) stringToUTF8Array(str, HEAP8, ret, size);
  return ret;
}

// Allocate stack space for a JS string, and write it there.
function allocateUTF8OnStack(str) {
  var size = lengthBytesUTF8(str) + 1;
  var ret = stackAlloc(size);
  stringToUTF8Array(str, HEAP8, ret, size);
  return ret;
}

function demangle(func) {
  return func;
}

function demangleAll(text) {
  var regex =
    /__Z[\w\d_]+/g;
  return text.replace(regex,
    function(x) {
      var y = demangle(x);
      return x === y ? x : (x + ' [' + y + ']');
    });
}

function jsStackTrace() {
  var err = new Error();
  if (!err.stack) {
    // IE10+ special cases: It does have callstack info, but it is only populated if an Error object is thrown,
    // so try that as a special-case.
    try {
      throw new Error(0);
    } catch(e) {
      err = e;
    }
    if (!err.stack) {
      return '(no stack trace available)';
    }
  }
  return err.stack.toString();
}

function stackTrace() {
  var js = jsStackTrace();
  if (Module['extraStackTrace']) js += '\n' + Module['extraStackTrace']();
  return demangleAll(js);
}

// Memory management

var PAGE_SIZE = 16384;
var WASM_PAGE_SIZE = 65536;
var ASMJS_PAGE_SIZE = 16777216;
var MIN_TOTAL_MEMORY = 16777216;

function alignUp(x, multiple) {
  if (x % multiple > 0) {
    x += multiple - (x % multiple);
  }
  return x;
}

var HEAP,
/** @type {ArrayBuffer} */
  buffer,
/** @type {Int8Array} */
  HEAP8,
/** @type {Uint8Array} */
  HEAPU8,
/** @type {Int16Array} */
  HEAP16,
/** @type {Uint16Array} */
  HEAPU16,
/** @type {Int32Array} */
  HEAP32,
/** @type {Uint32Array} */
  HEAPU32,
/** @type {Float32Array} */
  HEAPF32,
/** @type {Float64Array} */
  HEAPF64;

function updateGlobalBuffer(buf) {
  Module['buffer'] = buffer = buf;
}

function updateGlobalBufferViews() {
  Module['HEAP8'] = HEAP8 = new Int8Array(buffer);
  Module['HEAP16'] = HEAP16 = new Int16Array(buffer);
  Module['HEAP32'] = HEAP32 = new Int32Array(buffer);
  Module['HEAPU8'] = HEAPU8 = new Uint8Array(buffer);
  Module['HEAPU16'] = HEAPU16 = new Uint16Array(buffer);
  Module['HEAPU32'] = HEAPU32 = new Uint32Array(buffer);
  Module['HEAPF32'] = HEAPF32 = new Float32Array(buffer);
  Module['HEAPF64'] = HEAPF64 = new Float64Array(buffer);
}

var STATIC_BASE, STATICTOP, staticSealed; // static area
var STACK_BASE, STACKTOP, STACK_MAX; // stack area
var DYNAMIC_BASE, DYNAMICTOP_PTR; // dynamic area handled by sbrk

  STATIC_BASE = STATICTOP = STACK_BASE = STACKTOP = STACK_MAX = DYNAMIC_BASE = DYNAMICTOP_PTR = 0;
  staticSealed = false;



function abortOnCannotGrowMemory() {
  abort('Cannot enlarge memory arrays. Either (1) compile with  -s TOTAL_MEMORY=X  with X higher than the current value ' + TOTAL_MEMORY + ', (2) compile with  -s ALLOW_MEMORY_GROWTH=1  which allows increasing the size at runtime but prevents some optimizations, (3) set Module.TOTAL_MEMORY to a higher value before the program runs, or (4) if you want malloc to return NULL (0) instead of this abort, compile with  -s ABORTING_MALLOC=0 ');
}


function enlargeMemory() {
  abortOnCannotGrowMemory();
}


var TOTAL_STACK = Module['TOTAL_STACK'] || 5242880;
var TOTAL_MEMORY = Module['TOTAL_MEMORY'] || 16777216;
if (TOTAL_MEMORY < TOTAL_STACK) Module.printErr('TOTAL_MEMORY should be larger than TOTAL_STACK, was ' + TOTAL_MEMORY + '! (TOTAL_STACK=' + TOTAL_STACK + ')');

// Initialize the runtime's memory



// Use a provided buffer, if there is one, or else allocate a new one
if (Module['buffer']) {
  buffer = Module['buffer'];
} else {
  // Use a WebAssembly memory where available
  {
    buffer = new ArrayBuffer(TOTAL_MEMORY);
  }
  Module['buffer'] = buffer;
}
updateGlobalBufferViews();


function getTotalMemory() {
  return TOTAL_MEMORY;
}

// Endianness check (note: assumes compiler arch was little-endian)
  HEAP32[0] = 0x63736d65; /* 'emsc' */
HEAP16[1] = 0x6373;
if (HEAPU8[2] !== 0x73 || HEAPU8[3] !== 0x63) throw 'Runtime error: expected the system to be little-endian!';

function callRuntimeCallbacks(callbacks) {
  while(callbacks.length > 0) {
    var callback = callbacks.shift();
    if (typeof callback == 'function') {
      callback();
      continue;
    }
    var func = callback.func;
    if (typeof func === 'number') {
      if (callback.arg === undefined) {
        Module['dynCall_v'](func);
      } else {
        Module['dynCall_vi'](func, callback.arg);
      }
    } else {
      func(callback.arg === undefined ? null : callback.arg);
    }
  }
}

var __ATPRERUN__  = []; // functions called before the runtime is initialized
var __ATINIT__    = []; // functions called during startup
var __ATMAIN__    = []; // functions called when main() is to be run
var __ATEXIT__    = []; // functions called during shutdown
var __ATPOSTRUN__ = []; // functions called after the runtime has exited

var runtimeInitialized = false;
var runtimeExited = false;


function preRun() {
  // compatibility - merge in anything from Module['preRun'] at this time
  if (Module['preRun']) {
    if (typeof Module['preRun'] == 'function') Module['preRun'] = [Module['preRun']];
    while (Module['preRun'].length) {
      addOnPreRun(Module['preRun'].shift());
    }
  }
  callRuntimeCallbacks(__ATPRERUN__);
}

function ensureInitRuntime() {
  if (runtimeInitialized) return;
  runtimeInitialized = true;
  callRuntimeCallbacks(__ATINIT__);
}

function preMain() {
  callRuntimeCallbacks(__ATMAIN__);
}

function exitRuntime() {
  callRuntimeCallbacks(__ATEXIT__);
  runtimeExited = true;
}

function postRun() {
  // compatibility - merge in anything from Module['postRun'] at this time
  if (Module['postRun']) {
    if (typeof Module['postRun'] == 'function') Module['postRun'] = [Module['postRun']];
    while (Module['postRun'].length) {
      addOnPostRun(Module['postRun'].shift());
    }
  }
  callRuntimeCallbacks(__ATPOSTRUN__);
}

function addOnPreRun(cb) {
  __ATPRERUN__.unshift(cb);
}

function addOnInit(cb) {
  __ATINIT__.unshift(cb);
}

function addOnPreMain(cb) {
  __ATMAIN__.unshift(cb);
}

function addOnExit(cb) {
  __ATEXIT__.unshift(cb);
}

function addOnPostRun(cb) {
  __ATPOSTRUN__.unshift(cb);
}

// Deprecated: This function should not be called because it is unsafe and does not provide
// a maximum length limit of how many bytes it is allowed to write. Prefer calling the
// function stringToUTF8Array() instead, which takes in a maximum length that can be used
// to be secure from out of bounds writes.
/** @deprecated */
function writeStringToMemory(string, buffer, dontAddNull) {
  warnOnce('writeStringToMemory is deprecated and should not be called! Use stringToUTF8() instead!');

  var /** @type {number} */ lastChar, /** @type {number} */ end;
  if (dontAddNull) {
    // stringToUTF8Array always appends null. If we don't want to do that, remember the
    // character that existed at the location where the null will be placed, and restore
    // that after the write (below).
    end = buffer + lengthBytesUTF8(string);
    lastChar = HEAP8[end];
  }
  stringToUTF8(string, buffer, Infinity);
  if (dontAddNull) HEAP8[end] = lastChar; // Restore the value under the null character.
}

function writeArrayToMemory(array, buffer) {
  HEAP8.set(array, buffer);
}

function writeAsciiToMemory(str, buffer, dontAddNull) {
  for (var i = 0; i < str.length; ++i) {
    HEAP8[((buffer++)>>0)]=str.charCodeAt(i);
  }
  // Null-terminate the pointer to the HEAP.
  if (!dontAddNull) HEAP8[((buffer)>>0)]=0;
}

function unSign(value, bits, ignore) {
  if (value >= 0) {
    return value;
  }
  return bits <= 32 ? 2*Math.abs(1 << (bits-1)) + value // Need some trickery, since if bits == 32, we are right at the limit of the bits JS uses in bitshifts
                    : Math.pow(2, bits)         + value;
}
function reSign(value, bits, ignore) {
  if (value <= 0) {
    return value;
  }
  var half = bits <= 32 ? Math.abs(1 << (bits-1)) // abs is needed if bits == 32
                        : Math.pow(2, bits-1);
  if (value >= half && (bits <= 32 || value > half)) { // for huge values, we can hit the precision limit and always get true here. so don't do that
                                                       // but, in general there is no perfect solution here. With 64-bit ints, we get rounding and errors
                                                       // TODO: In i64 mode 1, resign the two parts separately and safely
    value = -2*half + value; // Cannot bitshift half, as it may be at the limit of the bits JS uses in bitshifts
  }
  return value;
}


var Math_abs = Math.abs;
var Math_cos = Math.cos;
var Math_sin = Math.sin;
var Math_tan = Math.tan;
var Math_acos = Math.acos;
var Math_asin = Math.asin;
var Math_atan = Math.atan;
var Math_atan2 = Math.atan2;
var Math_exp = Math.exp;
var Math_log = Math.log;
var Math_sqrt = Math.sqrt;
var Math_ceil = Math.ceil;
var Math_floor = Math.floor;
var Math_pow = Math.pow;
var Math_imul = Math.imul;
var Math_fround = Math.fround;
var Math_round = Math.round;
var Math_min = Math.min;
var Math_max = Math.max;
var Math_clz32 = Math.clz32;
var Math_trunc = Math.trunc;

// A counter of dependencies for calling run(). If we need to
// do asynchronous work before running, increment this and
// decrement it. Incrementing must happen in a place like
// PRE_RUN_ADDITIONS (used by emcc to add file preloading).
// Note that you can add dependencies in preRun, even though
// it happens right before run - run will be postponed until
// the dependencies are met.
var runDependencies = 0;
var runDependencyWatcher = null;
var dependenciesFulfilled = null; // overridden to take different actions when all run dependencies are fulfilled

function getUniqueRunDependency(id) {
  return id;
}

function addRunDependency(id) {
  runDependencies++;
  if (Module['monitorRunDependencies']) {
    Module['monitorRunDependencies'](runDependencies);
  }
}

function removeRunDependency(id) {
  runDependencies--;
  if (Module['monitorRunDependencies']) {
    Module['monitorRunDependencies'](runDependencies);
  }
  if (runDependencies == 0) {
    if (runDependencyWatcher !== null) {
      clearInterval(runDependencyWatcher);
      runDependencyWatcher = null;
    }
    if (dependenciesFulfilled) {
      var callback = dependenciesFulfilled;
      dependenciesFulfilled = null;
      callback(); // can add another dependenciesFulfilled
    }
  }
}

Module["preloadedImages"] = {}; // maps url to image data
Module["preloadedAudios"] = {}; // maps url to audio data



var memoryInitializer = null;






// Prefix of data URIs emitted by SINGLE_FILE and related options.
var dataURIPrefix = 'data:application/octet-stream;base64,';

// Indicates whether filename is a base64 data URI.
function isDataURI(filename) {
  return String.prototype.startsWith ?
      filename.startsWith(dataURIPrefix) :
      filename.indexOf(dataURIPrefix) === 0;
}





// === Body ===

var ASM_CONSTS = [];





STATIC_BASE = GLOBAL_BASE;

STATICTOP = STATIC_BASE + 512;
/* global initializers */  __ATINIT__.push();








/* no memory initializer */
var tempDoublePtr = STATICTOP; STATICTOP += 16;

function copyTempFloat(ptr) { // functions, because inlining this code increases code size too much

  HEAP8[tempDoublePtr] = HEAP8[ptr];

  HEAP8[tempDoublePtr+1] = HEAP8[ptr+1];

  HEAP8[tempDoublePtr+2] = HEAP8[ptr+2];

  HEAP8[tempDoublePtr+3] = HEAP8[ptr+3];

}

function copyTempDouble(ptr) {

  HEAP8[tempDoublePtr] = HEAP8[ptr];

  HEAP8[tempDoublePtr+1] = HEAP8[ptr+1];

  HEAP8[tempDoublePtr+2] = HEAP8[ptr+2];

  HEAP8[tempDoublePtr+3] = HEAP8[ptr+3];

  HEAP8[tempDoublePtr+4] = HEAP8[ptr+4];

  HEAP8[tempDoublePtr+5] = HEAP8[ptr+5];

  HEAP8[tempDoublePtr+6] = HEAP8[ptr+6];

  HEAP8[tempDoublePtr+7] = HEAP8[ptr+7];

}

// {{PRE_LIBRARY}}


  function _abort() {
      Module['abort']();
    }

  var _llvm_fabs_f32=Math_abs;

  
  function _emscripten_memcpy_big(dest, src, num) {
      HEAPU8.set(HEAPU8.subarray(src, src+num), dest);
      return dest;
    } 

   

  
  function ___setErrNo(value) {
      if (Module['___errno_location']) HEAP32[((Module['___errno_location']())>>2)]=value;
      return value;
    } 
DYNAMICTOP_PTR = staticAlloc(4);

STACK_BASE = STACKTOP = alignMemory(STATICTOP);

STACK_MAX = STACK_BASE + TOTAL_STACK;

DYNAMIC_BASE = alignMemory(STACK_MAX);

HEAP32[DYNAMICTOP_PTR>>2] = DYNAMIC_BASE;

staticSealed = true; // seal the static portion of memory

var ASSERTIONS = false;

/** @type {function(string, boolean=, number=)} */
function intArrayFromString(stringy, dontAddNull, length) {
  var len = length > 0 ? length : lengthBytesUTF8(stringy)+1;
  var u8array = new Array(len);
  var numBytesWritten = stringToUTF8Array(stringy, u8array, 0, u8array.length);
  if (dontAddNull) u8array.length = numBytesWritten;
  return u8array;
}

function intArrayToString(array) {
  var ret = [];
  for (var i = 0; i < array.length; i++) {
    var chr = array[i];
    if (chr > 0xFF) {
      if (ASSERTIONS) {
        assert(false, 'Character code ' + chr + ' (' + String.fromCharCode(chr) + ')  at offset ' + i + ' not in 0x00-0xFF.');
      }
      chr &= 0xFF;
    }
    ret.push(String.fromCharCode(chr));
  }
  return ret.join('');
}


// Copied from https://github.com/strophe/strophejs/blob/e06d027/src/polyfills.js#L149

// This code was written by Tyler Akins and has been placed in the
// public domain.  It would be nice if you left this header intact.
// Base64 code from Tyler Akins -- http://rumkin.com

/**
 * Decodes a base64 string.
 * @param {String} input The string to decode.
 */
var decodeBase64 = typeof atob === 'function' ? atob : function (input) {
  var keyStr = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';

  var output = '';
  var chr1, chr2, chr3;
  var enc1, enc2, enc3, enc4;
  var i = 0;
  // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
  input = input.replace(/[^A-Za-z0-9\+\/\=]/g, '');
  do {
    enc1 = keyStr.indexOf(input.charAt(i++));
    enc2 = keyStr.indexOf(input.charAt(i++));
    enc3 = keyStr.indexOf(input.charAt(i++));
    enc4 = keyStr.indexOf(input.charAt(i++));

    chr1 = (enc1 << 2) | (enc2 >> 4);
    chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
    chr3 = ((enc3 & 3) << 6) | enc4;

    output = output + String.fromCharCode(chr1);

    if (enc3 !== 64) {
      output = output + String.fromCharCode(chr2);
    }
    if (enc4 !== 64) {
      output = output + String.fromCharCode(chr3);
    }
  } while (i < input.length);
  return output;
};

// Converts a string of base64 into a byte array.
// Throws error on invalid input.
function intArrayFromBase64(s) {
  if (typeof ENVIRONMENT_IS_NODE === 'boolean' && ENVIRONMENT_IS_NODE) {
    var buf;
    try {
      buf = Buffer.from(s, 'base64');
    } catch (_) {
      buf = new Buffer(s, 'base64');
    }
    return new Uint8Array(buf.buffer, buf.byteOffset, buf.byteLength);
  }

  try {
    var decoded = decodeBase64(s);
    var bytes = new Uint8Array(decoded.length);
    for (var i = 0 ; i < decoded.length ; ++i) {
      bytes[i] = decoded.charCodeAt(i);
    }
    return bytes;
  } catch (_) {
    throw new Error('Converting base64 string to bytes failed.');
  }
}

// If filename is a base64 data URI, parses and returns data (Buffer on node,
// Uint8Array otherwise). If filename is not a base64 data URI, returns undefined.
function tryParseAsDataURI(filename) {
  if (!isDataURI(filename)) {
    return;
  }

  return intArrayFromBase64(filename.slice(dataURIPrefix.length));
}



Module.asmGlobalArg = { "Math": Math, "Int8Array": Int8Array, "Int16Array": Int16Array, "Int32Array": Int32Array, "Uint8Array": Uint8Array, "Uint16Array": Uint16Array, "Uint32Array": Uint32Array, "Float32Array": Float32Array, "Float64Array": Float64Array, "NaN": NaN, "Infinity": Infinity };

Module.asmLibraryArg = { "abort": abort, "assert": assert, "enlargeMemory": enlargeMemory, "getTotalMemory": getTotalMemory, "abortOnCannotGrowMemory": abortOnCannotGrowMemory, "___setErrNo": ___setErrNo, "_abort": _abort, "_emscripten_memcpy_big": _emscripten_memcpy_big, "_llvm_fabs_f32": _llvm_fabs_f32, "DYNAMICTOP_PTR": DYNAMICTOP_PTR, "tempDoublePtr": tempDoublePtr, "ABORT": ABORT, "STACKTOP": STACKTOP, "STACK_MAX": STACK_MAX };
// EMSCRIPTEN_START_ASM
var asm = (/** @suppress {uselessCode} */ function(global, env, buffer) {
'use asm';


  var HEAP8 = new global.Int8Array(buffer);
  var HEAP16 = new global.Int16Array(buffer);
  var HEAP32 = new global.Int32Array(buffer);
  var HEAPU8 = new global.Uint8Array(buffer);
  var HEAPU16 = new global.Uint16Array(buffer);
  var HEAPU32 = new global.Uint32Array(buffer);
  var HEAPF32 = new global.Float32Array(buffer);
  var HEAPF64 = new global.Float64Array(buffer);

  var DYNAMICTOP_PTR=env.DYNAMICTOP_PTR|0;
  var tempDoublePtr=env.tempDoublePtr|0;
  var ABORT=env.ABORT|0;
  var STACKTOP=env.STACKTOP|0;
  var STACK_MAX=env.STACK_MAX|0;

  var __THREW__ = 0;
  var threwValue = 0;
  var setjmpId = 0;
  var undef = 0;
  var nan = global.NaN, inf = global.Infinity;
  var tempInt = 0, tempBigInt = 0, tempBigIntS = 0, tempValue = 0, tempDouble = 0.0;
  var tempRet0 = 0;

  var Math_floor=global.Math.floor;
  var Math_abs=global.Math.abs;
  var Math_sqrt=global.Math.sqrt;
  var Math_pow=global.Math.pow;
  var Math_cos=global.Math.cos;
  var Math_sin=global.Math.sin;
  var Math_tan=global.Math.tan;
  var Math_acos=global.Math.acos;
  var Math_asin=global.Math.asin;
  var Math_atan=global.Math.atan;
  var Math_atan2=global.Math.atan2;
  var Math_exp=global.Math.exp;
  var Math_log=global.Math.log;
  var Math_ceil=global.Math.ceil;
  var Math_imul=global.Math.imul;
  var Math_min=global.Math.min;
  var Math_max=global.Math.max;
  var Math_clz32=global.Math.clz32;
  var abort=env.abort;
  var assert=env.assert;
  var enlargeMemory=env.enlargeMemory;
  var getTotalMemory=env.getTotalMemory;
  var abortOnCannotGrowMemory=env.abortOnCannotGrowMemory;
  var ___setErrNo=env.___setErrNo;
  var _abort=env._abort;
  var _emscripten_memcpy_big=env._emscripten_memcpy_big;
  var _llvm_fabs_f32=env._llvm_fabs_f32;
  var tempFloat = 0.0;

// EMSCRIPTEN_START_FUNCS

function _malloc($0) {
 $0 = $0 | 0;
 var $$$0192$i = 0, $$$0193$i = 0, $$$4351$i = 0, $$$i = 0, $$0 = 0, $$0$i$i = 0, $$0$i$i$i = 0, $$0$i17$i = 0, $$0189$i = 0, $$0192$lcssa$i = 0, $$01926$i = 0, $$0193$lcssa$i = 0, $$01935$i = 0, $$0197 = 0, $$0199 = 0, $$0206$i$i = 0, $$0207$i$i = 0, $$0211$i$i = 0, $$0212$i$i = 0, $$024367$i = 0, $$0287$i$i = 0, $$0288$i$i = 0, $$0289$i$i = 0, $$0295$i$i = 0, $$0296$i$i = 0, $$0342$i = 0, $$0344$i = 0, $$0345$i = 0, $$0347$i = 0, $$0353$i = 0, $$0358$i = 0, $$0359$i = 0, $$0361$i = 0, $$0362$i = 0, $$0368$i = 0, $$1196$i = 0, $$1198$i = 0, $$124466$i = 0, $$1291$i$i = 0, $$1293$i$i = 0, $$1343$i = 0, $$1348$i = 0, $$1363$i = 0, $$1370$i = 0, $$1374$i = 0, $$2234243136$i = 0, $$2247$ph$i = 0, $$2253$ph$i = 0, $$2355$i = 0, $$3$i = 0, $$3$i$i = 0, $$3$i203 = 0, $$3350$i = 0, $$3372$i = 0, $$4$lcssa$i = 0, $$4$ph$i = 0, $$414$i = 0, $$4236$i = 0, $$4351$lcssa$i = 0, $$435113$i = 0, $$4357$$4$i = 0, $$4357$ph$i = 0, $$435712$i = 0, $$723947$i = 0, $$748$i = 0, $$pre$phi$i$iZ2D = 0, $$pre$phi$i19$iZ2D = 0, $$pre$phi$i211Z2D = 0, $$pre$phi$iZ2D = 0, $$pre$phi11$i$iZ2D = 0, $$pre$phiZ2D = 0, $1 = 0, $1004 = 0, $101 = 0, $1010 = 0, $1013 = 0, $1014 = 0, $102 = 0, $1032 = 0, $1034 = 0, $1041 = 0, $1042 = 0, $1043 = 0, $1052 = 0, $1054 = 0, $1055 = 0, $1056 = 0, $108 = 0, $112 = 0, $114 = 0, $115 = 0, $117 = 0, $119 = 0, $121 = 0, $123 = 0, $125 = 0, $127 = 0, $129 = 0, $134 = 0, $138 = 0, $14 = 0, $143 = 0, $146 = 0, $149 = 0, $150 = 0, $157 = 0, $159 = 0, $16 = 0, $162 = 0, $164 = 0, $167 = 0, $169 = 0, $17 = 0, $172 = 0, $175 = 0, $176 = 0, $178 = 0, $179 = 0, $18 = 0, $181 = 0, $182 = 0, $184 = 0, $185 = 0, $19 = 0, $190 = 0, $191 = 0, $20 = 0, $204 = 0, $208 = 0, $214 = 0, $221 = 0, $225 = 0, $234 = 0, $235 = 0, $237 = 0, $238 = 0, $242 = 0, $243 = 0, $251 = 0, $252 = 0, $253 = 0, $255 = 0, $256 = 0, $261 = 0, $262 = 0, $265 = 0, $267 = 0, $27 = 0, $270 = 0, $275 = 0, $282 = 0, $292 = 0, $296 = 0, $30 = 0, $302 = 0, $306 = 0, $309 = 0, $313 = 0, $315 = 0, $316 = 0, $318 = 0, $320 = 0, $322 = 0, $324 = 0, $326 = 0, $328 = 0, $330 = 0, $34 = 0, $340 = 0, $341 = 0, $352 = 0, $354 = 0, $357 = 0, $359 = 0, $362 = 0, $364 = 0, $367 = 0, $37 = 0, $370 = 0, $371 = 0, $373 = 0, $374 = 0, $376 = 0, $377 = 0, $379 = 0, $380 = 0, $385 = 0, $386 = 0, $391 = 0, $399 = 0, $403 = 0, $409 = 0, $41 = 0, $416 = 0, $420 = 0, $428 = 0, $431 = 0, $432 = 0, $433 = 0, $437 = 0, $438 = 0, $44 = 0, $444 = 0, $449 = 0, $450 = 0, $453 = 0, $455 = 0, $458 = 0, $463 = 0, $469 = 0, $47 = 0, $471 = 0, $473 = 0, $475 = 0, $49 = 0, $492 = 0, $494 = 0, $50 = 0, $501 = 0, $502 = 0, $503 = 0, $512 = 0, $514 = 0, $515 = 0, $517 = 0, $52 = 0, $526 = 0, $530 = 0, $532 = 0, $533 = 0, $534 = 0, $54 = 0, $545 = 0, $546 = 0, $547 = 0, $548 = 0, $549 = 0, $550 = 0, $552 = 0, $554 = 0, $555 = 0, $56 = 0, $561 = 0, $563 = 0, $565 = 0, $570 = 0, $572 = 0, $574 = 0, $575 = 0, $576 = 0, $58 = 0, $584 = 0, $585 = 0, $588 = 0, $592 = 0, $595 = 0, $597 = 0, $6 = 0, $60 = 0, $603 = 0, $607 = 0, $611 = 0, $62 = 0, $620 = 0, $621 = 0, $627 = 0, $629 = 0, $633 = 0, $636 = 0, $638 = 0, $64 = 0, $642 = 0, $644 = 0, $649 = 0, $650 = 0, $651 = 0, $657 = 0, $658 = 0, $659 = 0, $663 = 0, $67 = 0, $673 = 0, $675 = 0, $680 = 0, $681 = 0, $682 = 0, $688 = 0, $69 = 0, $690 = 0, $694 = 0, $7 = 0, $70 = 0, $700 = 0, $704 = 0, $71 = 0, $710 = 0, $712 = 0, $718 = 0, $72 = 0, $722 = 0, $723 = 0, $728 = 0, $73 = 0, $734 = 0, $739 = 0, $742 = 0, $743 = 0, $746 = 0, $748 = 0, $750 = 0, $753 = 0, $764 = 0, $769 = 0, $77 = 0, $771 = 0, $774 = 0, $776 = 0, $779 = 0, $782 = 0, $783 = 0, $784 = 0, $786 = 0, $788 = 0, $789 = 0, $791 = 0, $792 = 0, $797 = 0, $798 = 0, $8 = 0, $80 = 0, $812 = 0, $815 = 0, $816 = 0, $822 = 0, $83 = 0, $830 = 0, $836 = 0, $839 = 0, $84 = 0, $840 = 0, $841 = 0, $845 = 0, $846 = 0, $852 = 0, $857 = 0, $858 = 0, $861 = 0, $863 = 0, $866 = 0, $87 = 0, $871 = 0, $877 = 0, $879 = 0, $881 = 0, $882 = 0, $9 = 0, $900 = 0, $902 = 0, $909 = 0, $910 = 0, $911 = 0, $919 = 0, $92 = 0, $923 = 0, $927 = 0, $929 = 0, $93 = 0, $935 = 0, $936 = 0, $938 = 0, $939 = 0, $941 = 0, $943 = 0, $948 = 0, $949 = 0, $95 = 0, $950 = 0, $956 = 0, $958 = 0, $96 = 0, $964 = 0, $969 = 0, $972 = 0, $973 = 0, $974 = 0, $978 = 0, $979 = 0, $98 = 0, $985 = 0, $990 = 0, $991 = 0, $994 = 0, $996 = 0, $999 = 0, label = 0, sp = 0, $958$looptemp = 0;
 sp = STACKTOP;
 STACKTOP = STACKTOP + 16 | 0;
 $1 = sp;
 do if ($0 >>> 0 < 245) {
  $6 = $0 >>> 0 < 11 ? 16 : $0 + 11 & -8;
  $7 = $6 >>> 3;
  $8 = HEAP32[2] | 0;
  $9 = $8 >>> $7;
  if ($9 & 3 | 0) {
   $14 = ($9 & 1 ^ 1) + $7 | 0;
   $16 = 48 + ($14 << 1 << 2) | 0;
   $17 = $16 + 8 | 0;
   $18 = HEAP32[$17 >> 2] | 0;
   $19 = $18 + 8 | 0;
   $20 = HEAP32[$19 >> 2] | 0;
   do if (($20 | 0) == ($16 | 0)) HEAP32[2] = $8 & ~(1 << $14); else {
    if ((HEAP32[6] | 0) >>> 0 > $20 >>> 0) _abort();
    $27 = $20 + 12 | 0;
    if ((HEAP32[$27 >> 2] | 0) == ($18 | 0)) {
     HEAP32[$27 >> 2] = $16;
     HEAP32[$17 >> 2] = $20;
     break;
    } else _abort();
   } while (0);
   $30 = $14 << 3;
   HEAP32[$18 + 4 >> 2] = $30 | 3;
   $34 = $18 + $30 + 4 | 0;
   HEAP32[$34 >> 2] = HEAP32[$34 >> 2] | 1;
   $$0 = $19;
   STACKTOP = sp;
   return $$0 | 0;
  }
  $37 = HEAP32[4] | 0;
  if ($6 >>> 0 > $37 >>> 0) {
   if ($9 | 0) {
    $41 = 2 << $7;
    $44 = $9 << $7 & ($41 | 0 - $41);
    $47 = ($44 & 0 - $44) + -1 | 0;
    $49 = $47 >>> 12 & 16;
    $50 = $47 >>> $49;
    $52 = $50 >>> 5 & 8;
    $54 = $50 >>> $52;
    $56 = $54 >>> 2 & 4;
    $58 = $54 >>> $56;
    $60 = $58 >>> 1 & 2;
    $62 = $58 >>> $60;
    $64 = $62 >>> 1 & 1;
    $67 = ($52 | $49 | $56 | $60 | $64) + ($62 >>> $64) | 0;
    $69 = 48 + ($67 << 1 << 2) | 0;
    $70 = $69 + 8 | 0;
    $71 = HEAP32[$70 >> 2] | 0;
    $72 = $71 + 8 | 0;
    $73 = HEAP32[$72 >> 2] | 0;
    do if (($73 | 0) == ($69 | 0)) {
     $77 = $8 & ~(1 << $67);
     HEAP32[2] = $77;
     $98 = $77;
    } else {
     if ((HEAP32[6] | 0) >>> 0 > $73 >>> 0) _abort();
     $80 = $73 + 12 | 0;
     if ((HEAP32[$80 >> 2] | 0) == ($71 | 0)) {
      HEAP32[$80 >> 2] = $69;
      HEAP32[$70 >> 2] = $73;
      $98 = $8;
      break;
     } else _abort();
    } while (0);
    $83 = $67 << 3;
    $84 = $83 - $6 | 0;
    HEAP32[$71 + 4 >> 2] = $6 | 3;
    $87 = $71 + $6 | 0;
    HEAP32[$87 + 4 >> 2] = $84 | 1;
    HEAP32[$71 + $83 >> 2] = $84;
    if ($37 | 0) {
     $92 = HEAP32[7] | 0;
     $93 = $37 >>> 3;
     $95 = 48 + ($93 << 1 << 2) | 0;
     $96 = 1 << $93;
     if (!($98 & $96)) {
      HEAP32[2] = $98 | $96;
      $$0199 = $95;
      $$pre$phiZ2D = $95 + 8 | 0;
     } else {
      $101 = $95 + 8 | 0;
      $102 = HEAP32[$101 >> 2] | 0;
      if ((HEAP32[6] | 0) >>> 0 > $102 >>> 0) _abort(); else {
       $$0199 = $102;
       $$pre$phiZ2D = $101;
      }
     }
     HEAP32[$$pre$phiZ2D >> 2] = $92;
     HEAP32[$$0199 + 12 >> 2] = $92;
     HEAP32[$92 + 8 >> 2] = $$0199;
     HEAP32[$92 + 12 >> 2] = $95;
    }
    HEAP32[4] = $84;
    HEAP32[7] = $87;
    $$0 = $72;
    STACKTOP = sp;
    return $$0 | 0;
   }
   $108 = HEAP32[3] | 0;
   if (!$108) $$0197 = $6; else {
    $112 = ($108 & 0 - $108) + -1 | 0;
    $114 = $112 >>> 12 & 16;
    $115 = $112 >>> $114;
    $117 = $115 >>> 5 & 8;
    $119 = $115 >>> $117;
    $121 = $119 >>> 2 & 4;
    $123 = $119 >>> $121;
    $125 = $123 >>> 1 & 2;
    $127 = $123 >>> $125;
    $129 = $127 >>> 1 & 1;
    $134 = HEAP32[312 + (($117 | $114 | $121 | $125 | $129) + ($127 >>> $129) << 2) >> 2] | 0;
    $138 = (HEAP32[$134 + 4 >> 2] & -8) - $6 | 0;
    $143 = HEAP32[$134 + 16 + (((HEAP32[$134 + 16 >> 2] | 0) == 0 & 1) << 2) >> 2] | 0;
    if (!$143) {
     $$0192$lcssa$i = $134;
     $$0193$lcssa$i = $138;
    } else {
     $$01926$i = $134;
     $$01935$i = $138;
     $146 = $143;
     while (1) {
      $149 = (HEAP32[$146 + 4 >> 2] & -8) - $6 | 0;
      $150 = $149 >>> 0 < $$01935$i >>> 0;
      $$$0193$i = $150 ? $149 : $$01935$i;
      $$$0192$i = $150 ? $146 : $$01926$i;
      $146 = HEAP32[$146 + 16 + (((HEAP32[$146 + 16 >> 2] | 0) == 0 & 1) << 2) >> 2] | 0;
      if (!$146) {
       $$0192$lcssa$i = $$$0192$i;
       $$0193$lcssa$i = $$$0193$i;
       break;
      } else {
       $$01926$i = $$$0192$i;
       $$01935$i = $$$0193$i;
      }
     }
    }
    $157 = HEAP32[6] | 0;
    if ($157 >>> 0 > $$0192$lcssa$i >>> 0) _abort();
    $159 = $$0192$lcssa$i + $6 | 0;
    if ($159 >>> 0 <= $$0192$lcssa$i >>> 0) _abort();
    $162 = HEAP32[$$0192$lcssa$i + 24 >> 2] | 0;
    $164 = HEAP32[$$0192$lcssa$i + 12 >> 2] | 0;
    do if (($164 | 0) == ($$0192$lcssa$i | 0)) {
     $175 = $$0192$lcssa$i + 20 | 0;
     $176 = HEAP32[$175 >> 2] | 0;
     if (!$176) {
      $178 = $$0192$lcssa$i + 16 | 0;
      $179 = HEAP32[$178 >> 2] | 0;
      if (!$179) {
       $$3$i = 0;
       break;
      } else {
       $$1196$i = $179;
       $$1198$i = $178;
      }
     } else {
      $$1196$i = $176;
      $$1198$i = $175;
     }
     while (1) {
      $181 = $$1196$i + 20 | 0;
      $182 = HEAP32[$181 >> 2] | 0;
      if ($182 | 0) {
       $$1196$i = $182;
       $$1198$i = $181;
       continue;
      }
      $184 = $$1196$i + 16 | 0;
      $185 = HEAP32[$184 >> 2] | 0;
      if (!$185) break; else {
       $$1196$i = $185;
       $$1198$i = $184;
      }
     }
     if ($157 >>> 0 > $$1198$i >>> 0) _abort(); else {
      HEAP32[$$1198$i >> 2] = 0;
      $$3$i = $$1196$i;
      break;
     }
    } else {
     $167 = HEAP32[$$0192$lcssa$i + 8 >> 2] | 0;
     if ($157 >>> 0 > $167 >>> 0) _abort();
     $169 = $167 + 12 | 0;
     if ((HEAP32[$169 >> 2] | 0) != ($$0192$lcssa$i | 0)) _abort();
     $172 = $164 + 8 | 0;
     if ((HEAP32[$172 >> 2] | 0) == ($$0192$lcssa$i | 0)) {
      HEAP32[$169 >> 2] = $164;
      HEAP32[$172 >> 2] = $167;
      $$3$i = $164;
      break;
     } else _abort();
    } while (0);
    L73 : do if ($162 | 0) {
     $190 = HEAP32[$$0192$lcssa$i + 28 >> 2] | 0;
     $191 = 312 + ($190 << 2) | 0;
     do if (($$0192$lcssa$i | 0) == (HEAP32[$191 >> 2] | 0)) {
      HEAP32[$191 >> 2] = $$3$i;
      if (!$$3$i) {
       HEAP32[3] = $108 & ~(1 << $190);
       break L73;
      }
     } else if ((HEAP32[6] | 0) >>> 0 > $162 >>> 0) _abort(); else {
      HEAP32[$162 + 16 + (((HEAP32[$162 + 16 >> 2] | 0) != ($$0192$lcssa$i | 0) & 1) << 2) >> 2] = $$3$i;
      if (!$$3$i) break L73; else break;
     } while (0);
     $204 = HEAP32[6] | 0;
     if ($204 >>> 0 > $$3$i >>> 0) _abort();
     HEAP32[$$3$i + 24 >> 2] = $162;
     $208 = HEAP32[$$0192$lcssa$i + 16 >> 2] | 0;
     do if ($208 | 0) if ($204 >>> 0 > $208 >>> 0) _abort(); else {
      HEAP32[$$3$i + 16 >> 2] = $208;
      HEAP32[$208 + 24 >> 2] = $$3$i;
      break;
     } while (0);
     $214 = HEAP32[$$0192$lcssa$i + 20 >> 2] | 0;
     if ($214 | 0) if ((HEAP32[6] | 0) >>> 0 > $214 >>> 0) _abort(); else {
      HEAP32[$$3$i + 20 >> 2] = $214;
      HEAP32[$214 + 24 >> 2] = $$3$i;
      break;
     }
    } while (0);
    if ($$0193$lcssa$i >>> 0 < 16) {
     $221 = $$0193$lcssa$i + $6 | 0;
     HEAP32[$$0192$lcssa$i + 4 >> 2] = $221 | 3;
     $225 = $$0192$lcssa$i + $221 + 4 | 0;
     HEAP32[$225 >> 2] = HEAP32[$225 >> 2] | 1;
    } else {
     HEAP32[$$0192$lcssa$i + 4 >> 2] = $6 | 3;
     HEAP32[$159 + 4 >> 2] = $$0193$lcssa$i | 1;
     HEAP32[$159 + $$0193$lcssa$i >> 2] = $$0193$lcssa$i;
     if ($37 | 0) {
      $234 = HEAP32[7] | 0;
      $235 = $37 >>> 3;
      $237 = 48 + ($235 << 1 << 2) | 0;
      $238 = 1 << $235;
      if (!($8 & $238)) {
       HEAP32[2] = $8 | $238;
       $$0189$i = $237;
       $$pre$phi$iZ2D = $237 + 8 | 0;
      } else {
       $242 = $237 + 8 | 0;
       $243 = HEAP32[$242 >> 2] | 0;
       if ((HEAP32[6] | 0) >>> 0 > $243 >>> 0) _abort(); else {
        $$0189$i = $243;
        $$pre$phi$iZ2D = $242;
       }
      }
      HEAP32[$$pre$phi$iZ2D >> 2] = $234;
      HEAP32[$$0189$i + 12 >> 2] = $234;
      HEAP32[$234 + 8 >> 2] = $$0189$i;
      HEAP32[$234 + 12 >> 2] = $237;
     }
     HEAP32[4] = $$0193$lcssa$i;
     HEAP32[7] = $159;
    }
    $$0 = $$0192$lcssa$i + 8 | 0;
    STACKTOP = sp;
    return $$0 | 0;
   }
  } else $$0197 = $6;
 } else if ($0 >>> 0 > 4294967231) $$0197 = -1; else {
  $251 = $0 + 11 | 0;
  $252 = $251 & -8;
  $253 = HEAP32[3] | 0;
  if (!$253) $$0197 = $252; else {
   $255 = 0 - $252 | 0;
   $256 = $251 >>> 8;
   if (!$256) $$0358$i = 0; else if ($252 >>> 0 > 16777215) $$0358$i = 31; else {
    $261 = ($256 + 1048320 | 0) >>> 16 & 8;
    $262 = $256 << $261;
    $265 = ($262 + 520192 | 0) >>> 16 & 4;
    $267 = $262 << $265;
    $270 = ($267 + 245760 | 0) >>> 16 & 2;
    $275 = 14 - ($265 | $261 | $270) + ($267 << $270 >>> 15) | 0;
    $$0358$i = $252 >>> ($275 + 7 | 0) & 1 | $275 << 1;
   }
   $282 = HEAP32[312 + ($$0358$i << 2) >> 2] | 0;
   L117 : do if (!$282) {
    $$2355$i = 0;
    $$3$i203 = 0;
    $$3350$i = $255;
    label = 81;
   } else {
    $$0342$i = 0;
    $$0347$i = $255;
    $$0353$i = $282;
    $$0359$i = $252 << (($$0358$i | 0) == 31 ? 0 : 25 - ($$0358$i >>> 1) | 0);
    $$0362$i = 0;
    while (1) {
     $292 = (HEAP32[$$0353$i + 4 >> 2] & -8) - $252 | 0;
     if ($292 >>> 0 < $$0347$i >>> 0) if (!$292) {
      $$414$i = $$0353$i;
      $$435113$i = 0;
      $$435712$i = $$0353$i;
      label = 85;
      break L117;
     } else {
      $$1343$i = $$0353$i;
      $$1348$i = $292;
     } else {
      $$1343$i = $$0342$i;
      $$1348$i = $$0347$i;
     }
     $296 = HEAP32[$$0353$i + 20 >> 2] | 0;
     $$0353$i = HEAP32[$$0353$i + 16 + ($$0359$i >>> 31 << 2) >> 2] | 0;
     $$1363$i = ($296 | 0) == 0 | ($296 | 0) == ($$0353$i | 0) ? $$0362$i : $296;
     $302 = ($$0353$i | 0) == 0;
     if ($302) {
      $$2355$i = $$1363$i;
      $$3$i203 = $$1343$i;
      $$3350$i = $$1348$i;
      label = 81;
      break;
     } else {
      $$0342$i = $$1343$i;
      $$0347$i = $$1348$i;
      $$0359$i = $$0359$i << (($302 ^ 1) & 1);
      $$0362$i = $$1363$i;
     }
    }
   } while (0);
   if ((label | 0) == 81) {
    if (($$2355$i | 0) == 0 & ($$3$i203 | 0) == 0) {
     $306 = 2 << $$0358$i;
     $309 = $253 & ($306 | 0 - $306);
     if (!$309) {
      $$0197 = $252;
      break;
     }
     $313 = ($309 & 0 - $309) + -1 | 0;
     $315 = $313 >>> 12 & 16;
     $316 = $313 >>> $315;
     $318 = $316 >>> 5 & 8;
     $320 = $316 >>> $318;
     $322 = $320 >>> 2 & 4;
     $324 = $320 >>> $322;
     $326 = $324 >>> 1 & 2;
     $328 = $324 >>> $326;
     $330 = $328 >>> 1 & 1;
     $$4$ph$i = 0;
     $$4357$ph$i = HEAP32[312 + (($318 | $315 | $322 | $326 | $330) + ($328 >>> $330) << 2) >> 2] | 0;
    } else {
     $$4$ph$i = $$3$i203;
     $$4357$ph$i = $$2355$i;
    }
    if (!$$4357$ph$i) {
     $$4$lcssa$i = $$4$ph$i;
     $$4351$lcssa$i = $$3350$i;
    } else {
     $$414$i = $$4$ph$i;
     $$435113$i = $$3350$i;
     $$435712$i = $$4357$ph$i;
     label = 85;
    }
   }
   if ((label | 0) == 85) while (1) {
    label = 0;
    $340 = (HEAP32[$$435712$i + 4 >> 2] & -8) - $252 | 0;
    $341 = $340 >>> 0 < $$435113$i >>> 0;
    $$$4351$i = $341 ? $340 : $$435113$i;
    $$4357$$4$i = $341 ? $$435712$i : $$414$i;
    $$435712$i = HEAP32[$$435712$i + 16 + (((HEAP32[$$435712$i + 16 >> 2] | 0) == 0 & 1) << 2) >> 2] | 0;
    if (!$$435712$i) {
     $$4$lcssa$i = $$4357$$4$i;
     $$4351$lcssa$i = $$$4351$i;
     break;
    } else {
     $$414$i = $$4357$$4$i;
     $$435113$i = $$$4351$i;
     label = 85;
    }
   }
   if (!$$4$lcssa$i) $$0197 = $252; else if ($$4351$lcssa$i >>> 0 < ((HEAP32[4] | 0) - $252 | 0) >>> 0) {
    $352 = HEAP32[6] | 0;
    if ($352 >>> 0 > $$4$lcssa$i >>> 0) _abort();
    $354 = $$4$lcssa$i + $252 | 0;
    if ($354 >>> 0 <= $$4$lcssa$i >>> 0) _abort();
    $357 = HEAP32[$$4$lcssa$i + 24 >> 2] | 0;
    $359 = HEAP32[$$4$lcssa$i + 12 >> 2] | 0;
    do if (($359 | 0) == ($$4$lcssa$i | 0)) {
     $370 = $$4$lcssa$i + 20 | 0;
     $371 = HEAP32[$370 >> 2] | 0;
     if (!$371) {
      $373 = $$4$lcssa$i + 16 | 0;
      $374 = HEAP32[$373 >> 2] | 0;
      if (!$374) {
       $$3372$i = 0;
       break;
      } else {
       $$1370$i = $374;
       $$1374$i = $373;
      }
     } else {
      $$1370$i = $371;
      $$1374$i = $370;
     }
     while (1) {
      $376 = $$1370$i + 20 | 0;
      $377 = HEAP32[$376 >> 2] | 0;
      if ($377 | 0) {
       $$1370$i = $377;
       $$1374$i = $376;
       continue;
      }
      $379 = $$1370$i + 16 | 0;
      $380 = HEAP32[$379 >> 2] | 0;
      if (!$380) break; else {
       $$1370$i = $380;
       $$1374$i = $379;
      }
     }
     if ($352 >>> 0 > $$1374$i >>> 0) _abort(); else {
      HEAP32[$$1374$i >> 2] = 0;
      $$3372$i = $$1370$i;
      break;
     }
    } else {
     $362 = HEAP32[$$4$lcssa$i + 8 >> 2] | 0;
     if ($352 >>> 0 > $362 >>> 0) _abort();
     $364 = $362 + 12 | 0;
     if ((HEAP32[$364 >> 2] | 0) != ($$4$lcssa$i | 0)) _abort();
     $367 = $359 + 8 | 0;
     if ((HEAP32[$367 >> 2] | 0) == ($$4$lcssa$i | 0)) {
      HEAP32[$364 >> 2] = $359;
      HEAP32[$367 >> 2] = $362;
      $$3372$i = $359;
      break;
     } else _abort();
    } while (0);
    L164 : do if (!$357) $475 = $253; else {
     $385 = HEAP32[$$4$lcssa$i + 28 >> 2] | 0;
     $386 = 312 + ($385 << 2) | 0;
     do if (($$4$lcssa$i | 0) == (HEAP32[$386 >> 2] | 0)) {
      HEAP32[$386 >> 2] = $$3372$i;
      if (!$$3372$i) {
       $391 = $253 & ~(1 << $385);
       HEAP32[3] = $391;
       $475 = $391;
       break L164;
      }
     } else if ((HEAP32[6] | 0) >>> 0 > $357 >>> 0) _abort(); else {
      HEAP32[$357 + 16 + (((HEAP32[$357 + 16 >> 2] | 0) != ($$4$lcssa$i | 0) & 1) << 2) >> 2] = $$3372$i;
      if (!$$3372$i) {
       $475 = $253;
       break L164;
      } else break;
     } while (0);
     $399 = HEAP32[6] | 0;
     if ($399 >>> 0 > $$3372$i >>> 0) _abort();
     HEAP32[$$3372$i + 24 >> 2] = $357;
     $403 = HEAP32[$$4$lcssa$i + 16 >> 2] | 0;
     do if ($403 | 0) if ($399 >>> 0 > $403 >>> 0) _abort(); else {
      HEAP32[$$3372$i + 16 >> 2] = $403;
      HEAP32[$403 + 24 >> 2] = $$3372$i;
      break;
     } while (0);
     $409 = HEAP32[$$4$lcssa$i + 20 >> 2] | 0;
     if (!$409) $475 = $253; else if ((HEAP32[6] | 0) >>> 0 > $409 >>> 0) _abort(); else {
      HEAP32[$$3372$i + 20 >> 2] = $409;
      HEAP32[$409 + 24 >> 2] = $$3372$i;
      $475 = $253;
      break;
     }
    } while (0);
    do if ($$4351$lcssa$i >>> 0 < 16) {
     $416 = $$4351$lcssa$i + $252 | 0;
     HEAP32[$$4$lcssa$i + 4 >> 2] = $416 | 3;
     $420 = $$4$lcssa$i + $416 + 4 | 0;
     HEAP32[$420 >> 2] = HEAP32[$420 >> 2] | 1;
    } else {
     HEAP32[$$4$lcssa$i + 4 >> 2] = $252 | 3;
     HEAP32[$354 + 4 >> 2] = $$4351$lcssa$i | 1;
     HEAP32[$354 + $$4351$lcssa$i >> 2] = $$4351$lcssa$i;
     $428 = $$4351$lcssa$i >>> 3;
     if ($$4351$lcssa$i >>> 0 < 256) {
      $431 = 48 + ($428 << 1 << 2) | 0;
      $432 = HEAP32[2] | 0;
      $433 = 1 << $428;
      if (!($432 & $433)) {
       HEAP32[2] = $432 | $433;
       $$0368$i = $431;
       $$pre$phi$i211Z2D = $431 + 8 | 0;
      } else {
       $437 = $431 + 8 | 0;
       $438 = HEAP32[$437 >> 2] | 0;
       if ((HEAP32[6] | 0) >>> 0 > $438 >>> 0) _abort(); else {
        $$0368$i = $438;
        $$pre$phi$i211Z2D = $437;
       }
      }
      HEAP32[$$pre$phi$i211Z2D >> 2] = $354;
      HEAP32[$$0368$i + 12 >> 2] = $354;
      HEAP32[$354 + 8 >> 2] = $$0368$i;
      HEAP32[$354 + 12 >> 2] = $431;
      break;
     }
     $444 = $$4351$lcssa$i >>> 8;
     if (!$444) $$0361$i = 0; else if ($$4351$lcssa$i >>> 0 > 16777215) $$0361$i = 31; else {
      $449 = ($444 + 1048320 | 0) >>> 16 & 8;
      $450 = $444 << $449;
      $453 = ($450 + 520192 | 0) >>> 16 & 4;
      $455 = $450 << $453;
      $458 = ($455 + 245760 | 0) >>> 16 & 2;
      $463 = 14 - ($453 | $449 | $458) + ($455 << $458 >>> 15) | 0;
      $$0361$i = $$4351$lcssa$i >>> ($463 + 7 | 0) & 1 | $463 << 1;
     }
     $469 = 312 + ($$0361$i << 2) | 0;
     HEAP32[$354 + 28 >> 2] = $$0361$i;
     $471 = $354 + 16 | 0;
     HEAP32[$471 + 4 >> 2] = 0;
     HEAP32[$471 >> 2] = 0;
     $473 = 1 << $$0361$i;
     if (!($475 & $473)) {
      HEAP32[3] = $475 | $473;
      HEAP32[$469 >> 2] = $354;
      HEAP32[$354 + 24 >> 2] = $469;
      HEAP32[$354 + 12 >> 2] = $354;
      HEAP32[$354 + 8 >> 2] = $354;
      break;
     }
     $$0344$i = $$4351$lcssa$i << (($$0361$i | 0) == 31 ? 0 : 25 - ($$0361$i >>> 1) | 0);
     $$0345$i = HEAP32[$469 >> 2] | 0;
     while (1) {
      if ((HEAP32[$$0345$i + 4 >> 2] & -8 | 0) == ($$4351$lcssa$i | 0)) {
       label = 139;
       break;
      }
      $492 = $$0345$i + 16 + ($$0344$i >>> 31 << 2) | 0;
      $494 = HEAP32[$492 >> 2] | 0;
      if (!$494) {
       label = 136;
       break;
      } else {
       $$0344$i = $$0344$i << 1;
       $$0345$i = $494;
      }
     }
     if ((label | 0) == 136) if ((HEAP32[6] | 0) >>> 0 > $492 >>> 0) _abort(); else {
      HEAP32[$492 >> 2] = $354;
      HEAP32[$354 + 24 >> 2] = $$0345$i;
      HEAP32[$354 + 12 >> 2] = $354;
      HEAP32[$354 + 8 >> 2] = $354;
      break;
     } else if ((label | 0) == 139) {
      $501 = $$0345$i + 8 | 0;
      $502 = HEAP32[$501 >> 2] | 0;
      $503 = HEAP32[6] | 0;
      if ($503 >>> 0 <= $502 >>> 0 & $503 >>> 0 <= $$0345$i >>> 0) {
       HEAP32[$502 + 12 >> 2] = $354;
       HEAP32[$501 >> 2] = $354;
       HEAP32[$354 + 8 >> 2] = $502;
       HEAP32[$354 + 12 >> 2] = $$0345$i;
       HEAP32[$354 + 24 >> 2] = 0;
       break;
      } else _abort();
     }
    } while (0);
    $$0 = $$4$lcssa$i + 8 | 0;
    STACKTOP = sp;
    return $$0 | 0;
   } else $$0197 = $252;
  }
 } while (0);
 $512 = HEAP32[4] | 0;
 if ($512 >>> 0 >= $$0197 >>> 0) {
  $514 = $512 - $$0197 | 0;
  $515 = HEAP32[7] | 0;
  if ($514 >>> 0 > 15) {
   $517 = $515 + $$0197 | 0;
   HEAP32[7] = $517;
   HEAP32[4] = $514;
   HEAP32[$517 + 4 >> 2] = $514 | 1;
   HEAP32[$515 + $512 >> 2] = $514;
   HEAP32[$515 + 4 >> 2] = $$0197 | 3;
  } else {
   HEAP32[4] = 0;
   HEAP32[7] = 0;
   HEAP32[$515 + 4 >> 2] = $512 | 3;
   $526 = $515 + $512 + 4 | 0;
   HEAP32[$526 >> 2] = HEAP32[$526 >> 2] | 1;
  }
  $$0 = $515 + 8 | 0;
  STACKTOP = sp;
  return $$0 | 0;
 }
 $530 = HEAP32[5] | 0;
 if ($530 >>> 0 > $$0197 >>> 0) {
  $532 = $530 - $$0197 | 0;
  HEAP32[5] = $532;
  $533 = HEAP32[8] | 0;
  $534 = $533 + $$0197 | 0;
  HEAP32[8] = $534;
  HEAP32[$534 + 4 >> 2] = $532 | 1;
  HEAP32[$533 + 4 >> 2] = $$0197 | 3;
  $$0 = $533 + 8 | 0;
  STACKTOP = sp;
  return $$0 | 0;
 }
 if (!(HEAP32[120] | 0)) {
  HEAP32[122] = 4096;
  HEAP32[121] = 4096;
  HEAP32[123] = -1;
  HEAP32[124] = -1;
  HEAP32[125] = 0;
  HEAP32[113] = 0;
  HEAP32[120] = $1 & -16 ^ 1431655768;
  $548 = 4096;
 } else $548 = HEAP32[122] | 0;
 $545 = $$0197 + 48 | 0;
 $546 = $$0197 + 47 | 0;
 $547 = $548 + $546 | 0;
 $549 = 0 - $548 | 0;
 $550 = $547 & $549;
 if ($550 >>> 0 <= $$0197 >>> 0) {
  $$0 = 0;
  STACKTOP = sp;
  return $$0 | 0;
 }
 $552 = HEAP32[112] | 0;
 if ($552 | 0) {
  $554 = HEAP32[110] | 0;
  $555 = $554 + $550 | 0;
  if ($555 >>> 0 <= $554 >>> 0 | $555 >>> 0 > $552 >>> 0) {
   $$0 = 0;
   STACKTOP = sp;
   return $$0 | 0;
  }
 }
 L244 : do if (!(HEAP32[113] & 4)) {
  $561 = HEAP32[8] | 0;
  L246 : do if (!$561) label = 163; else {
   $$0$i$i = 456;
   while (1) {
    $563 = HEAP32[$$0$i$i >> 2] | 0;
    if ($563 >>> 0 <= $561 >>> 0) {
     $565 = $$0$i$i + 4 | 0;
     if (($563 + (HEAP32[$565 >> 2] | 0) | 0) >>> 0 > $561 >>> 0) break;
    }
    $570 = HEAP32[$$0$i$i + 8 >> 2] | 0;
    if (!$570) {
     label = 163;
     break L246;
    } else $$0$i$i = $570;
   }
   $595 = $547 - $530 & $549;
   if ($595 >>> 0 < 2147483647) {
    $597 = _sbrk($595 | 0) | 0;
    if (($597 | 0) == ((HEAP32[$$0$i$i >> 2] | 0) + (HEAP32[$565 >> 2] | 0) | 0)) if (($597 | 0) == (-1 | 0)) $$2234243136$i = $595; else {
     $$723947$i = $595;
     $$748$i = $597;
     label = 180;
     break L244;
    } else {
     $$2247$ph$i = $597;
     $$2253$ph$i = $595;
     label = 171;
    }
   } else $$2234243136$i = 0;
  } while (0);
  do if ((label | 0) == 163) {
   $572 = _sbrk(0) | 0;
   if (($572 | 0) == (-1 | 0)) $$2234243136$i = 0; else {
    $574 = $572;
    $575 = HEAP32[121] | 0;
    $576 = $575 + -1 | 0;
    $$$i = (($576 & $574 | 0) == 0 ? 0 : ($576 + $574 & 0 - $575) - $574 | 0) + $550 | 0;
    $584 = HEAP32[110] | 0;
    $585 = $$$i + $584 | 0;
    if ($$$i >>> 0 > $$0197 >>> 0 & $$$i >>> 0 < 2147483647) {
     $588 = HEAP32[112] | 0;
     if ($588 | 0) if ($585 >>> 0 <= $584 >>> 0 | $585 >>> 0 > $588 >>> 0) {
      $$2234243136$i = 0;
      break;
     }
     $592 = _sbrk($$$i | 0) | 0;
     if (($592 | 0) == ($572 | 0)) {
      $$723947$i = $$$i;
      $$748$i = $572;
      label = 180;
      break L244;
     } else {
      $$2247$ph$i = $592;
      $$2253$ph$i = $$$i;
      label = 171;
     }
    } else $$2234243136$i = 0;
   }
  } while (0);
  do if ((label | 0) == 171) {
   $603 = 0 - $$2253$ph$i | 0;
   if (!($545 >>> 0 > $$2253$ph$i >>> 0 & ($$2253$ph$i >>> 0 < 2147483647 & ($$2247$ph$i | 0) != (-1 | 0)))) if (($$2247$ph$i | 0) == (-1 | 0)) {
    $$2234243136$i = 0;
    break;
   } else {
    $$723947$i = $$2253$ph$i;
    $$748$i = $$2247$ph$i;
    label = 180;
    break L244;
   }
   $607 = HEAP32[122] | 0;
   $611 = $546 - $$2253$ph$i + $607 & 0 - $607;
   if ($611 >>> 0 >= 2147483647) {
    $$723947$i = $$2253$ph$i;
    $$748$i = $$2247$ph$i;
    label = 180;
    break L244;
   }
   if ((_sbrk($611 | 0) | 0) == (-1 | 0)) {
    _sbrk($603 | 0) | 0;
    $$2234243136$i = 0;
    break;
   } else {
    $$723947$i = $611 + $$2253$ph$i | 0;
    $$748$i = $$2247$ph$i;
    label = 180;
    break L244;
   }
  } while (0);
  HEAP32[113] = HEAP32[113] | 4;
  $$4236$i = $$2234243136$i;
  label = 178;
 } else {
  $$4236$i = 0;
  label = 178;
 } while (0);
 if ((label | 0) == 178) if ($550 >>> 0 < 2147483647) {
  $620 = _sbrk($550 | 0) | 0;
  $621 = _sbrk(0) | 0;
  $627 = $621 - $620 | 0;
  $629 = $627 >>> 0 > ($$0197 + 40 | 0) >>> 0;
  if (!(($620 | 0) == (-1 | 0) | $629 ^ 1 | $620 >>> 0 < $621 >>> 0 & (($620 | 0) != (-1 | 0) & ($621 | 0) != (-1 | 0)) ^ 1)) {
   $$723947$i = $629 ? $627 : $$4236$i;
   $$748$i = $620;
   label = 180;
  }
 }
 if ((label | 0) == 180) {
  $633 = (HEAP32[110] | 0) + $$723947$i | 0;
  HEAP32[110] = $633;
  if ($633 >>> 0 > (HEAP32[111] | 0) >>> 0) HEAP32[111] = $633;
  $636 = HEAP32[8] | 0;
  do if (!$636) {
   $638 = HEAP32[6] | 0;
   if (($638 | 0) == 0 | $$748$i >>> 0 < $638 >>> 0) HEAP32[6] = $$748$i;
   HEAP32[114] = $$748$i;
   HEAP32[115] = $$723947$i;
   HEAP32[117] = 0;
   HEAP32[11] = HEAP32[120];
   HEAP32[10] = -1;
   HEAP32[15] = 48;
   HEAP32[14] = 48;
   HEAP32[17] = 56;
   HEAP32[16] = 56;
   HEAP32[19] = 64;
   HEAP32[18] = 64;
   HEAP32[21] = 72;
   HEAP32[20] = 72;
   HEAP32[23] = 80;
   HEAP32[22] = 80;
   HEAP32[25] = 88;
   HEAP32[24] = 88;
   HEAP32[27] = 96;
   HEAP32[26] = 96;
   HEAP32[29] = 104;
   HEAP32[28] = 104;
   HEAP32[31] = 112;
   HEAP32[30] = 112;
   HEAP32[33] = 120;
   HEAP32[32] = 120;
   HEAP32[35] = 128;
   HEAP32[34] = 128;
   HEAP32[37] = 136;
   HEAP32[36] = 136;
   HEAP32[39] = 144;
   HEAP32[38] = 144;
   HEAP32[41] = 152;
   HEAP32[40] = 152;
   HEAP32[43] = 160;
   HEAP32[42] = 160;
   HEAP32[45] = 168;
   HEAP32[44] = 168;
   HEAP32[47] = 176;
   HEAP32[46] = 176;
   HEAP32[49] = 184;
   HEAP32[48] = 184;
   HEAP32[51] = 192;
   HEAP32[50] = 192;
   HEAP32[53] = 200;
   HEAP32[52] = 200;
   HEAP32[55] = 208;
   HEAP32[54] = 208;
   HEAP32[57] = 216;
   HEAP32[56] = 216;
   HEAP32[59] = 224;
   HEAP32[58] = 224;
   HEAP32[61] = 232;
   HEAP32[60] = 232;
   HEAP32[63] = 240;
   HEAP32[62] = 240;
   HEAP32[65] = 248;
   HEAP32[64] = 248;
   HEAP32[67] = 256;
   HEAP32[66] = 256;
   HEAP32[69] = 264;
   HEAP32[68] = 264;
   HEAP32[71] = 272;
   HEAP32[70] = 272;
   HEAP32[73] = 280;
   HEAP32[72] = 280;
   HEAP32[75] = 288;
   HEAP32[74] = 288;
   HEAP32[77] = 296;
   HEAP32[76] = 296;
   $642 = $$723947$i + -40 | 0;
   $644 = $$748$i + 8 | 0;
   $649 = ($644 & 7 | 0) == 0 ? 0 : 0 - $644 & 7;
   $650 = $$748$i + $649 | 0;
   $651 = $642 - $649 | 0;
   HEAP32[8] = $650;
   HEAP32[5] = $651;
   HEAP32[$650 + 4 >> 2] = $651 | 1;
   HEAP32[$$748$i + $642 + 4 >> 2] = 40;
   HEAP32[9] = HEAP32[124];
  } else {
   $$024367$i = 456;
   while (1) {
    $657 = HEAP32[$$024367$i >> 2] | 0;
    $658 = $$024367$i + 4 | 0;
    $659 = HEAP32[$658 >> 2] | 0;
    if (($$748$i | 0) == ($657 + $659 | 0)) {
     label = 188;
     break;
    }
    $663 = HEAP32[$$024367$i + 8 >> 2] | 0;
    if (!$663) break; else $$024367$i = $663;
   }
   if ((label | 0) == 188) if (!(HEAP32[$$024367$i + 12 >> 2] & 8)) if ($$748$i >>> 0 > $636 >>> 0 & $657 >>> 0 <= $636 >>> 0) {
    HEAP32[$658 >> 2] = $659 + $$723947$i;
    $673 = (HEAP32[5] | 0) + $$723947$i | 0;
    $675 = $636 + 8 | 0;
    $680 = ($675 & 7 | 0) == 0 ? 0 : 0 - $675 & 7;
    $681 = $636 + $680 | 0;
    $682 = $673 - $680 | 0;
    HEAP32[8] = $681;
    HEAP32[5] = $682;
    HEAP32[$681 + 4 >> 2] = $682 | 1;
    HEAP32[$636 + $673 + 4 >> 2] = 40;
    HEAP32[9] = HEAP32[124];
    break;
   }
   $688 = HEAP32[6] | 0;
   if ($$748$i >>> 0 < $688 >>> 0) {
    HEAP32[6] = $$748$i;
    $753 = $$748$i;
   } else $753 = $688;
   $690 = $$748$i + $$723947$i | 0;
   $$124466$i = 456;
   while (1) {
    if ((HEAP32[$$124466$i >> 2] | 0) == ($690 | 0)) {
     label = 196;
     break;
    }
    $694 = HEAP32[$$124466$i + 8 >> 2] | 0;
    if (!$694) {
     $$0$i$i$i = 456;
     break;
    } else $$124466$i = $694;
   }
   if ((label | 0) == 196) if (!(HEAP32[$$124466$i + 12 >> 2] & 8)) {
    HEAP32[$$124466$i >> 2] = $$748$i;
    $700 = $$124466$i + 4 | 0;
    HEAP32[$700 >> 2] = (HEAP32[$700 >> 2] | 0) + $$723947$i;
    $704 = $$748$i + 8 | 0;
    $710 = $$748$i + (($704 & 7 | 0) == 0 ? 0 : 0 - $704 & 7) | 0;
    $712 = $690 + 8 | 0;
    $718 = $690 + (($712 & 7 | 0) == 0 ? 0 : 0 - $712 & 7) | 0;
    $722 = $710 + $$0197 | 0;
    $723 = $718 - $710 - $$0197 | 0;
    HEAP32[$710 + 4 >> 2] = $$0197 | 3;
    do if (($636 | 0) == ($718 | 0)) {
     $728 = (HEAP32[5] | 0) + $723 | 0;
     HEAP32[5] = $728;
     HEAP32[8] = $722;
     HEAP32[$722 + 4 >> 2] = $728 | 1;
    } else {
     if ((HEAP32[7] | 0) == ($718 | 0)) {
      $734 = (HEAP32[4] | 0) + $723 | 0;
      HEAP32[4] = $734;
      HEAP32[7] = $722;
      HEAP32[$722 + 4 >> 2] = $734 | 1;
      HEAP32[$722 + $734 >> 2] = $734;
      break;
     }
     $739 = HEAP32[$718 + 4 >> 2] | 0;
     if (($739 & 3 | 0) == 1) {
      $742 = $739 & -8;
      $743 = $739 >>> 3;
      L311 : do if ($739 >>> 0 < 256) {
       $746 = HEAP32[$718 + 8 >> 2] | 0;
       $748 = HEAP32[$718 + 12 >> 2] | 0;
       $750 = 48 + ($743 << 1 << 2) | 0;
       do if (($746 | 0) != ($750 | 0)) {
        if ($753 >>> 0 > $746 >>> 0) _abort();
        if ((HEAP32[$746 + 12 >> 2] | 0) == ($718 | 0)) break;
        _abort();
       } while (0);
       if (($748 | 0) == ($746 | 0)) {
        HEAP32[2] = HEAP32[2] & ~(1 << $743);
        break;
       }
       do if (($748 | 0) == ($750 | 0)) $$pre$phi11$i$iZ2D = $748 + 8 | 0; else {
        if ($753 >>> 0 > $748 >>> 0) _abort();
        $764 = $748 + 8 | 0;
        if ((HEAP32[$764 >> 2] | 0) == ($718 | 0)) {
         $$pre$phi11$i$iZ2D = $764;
         break;
        }
        _abort();
       } while (0);
       HEAP32[$746 + 12 >> 2] = $748;
       HEAP32[$$pre$phi11$i$iZ2D >> 2] = $746;
      } else {
       $769 = HEAP32[$718 + 24 >> 2] | 0;
       $771 = HEAP32[$718 + 12 >> 2] | 0;
       do if (($771 | 0) == ($718 | 0)) {
        $782 = $718 + 16 | 0;
        $783 = $782 + 4 | 0;
        $784 = HEAP32[$783 >> 2] | 0;
        if (!$784) {
         $786 = HEAP32[$782 >> 2] | 0;
         if (!$786) {
          $$3$i$i = 0;
          break;
         } else {
          $$1291$i$i = $786;
          $$1293$i$i = $782;
         }
        } else {
         $$1291$i$i = $784;
         $$1293$i$i = $783;
        }
        while (1) {
         $788 = $$1291$i$i + 20 | 0;
         $789 = HEAP32[$788 >> 2] | 0;
         if ($789 | 0) {
          $$1291$i$i = $789;
          $$1293$i$i = $788;
          continue;
         }
         $791 = $$1291$i$i + 16 | 0;
         $792 = HEAP32[$791 >> 2] | 0;
         if (!$792) break; else {
          $$1291$i$i = $792;
          $$1293$i$i = $791;
         }
        }
        if ($753 >>> 0 > $$1293$i$i >>> 0) _abort(); else {
         HEAP32[$$1293$i$i >> 2] = 0;
         $$3$i$i = $$1291$i$i;
         break;
        }
       } else {
        $774 = HEAP32[$718 + 8 >> 2] | 0;
        if ($753 >>> 0 > $774 >>> 0) _abort();
        $776 = $774 + 12 | 0;
        if ((HEAP32[$776 >> 2] | 0) != ($718 | 0)) _abort();
        $779 = $771 + 8 | 0;
        if ((HEAP32[$779 >> 2] | 0) == ($718 | 0)) {
         HEAP32[$776 >> 2] = $771;
         HEAP32[$779 >> 2] = $774;
         $$3$i$i = $771;
         break;
        } else _abort();
       } while (0);
       if (!$769) break;
       $797 = HEAP32[$718 + 28 >> 2] | 0;
       $798 = 312 + ($797 << 2) | 0;
       do if ((HEAP32[$798 >> 2] | 0) == ($718 | 0)) {
        HEAP32[$798 >> 2] = $$3$i$i;
        if ($$3$i$i | 0) break;
        HEAP32[3] = HEAP32[3] & ~(1 << $797);
        break L311;
       } else if ((HEAP32[6] | 0) >>> 0 > $769 >>> 0) _abort(); else {
        HEAP32[$769 + 16 + (((HEAP32[$769 + 16 >> 2] | 0) != ($718 | 0) & 1) << 2) >> 2] = $$3$i$i;
        if (!$$3$i$i) break L311; else break;
       } while (0);
       $812 = HEAP32[6] | 0;
       if ($812 >>> 0 > $$3$i$i >>> 0) _abort();
       HEAP32[$$3$i$i + 24 >> 2] = $769;
       $815 = $718 + 16 | 0;
       $816 = HEAP32[$815 >> 2] | 0;
       do if ($816 | 0) if ($812 >>> 0 > $816 >>> 0) _abort(); else {
        HEAP32[$$3$i$i + 16 >> 2] = $816;
        HEAP32[$816 + 24 >> 2] = $$3$i$i;
        break;
       } while (0);
       $822 = HEAP32[$815 + 4 >> 2] | 0;
       if (!$822) break;
       if ((HEAP32[6] | 0) >>> 0 > $822 >>> 0) _abort(); else {
        HEAP32[$$3$i$i + 20 >> 2] = $822;
        HEAP32[$822 + 24 >> 2] = $$3$i$i;
        break;
       }
      } while (0);
      $$0$i17$i = $718 + $742 | 0;
      $$0287$i$i = $742 + $723 | 0;
     } else {
      $$0$i17$i = $718;
      $$0287$i$i = $723;
     }
     $830 = $$0$i17$i + 4 | 0;
     HEAP32[$830 >> 2] = HEAP32[$830 >> 2] & -2;
     HEAP32[$722 + 4 >> 2] = $$0287$i$i | 1;
     HEAP32[$722 + $$0287$i$i >> 2] = $$0287$i$i;
     $836 = $$0287$i$i >>> 3;
     if ($$0287$i$i >>> 0 < 256) {
      $839 = 48 + ($836 << 1 << 2) | 0;
      $840 = HEAP32[2] | 0;
      $841 = 1 << $836;
      do if (!($840 & $841)) {
       HEAP32[2] = $840 | $841;
       $$0295$i$i = $839;
       $$pre$phi$i19$iZ2D = $839 + 8 | 0;
      } else {
       $845 = $839 + 8 | 0;
       $846 = HEAP32[$845 >> 2] | 0;
       if ((HEAP32[6] | 0) >>> 0 <= $846 >>> 0) {
        $$0295$i$i = $846;
        $$pre$phi$i19$iZ2D = $845;
        break;
       }
       _abort();
      } while (0);
      HEAP32[$$pre$phi$i19$iZ2D >> 2] = $722;
      HEAP32[$$0295$i$i + 12 >> 2] = $722;
      HEAP32[$722 + 8 >> 2] = $$0295$i$i;
      HEAP32[$722 + 12 >> 2] = $839;
      break;
     }
     $852 = $$0287$i$i >>> 8;
     do if (!$852) $$0296$i$i = 0; else {
      if ($$0287$i$i >>> 0 > 16777215) {
       $$0296$i$i = 31;
       break;
      }
      $857 = ($852 + 1048320 | 0) >>> 16 & 8;
      $858 = $852 << $857;
      $861 = ($858 + 520192 | 0) >>> 16 & 4;
      $863 = $858 << $861;
      $866 = ($863 + 245760 | 0) >>> 16 & 2;
      $871 = 14 - ($861 | $857 | $866) + ($863 << $866 >>> 15) | 0;
      $$0296$i$i = $$0287$i$i >>> ($871 + 7 | 0) & 1 | $871 << 1;
     } while (0);
     $877 = 312 + ($$0296$i$i << 2) | 0;
     HEAP32[$722 + 28 >> 2] = $$0296$i$i;
     $879 = $722 + 16 | 0;
     HEAP32[$879 + 4 >> 2] = 0;
     HEAP32[$879 >> 2] = 0;
     $881 = HEAP32[3] | 0;
     $882 = 1 << $$0296$i$i;
     if (!($881 & $882)) {
      HEAP32[3] = $881 | $882;
      HEAP32[$877 >> 2] = $722;
      HEAP32[$722 + 24 >> 2] = $877;
      HEAP32[$722 + 12 >> 2] = $722;
      HEAP32[$722 + 8 >> 2] = $722;
      break;
     }
     $$0288$i$i = $$0287$i$i << (($$0296$i$i | 0) == 31 ? 0 : 25 - ($$0296$i$i >>> 1) | 0);
     $$0289$i$i = HEAP32[$877 >> 2] | 0;
     while (1) {
      if ((HEAP32[$$0289$i$i + 4 >> 2] & -8 | 0) == ($$0287$i$i | 0)) {
       label = 263;
       break;
      }
      $900 = $$0289$i$i + 16 + ($$0288$i$i >>> 31 << 2) | 0;
      $902 = HEAP32[$900 >> 2] | 0;
      if (!$902) {
       label = 260;
       break;
      } else {
       $$0288$i$i = $$0288$i$i << 1;
       $$0289$i$i = $902;
      }
     }
     if ((label | 0) == 260) if ((HEAP32[6] | 0) >>> 0 > $900 >>> 0) _abort(); else {
      HEAP32[$900 >> 2] = $722;
      HEAP32[$722 + 24 >> 2] = $$0289$i$i;
      HEAP32[$722 + 12 >> 2] = $722;
      HEAP32[$722 + 8 >> 2] = $722;
      break;
     } else if ((label | 0) == 263) {
      $909 = $$0289$i$i + 8 | 0;
      $910 = HEAP32[$909 >> 2] | 0;
      $911 = HEAP32[6] | 0;
      if ($911 >>> 0 <= $910 >>> 0 & $911 >>> 0 <= $$0289$i$i >>> 0) {
       HEAP32[$910 + 12 >> 2] = $722;
       HEAP32[$909 >> 2] = $722;
       HEAP32[$722 + 8 >> 2] = $910;
       HEAP32[$722 + 12 >> 2] = $$0289$i$i;
       HEAP32[$722 + 24 >> 2] = 0;
       break;
      } else _abort();
     }
    } while (0);
    $$0 = $710 + 8 | 0;
    STACKTOP = sp;
    return $$0 | 0;
   } else $$0$i$i$i = 456;
   while (1) {
    $919 = HEAP32[$$0$i$i$i >> 2] | 0;
    if ($919 >>> 0 <= $636 >>> 0) {
     $923 = $919 + (HEAP32[$$0$i$i$i + 4 >> 2] | 0) | 0;
     if ($923 >>> 0 > $636 >>> 0) break;
    }
    $$0$i$i$i = HEAP32[$$0$i$i$i + 8 >> 2] | 0;
   }
   $927 = $923 + -47 | 0;
   $929 = $927 + 8 | 0;
   $935 = $927 + (($929 & 7 | 0) == 0 ? 0 : 0 - $929 & 7) | 0;
   $936 = $636 + 16 | 0;
   $938 = $935 >>> 0 < $936 >>> 0 ? $636 : $935;
   $939 = $938 + 8 | 0;
   $941 = $$723947$i + -40 | 0;
   $943 = $$748$i + 8 | 0;
   $948 = ($943 & 7 | 0) == 0 ? 0 : 0 - $943 & 7;
   $949 = $$748$i + $948 | 0;
   $950 = $941 - $948 | 0;
   HEAP32[8] = $949;
   HEAP32[5] = $950;
   HEAP32[$949 + 4 >> 2] = $950 | 1;
   HEAP32[$$748$i + $941 + 4 >> 2] = 40;
   HEAP32[9] = HEAP32[124];
   $956 = $938 + 4 | 0;
   HEAP32[$956 >> 2] = 27;
   HEAP32[$939 >> 2] = HEAP32[114];
   HEAP32[$939 + 4 >> 2] = HEAP32[115];
   HEAP32[$939 + 8 >> 2] = HEAP32[116];
   HEAP32[$939 + 12 >> 2] = HEAP32[117];
   HEAP32[114] = $$748$i;
   HEAP32[115] = $$723947$i;
   HEAP32[117] = 0;
   HEAP32[116] = $939;
   $958 = $938 + 24 | 0;
   do {
    $958$looptemp = $958;
    $958 = $958 + 4 | 0;
    HEAP32[$958 >> 2] = 7;
   } while (($958$looptemp + 8 | 0) >>> 0 < $923 >>> 0);
   if (($938 | 0) != ($636 | 0)) {
    $964 = $938 - $636 | 0;
    HEAP32[$956 >> 2] = HEAP32[$956 >> 2] & -2;
    HEAP32[$636 + 4 >> 2] = $964 | 1;
    HEAP32[$938 >> 2] = $964;
    $969 = $964 >>> 3;
    if ($964 >>> 0 < 256) {
     $972 = 48 + ($969 << 1 << 2) | 0;
     $973 = HEAP32[2] | 0;
     $974 = 1 << $969;
     if (!($973 & $974)) {
      HEAP32[2] = $973 | $974;
      $$0211$i$i = $972;
      $$pre$phi$i$iZ2D = $972 + 8 | 0;
     } else {
      $978 = $972 + 8 | 0;
      $979 = HEAP32[$978 >> 2] | 0;
      if ((HEAP32[6] | 0) >>> 0 > $979 >>> 0) _abort(); else {
       $$0211$i$i = $979;
       $$pre$phi$i$iZ2D = $978;
      }
     }
     HEAP32[$$pre$phi$i$iZ2D >> 2] = $636;
     HEAP32[$$0211$i$i + 12 >> 2] = $636;
     HEAP32[$636 + 8 >> 2] = $$0211$i$i;
     HEAP32[$636 + 12 >> 2] = $972;
     break;
    }
    $985 = $964 >>> 8;
    if (!$985) $$0212$i$i = 0; else if ($964 >>> 0 > 16777215) $$0212$i$i = 31; else {
     $990 = ($985 + 1048320 | 0) >>> 16 & 8;
     $991 = $985 << $990;
     $994 = ($991 + 520192 | 0) >>> 16 & 4;
     $996 = $991 << $994;
     $999 = ($996 + 245760 | 0) >>> 16 & 2;
     $1004 = 14 - ($994 | $990 | $999) + ($996 << $999 >>> 15) | 0;
     $$0212$i$i = $964 >>> ($1004 + 7 | 0) & 1 | $1004 << 1;
    }
    $1010 = 312 + ($$0212$i$i << 2) | 0;
    HEAP32[$636 + 28 >> 2] = $$0212$i$i;
    HEAP32[$636 + 20 >> 2] = 0;
    HEAP32[$936 >> 2] = 0;
    $1013 = HEAP32[3] | 0;
    $1014 = 1 << $$0212$i$i;
    if (!($1013 & $1014)) {
     HEAP32[3] = $1013 | $1014;
     HEAP32[$1010 >> 2] = $636;
     HEAP32[$636 + 24 >> 2] = $1010;
     HEAP32[$636 + 12 >> 2] = $636;
     HEAP32[$636 + 8 >> 2] = $636;
     break;
    }
    $$0206$i$i = $964 << (($$0212$i$i | 0) == 31 ? 0 : 25 - ($$0212$i$i >>> 1) | 0);
    $$0207$i$i = HEAP32[$1010 >> 2] | 0;
    while (1) {
     if ((HEAP32[$$0207$i$i + 4 >> 2] & -8 | 0) == ($964 | 0)) {
      label = 289;
      break;
     }
     $1032 = $$0207$i$i + 16 + ($$0206$i$i >>> 31 << 2) | 0;
     $1034 = HEAP32[$1032 >> 2] | 0;
     if (!$1034) {
      label = 286;
      break;
     } else {
      $$0206$i$i = $$0206$i$i << 1;
      $$0207$i$i = $1034;
     }
    }
    if ((label | 0) == 286) if ((HEAP32[6] | 0) >>> 0 > $1032 >>> 0) _abort(); else {
     HEAP32[$1032 >> 2] = $636;
     HEAP32[$636 + 24 >> 2] = $$0207$i$i;
     HEAP32[$636 + 12 >> 2] = $636;
     HEAP32[$636 + 8 >> 2] = $636;
     break;
    } else if ((label | 0) == 289) {
     $1041 = $$0207$i$i + 8 | 0;
     $1042 = HEAP32[$1041 >> 2] | 0;
     $1043 = HEAP32[6] | 0;
     if ($1043 >>> 0 <= $1042 >>> 0 & $1043 >>> 0 <= $$0207$i$i >>> 0) {
      HEAP32[$1042 + 12 >> 2] = $636;
      HEAP32[$1041 >> 2] = $636;
      HEAP32[$636 + 8 >> 2] = $1042;
      HEAP32[$636 + 12 >> 2] = $$0207$i$i;
      HEAP32[$636 + 24 >> 2] = 0;
      break;
     } else _abort();
    }
   }
  } while (0);
  $1052 = HEAP32[5] | 0;
  if ($1052 >>> 0 > $$0197 >>> 0) {
   $1054 = $1052 - $$0197 | 0;
   HEAP32[5] = $1054;
   $1055 = HEAP32[8] | 0;
   $1056 = $1055 + $$0197 | 0;
   HEAP32[8] = $1056;
   HEAP32[$1056 + 4 >> 2] = $1054 | 1;
   HEAP32[$1055 + 4 >> 2] = $$0197 | 3;
   $$0 = $1055 + 8 | 0;
   STACKTOP = sp;
   return $$0 | 0;
  }
 }
 HEAP32[(___errno_location() | 0) >> 2] = 12;
 $$0 = 0;
 STACKTOP = sp;
 return $$0 | 0;
}

function _free($0) {
 $0 = $0 | 0;
 var $$0212$i = 0, $$0212$in$i = 0, $$0383 = 0, $$0384 = 0, $$0396 = 0, $$0403 = 0, $$1 = 0, $$1382 = 0, $$1387 = 0, $$1390 = 0, $$1398 = 0, $$1402 = 0, $$2 = 0, $$3 = 0, $$3400 = 0, $$pre$phi442Z2D = 0, $$pre$phi444Z2D = 0, $$pre$phiZ2D = 0, $10 = 0, $105 = 0, $106 = 0, $114 = 0, $115 = 0, $116 = 0, $124 = 0, $13 = 0, $132 = 0, $137 = 0, $138 = 0, $141 = 0, $143 = 0, $145 = 0, $16 = 0, $160 = 0, $165 = 0, $167 = 0, $17 = 0, $170 = 0, $173 = 0, $176 = 0, $179 = 0, $180 = 0, $181 = 0, $183 = 0, $185 = 0, $186 = 0, $188 = 0, $189 = 0, $195 = 0, $196 = 0, $2 = 0, $21 = 0, $210 = 0, $213 = 0, $214 = 0, $220 = 0, $235 = 0, $238 = 0, $239 = 0, $24 = 0, $240 = 0, $244 = 0, $245 = 0, $251 = 0, $256 = 0, $257 = 0, $26 = 0, $260 = 0, $262 = 0, $265 = 0, $270 = 0, $276 = 0, $28 = 0, $280 = 0, $281 = 0, $299 = 0, $3 = 0, $301 = 0, $308 = 0, $309 = 0, $310 = 0, $319 = 0, $41 = 0, $46 = 0, $48 = 0, $51 = 0, $53 = 0, $56 = 0, $59 = 0, $6 = 0, $60 = 0, $61 = 0, $63 = 0, $65 = 0, $66 = 0, $68 = 0, $69 = 0, $7 = 0, $74 = 0, $75 = 0, $89 = 0, $9 = 0, $92 = 0, $93 = 0, $99 = 0, label = 0;
 if (!$0) return;
 $2 = $0 + -8 | 0;
 $3 = HEAP32[6] | 0;
 if ($2 >>> 0 < $3 >>> 0) _abort();
 $6 = HEAP32[$0 + -4 >> 2] | 0;
 $7 = $6 & 3;
 if (($7 | 0) == 1) _abort();
 $9 = $6 & -8;
 $10 = $2 + $9 | 0;
 L10 : do if (!($6 & 1)) {
  $13 = HEAP32[$2 >> 2] | 0;
  if (!$7) return;
  $16 = $2 + (0 - $13) | 0;
  $17 = $13 + $9 | 0;
  if ($16 >>> 0 < $3 >>> 0) _abort();
  if ((HEAP32[7] | 0) == ($16 | 0)) {
   $105 = $10 + 4 | 0;
   $106 = HEAP32[$105 >> 2] | 0;
   if (($106 & 3 | 0) != 3) {
    $$1 = $16;
    $$1382 = $17;
    $114 = $16;
    break;
   }
   HEAP32[4] = $17;
   HEAP32[$105 >> 2] = $106 & -2;
   HEAP32[$16 + 4 >> 2] = $17 | 1;
   HEAP32[$16 + $17 >> 2] = $17;
   return;
  }
  $21 = $13 >>> 3;
  if ($13 >>> 0 < 256) {
   $24 = HEAP32[$16 + 8 >> 2] | 0;
   $26 = HEAP32[$16 + 12 >> 2] | 0;
   $28 = 48 + ($21 << 1 << 2) | 0;
   if (($24 | 0) != ($28 | 0)) {
    if ($3 >>> 0 > $24 >>> 0) _abort();
    if ((HEAP32[$24 + 12 >> 2] | 0) != ($16 | 0)) _abort();
   }
   if (($26 | 0) == ($24 | 0)) {
    HEAP32[2] = HEAP32[2] & ~(1 << $21);
    $$1 = $16;
    $$1382 = $17;
    $114 = $16;
    break;
   }
   if (($26 | 0) == ($28 | 0)) $$pre$phi444Z2D = $26 + 8 | 0; else {
    if ($3 >>> 0 > $26 >>> 0) _abort();
    $41 = $26 + 8 | 0;
    if ((HEAP32[$41 >> 2] | 0) == ($16 | 0)) $$pre$phi444Z2D = $41; else _abort();
   }
   HEAP32[$24 + 12 >> 2] = $26;
   HEAP32[$$pre$phi444Z2D >> 2] = $24;
   $$1 = $16;
   $$1382 = $17;
   $114 = $16;
   break;
  }
  $46 = HEAP32[$16 + 24 >> 2] | 0;
  $48 = HEAP32[$16 + 12 >> 2] | 0;
  do if (($48 | 0) == ($16 | 0)) {
   $59 = $16 + 16 | 0;
   $60 = $59 + 4 | 0;
   $61 = HEAP32[$60 >> 2] | 0;
   if (!$61) {
    $63 = HEAP32[$59 >> 2] | 0;
    if (!$63) {
     $$3 = 0;
     break;
    } else {
     $$1387 = $63;
     $$1390 = $59;
    }
   } else {
    $$1387 = $61;
    $$1390 = $60;
   }
   while (1) {
    $65 = $$1387 + 20 | 0;
    $66 = HEAP32[$65 >> 2] | 0;
    if ($66 | 0) {
     $$1387 = $66;
     $$1390 = $65;
     continue;
    }
    $68 = $$1387 + 16 | 0;
    $69 = HEAP32[$68 >> 2] | 0;
    if (!$69) break; else {
     $$1387 = $69;
     $$1390 = $68;
    }
   }
   if ($3 >>> 0 > $$1390 >>> 0) _abort(); else {
    HEAP32[$$1390 >> 2] = 0;
    $$3 = $$1387;
    break;
   }
  } else {
   $51 = HEAP32[$16 + 8 >> 2] | 0;
   if ($3 >>> 0 > $51 >>> 0) _abort();
   $53 = $51 + 12 | 0;
   if ((HEAP32[$53 >> 2] | 0) != ($16 | 0)) _abort();
   $56 = $48 + 8 | 0;
   if ((HEAP32[$56 >> 2] | 0) == ($16 | 0)) {
    HEAP32[$53 >> 2] = $48;
    HEAP32[$56 >> 2] = $51;
    $$3 = $48;
    break;
   } else _abort();
  } while (0);
  if (!$46) {
   $$1 = $16;
   $$1382 = $17;
   $114 = $16;
  } else {
   $74 = HEAP32[$16 + 28 >> 2] | 0;
   $75 = 312 + ($74 << 2) | 0;
   do if ((HEAP32[$75 >> 2] | 0) == ($16 | 0)) {
    HEAP32[$75 >> 2] = $$3;
    if (!$$3) {
     HEAP32[3] = HEAP32[3] & ~(1 << $74);
     $$1 = $16;
     $$1382 = $17;
     $114 = $16;
     break L10;
    }
   } else if ((HEAP32[6] | 0) >>> 0 > $46 >>> 0) _abort(); else {
    HEAP32[$46 + 16 + (((HEAP32[$46 + 16 >> 2] | 0) != ($16 | 0) & 1) << 2) >> 2] = $$3;
    if (!$$3) {
     $$1 = $16;
     $$1382 = $17;
     $114 = $16;
     break L10;
    } else break;
   } while (0);
   $89 = HEAP32[6] | 0;
   if ($89 >>> 0 > $$3 >>> 0) _abort();
   HEAP32[$$3 + 24 >> 2] = $46;
   $92 = $16 + 16 | 0;
   $93 = HEAP32[$92 >> 2] | 0;
   do if ($93 | 0) if ($89 >>> 0 > $93 >>> 0) _abort(); else {
    HEAP32[$$3 + 16 >> 2] = $93;
    HEAP32[$93 + 24 >> 2] = $$3;
    break;
   } while (0);
   $99 = HEAP32[$92 + 4 >> 2] | 0;
   if (!$99) {
    $$1 = $16;
    $$1382 = $17;
    $114 = $16;
   } else if ((HEAP32[6] | 0) >>> 0 > $99 >>> 0) _abort(); else {
    HEAP32[$$3 + 20 >> 2] = $99;
    HEAP32[$99 + 24 >> 2] = $$3;
    $$1 = $16;
    $$1382 = $17;
    $114 = $16;
    break;
   }
  }
 } else {
  $$1 = $2;
  $$1382 = $9;
  $114 = $2;
 } while (0);
 if ($114 >>> 0 >= $10 >>> 0) _abort();
 $115 = $10 + 4 | 0;
 $116 = HEAP32[$115 >> 2] | 0;
 if (!($116 & 1)) _abort();
 if (!($116 & 2)) {
  if ((HEAP32[8] | 0) == ($10 | 0)) {
   $124 = (HEAP32[5] | 0) + $$1382 | 0;
   HEAP32[5] = $124;
   HEAP32[8] = $$1;
   HEAP32[$$1 + 4 >> 2] = $124 | 1;
   if (($$1 | 0) != (HEAP32[7] | 0)) return;
   HEAP32[7] = 0;
   HEAP32[4] = 0;
   return;
  }
  if ((HEAP32[7] | 0) == ($10 | 0)) {
   $132 = (HEAP32[4] | 0) + $$1382 | 0;
   HEAP32[4] = $132;
   HEAP32[7] = $114;
   HEAP32[$$1 + 4 >> 2] = $132 | 1;
   HEAP32[$114 + $132 >> 2] = $132;
   return;
  }
  $137 = ($116 & -8) + $$1382 | 0;
  $138 = $116 >>> 3;
  L108 : do if ($116 >>> 0 < 256) {
   $141 = HEAP32[$10 + 8 >> 2] | 0;
   $143 = HEAP32[$10 + 12 >> 2] | 0;
   $145 = 48 + ($138 << 1 << 2) | 0;
   if (($141 | 0) != ($145 | 0)) {
    if ((HEAP32[6] | 0) >>> 0 > $141 >>> 0) _abort();
    if ((HEAP32[$141 + 12 >> 2] | 0) != ($10 | 0)) _abort();
   }
   if (($143 | 0) == ($141 | 0)) {
    HEAP32[2] = HEAP32[2] & ~(1 << $138);
    break;
   }
   if (($143 | 0) == ($145 | 0)) $$pre$phi442Z2D = $143 + 8 | 0; else {
    if ((HEAP32[6] | 0) >>> 0 > $143 >>> 0) _abort();
    $160 = $143 + 8 | 0;
    if ((HEAP32[$160 >> 2] | 0) == ($10 | 0)) $$pre$phi442Z2D = $160; else _abort();
   }
   HEAP32[$141 + 12 >> 2] = $143;
   HEAP32[$$pre$phi442Z2D >> 2] = $141;
  } else {
   $165 = HEAP32[$10 + 24 >> 2] | 0;
   $167 = HEAP32[$10 + 12 >> 2] | 0;
   do if (($167 | 0) == ($10 | 0)) {
    $179 = $10 + 16 | 0;
    $180 = $179 + 4 | 0;
    $181 = HEAP32[$180 >> 2] | 0;
    if (!$181) {
     $183 = HEAP32[$179 >> 2] | 0;
     if (!$183) {
      $$3400 = 0;
      break;
     } else {
      $$1398 = $183;
      $$1402 = $179;
     }
    } else {
     $$1398 = $181;
     $$1402 = $180;
    }
    while (1) {
     $185 = $$1398 + 20 | 0;
     $186 = HEAP32[$185 >> 2] | 0;
     if ($186 | 0) {
      $$1398 = $186;
      $$1402 = $185;
      continue;
     }
     $188 = $$1398 + 16 | 0;
     $189 = HEAP32[$188 >> 2] | 0;
     if (!$189) break; else {
      $$1398 = $189;
      $$1402 = $188;
     }
    }
    if ((HEAP32[6] | 0) >>> 0 > $$1402 >>> 0) _abort(); else {
     HEAP32[$$1402 >> 2] = 0;
     $$3400 = $$1398;
     break;
    }
   } else {
    $170 = HEAP32[$10 + 8 >> 2] | 0;
    if ((HEAP32[6] | 0) >>> 0 > $170 >>> 0) _abort();
    $173 = $170 + 12 | 0;
    if ((HEAP32[$173 >> 2] | 0) != ($10 | 0)) _abort();
    $176 = $167 + 8 | 0;
    if ((HEAP32[$176 >> 2] | 0) == ($10 | 0)) {
     HEAP32[$173 >> 2] = $167;
     HEAP32[$176 >> 2] = $170;
     $$3400 = $167;
     break;
    } else _abort();
   } while (0);
   if ($165 | 0) {
    $195 = HEAP32[$10 + 28 >> 2] | 0;
    $196 = 312 + ($195 << 2) | 0;
    do if ((HEAP32[$196 >> 2] | 0) == ($10 | 0)) {
     HEAP32[$196 >> 2] = $$3400;
     if (!$$3400) {
      HEAP32[3] = HEAP32[3] & ~(1 << $195);
      break L108;
     }
    } else if ((HEAP32[6] | 0) >>> 0 > $165 >>> 0) _abort(); else {
     HEAP32[$165 + 16 + (((HEAP32[$165 + 16 >> 2] | 0) != ($10 | 0) & 1) << 2) >> 2] = $$3400;
     if (!$$3400) break L108; else break;
    } while (0);
    $210 = HEAP32[6] | 0;
    if ($210 >>> 0 > $$3400 >>> 0) _abort();
    HEAP32[$$3400 + 24 >> 2] = $165;
    $213 = $10 + 16 | 0;
    $214 = HEAP32[$213 >> 2] | 0;
    do if ($214 | 0) if ($210 >>> 0 > $214 >>> 0) _abort(); else {
     HEAP32[$$3400 + 16 >> 2] = $214;
     HEAP32[$214 + 24 >> 2] = $$3400;
     break;
    } while (0);
    $220 = HEAP32[$213 + 4 >> 2] | 0;
    if ($220 | 0) if ((HEAP32[6] | 0) >>> 0 > $220 >>> 0) _abort(); else {
     HEAP32[$$3400 + 20 >> 2] = $220;
     HEAP32[$220 + 24 >> 2] = $$3400;
     break;
    }
   }
  } while (0);
  HEAP32[$$1 + 4 >> 2] = $137 | 1;
  HEAP32[$114 + $137 >> 2] = $137;
  if (($$1 | 0) == (HEAP32[7] | 0)) {
   HEAP32[4] = $137;
   return;
  } else $$2 = $137;
 } else {
  HEAP32[$115 >> 2] = $116 & -2;
  HEAP32[$$1 + 4 >> 2] = $$1382 | 1;
  HEAP32[$114 + $$1382 >> 2] = $$1382;
  $$2 = $$1382;
 }
 $235 = $$2 >>> 3;
 if ($$2 >>> 0 < 256) {
  $238 = 48 + ($235 << 1 << 2) | 0;
  $239 = HEAP32[2] | 0;
  $240 = 1 << $235;
  if (!($239 & $240)) {
   HEAP32[2] = $239 | $240;
   $$0403 = $238;
   $$pre$phiZ2D = $238 + 8 | 0;
  } else {
   $244 = $238 + 8 | 0;
   $245 = HEAP32[$244 >> 2] | 0;
   if ((HEAP32[6] | 0) >>> 0 > $245 >>> 0) _abort(); else {
    $$0403 = $245;
    $$pre$phiZ2D = $244;
   }
  }
  HEAP32[$$pre$phiZ2D >> 2] = $$1;
  HEAP32[$$0403 + 12 >> 2] = $$1;
  HEAP32[$$1 + 8 >> 2] = $$0403;
  HEAP32[$$1 + 12 >> 2] = $238;
  return;
 }
 $251 = $$2 >>> 8;
 if (!$251) $$0396 = 0; else if ($$2 >>> 0 > 16777215) $$0396 = 31; else {
  $256 = ($251 + 1048320 | 0) >>> 16 & 8;
  $257 = $251 << $256;
  $260 = ($257 + 520192 | 0) >>> 16 & 4;
  $262 = $257 << $260;
  $265 = ($262 + 245760 | 0) >>> 16 & 2;
  $270 = 14 - ($260 | $256 | $265) + ($262 << $265 >>> 15) | 0;
  $$0396 = $$2 >>> ($270 + 7 | 0) & 1 | $270 << 1;
 }
 $276 = 312 + ($$0396 << 2) | 0;
 HEAP32[$$1 + 28 >> 2] = $$0396;
 HEAP32[$$1 + 20 >> 2] = 0;
 HEAP32[$$1 + 16 >> 2] = 0;
 $280 = HEAP32[3] | 0;
 $281 = 1 << $$0396;
 do if (!($280 & $281)) {
  HEAP32[3] = $280 | $281;
  HEAP32[$276 >> 2] = $$1;
  HEAP32[$$1 + 24 >> 2] = $276;
  HEAP32[$$1 + 12 >> 2] = $$1;
  HEAP32[$$1 + 8 >> 2] = $$1;
 } else {
  $$0383 = $$2 << (($$0396 | 0) == 31 ? 0 : 25 - ($$0396 >>> 1) | 0);
  $$0384 = HEAP32[$276 >> 2] | 0;
  while (1) {
   if ((HEAP32[$$0384 + 4 >> 2] & -8 | 0) == ($$2 | 0)) {
    label = 124;
    break;
   }
   $299 = $$0384 + 16 + ($$0383 >>> 31 << 2) | 0;
   $301 = HEAP32[$299 >> 2] | 0;
   if (!$301) {
    label = 121;
    break;
   } else {
    $$0383 = $$0383 << 1;
    $$0384 = $301;
   }
  }
  if ((label | 0) == 121) if ((HEAP32[6] | 0) >>> 0 > $299 >>> 0) _abort(); else {
   HEAP32[$299 >> 2] = $$1;
   HEAP32[$$1 + 24 >> 2] = $$0384;
   HEAP32[$$1 + 12 >> 2] = $$1;
   HEAP32[$$1 + 8 >> 2] = $$1;
   break;
  } else if ((label | 0) == 124) {
   $308 = $$0384 + 8 | 0;
   $309 = HEAP32[$308 >> 2] | 0;
   $310 = HEAP32[6] | 0;
   if ($310 >>> 0 <= $309 >>> 0 & $310 >>> 0 <= $$0384 >>> 0) {
    HEAP32[$309 + 12 >> 2] = $$1;
    HEAP32[$308 >> 2] = $$1;
    HEAP32[$$1 + 8 >> 2] = $309;
    HEAP32[$$1 + 12 >> 2] = $$0384;
    HEAP32[$$1 + 24 >> 2] = 0;
    break;
   } else _abort();
  }
 } while (0);
 $319 = (HEAP32[10] | 0) + -1 | 0;
 HEAP32[10] = $319;
 if (!$319) $$0212$in$i = 464; else return;
 while (1) {
  $$0212$i = HEAP32[$$0212$in$i >> 2] | 0;
  if (!$$0212$i) break; else $$0212$in$i = $$0212$i + 8 | 0;
 }
 HEAP32[10] = -1;
 return;
}

function _glmc_mat4_mulN($0, $1, $2) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = $2 | 0;
 var $$010$i = 0, $10 = 0.0, $106 = 0, $113 = 0.0, $114 = 0, $12 = 0.0, $121 = 0.0, $122 = 0, $129 = 0.0, $130 = 0, $137 = 0.0, $138 = 0, $14 = 0.0, $145 = 0.0, $146 = 0, $153 = 0.0, $154 = 0, $16 = 0.0, $161 = 0.0, $162 = 0, $169 = 0.0, $170 = 0, $177 = 0.0, $178 = 0, $18 = 0.0, $185 = 0.0, $186 = 0, $193 = 0.0, $194 = 0, $197 = 0, $198 = 0.0, $199 = 0.0, $20 = 0.0, $200 = 0.0, $201 = 0.0, $202 = 0.0, $204 = 0.0, $206 = 0.0, $208 = 0.0, $210 = 0.0, $212 = 0.0, $214 = 0.0, $216 = 0.0, $218 = 0.0, $22 = 0.0, $220 = 0.0, $222 = 0.0, $224 = 0.0, $226 = 0.0, $228 = 0.0, $230 = 0.0, $232 = 0.0, $235 = 0.0, $238 = 0.0, $24 = 0.0, $241 = 0.0, $245 = 0.0, $248 = 0.0, $251 = 0.0, $255 = 0.0, $258 = 0.0, $26 = 0.0, $261 = 0.0, $265 = 0.0, $268 = 0.0, $271 = 0.0, $28 = 0.0, $3 = 0, $30 = 0.0, $32 = 0.0, $34 = 0.0, $36 = 0.0, $37 = 0.0, $39 = 0.0, $41 = 0.0, $43 = 0.0, $45 = 0.0, $47 = 0.0, $49 = 0.0, $5 = 0, $51 = 0.0, $53 = 0.0, $55 = 0.0, $57 = 0.0, $59 = 0.0, $6 = 0.0, $61 = 0.0, $63 = 0.0, $65 = 0.0, $67 = 0.0, $8 = 0.0, $82 = 0, $90 = 0, $98 = 0, $235$looptemp = 0.0, $238$looptemp = 0.0, $245$looptemp = 0.0, $248$looptemp = 0.0, $255$looptemp = 0.0, $258$looptemp = 0.0, $265$looptemp = 0.0, $268$looptemp = 0.0;
 $3 = HEAP32[$0 >> 2] | 0;
 $5 = HEAP32[$0 + 4 >> 2] | 0;
 $6 = +HEAPF32[$3 >> 2];
 $8 = +HEAPF32[$3 + 4 >> 2];
 $10 = +HEAPF32[$3 + 8 >> 2];
 $12 = +HEAPF32[$3 + 12 >> 2];
 $14 = +HEAPF32[$3 + 16 >> 2];
 $16 = +HEAPF32[$3 + 20 >> 2];
 $18 = +HEAPF32[$3 + 24 >> 2];
 $20 = +HEAPF32[$3 + 28 >> 2];
 $22 = +HEAPF32[$3 + 32 >> 2];
 $24 = +HEAPF32[$3 + 36 >> 2];
 $26 = +HEAPF32[$3 + 40 >> 2];
 $28 = +HEAPF32[$3 + 44 >> 2];
 $30 = +HEAPF32[$3 + 48 >> 2];
 $32 = +HEAPF32[$3 + 52 >> 2];
 $34 = +HEAPF32[$3 + 56 >> 2];
 $36 = +HEAPF32[$3 + 60 >> 2];
 $37 = +HEAPF32[$5 >> 2];
 $39 = +HEAPF32[$5 + 4 >> 2];
 $41 = +HEAPF32[$5 + 8 >> 2];
 $43 = +HEAPF32[$5 + 12 >> 2];
 $45 = +HEAPF32[$5 + 16 >> 2];
 $47 = +HEAPF32[$5 + 20 >> 2];
 $49 = +HEAPF32[$5 + 24 >> 2];
 $51 = +HEAPF32[$5 + 28 >> 2];
 $53 = +HEAPF32[$5 + 32 >> 2];
 $55 = +HEAPF32[$5 + 36 >> 2];
 $57 = +HEAPF32[$5 + 40 >> 2];
 $59 = +HEAPF32[$5 + 44 >> 2];
 $61 = +HEAPF32[$5 + 48 >> 2];
 $63 = +HEAPF32[$5 + 52 >> 2];
 $65 = +HEAPF32[$5 + 56 >> 2];
 $67 = +HEAPF32[$5 + 60 >> 2];
 HEAPF32[$2 >> 2] = $6 * $37 + $14 * $39 + $22 * $41 + $30 * $43;
 $82 = $2 + 4 | 0;
 HEAPF32[$82 >> 2] = $8 * $37 + $16 * $39 + $24 * $41 + $32 * $43;
 $90 = $2 + 8 | 0;
 HEAPF32[$90 >> 2] = $10 * $37 + $18 * $39 + $26 * $41 + $34 * $43;
 $98 = $2 + 12 | 0;
 HEAPF32[$98 >> 2] = $12 * $37 + $20 * $39 + $28 * $41 + $36 * $43;
 $106 = $2 + 16 | 0;
 HEAPF32[$106 >> 2] = $6 * $45 + $14 * $47 + $22 * $49 + $30 * $51;
 $113 = $8 * $45 + $16 * $47 + $24 * $49 + $32 * $51;
 $114 = $2 + 20 | 0;
 HEAPF32[$114 >> 2] = $113;
 $121 = $10 * $45 + $18 * $47 + $26 * $49 + $34 * $51;
 $122 = $2 + 24 | 0;
 HEAPF32[$122 >> 2] = $121;
 $129 = $12 * $45 + $20 * $47 + $28 * $49 + $36 * $51;
 $130 = $2 + 28 | 0;
 HEAPF32[$130 >> 2] = $129;
 $137 = $6 * $53 + $14 * $55 + $22 * $57 + $30 * $59;
 $138 = $2 + 32 | 0;
 HEAPF32[$138 >> 2] = $137;
 $145 = $8 * $53 + $16 * $55 + $24 * $57 + $32 * $59;
 $146 = $2 + 36 | 0;
 HEAPF32[$146 >> 2] = $145;
 $153 = $10 * $53 + $18 * $55 + $26 * $57 + $34 * $59;
 $154 = $2 + 40 | 0;
 HEAPF32[$154 >> 2] = $153;
 $161 = $12 * $53 + $20 * $55 + $28 * $57 + $36 * $59;
 $162 = $2 + 44 | 0;
 HEAPF32[$162 >> 2] = $161;
 $169 = $6 * $61 + $14 * $63 + $22 * $65 + $30 * $67;
 $170 = $2 + 48 | 0;
 HEAPF32[$170 >> 2] = $169;
 $177 = $8 * $61 + $16 * $63 + $24 * $65 + $32 * $67;
 $178 = $2 + 52 | 0;
 HEAPF32[$178 >> 2] = $177;
 $185 = $10 * $61 + $18 * $63 + $26 * $65 + $34 * $67;
 $186 = $2 + 56 | 0;
 HEAPF32[$186 >> 2] = $185;
 $193 = $12 * $61 + $20 * $63 + $28 * $65 + $36 * $67;
 $194 = $2 + 60 | 0;
 HEAPF32[$194 >> 2] = $193;
 if ($1 >>> 0 <= 2) return;
 $$010$i = 2;
 $235 = +HEAPF32[$106 >> 2];
 $238 = $137;
 $241 = $169;
 $245 = $113;
 $248 = $145;
 $251 = $177;
 $255 = $121;
 $258 = $153;
 $261 = $185;
 $265 = $129;
 $268 = $161;
 $271 = $193;
 do {
  $197 = HEAP32[$0 + ($$010$i << 2) >> 2] | 0;
  $198 = +HEAPF32[$2 >> 2];
  $199 = +HEAPF32[$82 >> 2];
  $200 = +HEAPF32[$90 >> 2];
  $201 = +HEAPF32[$98 >> 2];
  $202 = +HEAPF32[$197 >> 2];
  $204 = +HEAPF32[$197 + 4 >> 2];
  $206 = +HEAPF32[$197 + 8 >> 2];
  $208 = +HEAPF32[$197 + 12 >> 2];
  $210 = +HEAPF32[$197 + 16 >> 2];
  $212 = +HEAPF32[$197 + 20 >> 2];
  $214 = +HEAPF32[$197 + 24 >> 2];
  $216 = +HEAPF32[$197 + 28 >> 2];
  $218 = +HEAPF32[$197 + 32 >> 2];
  $220 = +HEAPF32[$197 + 36 >> 2];
  $222 = +HEAPF32[$197 + 40 >> 2];
  $224 = +HEAPF32[$197 + 44 >> 2];
  $226 = +HEAPF32[$197 + 48 >> 2];
  $228 = +HEAPF32[$197 + 52 >> 2];
  $230 = +HEAPF32[$197 + 56 >> 2];
  $232 = +HEAPF32[$197 + 60 >> 2];
  HEAPF32[$2 >> 2] = $198 * $202 + $235 * $204 + $238 * $206 + $241 * $208;
  HEAPF32[$82 >> 2] = $199 * $202 + $245 * $204 + $248 * $206 + $251 * $208;
  HEAPF32[$90 >> 2] = $200 * $202 + $255 * $204 + $258 * $206 + $261 * $208;
  HEAPF32[$98 >> 2] = $201 * $202 + $265 * $204 + $268 * $206 + $271 * $208;
  $235$looptemp = $235;
  $235 = $198 * $210 + $235 * $212 + $238 * $214 + $241 * $216;
  HEAPF32[$106 >> 2] = $235;
  $245$looptemp = $245;
  $245 = $199 * $210 + $245 * $212 + $248 * $214 + $251 * $216;
  HEAPF32[$114 >> 2] = $245;
  $255$looptemp = $255;
  $255 = $200 * $210 + $255 * $212 + $258 * $214 + $261 * $216;
  HEAPF32[$122 >> 2] = $255;
  $265$looptemp = $265;
  $265 = $201 * $210 + $265 * $212 + $268 * $214 + $271 * $216;
  HEAPF32[$130 >> 2] = $265;
  $238$looptemp = $238;
  $238 = $198 * $218 + $235$looptemp * $220 + $238 * $222 + $241 * $224;
  HEAPF32[$138 >> 2] = $238;
  $248$looptemp = $248;
  $248 = $199 * $218 + $245$looptemp * $220 + $248 * $222 + $251 * $224;
  HEAPF32[$146 >> 2] = $248;
  $258$looptemp = $258;
  $258 = $200 * $218 + $255$looptemp * $220 + $258 * $222 + $261 * $224;
  HEAPF32[$154 >> 2] = $258;
  $268$looptemp = $268;
  $268 = $201 * $218 + $265$looptemp * $220 + $268 * $222 + $271 * $224;
  HEAPF32[$162 >> 2] = $268;
  $241 = $198 * $226 + $235$looptemp * $228 + $238$looptemp * $230 + $241 * $232;
  HEAPF32[$170 >> 2] = $241;
  $251 = $199 * $226 + $245$looptemp * $228 + $248$looptemp * $230 + $251 * $232;
  HEAPF32[$178 >> 2] = $251;
  $261 = $200 * $226 + $255$looptemp * $228 + $258$looptemp * $230 + $261 * $232;
  HEAPF32[$186 >> 2] = $261;
  $271 = $201 * $226 + $265$looptemp * $228 + $268$looptemp * $230 + $271 * $232;
  HEAPF32[$194 >> 2] = $271;
  $$010$i = $$010$i + 1 | 0;
 } while (($$010$i | 0) != ($1 | 0));
 return;
}

function _glmc_frustum_corners_at($0, $1, $2, $3) {
 $0 = $0 | 0;
 $1 = +$1;
 $2 = +$2;
 $3 = $3 | 0;
 var $$idx$i = 0, $$idx28$i = 0, $$idx30$i = 0, $$idx31$i = 0, $$sroa$0$0$i = 0.0, $$sroa$0$1$i = 0.0, $$sroa$0$2$i = 0.0, $$sroa$0$3$i = 0.0, $$sroa$22$0$i = 0.0, $$sroa$22$1$i = 0.0, $$sroa$22$2$i = 0.0, $$sroa$22$3$i = 0.0, $$sroa$42$0$i = 0.0, $$sroa$42$1$i = 0.0, $$sroa$42$2$i = 0.0, $$sroa$42$3$i = 0.0, $$sroa$62$0$i = 0.0, $$sroa$62$1$i = 0.0, $$sroa$62$2$i = 0.0, $$sroa$62$3$i = 0.0, $106 = 0.0, $107 = 0.0, $11 = 0.0, $110 = 0.0, $113 = 0.0, $116 = 0, $118 = 0.0, $126 = 0.0, $128 = 0.0, $147 = 0.0, $148 = 0.0, $151 = 0, $153 = 0.0, $156 = 0, $158 = 0.0, $16 = 0.0, $161 = 0, $163 = 0.0, $171 = 0.0, $173 = 0.0, $178 = 0, $179 = 0.0, $180 = 0.0, $181 = 0.0, $182 = 0, $183 = 0.0, $184 = 0.0, $185 = 0, $186 = 0.0, $187 = 0.0, $188 = 0, $19 = 0.0, $20 = 0.0, $23 = 0, $25 = 0.0, $28 = 0, $30 = 0.0, $33 = 0, $35 = 0.0, $4 = 0, $43 = 0.0, $45 = 0.0, $5 = 0, $6 = 0.0, $63 = 0.0, $64 = 0.0, $67 = 0, $69 = 0.0, $72 = 0, $74 = 0.0, $77 = 0, $79 = 0.0, $8 = 0.0, $87 = 0.0, $89 = 0.0;
 $4 = $0 + 96 | 0;
 $5 = $0 + 32 | 0;
 $$idx$i = $0 + 100 | 0;
 $$idx28$i = $0 + 104 | 0;
 $$idx30$i = $0 + 36 | 0;
 $$idx31$i = $0 + 40 | 0;
 $6 = +HEAPF32[$5 >> 2] - +HEAPF32[$4 >> 2];
 $8 = +HEAPF32[$$idx30$i >> 2] - +HEAPF32[$$idx$i >> 2];
 $11 = +HEAPF32[$$idx31$i >> 2] - +HEAPF32[$$idx28$i >> 2];
 $16 = $1 / $2 * +Math_sqrt(+($6 * $6 + $8 * $8 + $11 * $11));
 $19 = +HEAPF32[$0 >> 2];
 $20 = +HEAPF32[$0 + 64 >> 2] - $19;
 $23 = $0 + 4 | 0;
 $25 = +HEAPF32[$0 + 68 >> 2] - +HEAPF32[$23 >> 2];
 $28 = $0 + 8 | 0;
 $30 = +HEAPF32[$0 + 72 >> 2] - +HEAPF32[$28 >> 2];
 $33 = $0 + 12 | 0;
 $35 = +HEAPF32[$0 + 76 >> 2] - +HEAPF32[$33 >> 2];
 $43 = +Math_sqrt(+($20 * $20 + $25 * $25 + $30 * $30 + $35 * $35));
 if ($43 == 0.0) {
  $$sroa$0$0$i = 0.0;
  $$sroa$22$0$i = 0.0;
  $$sroa$42$0$i = 0.0;
  $$sroa$62$0$i = 0.0;
 } else {
  $45 = $16 / $43;
  $$sroa$0$0$i = $20 * $45;
  $$sroa$22$0$i = $25 * $45;
  $$sroa$42$0$i = $30 * $45;
  $$sroa$62$0$i = $35 * $45;
 }
 HEAPF32[$3 >> 2] = $19 + $$sroa$0$0$i;
 HEAPF32[$3 + 4 >> 2] = $$sroa$22$0$i + +HEAPF32[$23 >> 2];
 HEAPF32[$3 + 8 >> 2] = $$sroa$42$0$i + +HEAPF32[$28 >> 2];
 HEAPF32[$3 + 12 >> 2] = $$sroa$62$0$i + +HEAPF32[$33 >> 2];
 $63 = +HEAPF32[$0 + 16 >> 2];
 $64 = +HEAPF32[$0 + 80 >> 2] - $63;
 $67 = $0 + 20 | 0;
 $69 = +HEAPF32[$0 + 84 >> 2] - +HEAPF32[$67 >> 2];
 $72 = $0 + 24 | 0;
 $74 = +HEAPF32[$0 + 88 >> 2] - +HEAPF32[$72 >> 2];
 $77 = $0 + 28 | 0;
 $79 = +HEAPF32[$0 + 92 >> 2] - +HEAPF32[$77 >> 2];
 $87 = +Math_sqrt(+($64 * $64 + $69 * $69 + $74 * $74 + $79 * $79));
 if ($87 == 0.0) {
  $$sroa$0$1$i = 0.0;
  $$sroa$22$1$i = 0.0;
  $$sroa$42$1$i = 0.0;
  $$sroa$62$1$i = 0.0;
 } else {
  $89 = $16 / $87;
  $$sroa$0$1$i = $64 * $89;
  $$sroa$22$1$i = $69 * $89;
  $$sroa$42$1$i = $74 * $89;
  $$sroa$62$1$i = $79 * $89;
 }
 HEAPF32[$3 + 16 >> 2] = $63 + $$sroa$0$1$i;
 HEAPF32[$3 + 20 >> 2] = $$sroa$22$1$i + +HEAPF32[$67 >> 2];
 HEAPF32[$3 + 24 >> 2] = $$sroa$42$1$i + +HEAPF32[$72 >> 2];
 HEAPF32[$3 + 28 >> 2] = $$sroa$62$1$i + +HEAPF32[$77 >> 2];
 $106 = +HEAPF32[$5 >> 2];
 $107 = +HEAPF32[$4 >> 2] - $106;
 $110 = +HEAPF32[$$idx$i >> 2] - +HEAPF32[$$idx30$i >> 2];
 $113 = +HEAPF32[$$idx28$i >> 2] - +HEAPF32[$$idx31$i >> 2];
 $116 = $0 + 44 | 0;
 $118 = +HEAPF32[$0 + 108 >> 2] - +HEAPF32[$116 >> 2];
 $126 = +Math_sqrt(+($107 * $107 + $110 * $110 + $113 * $113 + $118 * $118));
 if ($126 == 0.0) {
  $$sroa$0$2$i = 0.0;
  $$sroa$22$2$i = 0.0;
  $$sroa$42$2$i = 0.0;
  $$sroa$62$2$i = 0.0;
 } else {
  $128 = $16 / $126;
  $$sroa$0$2$i = $107 * $128;
  $$sroa$22$2$i = $110 * $128;
  $$sroa$42$2$i = $113 * $128;
  $$sroa$62$2$i = $118 * $128;
 }
 HEAPF32[$3 + 32 >> 2] = $106 + $$sroa$0$2$i;
 HEAPF32[$3 + 36 >> 2] = $$sroa$22$2$i + +HEAPF32[$$idx30$i >> 2];
 HEAPF32[$3 + 40 >> 2] = $$sroa$42$2$i + +HEAPF32[$$idx31$i >> 2];
 HEAPF32[$3 + 44 >> 2] = $$sroa$62$2$i + +HEAPF32[$116 >> 2];
 $147 = +HEAPF32[$0 + 48 >> 2];
 $148 = +HEAPF32[$0 + 112 >> 2] - $147;
 $151 = $0 + 52 | 0;
 $153 = +HEAPF32[$0 + 116 >> 2] - +HEAPF32[$151 >> 2];
 $156 = $0 + 56 | 0;
 $158 = +HEAPF32[$0 + 120 >> 2] - +HEAPF32[$156 >> 2];
 $161 = $0 + 60 | 0;
 $163 = +HEAPF32[$0 + 124 >> 2] - +HEAPF32[$161 >> 2];
 $171 = +Math_sqrt(+($148 * $148 + $153 * $153 + $158 * $158 + $163 * $163));
 if ($171 == 0.0) {
  $$sroa$0$3$i = 0.0;
  $$sroa$22$3$i = 0.0;
  $$sroa$42$3$i = 0.0;
  $$sroa$62$3$i = 0.0;
  $178 = $3 + 48 | 0;
  $179 = $147 + $$sroa$0$3$i;
  HEAPF32[$178 >> 2] = $179;
  $180 = +HEAPF32[$151 >> 2];
  $181 = $$sroa$22$3$i + $180;
  $182 = $3 + 52 | 0;
  HEAPF32[$182 >> 2] = $181;
  $183 = +HEAPF32[$156 >> 2];
  $184 = $$sroa$42$3$i + $183;
  $185 = $3 + 56 | 0;
  HEAPF32[$185 >> 2] = $184;
  $186 = +HEAPF32[$161 >> 2];
  $187 = $$sroa$62$3$i + $186;
  $188 = $3 + 60 | 0;
  HEAPF32[$188 >> 2] = $187;
  return;
 }
 $173 = $16 / $171;
 $$sroa$0$3$i = $148 * $173;
 $$sroa$22$3$i = $153 * $173;
 $$sroa$42$3$i = $158 * $173;
 $$sroa$62$3$i = $163 * $173;
 $178 = $3 + 48 | 0;
 $179 = $147 + $$sroa$0$3$i;
 HEAPF32[$178 >> 2] = $179;
 $180 = +HEAPF32[$151 >> 2];
 $181 = $$sroa$22$3$i + $180;
 $182 = $3 + 52 | 0;
 HEAPF32[$182 >> 2] = $181;
 $183 = +HEAPF32[$156 >> 2];
 $184 = $$sroa$42$3$i + $183;
 $185 = $3 + 56 | 0;
 HEAPF32[$185 >> 2] = $184;
 $186 = +HEAPF32[$161 >> 2];
 $187 = $$sroa$62$3$i + $186;
 $188 = $3 + 60 | 0;
 HEAPF32[$188 >> 2] = $187;
 return;
}

function _glmc_rotate_at($0, $1, $2, $3) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = +$2;
 $3 = $3 | 0;
 var $$idx$val = 0.0, $$idx12$val$i = 0.0, $$idx13$val$i = 0.0, $$idx4$val = 0.0, $$sink$i$i$i$i = 0.0, $$sroa$011$0$i$i$i = 0.0, $$sroa$9$0$i$i$i = 0.0, $$val = 0.0, $$val11$i = 0.0, $100 = 0.0, $101 = 0.0, $102 = 0.0, $107 = 0.0, $112 = 0.0, $117 = 0.0, $12 = 0, $122 = 0.0, $127 = 0.0, $132 = 0.0, $137 = 0.0, $142 = 0.0, $147 = 0.0, $15 = 0, $152 = 0.0, $157 = 0.0, $16 = 0.0, $162 = 0.0, $18 = 0, $184 = 0.0, $185 = 0.0, $186 = 0.0, $19 = 0.0, $21 = 0, $22 = 0.0, $24 = 0, $25 = 0.0, $27 = 0, $28 = 0.0, $30 = 0, $31 = 0.0, $33 = 0, $34 = 0.0, $36 = 0, $37 = 0.0, $39 = 0, $4 = 0.0, $40 = 0.0, $42 = 0, $45 = 0, $48 = 0, $5 = 0.0, $51 = 0, $55 = 0.0, $56 = 0.0, $57 = 0.0, $6 = 0.0, $62 = 0.0, $68 = 0.0, $69 = 0, $70 = 0.0, $74 = 0.0, $75 = 0.0, $76 = 0.0, $77 = 0.0, $78 = 0.0, $79 = 0.0, $80 = 0.0, $81 = 0.0, $9 = 0, $91 = 0.0, $92 = 0.0, $93 = 0.0, $94 = 0.0, $95 = 0.0, $96 = 0.0, $97 = 0.0, $98 = 0.0, $99 = 0.0;
 $$val = +HEAPF32[$1 >> 2];
 $$idx$val = +HEAPF32[$1 + 4 >> 2];
 $$idx4$val = +HEAPF32[$1 + 8 >> 2];
 $4 = -$$val;
 $5 = -$$idx$val;
 $6 = -$$idx4$val;
 $9 = $0 + 4 | 0;
 $12 = $0 + 8 | 0;
 $15 = $0 + 12 | 0;
 $16 = +HEAPF32[$15 >> 2];
 $18 = $0 + 16 | 0;
 $19 = +HEAPF32[$18 >> 2];
 $21 = $0 + 20 | 0;
 $22 = +HEAPF32[$21 >> 2];
 $24 = $0 + 24 | 0;
 $25 = +HEAPF32[$24 >> 2];
 $27 = $0 + 28 | 0;
 $28 = +HEAPF32[$27 >> 2];
 $30 = $0 + 32 | 0;
 $31 = +HEAPF32[$30 >> 2];
 $33 = $0 + 36 | 0;
 $34 = +HEAPF32[$33 >> 2];
 $36 = $0 + 40 | 0;
 $37 = +HEAPF32[$36 >> 2];
 $39 = $0 + 44 | 0;
 $40 = +HEAPF32[$39 >> 2];
 $42 = $0 + 48 | 0;
 $45 = $0 + 52 | 0;
 $48 = $0 + 56 | 0;
 $51 = $0 + 60 | 0;
 $55 = $$idx$val * $22 + ($$val * +HEAPF32[$9 >> 2] + +HEAPF32[$45 >> 2]);
 $56 = $$idx$val * $25 + ($$val * +HEAPF32[$12 >> 2] + +HEAPF32[$48 >> 2]);
 $57 = $$idx$val * $28 + ($$val * $16 + +HEAPF32[$51 >> 2]);
 HEAPF32[$42 >> 2] = $$idx4$val * $31 + ($$idx$val * $19 + ($$val * +HEAPF32[$0 >> 2] + +HEAPF32[$42 >> 2]));
 HEAPF32[$45 >> 2] = $$idx4$val * $34 + $55;
 HEAPF32[$48 >> 2] = $$idx4$val * $37 + $56;
 HEAPF32[$51 >> 2] = $$idx4$val * $40 + $57;
 $$val11$i = +HEAPF32[$3 >> 2];
 $$idx12$val$i = +HEAPF32[$3 + 4 >> 2];
 $$idx13$val$i = +HEAPF32[$3 + 8 >> 2];
 $62 = +Math_cos(+$2);
 $68 = +Math_sqrt(+($$val11$i * $$val11$i + $$idx12$val$i * $$idx12$val$i + $$idx13$val$i * $$idx13$val$i));
 $69 = $68 == 0.0;
 $70 = 1.0 / $68;
 $$sroa$011$0$i$i$i = $69 ? 0.0 : $$val11$i * $70;
 $$sroa$9$0$i$i$i = $69 ? 0.0 : $$idx12$val$i * $70;
 $$sink$i$i$i$i = $69 ? 0.0 : $$idx13$val$i * $70;
 $74 = 1.0 - $62;
 $75 = $74 * $$sroa$011$0$i$i$i;
 $76 = $74 * $$sroa$9$0$i$i$i;
 $77 = $74 * $$sink$i$i$i$i;
 $78 = +Math_sin(+$2);
 $79 = $78 * $$sroa$011$0$i$i$i;
 $80 = $78 * $$sroa$9$0$i$i$i;
 $81 = $78 * $$sink$i$i$i$i;
 $91 = $62 + $$sroa$011$0$i$i$i * $75;
 $92 = $$sroa$011$0$i$i$i * $76 - $81;
 $93 = $80 + $$sroa$011$0$i$i$i * $77;
 $94 = $81 + $$sroa$9$0$i$i$i * $75;
 $95 = $62 + $$sroa$9$0$i$i$i * $76;
 $96 = $$sroa$9$0$i$i$i * $77 - $79;
 $97 = $$sink$i$i$i$i * $75 - $80;
 $98 = $79 + $$sink$i$i$i$i * $76;
 $99 = $62 + $$sink$i$i$i$i * $77;
 $100 = +HEAPF32[$0 >> 2];
 $101 = +HEAPF32[$9 >> 2];
 $102 = +HEAPF32[$12 >> 2];
 $107 = $31 * $97 + ($100 * $91 + $19 * $94);
 HEAPF32[$0 >> 2] = $107;
 $112 = $34 * $97 + ($101 * $91 + $22 * $94);
 HEAPF32[$9 >> 2] = $112;
 $117 = $37 * $97 + ($102 * $91 + $25 * $94);
 HEAPF32[$12 >> 2] = $117;
 $122 = $40 * $97 + ($16 * $91 + $28 * $94);
 HEAPF32[$15 >> 2] = $122;
 $127 = $31 * $98 + ($100 * $92 + $19 * $95);
 HEAPF32[$18 >> 2] = $127;
 $132 = $34 * $98 + ($101 * $92 + $22 * $95);
 HEAPF32[$21 >> 2] = $132;
 $137 = $37 * $98 + ($102 * $92 + $25 * $95);
 HEAPF32[$24 >> 2] = $137;
 $142 = $40 * $98 + ($16 * $92 + $28 * $95);
 HEAPF32[$27 >> 2] = $142;
 $147 = $31 * $99 + ($100 * $93 + $19 * $96);
 HEAPF32[$30 >> 2] = $147;
 $152 = $34 * $99 + ($101 * $93 + $22 * $96);
 HEAPF32[$33 >> 2] = $152;
 $157 = $37 * $99 + ($102 * $93 + $25 * $96);
 HEAPF32[$36 >> 2] = $157;
 $162 = $40 * $99 + ($16 * $93 + $28 * $96);
 HEAPF32[$39 >> 2] = $162;
 $184 = $132 * $5 + ($112 * $4 + +HEAPF32[$45 >> 2]);
 $185 = $137 * $5 + ($117 * $4 + +HEAPF32[$48 >> 2]);
 $186 = $142 * $5 + ($122 * $4 + +HEAPF32[$51 >> 2]);
 HEAPF32[$42 >> 2] = $147 * $6 + ($127 * $5 + ($107 * $4 + +HEAPF32[$42 >> 2]));
 HEAPF32[$45 >> 2] = $152 * $6 + $184;
 HEAPF32[$48 >> 2] = $157 * $6 + $185;
 HEAPF32[$51 >> 2] = $162 * $6 + $186;
 return;
}

function _glmc_decompose($0, $1, $2, $3) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = $2 | 0;
 $3 = $3 | 0;
 var $$idx34$val$i$i = 0.0, $$idx35$val$i$i = 0.0, $$idx37$val$i$i = 0.0, $$idx38$val$i$i = 0.0, $$idx40$val$i$i = 0.0, $$idx41$val$i$i = 0.0, $$val33$i$i = 0.0, $$val36$i$i = 0.0, $$val39$i$i = 0.0, $101 = 0.0, $102 = 0.0, $103 = 0.0, $105 = 0.0, $106 = 0.0, $109 = 0.0, $111 = 0.0, $16 = 0, $18 = 0, $19 = 0, $21 = 0, $24 = 0, $25 = 0, $26 = 0, $28 = 0, $30 = 0, $31 = 0, $33 = 0, $36 = 0, $37 = 0, $38 = 0, $40 = 0, $42 = 0, $43 = 0, $45 = 0, $48 = 0, $58 = 0.0, $65 = 0, $72 = 0, $73 = 0.0, $75 = 0.0, $77 = 0.0, $79 = 0.0, $81 = 0.0, $83 = 0.0, $85 = 0.0, $87 = 0.0, $89 = 0.0, $91 = 0.0, $93 = 0.0, $95 = 0.0, $97 = 0.0, $99 = 0.0;
 HEAP32[$1 >> 2] = HEAP32[$0 + 48 >> 2];
 HEAP32[$1 + 4 >> 2] = HEAP32[$0 + 52 >> 2];
 HEAP32[$1 + 8 >> 2] = HEAP32[$0 + 56 >> 2];
 HEAP32[$1 + 12 >> 2] = HEAP32[$0 + 60 >> 2];
 HEAP32[$2 >> 2] = HEAP32[$0 >> 2];
 $16 = $0 + 4 | 0;
 $18 = $2 + 4 | 0;
 HEAP32[$18 >> 2] = HEAP32[$16 >> 2];
 $19 = $0 + 8 | 0;
 $21 = $2 + 8 | 0;
 HEAP32[$21 >> 2] = HEAP32[$19 >> 2];
 $24 = $2 + 12 | 0;
 HEAP32[$24 >> 2] = HEAP32[$0 + 12 >> 2];
 $25 = $0 + 16 | 0;
 $26 = $2 + 16 | 0;
 HEAP32[$26 >> 2] = HEAP32[$25 >> 2];
 $28 = $0 + 20 | 0;
 $30 = $2 + 20 | 0;
 HEAP32[$30 >> 2] = HEAP32[$28 >> 2];
 $31 = $0 + 24 | 0;
 $33 = $2 + 24 | 0;
 HEAP32[$33 >> 2] = HEAP32[$31 >> 2];
 $36 = $2 + 28 | 0;
 HEAP32[$36 >> 2] = HEAP32[$0 + 28 >> 2];
 $37 = $0 + 32 | 0;
 $38 = $2 + 32 | 0;
 HEAP32[$38 >> 2] = HEAP32[$37 >> 2];
 $40 = $0 + 36 | 0;
 $42 = $2 + 36 | 0;
 HEAP32[$42 >> 2] = HEAP32[$40 >> 2];
 $43 = $0 + 40 | 0;
 $45 = $2 + 40 | 0;
 HEAP32[$45 >> 2] = HEAP32[$43 >> 2];
 $48 = $2 + 44 | 0;
 HEAP32[$48 >> 2] = HEAP32[$0 + 44 >> 2];
 HEAP32[$2 + 48 >> 2] = 0;
 HEAP32[$2 + 52 >> 2] = 0;
 HEAP32[$2 + 56 >> 2] = 0;
 HEAP32[$2 + 60 >> 2] = 1065353216;
 $$val39$i$i = +HEAPF32[$0 >> 2];
 $$idx40$val$i$i = +HEAPF32[$16 >> 2];
 $$idx41$val$i$i = +HEAPF32[$19 >> 2];
 $58 = +Math_sqrt(+($$val39$i$i * $$val39$i$i + $$idx40$val$i$i * $$idx40$val$i$i + $$idx41$val$i$i * $$idx41$val$i$i));
 HEAPF32[$3 >> 2] = $58;
 $$val36$i$i = +HEAPF32[$25 >> 2];
 $$idx37$val$i$i = +HEAPF32[$28 >> 2];
 $$idx38$val$i$i = +HEAPF32[$31 >> 2];
 $65 = $3 + 4 | 0;
 HEAPF32[$65 >> 2] = +Math_sqrt(+($$val36$i$i * $$val36$i$i + $$idx37$val$i$i * $$idx37$val$i$i + $$idx38$val$i$i * $$idx38$val$i$i));
 $$val33$i$i = +HEAPF32[$37 >> 2];
 $$idx34$val$i$i = +HEAPF32[$40 >> 2];
 $$idx35$val$i$i = +HEAPF32[$43 >> 2];
 $72 = $3 + 8 | 0;
 HEAPF32[$72 >> 2] = +Math_sqrt(+($$val33$i$i * $$val33$i$i + $$idx34$val$i$i * $$idx34$val$i$i + $$idx35$val$i$i * $$idx35$val$i$i));
 $73 = 1.0 / $58;
 $75 = $73 * +HEAPF32[$2 >> 2];
 HEAPF32[$2 >> 2] = $75;
 $77 = $73 * +HEAPF32[$18 >> 2];
 HEAPF32[$18 >> 2] = $77;
 $79 = $73 * +HEAPF32[$21 >> 2];
 HEAPF32[$21 >> 2] = $79;
 $81 = $73 * +HEAPF32[$24 >> 2];
 HEAPF32[$24 >> 2] = $81;
 $83 = 1.0 / +HEAPF32[$65 >> 2];
 $85 = +HEAPF32[$26 >> 2] * $83;
 HEAPF32[$26 >> 2] = $85;
 $87 = $83 * +HEAPF32[$30 >> 2];
 HEAPF32[$30 >> 2] = $87;
 $89 = $83 * +HEAPF32[$33 >> 2];
 HEAPF32[$33 >> 2] = $89;
 $91 = $83 * +HEAPF32[$36 >> 2];
 HEAPF32[$36 >> 2] = $91;
 $93 = 1.0 / +HEAPF32[$72 >> 2];
 $95 = +HEAPF32[$38 >> 2] * $93;
 HEAPF32[$38 >> 2] = $95;
 $97 = $93 * +HEAPF32[$42 >> 2];
 HEAPF32[$42 >> 2] = $97;
 $99 = $93 * +HEAPF32[$45 >> 2];
 HEAPF32[$45 >> 2] = $99;
 $101 = $93 * +HEAPF32[$48 >> 2];
 HEAPF32[$48 >> 2] = $101;
 $102 = +HEAPF32[$16 >> 2];
 $103 = +HEAPF32[$31 >> 2];
 $105 = +HEAPF32[$19 >> 2];
 $106 = +HEAPF32[$28 >> 2];
 $109 = +HEAPF32[$25 >> 2];
 $111 = +HEAPF32[$0 >> 2];
 if (!(($102 * $103 - $105 * $106) * +HEAPF32[$37 >> 2] + ($105 * $109 - $103 * $111) * +HEAPF32[$40 >> 2] + ($106 * $111 - $102 * $109) * +HEAPF32[$43 >> 2] < 0.0)) return;
 HEAPF32[$2 >> 2] = -$75;
 HEAPF32[$18 >> 2] = -$77;
 HEAPF32[$21 >> 2] = -$79;
 HEAPF32[$24 >> 2] = -$81;
 HEAPF32[$26 >> 2] = -$85;
 HEAPF32[$30 >> 2] = -$87;
 HEAPF32[$33 >> 2] = -$89;
 HEAPF32[$36 >> 2] = -$91;
 HEAPF32[$38 >> 2] = -$95;
 HEAPF32[$42 >> 2] = -$97;
 HEAPF32[$45 >> 2] = -$99;
 HEAPF32[$48 >> 2] = -$101;
 HEAPF32[$3 >> 2] = -+HEAPF32[$3 >> 2];
 HEAPF32[$65 >> 2] = -+HEAPF32[$65 >> 2];
 HEAPF32[$72 >> 2] = -+HEAPF32[$72 >> 2];
 return;
}

function _glmc_quat_rotate_at($0, $1, $2) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = $2 | 0;
 var $$idx$val = 0.0, $$idx3$val = 0.0, $$val = 0.0, $100 = 0.0, $101 = 0.0, $102 = 0.0, $103 = 0.0, $108 = 0.0, $11 = 0, $113 = 0.0, $118 = 0.0, $12 = 0.0, $123 = 0.0, $128 = 0.0, $133 = 0.0, $138 = 0.0, $14 = 0, $143 = 0.0, $148 = 0.0, $15 = 0.0, $153 = 0.0, $158 = 0.0, $163 = 0.0, $17 = 0, $18 = 0.0, $20 = 0, $21 = 0.0, $23 = 0, $24 = 0.0, $26 = 0, $27 = 0.0, $29 = 0, $3 = 0.0, $30 = 0.0, $32 = 0, $33 = 0.0, $35 = 0, $36 = 0.0, $38 = 0, $39 = 0.0, $4 = 0.0, $41 = 0, $44 = 0, $47 = 0, $5 = 0.0, $50 = 0, $54 = 0.0, $55 = 0.0, $56 = 0.0, $57 = 0.0, $58 = 0.0, $59 = 0.0, $60 = 0.0, $61 = 0.0, $64 = 0.0, $68 = 0.0, $72 = 0.0, $75 = 0.0, $78 = 0.0, $79 = 0.0, $8 = 0, $81 = 0.0, $82 = 0.0, $83 = 0.0, $84 = 0.0, $85 = 0.0, $86 = 0.0, $87 = 0.0, $89 = 0.0, $9 = 0.0, $90 = 0.0, $91 = 0.0, $93 = 0.0, $94 = 0.0, $95 = 0.0, $96 = 0.0, $97 = 0.0, $98 = 0.0, $99 = 0.0;
 $$val = +HEAPF32[$2 >> 2];
 $$idx$val = +HEAPF32[$2 + 4 >> 2];
 $$idx3$val = +HEAPF32[$2 + 8 >> 2];
 $3 = -$$val;
 $4 = -$$idx$val;
 $5 = -$$idx3$val;
 $8 = $0 + 4 | 0;
 $9 = +HEAPF32[$8 >> 2];
 $11 = $0 + 8 | 0;
 $12 = +HEAPF32[$11 >> 2];
 $14 = $0 + 12 | 0;
 $15 = +HEAPF32[$14 >> 2];
 $17 = $0 + 16 | 0;
 $18 = +HEAPF32[$17 >> 2];
 $20 = $0 + 20 | 0;
 $21 = +HEAPF32[$20 >> 2];
 $23 = $0 + 24 | 0;
 $24 = +HEAPF32[$23 >> 2];
 $26 = $0 + 28 | 0;
 $27 = +HEAPF32[$26 >> 2];
 $29 = $0 + 32 | 0;
 $30 = +HEAPF32[$29 >> 2];
 $32 = $0 + 36 | 0;
 $33 = +HEAPF32[$32 >> 2];
 $35 = $0 + 40 | 0;
 $36 = +HEAPF32[$35 >> 2];
 $38 = $0 + 44 | 0;
 $39 = +HEAPF32[$38 >> 2];
 $41 = $0 + 48 | 0;
 $44 = $0 + 52 | 0;
 $47 = $0 + 56 | 0;
 $50 = $0 + 60 | 0;
 $54 = $$idx$val * $21 + ($$val * $9 + +HEAPF32[$44 >> 2]);
 $55 = $$idx$val * $24 + ($$val * $12 + +HEAPF32[$47 >> 2]);
 $56 = $$idx$val * $27 + ($$val * $15 + +HEAPF32[$50 >> 2]);
 $57 = $$idx3$val * $30 + ($$idx$val * $18 + ($$val * +HEAPF32[$0 >> 2] + +HEAPF32[$41 >> 2]));
 HEAPF32[$41 >> 2] = $57;
 $58 = $$idx3$val * $33 + $54;
 HEAPF32[$44 >> 2] = $58;
 $59 = $$idx3$val * $36 + $55;
 HEAPF32[$47 >> 2] = $59;
 $60 = $$idx3$val * $39 + $56;
 HEAPF32[$50 >> 2] = $60;
 $61 = +HEAPF32[$1 >> 2];
 $64 = +HEAPF32[$1 + 4 >> 2];
 $68 = +HEAPF32[$1 + 8 >> 2];
 $72 = +HEAPF32[$1 + 12 >> 2];
 $75 = +Math_sqrt(+($61 * $61 + $64 * $64 + $68 * $68 + $72 * $72));
 $78 = $75 > 0.0 ? 2.0 / $75 : 0.0;
 $79 = $61 * $78;
 $81 = $64 * $79;
 $82 = $72 * $78;
 $83 = $61 * $82;
 $84 = $64 * $78;
 $85 = $64 * $84;
 $86 = $68 * $84;
 $87 = $64 * $82;
 $89 = $68 * ($68 * $78);
 $90 = $68 * $79;
 $91 = $68 * $82;
 $93 = 1.0 - $85 - $89;
 $94 = 1.0 - $61 * $79;
 $95 = $94 - $89;
 $96 = $94 - $85;
 $97 = $81 + $91;
 $98 = $86 + $83;
 $99 = $90 + $87;
 $100 = $81 - $91;
 $101 = $86 - $83;
 $102 = $90 - $87;
 $103 = +HEAPF32[$0 >> 2];
 $108 = $30 * $102 + ($18 * $97 + $103 * $93);
 HEAPF32[$0 >> 2] = $108;
 $113 = $33 * $102 + ($21 * $97 + $9 * $93);
 HEAPF32[$8 >> 2] = $113;
 $118 = $36 * $102 + ($24 * $97 + $12 * $93);
 HEAPF32[$11 >> 2] = $118;
 $123 = $39 * $102 + ($27 * $97 + $15 * $93);
 HEAPF32[$14 >> 2] = $123;
 $128 = $30 * $98 + ($103 * $100 + $18 * $95);
 HEAPF32[$17 >> 2] = $128;
 $133 = $33 * $98 + ($9 * $100 + $21 * $95);
 HEAPF32[$20 >> 2] = $133;
 $138 = $36 * $98 + ($12 * $100 + $24 * $95);
 HEAPF32[$23 >> 2] = $138;
 $143 = $39 * $98 + ($15 * $100 + $27 * $95);
 HEAPF32[$26 >> 2] = $143;
 $148 = $103 * $99 + $18 * $101 + $30 * $96;
 HEAPF32[$29 >> 2] = $148;
 $153 = $9 * $99 + $21 * $101 + $33 * $96;
 HEAPF32[$32 >> 2] = $153;
 $158 = $12 * $99 + $24 * $101 + $36 * $96;
 HEAPF32[$35 >> 2] = $158;
 $163 = $15 * $99 + $27 * $101 + $39 * $96;
 HEAPF32[$38 >> 2] = $163;
 HEAPF32[$41 >> 2] = $148 * $5 + ($128 * $4 + ($57 + $108 * $3));
 HEAPF32[$44 >> 2] = $153 * $5 + ($133 * $4 + ($58 + $113 * $3));
 HEAPF32[$47 >> 2] = $158 * $5 + ($138 * $4 + ($59 + $118 * $3));
 HEAPF32[$50 >> 2] = $163 * $5 + ($143 * $4 + ($60 + $123 * $3));
 return;
}

function _glmc_decompose_rs($0, $1, $2) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = $2 | 0;
 var $$idx34$val$i = 0.0, $$idx35$val$i = 0.0, $$idx37$val$i = 0.0, $$idx38$val$i = 0.0, $$idx40$val$i = 0.0, $$idx41$val$i = 0.0, $$val33$i = 0.0, $$val36$i = 0.0, $$val39$i = 0.0, $12 = 0, $13 = 0, $14 = 0, $16 = 0, $18 = 0, $19 = 0, $21 = 0, $24 = 0, $25 = 0, $26 = 0, $28 = 0, $30 = 0, $31 = 0, $33 = 0, $36 = 0, $4 = 0, $46 = 0.0, $53 = 0, $6 = 0, $60 = 0, $61 = 0.0, $63 = 0.0, $65 = 0.0, $67 = 0.0, $69 = 0.0, $7 = 0, $71 = 0.0, $73 = 0.0, $75 = 0.0, $77 = 0.0, $79 = 0.0, $81 = 0.0, $83 = 0.0, $85 = 0.0, $87 = 0.0, $89 = 0.0, $9 = 0, $90 = 0.0, $91 = 0.0, $93 = 0.0, $94 = 0.0, $97 = 0.0, $99 = 0.0;
 HEAP32[$1 >> 2] = HEAP32[$0 >> 2];
 $4 = $0 + 4 | 0;
 $6 = $1 + 4 | 0;
 HEAP32[$6 >> 2] = HEAP32[$4 >> 2];
 $7 = $0 + 8 | 0;
 $9 = $1 + 8 | 0;
 HEAP32[$9 >> 2] = HEAP32[$7 >> 2];
 $12 = $1 + 12 | 0;
 HEAP32[$12 >> 2] = HEAP32[$0 + 12 >> 2];
 $13 = $0 + 16 | 0;
 $14 = $1 + 16 | 0;
 HEAP32[$14 >> 2] = HEAP32[$13 >> 2];
 $16 = $0 + 20 | 0;
 $18 = $1 + 20 | 0;
 HEAP32[$18 >> 2] = HEAP32[$16 >> 2];
 $19 = $0 + 24 | 0;
 $21 = $1 + 24 | 0;
 HEAP32[$21 >> 2] = HEAP32[$19 >> 2];
 $24 = $1 + 28 | 0;
 HEAP32[$24 >> 2] = HEAP32[$0 + 28 >> 2];
 $25 = $0 + 32 | 0;
 $26 = $1 + 32 | 0;
 HEAP32[$26 >> 2] = HEAP32[$25 >> 2];
 $28 = $0 + 36 | 0;
 $30 = $1 + 36 | 0;
 HEAP32[$30 >> 2] = HEAP32[$28 >> 2];
 $31 = $0 + 40 | 0;
 $33 = $1 + 40 | 0;
 HEAP32[$33 >> 2] = HEAP32[$31 >> 2];
 $36 = $1 + 44 | 0;
 HEAP32[$36 >> 2] = HEAP32[$0 + 44 >> 2];
 HEAP32[$1 + 48 >> 2] = 0;
 HEAP32[$1 + 52 >> 2] = 0;
 HEAP32[$1 + 56 >> 2] = 0;
 HEAP32[$1 + 60 >> 2] = 1065353216;
 $$val39$i = +HEAPF32[$0 >> 2];
 $$idx40$val$i = +HEAPF32[$4 >> 2];
 $$idx41$val$i = +HEAPF32[$7 >> 2];
 $46 = +Math_sqrt(+($$val39$i * $$val39$i + $$idx40$val$i * $$idx40$val$i + $$idx41$val$i * $$idx41$val$i));
 HEAPF32[$2 >> 2] = $46;
 $$val36$i = +HEAPF32[$13 >> 2];
 $$idx37$val$i = +HEAPF32[$16 >> 2];
 $$idx38$val$i = +HEAPF32[$19 >> 2];
 $53 = $2 + 4 | 0;
 HEAPF32[$53 >> 2] = +Math_sqrt(+($$val36$i * $$val36$i + $$idx37$val$i * $$idx37$val$i + $$idx38$val$i * $$idx38$val$i));
 $$val33$i = +HEAPF32[$25 >> 2];
 $$idx34$val$i = +HEAPF32[$28 >> 2];
 $$idx35$val$i = +HEAPF32[$31 >> 2];
 $60 = $2 + 8 | 0;
 HEAPF32[$60 >> 2] = +Math_sqrt(+($$val33$i * $$val33$i + $$idx34$val$i * $$idx34$val$i + $$idx35$val$i * $$idx35$val$i));
 $61 = 1.0 / $46;
 $63 = $61 * +HEAPF32[$1 >> 2];
 HEAPF32[$1 >> 2] = $63;
 $65 = $61 * +HEAPF32[$6 >> 2];
 HEAPF32[$6 >> 2] = $65;
 $67 = $61 * +HEAPF32[$9 >> 2];
 HEAPF32[$9 >> 2] = $67;
 $69 = $61 * +HEAPF32[$12 >> 2];
 HEAPF32[$12 >> 2] = $69;
 $71 = 1.0 / +HEAPF32[$53 >> 2];
 $73 = +HEAPF32[$14 >> 2] * $71;
 HEAPF32[$14 >> 2] = $73;
 $75 = $71 * +HEAPF32[$18 >> 2];
 HEAPF32[$18 >> 2] = $75;
 $77 = $71 * +HEAPF32[$21 >> 2];
 HEAPF32[$21 >> 2] = $77;
 $79 = $71 * +HEAPF32[$24 >> 2];
 HEAPF32[$24 >> 2] = $79;
 $81 = 1.0 / +HEAPF32[$60 >> 2];
 $83 = +HEAPF32[$26 >> 2] * $81;
 HEAPF32[$26 >> 2] = $83;
 $85 = $81 * +HEAPF32[$30 >> 2];
 HEAPF32[$30 >> 2] = $85;
 $87 = $81 * +HEAPF32[$33 >> 2];
 HEAPF32[$33 >> 2] = $87;
 $89 = $81 * +HEAPF32[$36 >> 2];
 HEAPF32[$36 >> 2] = $89;
 $90 = +HEAPF32[$4 >> 2];
 $91 = +HEAPF32[$19 >> 2];
 $93 = +HEAPF32[$7 >> 2];
 $94 = +HEAPF32[$16 >> 2];
 $97 = +HEAPF32[$13 >> 2];
 $99 = +HEAPF32[$0 >> 2];
 if (!(($90 * $91 - $93 * $94) * +HEAPF32[$25 >> 2] + ($93 * $97 - $91 * $99) * +HEAPF32[$28 >> 2] + ($94 * $99 - $90 * $97) * +HEAPF32[$31 >> 2] < 0.0)) return;
 HEAPF32[$1 >> 2] = -$63;
 HEAPF32[$6 >> 2] = -$65;
 HEAPF32[$9 >> 2] = -$67;
 HEAPF32[$12 >> 2] = -$69;
 HEAPF32[$14 >> 2] = -$73;
 HEAPF32[$18 >> 2] = -$75;
 HEAPF32[$21 >> 2] = -$77;
 HEAPF32[$24 >> 2] = -$79;
 HEAPF32[$26 >> 2] = -$83;
 HEAPF32[$30 >> 2] = -$85;
 HEAPF32[$33 >> 2] = -$87;
 HEAPF32[$36 >> 2] = -$89;
 HEAPF32[$2 >> 2] = -+HEAPF32[$2 >> 2];
 HEAPF32[$53 >> 2] = -+HEAPF32[$53 >> 2];
 HEAPF32[$60 >> 2] = -+HEAPF32[$60 >> 2];
 return;
}

function _glmc_rotate_atm($0, $1, $2, $3) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = +$2;
 $3 = $3 | 0;
 var $$idx10$val$i = 0.0, $$idx9$val$i = 0.0, $$sink$i$i$i$i = 0.0, $$sroa$011$0$i$i$i = 0.0, $$sroa$9$0$i$i$i = 0.0, $$val8$i = 0.0, $100 = 0.0, $101 = 0.0, $11 = 0.0, $12 = 0, $120 = 0, $124 = 0.0, $125 = 0.0, $126 = 0.0, $14 = 0, $16 = 0, $18 = 0, $21 = 0, $23 = 0, $24 = 0.0, $30 = 0.0, $31 = 0, $32 = 0.0, $36 = 0.0, $37 = 0.0, $38 = 0.0, $39 = 0.0, $40 = 0.0, $41 = 0.0, $42 = 0.0, $43 = 0.0, $5 = 0.0, $53 = 0.0, $54 = 0.0, $55 = 0.0, $56 = 0.0, $57 = 0.0, $58 = 0.0, $59 = 0.0, $6 = 0, $60 = 0.0, $61 = 0.0, $72 = 0.0, $74 = 0.0, $75 = 0.0, $76 = 0.0, $78 = 0.0, $79 = 0.0, $8 = 0.0, $80 = 0.0, $81 = 0.0, $82 = 0.0, $84 = 0.0, $85 = 0.0, $86 = 0.0, $88 = 0.0, $89 = 0.0, $9 = 0, $90 = 0.0, $91 = 0.0, $92 = 0.0, $94 = 0.0, $95 = 0.0, $96 = 0.0, $98 = 0.0, $99 = 0.0;
 $5 = -+HEAPF32[$1 >> 2];
 $6 = $1 + 4 | 0;
 $8 = -+HEAPF32[$6 >> 2];
 $9 = $1 + 8 | 0;
 $11 = -+HEAPF32[$9 >> 2];
 $12 = $0 + 4 | 0;
 HEAP32[$0 >> 2] = 1065353216;
 $14 = $0 + 24 | 0;
 HEAP32[$12 >> 2] = 0;
 HEAP32[$12 + 4 >> 2] = 0;
 HEAP32[$12 + 8 >> 2] = 0;
 HEAP32[$12 + 12 >> 2] = 0;
 HEAP32[$0 + 20 >> 2] = 1065353216;
 $16 = $0 + 44 | 0;
 HEAP32[$14 >> 2] = 0;
 HEAP32[$14 + 4 >> 2] = 0;
 HEAP32[$14 + 8 >> 2] = 0;
 HEAP32[$14 + 12 >> 2] = 0;
 HEAP32[$0 + 40 >> 2] = 1065353216;
 HEAP32[$16 >> 2] = 0;
 HEAP32[$16 + 4 >> 2] = 0;
 HEAP32[$16 + 8 >> 2] = 0;
 HEAP32[$16 + 12 >> 2] = 0;
 HEAP32[$0 + 60 >> 2] = 1065353216;
 $18 = $0 + 48 | 0;
 HEAP32[$18 >> 2] = HEAP32[$1 >> 2];
 $21 = $0 + 52 | 0;
 HEAP32[$21 >> 2] = HEAP32[$6 >> 2];
 $23 = $0 + 56 | 0;
 HEAP32[$23 >> 2] = HEAP32[$9 >> 2];
 $$val8$i = +HEAPF32[$3 >> 2];
 $$idx9$val$i = +HEAPF32[$3 + 4 >> 2];
 $$idx10$val$i = +HEAPF32[$3 + 8 >> 2];
 $24 = +Math_cos(+$2);
 $30 = +Math_sqrt(+($$val8$i * $$val8$i + $$idx9$val$i * $$idx9$val$i + $$idx10$val$i * $$idx10$val$i));
 $31 = $30 == 0.0;
 $32 = 1.0 / $30;
 $$sroa$011$0$i$i$i = $31 ? 0.0 : $$val8$i * $32;
 $$sroa$9$0$i$i$i = $31 ? 0.0 : $$idx9$val$i * $32;
 $$sink$i$i$i$i = $31 ? 0.0 : $$idx10$val$i * $32;
 $36 = 1.0 - $24;
 $37 = $36 * $$sroa$011$0$i$i$i;
 $38 = $36 * $$sroa$9$0$i$i$i;
 $39 = $36 * $$sink$i$i$i$i;
 $40 = +Math_sin(+$2);
 $41 = $40 * $$sroa$011$0$i$i$i;
 $42 = $40 * $$sroa$9$0$i$i$i;
 $43 = $40 * $$sink$i$i$i$i;
 $53 = $24 + $$sroa$011$0$i$i$i * $37;
 $54 = $$sroa$011$0$i$i$i * $38 - $43;
 $55 = $42 + $$sroa$011$0$i$i$i * $39;
 $56 = $43 + $$sroa$9$0$i$i$i * $37;
 $57 = $24 + $$sroa$9$0$i$i$i * $38;
 $58 = $$sroa$9$0$i$i$i * $39 - $41;
 $59 = $$sink$i$i$i$i * $37 - $42;
 $60 = $41 + $$sink$i$i$i$i * $38;
 $61 = $24 + $$sink$i$i$i$i * $39;
 $72 = $56 * 0.0;
 $74 = $59 * 0.0;
 $75 = $74 + ($53 + $72);
 HEAPF32[$0 >> 2] = $75;
 $76 = $53 * 0.0;
 $78 = $74 + ($56 + $76);
 HEAPF32[$12 >> 2] = $78;
 $79 = $76 + $72;
 $80 = $59 + $79;
 HEAPF32[$0 + 8 >> 2] = $80;
 $81 = $74 + $79;
 HEAPF32[$0 + 12 >> 2] = $81;
 $82 = $57 * 0.0;
 $84 = $60 * 0.0;
 $85 = $84 + ($54 + $82);
 HEAPF32[$0 + 16 >> 2] = $85;
 $86 = $54 * 0.0;
 $88 = $84 + ($57 + $86);
 HEAPF32[$0 + 20 >> 2] = $88;
 $89 = $86 + $82;
 $90 = $60 + $89;
 HEAPF32[$0 + 24 >> 2] = $90;
 $91 = $84 + $89;
 HEAPF32[$0 + 28 >> 2] = $91;
 $92 = $58 * 0.0;
 $94 = $61 * 0.0;
 $95 = $94 + ($55 + $92);
 HEAPF32[$0 + 32 >> 2] = $95;
 $96 = $55 * 0.0;
 $98 = $94 + ($58 + $96);
 HEAPF32[$0 + 36 >> 2] = $98;
 $99 = $96 + $92;
 $100 = $61 + $99;
 HEAPF32[$0 + 40 >> 2] = $100;
 $101 = $94 + $99;
 HEAPF32[$0 + 44 >> 2] = $101;
 $120 = $0 + 60 | 0;
 $124 = $88 * $8 + ($78 * $5 + +HEAPF32[$21 >> 2]);
 $125 = $90 * $8 + ($80 * $5 + +HEAPF32[$23 >> 2]);
 $126 = $91 * $8 + ($81 * $5 + +HEAPF32[$120 >> 2]);
 HEAPF32[$18 >> 2] = $95 * $11 + ($85 * $8 + ($75 * $5 + +HEAPF32[$18 >> 2]));
 HEAPF32[$21 >> 2] = $98 * $11 + $124;
 HEAPF32[$23 >> 2] = $100 * $11 + $125;
 HEAPF32[$120 >> 2] = $101 * $11 + $126;
 return;
}

function _glmc_quat_rotate_atm($0, $1, $2) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = $2 | 0;
 var $10 = 0.0, $100 = 0.0, $102 = 0.0, $103 = 0.0, $104 = 0.0, $105 = 0.0, $11 = 0, $119 = 0.0, $121 = 0.0, $127 = 0.0, $13 = 0, $15 = 0, $17 = 0, $18 = 0, $19 = 0, $20 = 0, $21 = 0, $22 = 0, $23 = 0.0, $26 = 0.0, $30 = 0.0, $34 = 0.0, $37 = 0.0, $4 = 0.0, $40 = 0.0, $41 = 0.0, $43 = 0.0, $44 = 0.0, $45 = 0.0, $46 = 0.0, $47 = 0.0, $48 = 0.0, $49 = 0.0, $5 = 0, $51 = 0.0, $52 = 0.0, $53 = 0.0, $55 = 0.0, $56 = 0.0, $57 = 0.0, $58 = 0.0, $59 = 0.0, $60 = 0.0, $61 = 0.0, $62 = 0.0, $63 = 0.0, $64 = 0.0, $7 = 0.0, $76 = 0.0, $78 = 0.0, $79 = 0.0, $8 = 0, $80 = 0.0, $82 = 0.0, $83 = 0.0, $84 = 0.0, $85 = 0.0, $86 = 0.0, $88 = 0.0, $89 = 0.0, $90 = 0.0, $92 = 0.0, $93 = 0.0, $94 = 0.0, $95 = 0.0, $96 = 0.0, $98 = 0.0, $99 = 0.0;
 $4 = -+HEAPF32[$2 >> 2];
 $5 = $2 + 4 | 0;
 $7 = -+HEAPF32[$5 >> 2];
 $8 = $2 + 8 | 0;
 $10 = -+HEAPF32[$8 >> 2];
 $11 = $0 + 4 | 0;
 HEAP32[$0 >> 2] = 1065353216;
 $13 = $0 + 24 | 0;
 HEAP32[$11 >> 2] = 0;
 HEAP32[$11 + 4 >> 2] = 0;
 HEAP32[$11 + 8 >> 2] = 0;
 HEAP32[$11 + 12 >> 2] = 0;
 HEAP32[$0 + 20 >> 2] = 1065353216;
 $15 = $0 + 44 | 0;
 HEAP32[$13 >> 2] = 0;
 HEAP32[$13 + 4 >> 2] = 0;
 HEAP32[$13 + 8 >> 2] = 0;
 HEAP32[$13 + 12 >> 2] = 0;
 HEAP32[$0 + 40 >> 2] = 1065353216;
 HEAP32[$15 >> 2] = 0;
 HEAP32[$15 + 4 >> 2] = 0;
 HEAP32[$15 + 8 >> 2] = 0;
 HEAP32[$15 + 12 >> 2] = 0;
 HEAP32[$0 + 60 >> 2] = 1065353216;
 $17 = $0 + 48 | 0;
 $18 = HEAP32[$2 >> 2] | 0;
 HEAP32[$17 >> 2] = $18;
 $19 = HEAP32[$5 >> 2] | 0;
 $20 = $0 + 52 | 0;
 HEAP32[$20 >> 2] = $19;
 $21 = HEAP32[$8 >> 2] | 0;
 $22 = $0 + 56 | 0;
 HEAP32[$22 >> 2] = $21;
 $23 = +HEAPF32[$1 >> 2];
 $26 = +HEAPF32[$1 + 4 >> 2];
 $30 = +HEAPF32[$1 + 8 >> 2];
 $34 = +HEAPF32[$1 + 12 >> 2];
 $37 = +Math_sqrt(+($23 * $23 + $26 * $26 + $30 * $30 + $34 * $34));
 $40 = $37 > 0.0 ? 2.0 / $37 : 0.0;
 $41 = $23 * $40;
 $43 = $26 * $41;
 $44 = $34 * $40;
 $45 = $23 * $44;
 $46 = $26 * $40;
 $47 = $26 * $46;
 $48 = $30 * $46;
 $49 = $26 * $44;
 $51 = $30 * ($30 * $40);
 $52 = $30 * $41;
 $53 = $30 * $44;
 $55 = 1.0 - $47 - $51;
 $56 = 1.0 - $23 * $41;
 $57 = $56 - $51;
 $58 = $56 - $47;
 $59 = $43 + $53;
 $60 = $48 + $45;
 $61 = $52 + $49;
 $62 = $43 - $53;
 $63 = $48 - $45;
 $64 = $52 - $49;
 $76 = $59 * 0.0;
 $78 = $64 * 0.0;
 $79 = $78 + ($76 + $55);
 HEAPF32[$0 >> 2] = $79;
 $80 = $55 * 0.0;
 $82 = $78 + ($59 + $80);
 HEAPF32[$11 >> 2] = $82;
 $83 = $76 + $80;
 $84 = $64 + $83;
 HEAPF32[$0 + 8 >> 2] = $84;
 $85 = $78 + $83;
 HEAPF32[$0 + 12 >> 2] = $85;
 $86 = $57 * 0.0;
 $88 = $60 * 0.0;
 $89 = $88 + ($62 + $86);
 HEAPF32[$0 + 16 >> 2] = $89;
 $90 = $62 * 0.0;
 $92 = $88 + ($90 + $57);
 HEAPF32[$0 + 20 >> 2] = $92;
 $93 = $90 + $86;
 $94 = $60 + $93;
 HEAPF32[$0 + 24 >> 2] = $94;
 $95 = $88 + $93;
 HEAPF32[$0 + 28 >> 2] = $95;
 $96 = $63 * 0.0;
 $98 = $58 * 0.0;
 $99 = $61 + $96 + $98;
 HEAPF32[$0 + 32 >> 2] = $99;
 $100 = $61 * 0.0;
 $102 = $63 + $100 + $98;
 HEAPF32[$0 + 36 >> 2] = $102;
 $103 = $100 + $96;
 $104 = $58 + $103;
 HEAPF32[$0 + 40 >> 2] = $104;
 $105 = $103 + $98;
 HEAPF32[$0 + 44 >> 2] = $105;
 $119 = $79 * $4 + (HEAP32[tempDoublePtr >> 2] = $18, +HEAPF32[tempDoublePtr >> 2]);
 $121 = $82 * $4 + (HEAP32[tempDoublePtr >> 2] = $19, +HEAPF32[tempDoublePtr >> 2]);
 $127 = $94 * $7 + ($84 * $4 + (HEAP32[tempDoublePtr >> 2] = $21, +HEAPF32[tempDoublePtr >> 2]));
 HEAPF32[$17 >> 2] = $99 * $10 + ($89 * $7 + $119);
 HEAPF32[$20 >> 2] = $102 * $10 + ($92 * $7 + $121);
 HEAPF32[$22 >> 2] = $104 * $10 + $127;
 HEAPF32[$0 + 60 >> 2] = $105 * $10 + ($95 * $7 + ($85 * $4 + 1.0));
 return;
}

function _glmc_euler_by_order($0, $1, $2) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = $2 | 0;
 var $$idx$val = 0.0, $$idx2$val = 0.0, $$sink$i = 0.0, $$val = 0.0, $10 = 0.0, $11 = 0.0, $12 = 0.0, $126 = 0, $127 = 0, $128 = 0, $129 = 0, $13 = 0.0, $14 = 0.0, $15 = 0.0, $16 = 0.0, $17 = 0.0, $3 = 0.0, $4 = 0.0, $5 = 0.0, $6 = 0.0, $7 = 0.0, $8 = 0.0, $9 = 0.0;
 $$val = +HEAPF32[$0 >> 2];
 $$idx$val = +HEAPF32[$0 + 4 >> 2];
 $$idx2$val = +HEAPF32[$0 + 8 >> 2];
 $3 = +Math_sin(+$$val);
 $4 = +Math_cos(+$$val);
 $5 = +Math_sin(+$$idx$val);
 $6 = +Math_cos(+$$idx$val);
 $7 = +Math_sin(+$$idx2$val);
 $8 = +Math_cos(+$$idx2$val);
 $9 = $6 * $8;
 $10 = $6 * $7;
 $11 = $3 * $6;
 $12 = $4 * $6;
 $13 = $5 * $8;
 $14 = $4 * $8;
 $15 = $3 * $8;
 $16 = $4 * $7;
 $17 = $5 * $7;
 switch ($1 | 0) {
 case 24:
  {
   HEAPF32[$2 >> 2] = $9;
   HEAPF32[$2 + 4 >> 2] = $3 * $5 + $4 * $10;
   HEAPF32[$2 + 8 >> 2] = $11 * $7 - $4 * $5;
   HEAPF32[$2 + 16 >> 2] = -$7;
   HEAPF32[$2 + 20 >> 2] = $14;
   HEAPF32[$2 + 24 >> 2] = $15;
   HEAPF32[$2 + 32 >> 2] = $13;
   HEAPF32[$2 + 36 >> 2] = $4 * $17 - $11;
   $$sink$i = $12 + $3 * $17;
   break;
  }
 case 36:
  {
   HEAPF32[$2 >> 2] = $9;
   HEAPF32[$2 + 4 >> 2] = $16 + $5 * $15;
   HEAPF32[$2 + 8 >> 2] = $3 * $7 - $4 * $13;
   HEAPF32[$2 + 16 >> 2] = -$10;
   HEAPF32[$2 + 20 >> 2] = $14 - $3 * $17;
   HEAPF32[$2 + 24 >> 2] = $15 + $4 * $17;
   HEAPF32[$2 + 32 >> 2] = $5;
   HEAPF32[$2 + 36 >> 2] = -$11;
   $$sink$i = $12;
   break;
  }
 case 33:
  {
   HEAPF32[$2 >> 2] = $9 + $3 * $17;
   HEAPF32[$2 + 4 >> 2] = $16;
   HEAPF32[$2 + 8 >> 2] = $11 * $7 - $13;
   HEAPF32[$2 + 16 >> 2] = $5 * $15 - $10;
   HEAPF32[$2 + 20 >> 2] = $14;
   HEAPF32[$2 + 24 >> 2] = $17 + $3 * $9;
   HEAPF32[$2 + 32 >> 2] = $4 * $5;
   HEAPF32[$2 + 36 >> 2] = -$3;
   $$sink$i = $12;
   break;
  }
 case 9:
  {
   HEAPF32[$2 >> 2] = $9;
   HEAPF32[$2 + 4 >> 2] = $7;
   HEAPF32[$2 + 8 >> 2] = -$13;
   HEAPF32[$2 + 16 >> 2] = $3 * $5 - $4 * $10;
   HEAPF32[$2 + 20 >> 2] = $14;
   HEAPF32[$2 + 24 >> 2] = $11 + $4 * $17;
   HEAPF32[$2 + 32 >> 2] = $4 * $5 + $11 * $7;
   HEAPF32[$2 + 36 >> 2] = -$15;
   $$sink$i = $12 - $3 * $17;
   break;
  }
 case 18:
  {
   HEAPF32[$2 >> 2] = $9 - $3 * $17;
   HEAPF32[$2 + 4 >> 2] = $10 + $5 * $15;
   HEAPF32[$2 + 8 >> 2] = -($4 * $5);
   HEAPF32[$2 + 16 >> 2] = -$16;
   HEAPF32[$2 + 20 >> 2] = $14;
   HEAPF32[$2 + 24 >> 2] = $3;
   HEAPF32[$2 + 32 >> 2] = $11 * $7 + $13;
   HEAPF32[$2 + 36 >> 2] = $17 - $3 * $9;
   $$sink$i = $12;
   break;
  }
 case 6:
  {
   HEAPF32[$2 >> 2] = $9;
   HEAPF32[$2 + 4 >> 2] = $10;
   HEAPF32[$2 + 8 >> 2] = -$5;
   HEAPF32[$2 + 16 >> 2] = $5 * $15 - $16;
   HEAPF32[$2 + 20 >> 2] = $14 + $3 * $17;
   HEAPF32[$2 + 24 >> 2] = $11;
   HEAPF32[$2 + 32 >> 2] = $3 * $7 + $4 * $13;
   HEAPF32[$2 + 36 >> 2] = $4 * $17 - $15;
   $$sink$i = $12;
   break;
  }
 default:
  {
   $126 = $2 + 12 | 0;
   HEAPF32[$126 >> 2] = 0.0;
   $127 = $2 + 28 | 0;
   HEAPF32[$127 >> 2] = 0.0;
   $128 = $2 + 44 | 0;
   $129 = $2 + 60 | 0;
   HEAP32[$128 >> 2] = 0;
   HEAP32[$128 + 4 >> 2] = 0;
   HEAP32[$128 + 8 >> 2] = 0;
   HEAP32[$128 + 12 >> 2] = 0;
   HEAPF32[$129 >> 2] = 1.0;
   return;
  }
 }
 HEAPF32[$2 + 40 >> 2] = $$sink$i;
 $126 = $2 + 12 | 0;
 HEAPF32[$126 >> 2] = 0.0;
 $127 = $2 + 28 | 0;
 HEAPF32[$127 >> 2] = 0.0;
 $128 = $2 + 44 | 0;
 $129 = $2 + 60 | 0;
 HEAP32[$128 >> 2] = 0;
 HEAP32[$128 + 4 >> 2] = 0;
 HEAP32[$128 + 8 >> 2] = 0;
 HEAP32[$128 + 12 >> 2] = 0;
 HEAPF32[$129 >> 2] = 1.0;
 return;
}

function _glmc_mat4_inv_precise($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 var $10 = 0.0, $101 = 0, $104 = 0.0, $107 = 0.0, $110 = 0.0, $113 = 0.0, $116 = 0.0, $119 = 0.0, $12 = 0.0, $14 = 0.0, $146 = 0.0, $149 = 0.0, $152 = 0.0, $155 = 0.0, $158 = 0.0, $16 = 0.0, $161 = 0.0, $18 = 0.0, $186 = 0.0, $188 = 0.0, $191 = 0.0, $194 = 0.0, $197 = 0.0, $2 = 0.0, $20 = 0.0, $22 = 0.0, $24 = 0.0, $26 = 0.0, $28 = 0.0, $30 = 0.0, $32 = 0.0, $35 = 0.0, $38 = 0.0, $4 = 0.0, $41 = 0.0, $44 = 0.0, $47 = 0.0, $50 = 0.0, $6 = 0.0, $62 = 0, $68 = 0, $75 = 0, $8 = 0.0, $82 = 0, $88 = 0, $95 = 0;
 $2 = +HEAPF32[$0 >> 2];
 $4 = +HEAPF32[$0 + 4 >> 2];
 $6 = +HEAPF32[$0 + 8 >> 2];
 $8 = +HEAPF32[$0 + 12 >> 2];
 $10 = +HEAPF32[$0 + 16 >> 2];
 $12 = +HEAPF32[$0 + 20 >> 2];
 $14 = +HEAPF32[$0 + 24 >> 2];
 $16 = +HEAPF32[$0 + 28 >> 2];
 $18 = +HEAPF32[$0 + 32 >> 2];
 $20 = +HEAPF32[$0 + 36 >> 2];
 $22 = +HEAPF32[$0 + 40 >> 2];
 $24 = +HEAPF32[$0 + 44 >> 2];
 $26 = +HEAPF32[$0 + 48 >> 2];
 $28 = +HEAPF32[$0 + 52 >> 2];
 $30 = +HEAPF32[$0 + 56 >> 2];
 $32 = +HEAPF32[$0 + 60 >> 2];
 $35 = $22 * $32 - $24 * $30;
 $38 = $20 * $32 - $24 * $28;
 $41 = $20 * $30 - $22 * $28;
 $44 = $18 * $32 - $24 * $26;
 $47 = $18 * $30 - $22 * $26;
 $50 = $18 * $28 - $20 * $26;
 HEAPF32[$1 >> 2] = $16 * $41 + ($12 * $35 - $14 * $38);
 $62 = $1 + 16 | 0;
 HEAPF32[$62 >> 2] = -($16 * $47 + ($10 * $35 - $14 * $44));
 $68 = $1 + 32 | 0;
 HEAPF32[$68 >> 2] = $16 * $50 + ($10 * $38 - $12 * $44);
 $75 = $1 + 48 | 0;
 HEAPF32[$75 >> 2] = -($14 * $50 + ($10 * $41 - $12 * $47));
 $82 = $1 + 4 | 0;
 HEAPF32[$82 >> 2] = -($8 * $41 + ($4 * $35 - $6 * $38));
 $88 = $1 + 20 | 0;
 HEAPF32[$88 >> 2] = $8 * $47 + ($2 * $35 - $6 * $44);
 $95 = $1 + 36 | 0;
 HEAPF32[$95 >> 2] = -($8 * $50 + ($2 * $38 - $4 * $44));
 $101 = $1 + 52 | 0;
 HEAPF32[$101 >> 2] = $6 * $50 + ($2 * $41 - $4 * $47);
 $104 = $14 * $32 - $16 * $30;
 $107 = $12 * $32 - $16 * $28;
 $110 = $12 * $30 - $14 * $28;
 $113 = $10 * $32 - $16 * $26;
 $116 = $10 * $30 - $14 * $26;
 $119 = $10 * $28 - $12 * $26;
 $146 = $14 * $24 - $16 * $22;
 $149 = $12 * $24 - $16 * $20;
 $152 = $12 * $22 - $14 * $20;
 $155 = $10 * $24 - $16 * $18;
 $158 = $10 * $22 - $14 * $18;
 $161 = $10 * $20 - $12 * $18;
 $186 = +HEAPF32[$1 >> 2];
 $188 = +HEAPF32[$62 >> 2];
 $191 = +HEAPF32[$68 >> 2];
 $194 = +HEAPF32[$75 >> 2];
 $197 = 1.0 / ($2 * $186 + $4 * $188 + $6 * $191 + $8 * $194);
 HEAPF32[$1 >> 2] = $186 * $197;
 HEAPF32[$82 >> 2] = +HEAPF32[$82 >> 2] * $197;
 HEAPF32[$1 + 8 >> 2] = ($8 * $110 + ($4 * $104 - $6 * $107)) * $197;
 HEAPF32[$1 + 12 >> 2] = -(($8 * $152 + ($4 * $146 - $6 * $149)) * $197);
 HEAPF32[$62 >> 2] = $188 * $197;
 HEAPF32[$88 >> 2] = $197 * +HEAPF32[$88 >> 2];
 HEAPF32[$1 + 24 >> 2] = -(($8 * $116 + ($2 * $104 - $6 * $113)) * $197);
 HEAPF32[$1 + 28 >> 2] = ($8 * $158 + ($2 * $146 - $6 * $155)) * $197;
 HEAPF32[$68 >> 2] = $191 * $197;
 HEAPF32[$95 >> 2] = $197 * +HEAPF32[$95 >> 2];
 HEAPF32[$1 + 40 >> 2] = ($8 * $119 + ($2 * $107 - $4 * $113)) * $197;
 HEAPF32[$1 + 44 >> 2] = -(($8 * $161 + ($2 * $149 - $4 * $155)) * $197);
 HEAPF32[$75 >> 2] = $194 * $197;
 HEAPF32[$101 >> 2] = $197 * +HEAPF32[$101 >> 2];
 HEAPF32[$1 + 56 >> 2] = -(($6 * $119 + ($2 * $110 - $4 * $116)) * $197);
 HEAPF32[$1 + 60 >> 2] = ($6 * $161 + ($2 * $152 - $4 * $158)) * $197;
 return;
}

function _glmc_mat4_inv_fast($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 var $10 = 0.0, $101 = 0, $104 = 0.0, $107 = 0.0, $110 = 0.0, $113 = 0.0, $116 = 0.0, $119 = 0.0, $12 = 0.0, $14 = 0.0, $146 = 0.0, $149 = 0.0, $152 = 0.0, $155 = 0.0, $158 = 0.0, $16 = 0.0, $161 = 0.0, $18 = 0.0, $186 = 0.0, $188 = 0.0, $191 = 0.0, $194 = 0.0, $197 = 0.0, $2 = 0.0, $20 = 0.0, $22 = 0.0, $24 = 0.0, $26 = 0.0, $28 = 0.0, $30 = 0.0, $32 = 0.0, $35 = 0.0, $38 = 0.0, $4 = 0.0, $41 = 0.0, $44 = 0.0, $47 = 0.0, $50 = 0.0, $6 = 0.0, $62 = 0, $68 = 0, $75 = 0, $8 = 0.0, $82 = 0, $88 = 0, $95 = 0;
 $2 = +HEAPF32[$0 >> 2];
 $4 = +HEAPF32[$0 + 4 >> 2];
 $6 = +HEAPF32[$0 + 8 >> 2];
 $8 = +HEAPF32[$0 + 12 >> 2];
 $10 = +HEAPF32[$0 + 16 >> 2];
 $12 = +HEAPF32[$0 + 20 >> 2];
 $14 = +HEAPF32[$0 + 24 >> 2];
 $16 = +HEAPF32[$0 + 28 >> 2];
 $18 = +HEAPF32[$0 + 32 >> 2];
 $20 = +HEAPF32[$0 + 36 >> 2];
 $22 = +HEAPF32[$0 + 40 >> 2];
 $24 = +HEAPF32[$0 + 44 >> 2];
 $26 = +HEAPF32[$0 + 48 >> 2];
 $28 = +HEAPF32[$0 + 52 >> 2];
 $30 = +HEAPF32[$0 + 56 >> 2];
 $32 = +HEAPF32[$0 + 60 >> 2];
 $35 = $22 * $32 - $24 * $30;
 $38 = $20 * $32 - $24 * $28;
 $41 = $20 * $30 - $22 * $28;
 $44 = $18 * $32 - $24 * $26;
 $47 = $18 * $30 - $22 * $26;
 $50 = $18 * $28 - $20 * $26;
 HEAPF32[$1 >> 2] = $16 * $41 + ($12 * $35 - $14 * $38);
 $62 = $1 + 16 | 0;
 HEAPF32[$62 >> 2] = -($16 * $47 + ($10 * $35 - $14 * $44));
 $68 = $1 + 32 | 0;
 HEAPF32[$68 >> 2] = $16 * $50 + ($10 * $38 - $12 * $44);
 $75 = $1 + 48 | 0;
 HEAPF32[$75 >> 2] = -($14 * $50 + ($10 * $41 - $12 * $47));
 $82 = $1 + 4 | 0;
 HEAPF32[$82 >> 2] = -($8 * $41 + ($4 * $35 - $6 * $38));
 $88 = $1 + 20 | 0;
 HEAPF32[$88 >> 2] = $8 * $47 + ($2 * $35 - $6 * $44);
 $95 = $1 + 36 | 0;
 HEAPF32[$95 >> 2] = -($8 * $50 + ($2 * $38 - $4 * $44));
 $101 = $1 + 52 | 0;
 HEAPF32[$101 >> 2] = $6 * $50 + ($2 * $41 - $4 * $47);
 $104 = $14 * $32 - $16 * $30;
 $107 = $12 * $32 - $16 * $28;
 $110 = $12 * $30 - $14 * $28;
 $113 = $10 * $32 - $16 * $26;
 $116 = $10 * $30 - $14 * $26;
 $119 = $10 * $28 - $12 * $26;
 $146 = $14 * $24 - $16 * $22;
 $149 = $12 * $24 - $16 * $20;
 $152 = $12 * $22 - $14 * $20;
 $155 = $10 * $24 - $16 * $18;
 $158 = $10 * $22 - $14 * $18;
 $161 = $10 * $20 - $12 * $18;
 $186 = +HEAPF32[$1 >> 2];
 $188 = +HEAPF32[$62 >> 2];
 $191 = +HEAPF32[$68 >> 2];
 $194 = +HEAPF32[$75 >> 2];
 $197 = 1.0 / ($2 * $186 + $4 * $188 + $6 * $191 + $8 * $194);
 HEAPF32[$1 >> 2] = $186 * $197;
 HEAPF32[$82 >> 2] = +HEAPF32[$82 >> 2] * $197;
 HEAPF32[$1 + 8 >> 2] = ($8 * $110 + ($4 * $104 - $6 * $107)) * $197;
 HEAPF32[$1 + 12 >> 2] = -(($8 * $152 + ($4 * $146 - $6 * $149)) * $197);
 HEAPF32[$62 >> 2] = $188 * $197;
 HEAPF32[$88 >> 2] = $197 * +HEAPF32[$88 >> 2];
 HEAPF32[$1 + 24 >> 2] = -(($8 * $116 + ($2 * $104 - $6 * $113)) * $197);
 HEAPF32[$1 + 28 >> 2] = ($8 * $158 + ($2 * $146 - $6 * $155)) * $197;
 HEAPF32[$68 >> 2] = $191 * $197;
 HEAPF32[$95 >> 2] = $197 * +HEAPF32[$95 >> 2];
 HEAPF32[$1 + 40 >> 2] = ($8 * $119 + ($2 * $107 - $4 * $113)) * $197;
 HEAPF32[$1 + 44 >> 2] = -(($8 * $161 + ($2 * $149 - $4 * $155)) * $197);
 HEAPF32[$75 >> 2] = $194 * $197;
 HEAPF32[$101 >> 2] = $197 * +HEAPF32[$101 >> 2];
 HEAPF32[$1 + 56 >> 2] = -(($6 * $119 + ($2 * $110 - $4 * $116)) * $197);
 HEAPF32[$1 + 60 >> 2] = ($6 * $161 + ($2 * $152 - $4 * $158)) * $197;
 return;
}

function _glmc_mat4_inv($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 var $10 = 0.0, $101 = 0, $104 = 0.0, $107 = 0.0, $110 = 0.0, $113 = 0.0, $116 = 0.0, $119 = 0.0, $12 = 0.0, $14 = 0.0, $146 = 0.0, $149 = 0.0, $152 = 0.0, $155 = 0.0, $158 = 0.0, $16 = 0.0, $161 = 0.0, $18 = 0.0, $186 = 0.0, $188 = 0.0, $191 = 0.0, $194 = 0.0, $197 = 0.0, $2 = 0.0, $20 = 0.0, $22 = 0.0, $24 = 0.0, $26 = 0.0, $28 = 0.0, $30 = 0.0, $32 = 0.0, $35 = 0.0, $38 = 0.0, $4 = 0.0, $41 = 0.0, $44 = 0.0, $47 = 0.0, $50 = 0.0, $6 = 0.0, $62 = 0, $68 = 0, $75 = 0, $8 = 0.0, $82 = 0, $88 = 0, $95 = 0;
 $2 = +HEAPF32[$0 >> 2];
 $4 = +HEAPF32[$0 + 4 >> 2];
 $6 = +HEAPF32[$0 + 8 >> 2];
 $8 = +HEAPF32[$0 + 12 >> 2];
 $10 = +HEAPF32[$0 + 16 >> 2];
 $12 = +HEAPF32[$0 + 20 >> 2];
 $14 = +HEAPF32[$0 + 24 >> 2];
 $16 = +HEAPF32[$0 + 28 >> 2];
 $18 = +HEAPF32[$0 + 32 >> 2];
 $20 = +HEAPF32[$0 + 36 >> 2];
 $22 = +HEAPF32[$0 + 40 >> 2];
 $24 = +HEAPF32[$0 + 44 >> 2];
 $26 = +HEAPF32[$0 + 48 >> 2];
 $28 = +HEAPF32[$0 + 52 >> 2];
 $30 = +HEAPF32[$0 + 56 >> 2];
 $32 = +HEAPF32[$0 + 60 >> 2];
 $35 = $22 * $32 - $24 * $30;
 $38 = $20 * $32 - $24 * $28;
 $41 = $20 * $30 - $22 * $28;
 $44 = $18 * $32 - $24 * $26;
 $47 = $18 * $30 - $22 * $26;
 $50 = $18 * $28 - $20 * $26;
 HEAPF32[$1 >> 2] = $16 * $41 + ($12 * $35 - $14 * $38);
 $62 = $1 + 16 | 0;
 HEAPF32[$62 >> 2] = -($16 * $47 + ($10 * $35 - $14 * $44));
 $68 = $1 + 32 | 0;
 HEAPF32[$68 >> 2] = $16 * $50 + ($10 * $38 - $12 * $44);
 $75 = $1 + 48 | 0;
 HEAPF32[$75 >> 2] = -($14 * $50 + ($10 * $41 - $12 * $47));
 $82 = $1 + 4 | 0;
 HEAPF32[$82 >> 2] = -($8 * $41 + ($4 * $35 - $6 * $38));
 $88 = $1 + 20 | 0;
 HEAPF32[$88 >> 2] = $8 * $47 + ($2 * $35 - $6 * $44);
 $95 = $1 + 36 | 0;
 HEAPF32[$95 >> 2] = -($8 * $50 + ($2 * $38 - $4 * $44));
 $101 = $1 + 52 | 0;
 HEAPF32[$101 >> 2] = $6 * $50 + ($2 * $41 - $4 * $47);
 $104 = $14 * $32 - $16 * $30;
 $107 = $12 * $32 - $16 * $28;
 $110 = $12 * $30 - $14 * $28;
 $113 = $10 * $32 - $16 * $26;
 $116 = $10 * $30 - $14 * $26;
 $119 = $10 * $28 - $12 * $26;
 $146 = $14 * $24 - $16 * $22;
 $149 = $12 * $24 - $16 * $20;
 $152 = $12 * $22 - $14 * $20;
 $155 = $10 * $24 - $16 * $18;
 $158 = $10 * $22 - $14 * $18;
 $161 = $10 * $20 - $12 * $18;
 $186 = +HEAPF32[$1 >> 2];
 $188 = +HEAPF32[$62 >> 2];
 $191 = +HEAPF32[$68 >> 2];
 $194 = +HEAPF32[$75 >> 2];
 $197 = 1.0 / ($2 * $186 + $4 * $188 + $6 * $191 + $8 * $194);
 HEAPF32[$1 >> 2] = $186 * $197;
 HEAPF32[$82 >> 2] = +HEAPF32[$82 >> 2] * $197;
 HEAPF32[$1 + 8 >> 2] = ($8 * $110 + ($4 * $104 - $6 * $107)) * $197;
 HEAPF32[$1 + 12 >> 2] = -(($8 * $152 + ($4 * $146 - $6 * $149)) * $197);
 HEAPF32[$62 >> 2] = $188 * $197;
 HEAPF32[$88 >> 2] = $197 * +HEAPF32[$88 >> 2];
 HEAPF32[$1 + 24 >> 2] = -(($8 * $116 + ($2 * $104 - $6 * $113)) * $197);
 HEAPF32[$1 + 28 >> 2] = ($8 * $158 + ($2 * $146 - $6 * $155)) * $197;
 HEAPF32[$68 >> 2] = $191 * $197;
 HEAPF32[$95 >> 2] = $197 * +HEAPF32[$95 >> 2];
 HEAPF32[$1 + 40 >> 2] = ($8 * $119 + ($2 * $107 - $4 * $113)) * $197;
 HEAPF32[$1 + 44 >> 2] = -(($8 * $161 + ($2 * $149 - $4 * $155)) * $197);
 HEAPF32[$75 >> 2] = $194 * $197;
 HEAPF32[$101 >> 2] = $197 * +HEAPF32[$101 >> 2];
 HEAPF32[$1 + 56 >> 2] = -(($6 * $119 + ($2 * $110 - $4 * $116)) * $197);
 HEAPF32[$1 + 60 >> 2] = ($6 * $161 + ($2 * $152 - $4 * $158)) * $197;
 return;
}

function _glmc_frustum_corners($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 var $100 = 0.0, $108 = 0.0, $11 = 0.0, $116 = 0.0, $117 = 0.0, $125 = 0.0, $134 = 0.0, $14 = 0.0, $143 = 0.0, $152 = 0.0, $16 = 0.0, $161 = 0.0, $170 = 0.0, $179 = 0.0, $18 = 0.0, $2 = 0.0, $20 = 0.0, $23 = 0.0, $26 = 0.0, $28 = 0.0, $30 = 0.0, $32 = 0.0, $35 = 0.0, $38 = 0.0, $4 = 0.0, $40 = 0.0, $42 = 0.0, $44 = 0.0, $47 = 0.0, $48 = 0.0, $49 = 0.0, $52 = 0.0, $55 = 0.0, $58 = 0.0, $6 = 0.0, $60 = 0.0, $61 = 0.0, $64 = 0.0, $67 = 0.0, $70 = 0.0, $72 = 0.0, $73 = 0.0, $76 = 0.0, $79 = 0.0, $8 = 0.0, $82 = 0.0, $84 = 0.0, $92 = 0.0;
 $2 = +HEAPF32[$0 >> 2];
 $4 = +HEAPF32[$0 + 16 >> 2];
 $6 = -$4 - $2;
 $8 = +HEAPF32[$0 + 32 >> 2];
 $11 = +HEAPF32[$0 + 48 >> 2];
 $14 = +HEAPF32[$0 + 4 >> 2];
 $16 = +HEAPF32[$0 + 20 >> 2];
 $18 = -$16 - $14;
 $20 = +HEAPF32[$0 + 36 >> 2];
 $23 = +HEAPF32[$0 + 52 >> 2];
 $26 = +HEAPF32[$0 + 8 >> 2];
 $28 = +HEAPF32[$0 + 24 >> 2];
 $30 = -$28 - $26;
 $32 = +HEAPF32[$0 + 40 >> 2];
 $35 = +HEAPF32[$0 + 56 >> 2];
 $38 = +HEAPF32[$0 + 12 >> 2];
 $40 = +HEAPF32[$0 + 28 >> 2];
 $42 = -$40 - $38;
 $44 = +HEAPF32[$0 + 44 >> 2];
 $47 = +HEAPF32[$0 + 60 >> 2];
 $48 = $42 - $44 + $47;
 $49 = $4 - $2;
 $52 = $16 - $14;
 $55 = $28 - $26;
 $58 = $40 - $38;
 $60 = $58 - $44 + $47;
 $61 = $2 + $4;
 $64 = $14 + $16;
 $67 = $26 + $28;
 $70 = $38 + $40;
 $72 = $70 - $44 + $47;
 $73 = $2 - $4;
 $76 = $14 - $16;
 $79 = $26 - $28;
 $82 = $38 - $40;
 $84 = $82 - $44 + $47;
 $92 = $42 + $44 + $47;
 $100 = $58 + $44 + $47;
 $108 = $70 + $44 + $47;
 $116 = $82 + $44 + $47;
 $117 = 1.0 / $48;
 HEAPF32[$1 >> 2] = ($6 - $8 + $11) * $117;
 HEAPF32[$1 + 4 >> 2] = ($18 - $20 + $23) * $117;
 HEAPF32[$1 + 8 >> 2] = ($30 - $32 + $35) * $117;
 HEAPF32[$1 + 12 >> 2] = $48 * $117;
 $125 = 1.0 / $60;
 HEAPF32[$1 + 16 >> 2] = ($49 - $8 + $11) * $125;
 HEAPF32[$1 + 20 >> 2] = ($52 - $20 + $23) * $125;
 HEAPF32[$1 + 24 >> 2] = ($55 - $32 + $35) * $125;
 HEAPF32[$1 + 28 >> 2] = $60 * $125;
 $134 = 1.0 / $72;
 HEAPF32[$1 + 32 >> 2] = ($61 - $8 + $11) * $134;
 HEAPF32[$1 + 36 >> 2] = ($64 - $20 + $23) * $134;
 HEAPF32[$1 + 40 >> 2] = ($67 - $32 + $35) * $134;
 HEAPF32[$1 + 44 >> 2] = $72 * $134;
 $143 = 1.0 / $84;
 HEAPF32[$1 + 48 >> 2] = ($73 - $8 + $11) * $143;
 HEAPF32[$1 + 52 >> 2] = ($76 - $20 + $23) * $143;
 HEAPF32[$1 + 56 >> 2] = ($79 - $32 + $35) * $143;
 HEAPF32[$1 + 60 >> 2] = $84 * $143;
 $152 = 1.0 / $92;
 HEAPF32[$1 + 64 >> 2] = ($6 + $8 + $11) * $152;
 HEAPF32[$1 + 68 >> 2] = ($18 + $20 + $23) * $152;
 HEAPF32[$1 + 72 >> 2] = ($30 + $32 + $35) * $152;
 HEAPF32[$1 + 76 >> 2] = $92 * $152;
 $161 = 1.0 / $100;
 HEAPF32[$1 + 80 >> 2] = ($49 + $8 + $11) * $161;
 HEAPF32[$1 + 84 >> 2] = ($52 + $20 + $23) * $161;
 HEAPF32[$1 + 88 >> 2] = ($55 + $32 + $35) * $161;
 HEAPF32[$1 + 92 >> 2] = $100 * $161;
 $170 = 1.0 / $108;
 HEAPF32[$1 + 96 >> 2] = ($61 + $8 + $11) * $170;
 HEAPF32[$1 + 100 >> 2] = ($64 + $20 + $23) * $170;
 HEAPF32[$1 + 104 >> 2] = ($67 + $32 + $35) * $170;
 HEAPF32[$1 + 108 >> 2] = $108 * $170;
 $179 = 1.0 / $116;
 HEAPF32[$1 + 112 >> 2] = ($73 + $8 + $11) * $179;
 HEAPF32[$1 + 116 >> 2] = ($76 + $20 + $23) * $179;
 HEAPF32[$1 + 120 >> 2] = ($79 + $32 + $35) * $179;
 HEAPF32[$1 + 124 >> 2] = $116 * $179;
 return;
}

function _glmc_translate_to($0, $1, $2) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = $2 | 0;
 var $$idx$val$i = 0.0, $$idx4$val$i = 0.0, $$val$i = 0.0, $10 = 0, $13 = 0, $16 = 0, $19 = 0, $22 = 0, $25 = 0, $28 = 0, $31 = 0, $34 = 0, $37 = 0, $4 = 0, $40 = 0, $43 = 0, $46 = 0, $48 = 0, $50 = 0.0, $52 = 0.0, $54 = 0.0, $56 = 0.0, $58 = 0.0, $60 = 0.0, $62 = 0.0, $64 = 0.0, $66 = 0.0, $68 = 0.0, $7 = 0, $70 = 0.0, $72 = 0.0, $75 = 0.0, $78 = 0.0, $81 = 0.0, $88 = 0.0;
 $4 = HEAP32[$0 >> 2] | 0;
 HEAP32[$2 >> 2] = $4;
 $7 = HEAP32[$0 + 4 >> 2] | 0;
 HEAP32[$2 + 4 >> 2] = $7;
 $10 = HEAP32[$0 + 8 >> 2] | 0;
 HEAP32[$2 + 8 >> 2] = $10;
 $13 = HEAP32[$0 + 12 >> 2] | 0;
 HEAP32[$2 + 12 >> 2] = $13;
 $16 = HEAP32[$0 + 16 >> 2] | 0;
 HEAP32[$2 + 16 >> 2] = $16;
 $19 = HEAP32[$0 + 20 >> 2] | 0;
 HEAP32[$2 + 20 >> 2] = $19;
 $22 = HEAP32[$0 + 24 >> 2] | 0;
 HEAP32[$2 + 24 >> 2] = $22;
 $25 = HEAP32[$0 + 28 >> 2] | 0;
 HEAP32[$2 + 28 >> 2] = $25;
 $28 = HEAP32[$0 + 32 >> 2] | 0;
 HEAP32[$2 + 32 >> 2] = $28;
 $31 = HEAP32[$0 + 36 >> 2] | 0;
 HEAP32[$2 + 36 >> 2] = $31;
 $34 = HEAP32[$0 + 40 >> 2] | 0;
 HEAP32[$2 + 40 >> 2] = $34;
 $37 = HEAP32[$0 + 44 >> 2] | 0;
 HEAP32[$2 + 44 >> 2] = $37;
 $40 = HEAP32[$0 + 48 >> 2] | 0;
 HEAP32[$2 + 48 >> 2] = $40;
 $43 = HEAP32[$0 + 52 >> 2] | 0;
 HEAP32[$2 + 52 >> 2] = $43;
 $46 = HEAP32[$0 + 56 >> 2] | 0;
 HEAP32[$2 + 56 >> 2] = $46;
 $48 = HEAP32[$0 + 60 >> 2] | 0;
 HEAP32[$2 + 60 >> 2] = $48;
 $$val$i = +HEAPF32[$1 >> 2];
 $$idx$val$i = +HEAPF32[$1 + 4 >> 2];
 $$idx4$val$i = +HEAPF32[$1 + 8 >> 2];
 $50 = $$val$i * (HEAP32[tempDoublePtr >> 2] = $4, +HEAPF32[tempDoublePtr >> 2]);
 $52 = $$val$i * (HEAP32[tempDoublePtr >> 2] = $7, +HEAPF32[tempDoublePtr >> 2]);
 $54 = $$val$i * (HEAP32[tempDoublePtr >> 2] = $10, +HEAPF32[tempDoublePtr >> 2]);
 $56 = $$val$i * (HEAP32[tempDoublePtr >> 2] = $13, +HEAPF32[tempDoublePtr >> 2]);
 $58 = $$idx$val$i * (HEAP32[tempDoublePtr >> 2] = $16, +HEAPF32[tempDoublePtr >> 2]);
 $60 = $$idx$val$i * (HEAP32[tempDoublePtr >> 2] = $19, +HEAPF32[tempDoublePtr >> 2]);
 $62 = $$idx$val$i * (HEAP32[tempDoublePtr >> 2] = $22, +HEAPF32[tempDoublePtr >> 2]);
 $64 = $$idx$val$i * (HEAP32[tempDoublePtr >> 2] = $25, +HEAPF32[tempDoublePtr >> 2]);
 $66 = $$idx4$val$i * (HEAP32[tempDoublePtr >> 2] = $28, +HEAPF32[tempDoublePtr >> 2]);
 $68 = $$idx4$val$i * (HEAP32[tempDoublePtr >> 2] = $31, +HEAPF32[tempDoublePtr >> 2]);
 $70 = $$idx4$val$i * (HEAP32[tempDoublePtr >> 2] = $34, +HEAPF32[tempDoublePtr >> 2]);
 $72 = $$idx4$val$i * (HEAP32[tempDoublePtr >> 2] = $37, +HEAPF32[tempDoublePtr >> 2]);
 $75 = $50 + (HEAP32[tempDoublePtr >> 2] = $40, +HEAPF32[tempDoublePtr >> 2]);
 $78 = $52 + (HEAP32[tempDoublePtr >> 2] = $43, +HEAPF32[tempDoublePtr >> 2]);
 $81 = $54 + (HEAP32[tempDoublePtr >> 2] = $46, +HEAPF32[tempDoublePtr >> 2]);
 $88 = $64 + ($56 + (HEAP32[tempDoublePtr >> 2] = $48, +HEAPF32[tempDoublePtr >> 2]));
 HEAPF32[$2 + 48 >> 2] = $66 + ($58 + $75);
 HEAPF32[$2 + 52 >> 2] = $68 + ($60 + $78);
 HEAPF32[$2 + 56 >> 2] = $70 + ($62 + $81);
 HEAPF32[$2 + 60 >> 2] = $72 + $88;
 return;
}

function _glmc_unproject($0, $1, $2, $3) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = $2 | 0;
 $3 = $3 | 0;
 var $10 = 0.0, $100 = 0.0, $103 = 0.0, $106 = 0.0, $109 = 0.0, $112 = 0.0, $12 = 0.0, $135 = 0.0, $138 = 0.0, $14 = 0.0, $141 = 0.0, $144 = 0.0, $147 = 0.0, $150 = 0.0, $16 = 0.0, $178 = 0.0, $18 = 0.0, $20 = 0.0, $201 = 0.0, $209 = 0.0, $211 = 0.0, $22 = 0.0, $236 = 0.0, $24 = 0.0, $26 = 0.0, $28 = 0.0, $30 = 0.0, $32 = 0.0, $34 = 0.0, $37 = 0.0, $4 = 0.0, $40 = 0.0, $43 = 0.0, $46 = 0.0, $49 = 0.0, $52 = 0.0, $57 = 0.0, $6 = 0.0, $63 = 0.0, $68 = 0.0, $74 = 0.0, $8 = 0.0, $97 = 0.0;
 $4 = +HEAPF32[$1 >> 2];
 $6 = +HEAPF32[$1 + 4 >> 2];
 $8 = +HEAPF32[$1 + 8 >> 2];
 $10 = +HEAPF32[$1 + 12 >> 2];
 $12 = +HEAPF32[$1 + 16 >> 2];
 $14 = +HEAPF32[$1 + 20 >> 2];
 $16 = +HEAPF32[$1 + 24 >> 2];
 $18 = +HEAPF32[$1 + 28 >> 2];
 $20 = +HEAPF32[$1 + 32 >> 2];
 $22 = +HEAPF32[$1 + 36 >> 2];
 $24 = +HEAPF32[$1 + 40 >> 2];
 $26 = +HEAPF32[$1 + 44 >> 2];
 $28 = +HEAPF32[$1 + 48 >> 2];
 $30 = +HEAPF32[$1 + 52 >> 2];
 $32 = +HEAPF32[$1 + 56 >> 2];
 $34 = +HEAPF32[$1 + 60 >> 2];
 $37 = $24 * $34 - $26 * $32;
 $40 = $22 * $34 - $26 * $30;
 $43 = $22 * $32 - $24 * $30;
 $46 = $20 * $34 - $26 * $28;
 $49 = $20 * $32 - $24 * $28;
 $52 = $20 * $30 - $22 * $28;
 $57 = $18 * $43 + ($14 * $37 - $16 * $40);
 $63 = -($18 * $49 + ($12 * $37 - $16 * $46));
 $68 = $18 * $52 + ($12 * $40 - $14 * $46);
 $74 = -($16 * $52 + ($12 * $43 - $14 * $49));
 $97 = $16 * $34 - $18 * $32;
 $100 = $14 * $34 - $18 * $30;
 $103 = $14 * $32 - $16 * $30;
 $106 = $12 * $34 - $18 * $28;
 $109 = $12 * $32 - $16 * $28;
 $112 = $12 * $30 - $14 * $28;
 $135 = $16 * $26 - $18 * $24;
 $138 = $14 * $26 - $18 * $22;
 $141 = $14 * $24 - $16 * $22;
 $144 = $12 * $26 - $18 * $20;
 $147 = $12 * $24 - $16 * $20;
 $150 = $12 * $22 - $14 * $20;
 $178 = 1.0 / ($10 * $74 + ($8 * $68 + ($4 * $57 + $6 * $63)));
 $201 = (+HEAPF32[$0 >> 2] - +HEAPF32[$2 >> 2]) * 2.0 / +HEAPF32[$2 + 8 >> 2] + -1.0;
 $209 = (+HEAPF32[$0 + 4 >> 2] - +HEAPF32[$2 + 4 >> 2]) * 2.0 / +HEAPF32[$2 + 12 >> 2] + -1.0;
 $211 = +HEAPF32[$0 + 8 >> 2] * 2.0 + -1.0;
 $236 = 1.0 / (($8 * $150 + ($4 * $141 - $6 * $147)) * $178 + ($209 * (($10 * $147 + ($4 * $135 - $8 * $144)) * $178) - $201 * (($10 * $141 + ($6 * $135 - $8 * $138)) * $178) - $211 * (($10 * $150 + ($4 * $138 - $6 * $144)) * $178)));
 HEAPF32[$3 >> 2] = ($178 * $74 + ($211 * ($68 * $178) + ($201 * ($57 * $178) + $209 * ($178 * $63)))) * $236;
 HEAPF32[$3 + 4 >> 2] = (($8 * $52 + ($4 * $43 - $6 * $49)) * $178 + ($209 * (($10 * $49 + ($4 * $37 - $8 * $46)) * $178) - $201 * (($10 * $43 + ($6 * $37 - $8 * $40)) * $178) - $211 * (($10 * $52 + ($4 * $40 - $6 * $46)) * $178))) * $236;
 HEAPF32[$3 + 8 >> 2] = ($211 * (($10 * $112 + ($4 * $100 - $6 * $106)) * $178) + ($201 * (($10 * $103 + ($6 * $97 - $8 * $100)) * $178) - $209 * (($10 * $109 + ($4 * $97 - $8 * $106)) * $178)) - ($8 * $112 + ($4 * $103 - $6 * $109)) * $178) * $236;
 return;
}

function _glmc_rotate($0, $1, $2) {
 $0 = $0 | 0;
 $1 = +$1;
 $2 = $2 | 0;
 var $$idx$val = 0.0, $$idx3$val = 0.0, $$sink$i$i$i = 0.0, $$sroa$011$0$i$i = 0.0, $$sroa$9$0$i$i = 0.0, $$val = 0.0, $10 = 0, $11 = 0.0, $15 = 0.0, $16 = 0.0, $17 = 0.0, $18 = 0.0, $19 = 0.0, $20 = 0.0, $21 = 0.0, $22 = 0.0, $3 = 0.0, $32 = 0.0, $33 = 0.0, $34 = 0.0, $35 = 0.0, $36 = 0.0, $37 = 0.0, $38 = 0.0, $39 = 0.0, $40 = 0.0, $41 = 0.0, $42 = 0, $43 = 0.0, $44 = 0, $45 = 0.0, $46 = 0, $47 = 0.0, $48 = 0, $49 = 0.0, $50 = 0, $51 = 0.0, $52 = 0, $53 = 0.0, $54 = 0, $55 = 0.0, $56 = 0, $57 = 0.0, $58 = 0, $59 = 0.0, $60 = 0, $61 = 0.0, $62 = 0, $63 = 0.0, $9 = 0.0;
 $$val = +HEAPF32[$2 >> 2];
 $$idx$val = +HEAPF32[$2 + 4 >> 2];
 $$idx3$val = +HEAPF32[$2 + 8 >> 2];
 $3 = +Math_cos(+$1);
 $9 = +Math_sqrt(+($$val * $$val + $$idx$val * $$idx$val + $$idx3$val * $$idx3$val));
 $10 = $9 == 0.0;
 $11 = 1.0 / $9;
 $$sroa$011$0$i$i = $10 ? 0.0 : $$val * $11;
 $$sroa$9$0$i$i = $10 ? 0.0 : $$idx$val * $11;
 $$sink$i$i$i = $10 ? 0.0 : $$idx3$val * $11;
 $15 = 1.0 - $3;
 $16 = $15 * $$sroa$011$0$i$i;
 $17 = $15 * $$sroa$9$0$i$i;
 $18 = $15 * $$sink$i$i$i;
 $19 = +Math_sin(+$1);
 $20 = $19 * $$sroa$011$0$i$i;
 $21 = $19 * $$sroa$9$0$i$i;
 $22 = $19 * $$sink$i$i$i;
 $32 = $3 + $$sroa$011$0$i$i * $16;
 $33 = $$sroa$011$0$i$i * $17 - $22;
 $34 = $21 + $$sroa$011$0$i$i * $18;
 $35 = $22 + $$sroa$9$0$i$i * $16;
 $36 = $3 + $$sroa$9$0$i$i * $17;
 $37 = $$sroa$9$0$i$i * $18 - $20;
 $38 = $$sink$i$i$i * $16 - $21;
 $39 = $20 + $$sink$i$i$i * $17;
 $40 = $3 + $$sink$i$i$i * $18;
 $41 = +HEAPF32[$0 >> 2];
 $42 = $0 + 4 | 0;
 $43 = +HEAPF32[$42 >> 2];
 $44 = $0 + 8 | 0;
 $45 = +HEAPF32[$44 >> 2];
 $46 = $0 + 12 | 0;
 $47 = +HEAPF32[$46 >> 2];
 $48 = $0 + 16 | 0;
 $49 = +HEAPF32[$48 >> 2];
 $50 = $0 + 20 | 0;
 $51 = +HEAPF32[$50 >> 2];
 $52 = $0 + 24 | 0;
 $53 = +HEAPF32[$52 >> 2];
 $54 = $0 + 28 | 0;
 $55 = +HEAPF32[$54 >> 2];
 $56 = $0 + 32 | 0;
 $57 = +HEAPF32[$56 >> 2];
 $58 = $0 + 36 | 0;
 $59 = +HEAPF32[$58 >> 2];
 $60 = $0 + 40 | 0;
 $61 = +HEAPF32[$60 >> 2];
 $62 = $0 + 44 | 0;
 $63 = +HEAPF32[$62 >> 2];
 HEAPF32[$0 >> 2] = $41 * $32 + $49 * $35 + $38 * $57;
 HEAPF32[$42 >> 2] = $43 * $32 + $35 * $51 + $38 * $59;
 HEAPF32[$44 >> 2] = $45 * $32 + $35 * $53 + $38 * $61;
 HEAPF32[$46 >> 2] = $47 * $32 + $35 * $55 + $38 * $63;
 HEAPF32[$48 >> 2] = $41 * $33 + $49 * $36 + $39 * $57;
 HEAPF32[$50 >> 2] = $43 * $33 + $36 * $51 + $39 * $59;
 HEAPF32[$52 >> 2] = $45 * $33 + $36 * $53 + $39 * $61;
 HEAPF32[$54 >> 2] = $47 * $33 + $36 * $55 + $39 * $63;
 HEAPF32[$56 >> 2] = $41 * $34 + $49 * $37 + $40 * $57;
 HEAPF32[$58 >> 2] = $43 * $34 + $37 * $51 + $40 * $59;
 HEAPF32[$60 >> 2] = $45 * $34 + $37 * $53 + $40 * $61;
 HEAPF32[$62 >> 2] = $47 * $34 + $37 * $55 + $40 * $63;
 return;
}

function _glmc_frustum_planes($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 var $10 = 0.0, $108 = 0.0, $119 = 0.0, $12 = 0.0, $130 = 0.0, $14 = 0.0, $141 = 0.0, $16 = 0.0, $18 = 0.0, $2 = 0.0, $20 = 0.0, $22 = 0.0, $24 = 0.0, $26 = 0.0, $28 = 0.0, $30 = 0.0, $32 = 0.0, $33 = 0.0, $34 = 0.0, $36 = 0.0, $4 = 0.0, $41 = 0.0, $42 = 0.0, $44 = 0.0, $49 = 0.0, $50 = 0.0, $52 = 0.0, $57 = 0.0, $58 = 0.0, $6 = 0.0, $60 = 0.0, $65 = 0.0, $66 = 0.0, $68 = 0.0, $73 = 0.0, $74 = 0.0, $76 = 0.0, $8 = 0.0, $86 = 0.0, $97 = 0.0;
 $2 = +HEAPF32[$0 >> 2];
 $4 = +HEAPF32[$0 + 4 >> 2];
 $6 = +HEAPF32[$0 + 16 >> 2];
 $8 = +HEAPF32[$0 + 20 >> 2];
 $10 = +HEAPF32[$0 + 32 >> 2];
 $12 = +HEAPF32[$0 + 36 >> 2];
 $14 = +HEAPF32[$0 + 48 >> 2];
 $16 = +HEAPF32[$0 + 52 >> 2];
 $18 = +HEAPF32[$0 + 8 >> 2];
 $20 = +HEAPF32[$0 + 12 >> 2];
 $22 = +HEAPF32[$0 + 24 >> 2];
 $24 = +HEAPF32[$0 + 28 >> 2];
 $26 = +HEAPF32[$0 + 40 >> 2];
 $28 = +HEAPF32[$0 + 44 >> 2];
 $30 = +HEAPF32[$0 + 56 >> 2];
 $32 = +HEAPF32[$0 + 60 >> 2];
 $33 = $2 + $20;
 $34 = $6 + $24;
 $36 = $10 + $28;
 $41 = $20 - $2;
 $42 = $24 - $6;
 $44 = $28 - $10;
 $49 = $4 + $20;
 $50 = $8 + $24;
 $52 = $12 + $28;
 $57 = $20 - $4;
 $58 = $24 - $8;
 $60 = $28 - $12;
 $65 = $18 + $20;
 $66 = $22 + $24;
 $68 = $26 + $28;
 $73 = $20 - $18;
 $74 = $24 - $22;
 $76 = $28 - $26;
 $86 = 1.0 / +Math_sqrt(+($33 * $33 + $34 * $34 + $36 * $36));
 HEAPF32[$1 >> 2] = $33 * $86;
 HEAPF32[$1 + 4 >> 2] = $34 * $86;
 HEAPF32[$1 + 8 >> 2] = $36 * $86;
 HEAPF32[$1 + 12 >> 2] = ($14 + $32) * $86;
 $97 = 1.0 / +Math_sqrt(+($41 * $41 + $42 * $42 + $44 * $44));
 HEAPF32[$1 + 16 >> 2] = $41 * $97;
 HEAPF32[$1 + 20 >> 2] = $42 * $97;
 HEAPF32[$1 + 24 >> 2] = $44 * $97;
 HEAPF32[$1 + 28 >> 2] = ($32 - $14) * $97;
 $108 = 1.0 / +Math_sqrt(+($49 * $49 + $50 * $50 + $52 * $52));
 HEAPF32[$1 + 32 >> 2] = $49 * $108;
 HEAPF32[$1 + 36 >> 2] = $50 * $108;
 HEAPF32[$1 + 40 >> 2] = $52 * $108;
 HEAPF32[$1 + 44 >> 2] = ($16 + $32) * $108;
 $119 = 1.0 / +Math_sqrt(+($57 * $57 + $58 * $58 + $60 * $60));
 HEAPF32[$1 + 48 >> 2] = $57 * $119;
 HEAPF32[$1 + 52 >> 2] = $58 * $119;
 HEAPF32[$1 + 56 >> 2] = $60 * $119;
 HEAPF32[$1 + 60 >> 2] = ($32 - $16) * $119;
 $130 = 1.0 / +Math_sqrt(+($65 * $65 + $66 * $66 + $68 * $68));
 HEAPF32[$1 + 64 >> 2] = $65 * $130;
 HEAPF32[$1 + 68 >> 2] = $66 * $130;
 HEAPF32[$1 + 72 >> 2] = $68 * $130;
 HEAPF32[$1 + 76 >> 2] = ($30 + $32) * $130;
 $141 = 1.0 / +Math_sqrt(+($73 * $73 + $74 * $74 + $76 * $76));
 HEAPF32[$1 + 80 >> 2] = $73 * $141;
 HEAPF32[$1 + 84 >> 2] = $74 * $141;
 HEAPF32[$1 + 88 >> 2] = $76 * $141;
 HEAPF32[$1 + 92 >> 2] = ($32 - $30) * $141;
 return;
}

function _glmc_mat4_mul($0, $1, $2) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = $2 | 0;
 var $11 = 0.0, $13 = 0.0, $15 = 0.0, $17 = 0.0, $19 = 0.0, $21 = 0.0, $23 = 0.0, $25 = 0.0, $27 = 0.0, $29 = 0.0, $3 = 0.0, $31 = 0.0, $33 = 0.0, $34 = 0.0, $36 = 0.0, $38 = 0.0, $40 = 0.0, $42 = 0.0, $44 = 0.0, $46 = 0.0, $48 = 0.0, $5 = 0.0, $50 = 0.0, $52 = 0.0, $54 = 0.0, $56 = 0.0, $58 = 0.0, $60 = 0.0, $62 = 0.0, $64 = 0.0, $7 = 0.0, $9 = 0.0;
 $3 = +HEAPF32[$0 >> 2];
 $5 = +HEAPF32[$0 + 4 >> 2];
 $7 = +HEAPF32[$0 + 8 >> 2];
 $9 = +HEAPF32[$0 + 12 >> 2];
 $11 = +HEAPF32[$0 + 16 >> 2];
 $13 = +HEAPF32[$0 + 20 >> 2];
 $15 = +HEAPF32[$0 + 24 >> 2];
 $17 = +HEAPF32[$0 + 28 >> 2];
 $19 = +HEAPF32[$0 + 32 >> 2];
 $21 = +HEAPF32[$0 + 36 >> 2];
 $23 = +HEAPF32[$0 + 40 >> 2];
 $25 = +HEAPF32[$0 + 44 >> 2];
 $27 = +HEAPF32[$0 + 48 >> 2];
 $29 = +HEAPF32[$0 + 52 >> 2];
 $31 = +HEAPF32[$0 + 56 >> 2];
 $33 = +HEAPF32[$0 + 60 >> 2];
 $34 = +HEAPF32[$1 >> 2];
 $36 = +HEAPF32[$1 + 4 >> 2];
 $38 = +HEAPF32[$1 + 8 >> 2];
 $40 = +HEAPF32[$1 + 12 >> 2];
 $42 = +HEAPF32[$1 + 16 >> 2];
 $44 = +HEAPF32[$1 + 20 >> 2];
 $46 = +HEAPF32[$1 + 24 >> 2];
 $48 = +HEAPF32[$1 + 28 >> 2];
 $50 = +HEAPF32[$1 + 32 >> 2];
 $52 = +HEAPF32[$1 + 36 >> 2];
 $54 = +HEAPF32[$1 + 40 >> 2];
 $56 = +HEAPF32[$1 + 44 >> 2];
 $58 = +HEAPF32[$1 + 48 >> 2];
 $60 = +HEAPF32[$1 + 52 >> 2];
 $62 = +HEAPF32[$1 + 56 >> 2];
 $64 = +HEAPF32[$1 + 60 >> 2];
 HEAPF32[$2 >> 2] = $3 * $34 + $11 * $36 + $19 * $38 + $27 * $40;
 HEAPF32[$2 + 4 >> 2] = $5 * $34 + $13 * $36 + $21 * $38 + $29 * $40;
 HEAPF32[$2 + 8 >> 2] = $7 * $34 + $15 * $36 + $23 * $38 + $31 * $40;
 HEAPF32[$2 + 12 >> 2] = $9 * $34 + $17 * $36 + $25 * $38 + $33 * $40;
 HEAPF32[$2 + 16 >> 2] = $3 * $42 + $11 * $44 + $19 * $46 + $27 * $48;
 HEAPF32[$2 + 20 >> 2] = $5 * $42 + $13 * $44 + $21 * $46 + $29 * $48;
 HEAPF32[$2 + 24 >> 2] = $7 * $42 + $15 * $44 + $23 * $46 + $31 * $48;
 HEAPF32[$2 + 28 >> 2] = $9 * $42 + $17 * $44 + $25 * $46 + $33 * $48;
 HEAPF32[$2 + 32 >> 2] = $3 * $50 + $11 * $52 + $19 * $54 + $27 * $56;
 HEAPF32[$2 + 36 >> 2] = $5 * $50 + $13 * $52 + $21 * $54 + $29 * $56;
 HEAPF32[$2 + 40 >> 2] = $7 * $50 + $15 * $52 + $23 * $54 + $31 * $56;
 HEAPF32[$2 + 44 >> 2] = $9 * $50 + $17 * $52 + $25 * $54 + $33 * $56;
 HEAPF32[$2 + 48 >> 2] = $3 * $58 + $11 * $60 + $19 * $62 + $27 * $64;
 HEAPF32[$2 + 52 >> 2] = $5 * $58 + $13 * $60 + $21 * $62 + $29 * $64;
 HEAPF32[$2 + 56 >> 2] = $7 * $58 + $15 * $60 + $23 * $62 + $31 * $64;
 HEAPF32[$2 + 60 >> 2] = $9 * $58 + $17 * $60 + $25 * $62 + $33 * $64;
 return;
}

function _glmc_quat_rotate($0, $1, $2) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = $2 | 0;
 var $10 = 0.0, $14 = 0.0, $17 = 0.0, $20 = 0.0, $21 = 0.0, $23 = 0.0, $24 = 0.0, $25 = 0.0, $26 = 0.0, $27 = 0.0, $28 = 0.0, $29 = 0.0, $3 = 0.0, $31 = 0.0, $32 = 0.0, $33 = 0.0, $35 = 0.0, $36 = 0.0, $37 = 0.0, $38 = 0.0, $39 = 0.0, $40 = 0.0, $41 = 0.0, $42 = 0.0, $43 = 0.0, $44 = 0.0, $45 = 0.0, $47 = 0.0, $49 = 0.0, $51 = 0.0, $53 = 0.0, $55 = 0.0, $57 = 0.0, $59 = 0.0, $6 = 0.0, $61 = 0.0, $63 = 0.0, $65 = 0.0, $67 = 0.0, $69 = 0, $71 = 0, $73 = 0, $75 = 0;
 $3 = +HEAPF32[$1 >> 2];
 $6 = +HEAPF32[$1 + 4 >> 2];
 $10 = +HEAPF32[$1 + 8 >> 2];
 $14 = +HEAPF32[$1 + 12 >> 2];
 $17 = +Math_sqrt(+($3 * $3 + $6 * $6 + $10 * $10 + $14 * $14));
 $20 = $17 > 0.0 ? 2.0 / $17 : 0.0;
 $21 = $3 * $20;
 $23 = $6 * $21;
 $24 = $14 * $20;
 $25 = $3 * $24;
 $26 = $6 * $20;
 $27 = $6 * $26;
 $28 = $10 * $26;
 $29 = $6 * $24;
 $31 = $10 * ($10 * $20);
 $32 = $10 * $21;
 $33 = $10 * $24;
 $35 = 1.0 - $27 - $31;
 $36 = 1.0 - $3 * $21;
 $37 = $36 - $31;
 $38 = $36 - $27;
 $39 = $23 + $33;
 $40 = $28 + $25;
 $41 = $32 + $29;
 $42 = $23 - $33;
 $43 = $28 - $25;
 $44 = $32 - $29;
 $45 = +HEAPF32[$0 >> 2];
 $47 = +HEAPF32[$0 + 4 >> 2];
 $49 = +HEAPF32[$0 + 8 >> 2];
 $51 = +HEAPF32[$0 + 12 >> 2];
 $53 = +HEAPF32[$0 + 16 >> 2];
 $55 = +HEAPF32[$0 + 20 >> 2];
 $57 = +HEAPF32[$0 + 24 >> 2];
 $59 = +HEAPF32[$0 + 28 >> 2];
 $61 = +HEAPF32[$0 + 32 >> 2];
 $63 = +HEAPF32[$0 + 36 >> 2];
 $65 = +HEAPF32[$0 + 40 >> 2];
 $67 = +HEAPF32[$0 + 44 >> 2];
 $69 = HEAP32[$0 + 48 >> 2] | 0;
 $71 = HEAP32[$0 + 52 >> 2] | 0;
 $73 = HEAP32[$0 + 56 >> 2] | 0;
 $75 = HEAP32[$0 + 60 >> 2] | 0;
 HEAPF32[$2 >> 2] = $53 * $39 + $45 * $35 + $44 * $61;
 HEAPF32[$2 + 4 >> 2] = $47 * $35 + $39 * $55 + $44 * $63;
 HEAPF32[$2 + 8 >> 2] = $49 * $35 + $39 * $57 + $44 * $65;
 HEAPF32[$2 + 12 >> 2] = $51 * $35 + $39 * $59 + $44 * $67;
 HEAPF32[$2 + 16 >> 2] = $45 * $42 + $53 * $37 + $40 * $61;
 HEAPF32[$2 + 20 >> 2] = $47 * $42 + $55 * $37 + $40 * $63;
 HEAPF32[$2 + 24 >> 2] = $49 * $42 + $37 * $57 + $40 * $65;
 HEAPF32[$2 + 28 >> 2] = $51 * $42 + $37 * $59 + $40 * $67;
 HEAPF32[$2 + 32 >> 2] = $45 * $41 + $53 * $43 + $38 * $61;
 HEAPF32[$2 + 36 >> 2] = $47 * $41 + $43 * $55 + $38 * $63;
 HEAPF32[$2 + 40 >> 2] = $49 * $41 + $43 * $57 + $38 * $65;
 HEAPF32[$2 + 44 >> 2] = $51 * $41 + $43 * $59 + $38 * $67;
 HEAP32[$2 + 48 >> 2] = $69;
 HEAP32[$2 + 52 >> 2] = $71;
 HEAP32[$2 + 56 >> 2] = $73;
 HEAP32[$2 + 60 >> 2] = $75;
 return;
}

function _glmc_look_anyup($0, $1, $2) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = $2 | 0;
 var $$idx$val = 0.0, $$idx3$val = 0.0, $$sroa$014$0$i$i$i = 0.0, $$sroa$12$0$i$i$i = 0.0, $$sroa$22$0$i$i$i = 0.0, $$val = 0.0, $11 = 0, $12 = 0.0, $14 = 0.0, $15 = 0.0, $16 = 0.0, $22 = 0.0, $23 = 0, $24 = 0.0, $3 = 0.0, $30 = 0.0, $33 = 0.0, $36 = 0.0, $4 = 0.0, $42 = 0.0, $43 = 0, $44 = 0.0, $48 = 0.0, $49 = 0.0, $5 = 0.0, $50 = 0.0, $53 = 0.0, $56 = 0.0, $59 = 0.0, $6 = 0.0, $8 = 0, $9 = 0.0;
 $$val = +HEAPF32[$1 >> 2];
 $$idx$val = +HEAPF32[$1 + 4 >> 2];
 $$idx3$val = +HEAPF32[$1 + 8 >> 2];
 $3 = $$idx$val - $$idx3$val;
 $4 = $$idx3$val - $$val;
 $5 = $$val - $$idx$val;
 $6 = +HEAPF32[$0 >> 2];
 $8 = $0 + 4 | 0;
 $9 = +HEAPF32[$8 >> 2];
 $11 = $0 + 8 | 0;
 $12 = +HEAPF32[$11 >> 2];
 $14 = $$val + $6 - $6;
 $15 = $$idx$val + $9 - $9;
 $16 = $$idx3$val + $12 - $12;
 $22 = +Math_sqrt(+($14 * $14 + $15 * $15 + $16 * $16));
 $23 = $22 == 0.0;
 $24 = 1.0 / $22;
 $$sroa$014$0$i$i$i = $23 ? 0.0 : $14 * $24;
 $$sroa$12$0$i$i$i = $23 ? 0.0 : $15 * $24;
 $$sroa$22$0$i$i$i = $23 ? 0.0 : $16 * $24;
 $30 = $5 * $$sroa$12$0$i$i$i - $4 * $$sroa$22$0$i$i$i;
 $33 = $3 * $$sroa$22$0$i$i$i - $5 * $$sroa$014$0$i$i$i;
 $36 = $4 * $$sroa$014$0$i$i$i - $3 * $$sroa$12$0$i$i$i;
 $42 = +Math_sqrt(+($36 * $36 + ($30 * $30 + $33 * $33)));
 $43 = $42 == 0.0;
 $44 = 1.0 / $42;
 $48 = $43 ? 0.0 : $44 * $36;
 $49 = $43 ? 0.0 : $44 * $33;
 $50 = $43 ? 0.0 : $44 * $30;
 $53 = $$sroa$22$0$i$i$i * $49 - $$sroa$12$0$i$i$i * $48;
 $56 = $$sroa$014$0$i$i$i * $48 - $$sroa$22$0$i$i$i * $50;
 $59 = $$sroa$12$0$i$i$i * $50 - $$sroa$014$0$i$i$i * $49;
 HEAPF32[$2 >> 2] = $50;
 HEAPF32[$2 + 4 >> 2] = $53;
 HEAPF32[$2 + 8 >> 2] = -$$sroa$014$0$i$i$i;
 HEAPF32[$2 + 16 >> 2] = $49;
 HEAPF32[$2 + 20 >> 2] = $56;
 HEAPF32[$2 + 24 >> 2] = -$$sroa$12$0$i$i$i;
 HEAPF32[$2 + 32 >> 2] = $48;
 HEAPF32[$2 + 36 >> 2] = $59;
 HEAPF32[$2 + 40 >> 2] = -$$sroa$22$0$i$i$i;
 HEAPF32[$2 + 48 >> 2] = -($50 * +HEAPF32[$0 >> 2] + $49 * +HEAPF32[$8 >> 2] + $48 * +HEAPF32[$11 >> 2]);
 HEAPF32[$2 + 52 >> 2] = -($53 * +HEAPF32[$0 >> 2] + $56 * +HEAPF32[$8 >> 2] + $59 * +HEAPF32[$11 >> 2]);
 HEAPF32[$2 + 56 >> 2] = $$sroa$014$0$i$i$i * +HEAPF32[$0 >> 2] + $$sroa$12$0$i$i$i * +HEAPF32[$8 >> 2] + $$sroa$22$0$i$i$i * +HEAPF32[$11 >> 2];
 HEAPF32[$2 + 44 >> 2] = 0.0;
 HEAPF32[$2 + 28 >> 2] = 0.0;
 HEAPF32[$2 + 12 >> 2] = 0.0;
 HEAPF32[$2 + 60 >> 2] = 1.0;
 return;
}

function _glmc_aabb_frustum($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 var $$020$i = 0, $110 = 0.0, $117 = 0.0, $125 = 0.0, $137 = 0.0, $144 = 0.0, $152 = 0.0, $17 = 0.0, $2 = 0.0, $29 = 0.0, $36 = 0.0, $44 = 0.0, $56 = 0.0, $63 = 0.0, $71 = 0.0, $83 = 0.0, $9 = 0.0, $90 = 0.0, $98 = 0.0;
 $2 = +HEAPF32[$1 >> 2];
 $9 = +HEAPF32[$1 + 4 >> 2];
 $17 = +HEAPF32[$1 + 8 >> 2];
 if ($2 * +HEAPF32[$0 + (($2 > 0.0 & 1) * 12 | 0) >> 2] + $9 * +HEAPF32[$0 + (($9 > 0.0 & 1) * 12 | 0) + 4 >> 2] + $17 * +HEAPF32[$0 + (($17 > 0.0 & 1) * 12 | 0) + 8 >> 2] < -+HEAPF32[$1 + 12 >> 2]) {
  $$020$i = 0;
  return $$020$i | 0;
 }
 $29 = +HEAPF32[$1 + 16 >> 2];
 $36 = +HEAPF32[$1 + 20 >> 2];
 $44 = +HEAPF32[$1 + 24 >> 2];
 if ($29 * +HEAPF32[$0 + (($29 > 0.0 & 1) * 12 | 0) >> 2] + $36 * +HEAPF32[$0 + (($36 > 0.0 & 1) * 12 | 0) + 4 >> 2] + $44 * +HEAPF32[$0 + (($44 > 0.0 & 1) * 12 | 0) + 8 >> 2] < -+HEAPF32[$1 + 28 >> 2]) {
  $$020$i = 0;
  return $$020$i | 0;
 }
 $56 = +HEAPF32[$1 + 32 >> 2];
 $63 = +HEAPF32[$1 + 36 >> 2];
 $71 = +HEAPF32[$1 + 40 >> 2];
 if ($56 * +HEAPF32[$0 + (($56 > 0.0 & 1) * 12 | 0) >> 2] + $63 * +HEAPF32[$0 + (($63 > 0.0 & 1) * 12 | 0) + 4 >> 2] + $71 * +HEAPF32[$0 + (($71 > 0.0 & 1) * 12 | 0) + 8 >> 2] < -+HEAPF32[$1 + 44 >> 2]) {
  $$020$i = 0;
  return $$020$i | 0;
 }
 $83 = +HEAPF32[$1 + 48 >> 2];
 $90 = +HEAPF32[$1 + 52 >> 2];
 $98 = +HEAPF32[$1 + 56 >> 2];
 if ($83 * +HEAPF32[$0 + (($83 > 0.0 & 1) * 12 | 0) >> 2] + $90 * +HEAPF32[$0 + (($90 > 0.0 & 1) * 12 | 0) + 4 >> 2] + $98 * +HEAPF32[$0 + (($98 > 0.0 & 1) * 12 | 0) + 8 >> 2] < -+HEAPF32[$1 + 60 >> 2]) {
  $$020$i = 0;
  return $$020$i | 0;
 }
 $110 = +HEAPF32[$1 + 64 >> 2];
 $117 = +HEAPF32[$1 + 68 >> 2];
 $125 = +HEAPF32[$1 + 72 >> 2];
 if ($110 * +HEAPF32[$0 + (($110 > 0.0 & 1) * 12 | 0) >> 2] + $117 * +HEAPF32[$0 + (($117 > 0.0 & 1) * 12 | 0) + 4 >> 2] + $125 * +HEAPF32[$0 + (($125 > 0.0 & 1) * 12 | 0) + 8 >> 2] < -+HEAPF32[$1 + 76 >> 2]) {
  $$020$i = 0;
  return $$020$i | 0;
 }
 $137 = +HEAPF32[$1 + 80 >> 2];
 $144 = +HEAPF32[$1 + 84 >> 2];
 $152 = +HEAPF32[$1 + 88 >> 2];
 if ($137 * +HEAPF32[$0 + (($137 > 0.0 & 1) * 12 | 0) >> 2] + $144 * +HEAPF32[$0 + (($144 > 0.0 & 1) * 12 | 0) + 4 >> 2] + $152 * +HEAPF32[$0 + (($152 > 0.0 & 1) * 12 | 0) + 8 >> 2] < -+HEAPF32[$1 + 92 >> 2]) {
  $$020$i = 0;
  return $$020$i | 0;
 }
 $$020$i = 1;
 return $$020$i | 0;
}

function _glmc_look($0, $1, $2, $3) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = $2 | 0;
 $3 = $3 | 0;
 var $$sroa$014$0$i$i = 0.0, $$sroa$12$0$i$i = 0.0, $$sroa$22$0$i$i = 0.0, $10 = 0.0, $12 = 0.0, $13 = 0.0, $14 = 0.0, $20 = 0.0, $21 = 0, $22 = 0.0, $27 = 0.0, $30 = 0.0, $32 = 0.0, $33 = 0.0, $36 = 0.0, $39 = 0.0, $4 = 0.0, $45 = 0.0, $46 = 0, $47 = 0.0, $51 = 0.0, $52 = 0.0, $53 = 0.0, $56 = 0.0, $59 = 0.0, $6 = 0, $62 = 0.0, $7 = 0.0, $9 = 0;
 $4 = +HEAPF32[$0 >> 2];
 $6 = $0 + 4 | 0;
 $7 = +HEAPF32[$6 >> 2];
 $9 = $0 + 8 | 0;
 $10 = +HEAPF32[$9 >> 2];
 $12 = +HEAPF32[$1 >> 2] + $4 - $4;
 $13 = +HEAPF32[$1 + 4 >> 2] + $7 - $7;
 $14 = +HEAPF32[$1 + 8 >> 2] + $10 - $10;
 $20 = +Math_sqrt(+($12 * $12 + $13 * $13 + $14 * $14));
 $21 = $20 == 0.0;
 $22 = 1.0 / $20;
 $$sroa$014$0$i$i = $21 ? 0.0 : $12 * $22;
 $$sroa$12$0$i$i = $21 ? 0.0 : $13 * $22;
 $$sroa$22$0$i$i = $21 ? 0.0 : $14 * $22;
 $27 = +HEAPF32[$2 + 8 >> 2];
 $30 = +HEAPF32[$2 + 4 >> 2];
 $32 = $27 * $$sroa$12$0$i$i - $30 * $$sroa$22$0$i$i;
 $33 = +HEAPF32[$2 >> 2];
 $36 = $$sroa$22$0$i$i * $33 - $27 * $$sroa$014$0$i$i;
 $39 = $30 * $$sroa$014$0$i$i - $$sroa$12$0$i$i * $33;
 $45 = +Math_sqrt(+($39 * $39 + ($32 * $32 + $36 * $36)));
 $46 = $45 == 0.0;
 $47 = 1.0 / $45;
 $51 = $46 ? 0.0 : $47 * $39;
 $52 = $46 ? 0.0 : $47 * $36;
 $53 = $46 ? 0.0 : $32 * $47;
 $56 = $$sroa$22$0$i$i * $52 - $$sroa$12$0$i$i * $51;
 $59 = $$sroa$014$0$i$i * $51 - $$sroa$22$0$i$i * $53;
 $62 = $$sroa$12$0$i$i * $53 - $$sroa$014$0$i$i * $52;
 HEAPF32[$3 >> 2] = $53;
 HEAPF32[$3 + 4 >> 2] = $56;
 HEAPF32[$3 + 8 >> 2] = -$$sroa$014$0$i$i;
 HEAPF32[$3 + 16 >> 2] = $52;
 HEAPF32[$3 + 20 >> 2] = $59;
 HEAPF32[$3 + 24 >> 2] = -$$sroa$12$0$i$i;
 HEAPF32[$3 + 32 >> 2] = $51;
 HEAPF32[$3 + 36 >> 2] = $62;
 HEAPF32[$3 + 40 >> 2] = -$$sroa$22$0$i$i;
 HEAPF32[$3 + 48 >> 2] = -($53 * +HEAPF32[$0 >> 2] + $52 * +HEAPF32[$6 >> 2] + $51 * +HEAPF32[$9 >> 2]);
 HEAPF32[$3 + 52 >> 2] = -($56 * +HEAPF32[$0 >> 2] + $59 * +HEAPF32[$6 >> 2] + $62 * +HEAPF32[$9 >> 2]);
 HEAPF32[$3 + 56 >> 2] = $$sroa$014$0$i$i * +HEAPF32[$0 >> 2] + $$sroa$12$0$i$i * +HEAPF32[$6 >> 2] + $$sroa$22$0$i$i * +HEAPF32[$9 >> 2];
 HEAPF32[$3 + 44 >> 2] = 0.0;
 HEAPF32[$3 + 28 >> 2] = 0.0;
 HEAPF32[$3 + 12 >> 2] = 0.0;
 HEAPF32[$3 + 60 >> 2] = 1.0;
 return;
}

function _glmc_mul($0, $1, $2) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = $2 | 0;
 var $11 = 0.0, $13 = 0.0, $15 = 0.0, $17 = 0.0, $19 = 0.0, $21 = 0.0, $23 = 0.0, $25 = 0.0, $27 = 0.0, $29 = 0.0, $3 = 0.0, $31 = 0.0, $33 = 0.0, $34 = 0.0, $36 = 0.0, $38 = 0.0, $40 = 0.0, $42 = 0.0, $44 = 0.0, $46 = 0.0, $48 = 0.0, $5 = 0.0, $50 = 0.0, $52 = 0.0, $54 = 0.0, $56 = 0.0, $58 = 0.0, $7 = 0.0, $9 = 0.0;
 $3 = +HEAPF32[$0 >> 2];
 $5 = +HEAPF32[$0 + 4 >> 2];
 $7 = +HEAPF32[$0 + 8 >> 2];
 $9 = +HEAPF32[$0 + 12 >> 2];
 $11 = +HEAPF32[$0 + 16 >> 2];
 $13 = +HEAPF32[$0 + 20 >> 2];
 $15 = +HEAPF32[$0 + 24 >> 2];
 $17 = +HEAPF32[$0 + 28 >> 2];
 $19 = +HEAPF32[$0 + 32 >> 2];
 $21 = +HEAPF32[$0 + 36 >> 2];
 $23 = +HEAPF32[$0 + 40 >> 2];
 $25 = +HEAPF32[$0 + 44 >> 2];
 $27 = +HEAPF32[$0 + 48 >> 2];
 $29 = +HEAPF32[$0 + 52 >> 2];
 $31 = +HEAPF32[$0 + 56 >> 2];
 $33 = +HEAPF32[$0 + 60 >> 2];
 $34 = +HEAPF32[$1 >> 2];
 $36 = +HEAPF32[$1 + 4 >> 2];
 $38 = +HEAPF32[$1 + 8 >> 2];
 $40 = +HEAPF32[$1 + 16 >> 2];
 $42 = +HEAPF32[$1 + 20 >> 2];
 $44 = +HEAPF32[$1 + 24 >> 2];
 $46 = +HEAPF32[$1 + 32 >> 2];
 $48 = +HEAPF32[$1 + 36 >> 2];
 $50 = +HEAPF32[$1 + 40 >> 2];
 $52 = +HEAPF32[$1 + 48 >> 2];
 $54 = +HEAPF32[$1 + 52 >> 2];
 $56 = +HEAPF32[$1 + 56 >> 2];
 $58 = +HEAPF32[$1 + 60 >> 2];
 HEAPF32[$2 >> 2] = $3 * $34 + $11 * $36 + $19 * $38;
 HEAPF32[$2 + 4 >> 2] = $5 * $34 + $13 * $36 + $21 * $38;
 HEAPF32[$2 + 8 >> 2] = $7 * $34 + $15 * $36 + $23 * $38;
 HEAPF32[$2 + 12 >> 2] = $9 * $34 + $17 * $36 + $25 * $38;
 HEAPF32[$2 + 16 >> 2] = $3 * $40 + $11 * $42 + $19 * $44;
 HEAPF32[$2 + 20 >> 2] = $5 * $40 + $13 * $42 + $21 * $44;
 HEAPF32[$2 + 24 >> 2] = $7 * $40 + $15 * $42 + $23 * $44;
 HEAPF32[$2 + 28 >> 2] = $9 * $40 + $17 * $42 + $25 * $44;
 HEAPF32[$2 + 32 >> 2] = $3 * $46 + $11 * $48 + $19 * $50;
 HEAPF32[$2 + 36 >> 2] = $5 * $46 + $13 * $48 + $21 * $50;
 HEAPF32[$2 + 40 >> 2] = $7 * $46 + $15 * $48 + $23 * $50;
 HEAPF32[$2 + 44 >> 2] = $9 * $46 + $17 * $48 + $25 * $50;
 HEAPF32[$2 + 48 >> 2] = $3 * $52 + $11 * $54 + $19 * $56 + $27 * $58;
 HEAPF32[$2 + 52 >> 2] = $5 * $52 + $13 * $54 + $21 * $56 + $29 * $58;
 HEAPF32[$2 + 56 >> 2] = $7 * $52 + $15 * $54 + $23 * $56 + $31 * $58;
 HEAPF32[$2 + 60 >> 2] = $9 * $52 + $17 * $54 + $25 * $56 + $33 * $58;
 return;
}

function _glmc_quat_slerp($0, $1, $2, $3) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = +$2;
 $3 = $3 | 0;
 var $$$i$i$i$i$i = 0.0, $$$i1$i$i$i$i = 0.0, $$0$i = 0.0, $$sink$i = 0.0, $$sroa$031$0$in$i = 0.0, $$sroa$11$0$in$i = 0.0, $$sroa$20$0$in$i = 0.0, $$sroa$29$0$in$i = 0.0, $10 = 0.0, $13 = 0, $14 = 0.0, $16 = 0.0, $19 = 0, $20 = 0.0, $22 = 0.0, $24 = 0.0, $37 = 0.0, $4 = 0.0, $5 = 0.0, $59 = 0.0, $62 = 0.0, $68 = 0.0, $7 = 0, $77 = 0.0, $8 = 0.0, $84 = 0;
 $4 = +HEAPF32[$0 >> 2];
 $5 = +HEAPF32[$1 >> 2];
 $7 = $0 + 4 | 0;
 $8 = +HEAPF32[$7 >> 2];
 $10 = +HEAPF32[$1 + 4 >> 2];
 $13 = $0 + 8 | 0;
 $14 = +HEAPF32[$13 >> 2];
 $16 = +HEAPF32[$1 + 8 >> 2];
 $19 = $0 + 12 | 0;
 $20 = +HEAPF32[$19 >> 2];
 $22 = +HEAPF32[$1 + 12 >> 2];
 $24 = $4 * $5 + $8 * $10 + $14 * $16 + $20 * $22;
 if (+Math_abs(+$24) >= 1.0) {
  HEAPF32[$3 >> 2] = $4;
  HEAPF32[$3 + 4 >> 2] = $8;
  HEAPF32[$3 + 8 >> 2] = $14;
  $$sink$i = $20;
  $84 = $3 + 12 | 0;
  HEAPF32[$84 >> 2] = $$sink$i;
  return;
 }
 if ($24 < 0.0) {
  $$0$i = -$24;
  $$sroa$031$0$in$i = -$4;
  $$sroa$11$0$in$i = -$8;
  $$sroa$20$0$in$i = -$14;
  $$sroa$29$0$in$i = -$20;
 } else {
  $$0$i = $24;
  $$sroa$031$0$in$i = $4;
  $$sroa$11$0$in$i = $8;
  $$sroa$20$0$in$i = $14;
  $$sroa$29$0$in$i = $20;
 }
 $37 = +Math_sqrt(+(1.0 - $$0$i * $$0$i));
 if (+Math_abs(+$37) < 1.0000000474974513e-03) {
  $$$i$i$i$i$i = $2 > 0.0 ? $2 : 0.0;
  $$$i1$i$i$i$i = $$$i$i$i$i$i < 1.0 ? $$$i$i$i$i$i : 1.0;
  HEAPF32[$3 >> 2] = $4 + $$$i1$i$i$i$i * ($5 - $4);
  HEAPF32[$3 + 4 >> 2] = $$$i1$i$i$i$i * ($10 - $8) + +HEAPF32[$7 >> 2];
  HEAPF32[$3 + 8 >> 2] = $$$i1$i$i$i$i * ($16 - $14) + +HEAPF32[$13 >> 2];
  $$sink$i = $$$i1$i$i$i$i * ($22 - $20) + +HEAPF32[$19 >> 2];
  $84 = $3 + 12 | 0;
  HEAPF32[$84 >> 2] = $$sink$i;
  return;
 } else {
  $59 = +Math_acos(+$$0$i);
  $62 = +Math_sin(+((1.0 - $2) * $59));
  $68 = +Math_sin(+($59 * $2));
  $77 = 1.0 / $37;
  HEAPF32[$3 >> 2] = $77 * ($$sroa$031$0$in$i * $62 + $5 * $68);
  HEAPF32[$3 + 4 >> 2] = $77 * ($$sroa$11$0$in$i * $62 + $10 * $68);
  HEAPF32[$3 + 8 >> 2] = $77 * ($$sroa$20$0$in$i * $62 + $16 * $68);
  $$sink$i = $77 * ($$sroa$29$0$in$i * $62 + $22 * $68);
  $84 = $3 + 12 | 0;
  HEAPF32[$84 >> 2] = $$sink$i;
  return;
 }
}

function runPostSets() {}
function _memcpy(dest, src, num) {
 dest = dest | 0;
 src = src | 0;
 num = num | 0;
 var ret = 0, aligned_dest_end = 0, block_aligned_dest_end = 0, dest_end = 0;
 if ((num | 0) >= 8192) return _emscripten_memcpy_big(dest | 0, src | 0, num | 0) | 0;
 ret = dest | 0;
 dest_end = dest + num | 0;
 if ((dest & 3) == (src & 3)) {
  while (dest & 3) {
   if (!num) return ret | 0;
   HEAP8[dest >> 0] = HEAP8[src >> 0] | 0;
   dest = dest + 1 | 0;
   src = src + 1 | 0;
   num = num - 1 | 0;
  }
  aligned_dest_end = dest_end & -4 | 0;
  block_aligned_dest_end = aligned_dest_end - 64 | 0;
  while ((dest | 0) <= (block_aligned_dest_end | 0)) {
   HEAP32[dest >> 2] = HEAP32[src >> 2];
   HEAP32[dest + 4 >> 2] = HEAP32[src + 4 >> 2];
   HEAP32[dest + 8 >> 2] = HEAP32[src + 8 >> 2];
   HEAP32[dest + 12 >> 2] = HEAP32[src + 12 >> 2];
   HEAP32[dest + 16 >> 2] = HEAP32[src + 16 >> 2];
   HEAP32[dest + 20 >> 2] = HEAP32[src + 20 >> 2];
   HEAP32[dest + 24 >> 2] = HEAP32[src + 24 >> 2];
   HEAP32[dest + 28 >> 2] = HEAP32[src + 28 >> 2];
   HEAP32[dest + 32 >> 2] = HEAP32[src + 32 >> 2];
   HEAP32[dest + 36 >> 2] = HEAP32[src + 36 >> 2];
   HEAP32[dest + 40 >> 2] = HEAP32[src + 40 >> 2];
   HEAP32[dest + 44 >> 2] = HEAP32[src + 44 >> 2];
   HEAP32[dest + 48 >> 2] = HEAP32[src + 48 >> 2];
   HEAP32[dest + 52 >> 2] = HEAP32[src + 52 >> 2];
   HEAP32[dest + 56 >> 2] = HEAP32[src + 56 >> 2];
   HEAP32[dest + 60 >> 2] = HEAP32[src + 60 >> 2];
   dest = dest + 64 | 0;
   src = src + 64 | 0;
  }
  while ((dest | 0) < (aligned_dest_end | 0)) {
   HEAP32[dest >> 2] = HEAP32[src >> 2];
   dest = dest + 4 | 0;
   src = src + 4 | 0;
  }
 } else {
  aligned_dest_end = dest_end - 4 | 0;
  while ((dest | 0) < (aligned_dest_end | 0)) {
   HEAP8[dest >> 0] = HEAP8[src >> 0] | 0;
   HEAP8[dest + 1 >> 0] = HEAP8[src + 1 >> 0] | 0;
   HEAP8[dest + 2 >> 0] = HEAP8[src + 2 >> 0] | 0;
   HEAP8[dest + 3 >> 0] = HEAP8[src + 3 >> 0] | 0;
   dest = dest + 4 | 0;
   src = src + 4 | 0;
  }
 }
 while ((dest | 0) < (dest_end | 0)) {
  HEAP8[dest >> 0] = HEAP8[src >> 0] | 0;
  dest = dest + 1 | 0;
  src = src + 1 | 0;
 }
 return ret | 0;
}

function _glmc_lookat($0, $1, $2, $3) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = $2 | 0;
 $3 = $3 | 0;
 var $$sroa$014$0$i = 0.0, $$sroa$12$0$i = 0.0, $$sroa$22$0$i = 0.0, $11 = 0.0, $17 = 0.0, $18 = 0, $19 = 0.0, $24 = 0.0, $27 = 0.0, $29 = 0.0, $30 = 0.0, $33 = 0.0, $36 = 0.0, $42 = 0.0, $43 = 0, $44 = 0.0, $48 = 0.0, $49 = 0.0, $5 = 0.0, $50 = 0.0, $53 = 0.0, $56 = 0.0, $59 = 0.0, $6 = 0, $8 = 0.0, $9 = 0;
 $5 = +HEAPF32[$1 >> 2] - +HEAPF32[$0 >> 2];
 $6 = $0 + 4 | 0;
 $8 = +HEAPF32[$1 + 4 >> 2] - +HEAPF32[$6 >> 2];
 $9 = $0 + 8 | 0;
 $11 = +HEAPF32[$1 + 8 >> 2] - +HEAPF32[$9 >> 2];
 $17 = +Math_sqrt(+($5 * $5 + $8 * $8 + $11 * $11));
 $18 = $17 == 0.0;
 $19 = 1.0 / $17;
 $$sroa$014$0$i = $18 ? 0.0 : $5 * $19;
 $$sroa$12$0$i = $18 ? 0.0 : $8 * $19;
 $$sroa$22$0$i = $18 ? 0.0 : $11 * $19;
 $24 = +HEAPF32[$2 + 8 >> 2];
 $27 = +HEAPF32[$2 + 4 >> 2];
 $29 = $24 * $$sroa$12$0$i - $27 * $$sroa$22$0$i;
 $30 = +HEAPF32[$2 >> 2];
 $33 = $$sroa$22$0$i * $30 - $24 * $$sroa$014$0$i;
 $36 = $27 * $$sroa$014$0$i - $$sroa$12$0$i * $30;
 $42 = +Math_sqrt(+($36 * $36 + ($29 * $29 + $33 * $33)));
 $43 = $42 == 0.0;
 $44 = 1.0 / $42;
 $48 = $43 ? 0.0 : $44 * $36;
 $49 = $43 ? 0.0 : $44 * $33;
 $50 = $43 ? 0.0 : $29 * $44;
 $53 = $$sroa$22$0$i * $49 - $$sroa$12$0$i * $48;
 $56 = $$sroa$014$0$i * $48 - $$sroa$22$0$i * $50;
 $59 = $$sroa$12$0$i * $50 - $$sroa$014$0$i * $49;
 HEAPF32[$3 >> 2] = $50;
 HEAPF32[$3 + 4 >> 2] = $53;
 HEAPF32[$3 + 8 >> 2] = -$$sroa$014$0$i;
 HEAPF32[$3 + 16 >> 2] = $49;
 HEAPF32[$3 + 20 >> 2] = $56;
 HEAPF32[$3 + 24 >> 2] = -$$sroa$12$0$i;
 HEAPF32[$3 + 32 >> 2] = $48;
 HEAPF32[$3 + 36 >> 2] = $59;
 HEAPF32[$3 + 40 >> 2] = -$$sroa$22$0$i;
 HEAPF32[$3 + 48 >> 2] = -($50 * +HEAPF32[$0 >> 2] + $49 * +HEAPF32[$6 >> 2] + $48 * +HEAPF32[$9 >> 2]);
 HEAPF32[$3 + 52 >> 2] = -($53 * +HEAPF32[$0 >> 2] + $56 * +HEAPF32[$6 >> 2] + $59 * +HEAPF32[$9 >> 2]);
 HEAPF32[$3 + 56 >> 2] = $$sroa$014$0$i * +HEAPF32[$0 >> 2] + $$sroa$12$0$i * +HEAPF32[$6 >> 2] + $$sroa$22$0$i * +HEAPF32[$9 >> 2];
 HEAPF32[$3 + 44 >> 2] = 0.0;
 HEAPF32[$3 + 28 >> 2] = 0.0;
 HEAPF32[$3 + 12 >> 2] = 0.0;
 HEAPF32[$3 + 60 >> 2] = 1.0;
 return;
}

function _glmc_mat4_quat($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 var $$sink$sink$sink$i = 0.0, $10 = 0.0, $108 = 0, $13 = 0.0, $14 = 0.0, $2 = 0.0, $3 = 0, $39 = 0.0, $40 = 0.0, $5 = 0.0, $61 = 0.0, $64 = 0.0, $65 = 0.0, $7 = 0, $87 = 0.0, $88 = 0.0, $9 = 0.0;
 $2 = +HEAPF32[$0 >> 2];
 $3 = $0 + 16 | 0;
 $5 = +HEAPF32[$0 + 20 >> 2];
 $7 = $0 + 32 | 0;
 $9 = +HEAPF32[$0 + 40 >> 2];
 $10 = $2 + $5 + $9;
 if ($10 >= 0.0) {
  $13 = +Math_sqrt(+($10 + 1.0));
  $14 = .5 / $13;
  HEAPF32[$1 >> 2] = $14 * (+HEAPF32[$0 + 24 >> 2] - +HEAPF32[$0 + 36 >> 2]);
  HEAPF32[$1 + 4 >> 2] = $14 * (+HEAPF32[$7 >> 2] - +HEAPF32[$0 + 8 >> 2]);
  HEAPF32[$1 + 8 >> 2] = $14 * (+HEAPF32[$0 + 4 >> 2] - +HEAPF32[$3 >> 2]);
  $$sink$sink$sink$i = $13 * .5;
  $108 = $1 + 12 | 0;
  HEAPF32[$108 >> 2] = $$sink$sink$sink$i;
  return;
 }
 if (!(!($2 >= $5) | !($2 >= $9))) {
  $39 = +Math_sqrt(+($2 + (1.0 - $5 - $9)));
  $40 = .5 / $39;
  HEAPF32[$1 >> 2] = $39 * .5;
  HEAPF32[$1 + 4 >> 2] = $40 * (+HEAPF32[$0 + 4 >> 2] + +HEAPF32[$3 >> 2]);
  HEAPF32[$1 + 8 >> 2] = $40 * (+HEAPF32[$0 + 8 >> 2] + +HEAPF32[$7 >> 2]);
  $$sink$sink$sink$i = $40 * (+HEAPF32[$0 + 24 >> 2] - +HEAPF32[$0 + 36 >> 2]);
  $108 = $1 + 12 | 0;
  HEAPF32[$108 >> 2] = $$sink$sink$sink$i;
  return;
 }
 $61 = 1.0 - $2;
 if (!($5 >= $9)) {
  $87 = +Math_sqrt(+($61 - $5 + $9));
  $88 = .5 / $87;
  HEAPF32[$1 >> 2] = $88 * (+HEAPF32[$0 + 8 >> 2] + +HEAPF32[$7 >> 2]);
  HEAPF32[$1 + 4 >> 2] = $88 * (+HEAPF32[$0 + 24 >> 2] + +HEAPF32[$0 + 36 >> 2]);
  HEAPF32[$1 + 8 >> 2] = $87 * .5;
  $$sink$sink$sink$i = $88 * (+HEAPF32[$0 + 4 >> 2] - +HEAPF32[$3 >> 2]);
  $108 = $1 + 12 | 0;
  HEAPF32[$108 >> 2] = $$sink$sink$sink$i;
  return;
 } else {
  $64 = +Math_sqrt(+($5 + ($61 - $9)));
  $65 = .5 / $64;
  HEAPF32[$1 >> 2] = $65 * (+HEAPF32[$0 + 4 >> 2] + +HEAPF32[$3 >> 2]);
  HEAPF32[$1 + 4 >> 2] = $64 * .5;
  HEAPF32[$1 + 8 >> 2] = $65 * (+HEAPF32[$0 + 24 >> 2] + +HEAPF32[$0 + 36 >> 2]);
  $$sink$sink$sink$i = $65 * (+HEAPF32[$7 >> 2] - +HEAPF32[$0 + 8 >> 2]);
  $108 = $1 + 12 | 0;
  HEAPF32[$108 >> 2] = $$sink$sink$sink$i;
  return;
 }
}

function _glmc_mat3_quat($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 var $$sink$sink$sink$i = 0.0, $10 = 0.0, $108 = 0, $13 = 0.0, $14 = 0.0, $2 = 0.0, $3 = 0, $39 = 0.0, $40 = 0.0, $5 = 0.0, $61 = 0.0, $64 = 0.0, $65 = 0.0, $7 = 0, $87 = 0.0, $88 = 0.0, $9 = 0.0;
 $2 = +HEAPF32[$0 >> 2];
 $3 = $0 + 12 | 0;
 $5 = +HEAPF32[$0 + 16 >> 2];
 $7 = $0 + 24 | 0;
 $9 = +HEAPF32[$0 + 32 >> 2];
 $10 = $2 + $5 + $9;
 if ($10 >= 0.0) {
  $13 = +Math_sqrt(+($10 + 1.0));
  $14 = .5 / $13;
  HEAPF32[$1 >> 2] = $14 * (+HEAPF32[$0 + 20 >> 2] - +HEAPF32[$0 + 28 >> 2]);
  HEAPF32[$1 + 4 >> 2] = $14 * (+HEAPF32[$7 >> 2] - +HEAPF32[$0 + 8 >> 2]);
  HEAPF32[$1 + 8 >> 2] = $14 * (+HEAPF32[$0 + 4 >> 2] - +HEAPF32[$3 >> 2]);
  $$sink$sink$sink$i = $13 * .5;
  $108 = $1 + 12 | 0;
  HEAPF32[$108 >> 2] = $$sink$sink$sink$i;
  return;
 }
 if (!(!($2 >= $5) | !($2 >= $9))) {
  $39 = +Math_sqrt(+($2 + (1.0 - $5 - $9)));
  $40 = .5 / $39;
  HEAPF32[$1 >> 2] = $39 * .5;
  HEAPF32[$1 + 4 >> 2] = $40 * (+HEAPF32[$0 + 4 >> 2] + +HEAPF32[$3 >> 2]);
  HEAPF32[$1 + 8 >> 2] = $40 * (+HEAPF32[$0 + 8 >> 2] + +HEAPF32[$7 >> 2]);
  $$sink$sink$sink$i = $40 * (+HEAPF32[$0 + 20 >> 2] - +HEAPF32[$0 + 28 >> 2]);
  $108 = $1 + 12 | 0;
  HEAPF32[$108 >> 2] = $$sink$sink$sink$i;
  return;
 }
 $61 = 1.0 - $2;
 if (!($5 >= $9)) {
  $87 = +Math_sqrt(+($61 - $5 + $9));
  $88 = .5 / $87;
  HEAPF32[$1 >> 2] = $88 * (+HEAPF32[$0 + 8 >> 2] + +HEAPF32[$7 >> 2]);
  HEAPF32[$1 + 4 >> 2] = $88 * (+HEAPF32[$0 + 20 >> 2] + +HEAPF32[$0 + 28 >> 2]);
  HEAPF32[$1 + 8 >> 2] = $87 * .5;
  $$sink$sink$sink$i = $88 * (+HEAPF32[$0 + 4 >> 2] - +HEAPF32[$3 >> 2]);
  $108 = $1 + 12 | 0;
  HEAPF32[$108 >> 2] = $$sink$sink$sink$i;
  return;
 } else {
  $64 = +Math_sqrt(+($5 + ($61 - $9)));
  $65 = .5 / $64;
  HEAPF32[$1 >> 2] = $65 * (+HEAPF32[$0 + 4 >> 2] + +HEAPF32[$3 >> 2]);
  HEAPF32[$1 + 4 >> 2] = $64 * .5;
  HEAPF32[$1 + 8 >> 2] = $65 * (+HEAPF32[$0 + 20 >> 2] + +HEAPF32[$0 + 28 >> 2]);
  $$sink$sink$sink$i = $65 * (+HEAPF32[$7 >> 2] - +HEAPF32[$0 + 8 >> 2]);
  $108 = $1 + 12 | 0;
  HEAPF32[$108 >> 2] = $$sink$sink$sink$i;
  return;
 }
}

function _glmc_quat_forp($0, $1, $2, $3, $4) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = $2 | 0;
 $3 = $3 | 0;
 $4 = $4 | 0;
 var $$idx10$val = 0.0, $$idx11$val = 0.0, $$sroa$0$0$i$i = 0.0, $$sroa$12$0$i$i = 0.0, $$sroa$7$0$i$i = 0.0, $$val9 = 0.0, $12 = 0.0, $18 = 0, $20 = 0, $30 = 0.0, $33 = 0.0, $36 = 0.0, $39 = 0.0, $45 = 0.0, $46 = 0, $47 = 0.0, $5 = 0.0, $51 = 0.0, $52 = 0.0, $53 = 0.0, $59 = 0.0, $6 = 0.0, $60 = 0, $61 = 0.0, $7 = 0.0;
 $$val9 = +HEAPF32[$2 >> 2];
 $$idx10$val = +HEAPF32[$2 + 4 >> 2];
 $$idx11$val = +HEAPF32[$2 + 8 >> 2];
 $5 = +HEAPF32[$1 >> 2] - +HEAPF32[$0 >> 2];
 $6 = +HEAPF32[$1 + 4 >> 2] - +HEAPF32[$0 + 4 >> 2];
 $7 = +HEAPF32[$1 + 8 >> 2] - +HEAPF32[$0 + 8 >> 2];
 $12 = $5 * $$val9 + $6 * $$idx10$val + $7 * $$idx11$val;
 if (+Math_abs(+($12 + 1.0)) < 9.999999974752427e-07) {
  $18 = HEAP32[$3 + 4 >> 2] | 0;
  $20 = HEAP32[$3 + 8 >> 2] | 0;
  HEAP32[$4 >> 2] = HEAP32[$3 >> 2];
  HEAP32[$4 + 4 >> 2] = $18;
  HEAP32[$4 + 8 >> 2] = $20;
  HEAPF32[$4 + 12 >> 2] = 3.1415927410125732;
  return;
 }
 if (+Math_abs(+($12 + -1.0)) < 9.999999974752427e-07) {
  HEAP32[$4 >> 2] = 0;
  HEAP32[$4 + 4 >> 2] = 0;
  HEAP32[$4 + 8 >> 2] = 0;
  HEAP32[$4 + 12 >> 2] = 1065353216;
  return;
 } else {
  $30 = +Math_acos(+$12);
  $33 = $7 * $$idx10$val - $6 * $$idx11$val;
  $36 = $5 * $$idx11$val - $7 * $$val9;
  $39 = $6 * $$val9 - $5 * $$idx10$val;
  $45 = +Math_sqrt(+($39 * $39 + ($33 * $33 + $36 * $36)));
  $46 = $45 == 0.0;
  $47 = 1.0 / $45;
  $$sroa$12$0$i$i = $46 ? 0.0 : $39 * $47;
  $$sroa$7$0$i$i = $46 ? 0.0 : $36 * $47;
  $$sroa$0$0$i$i = $46 ? 0.0 : $33 * $47;
  $51 = $30 * .5;
  $52 = +Math_cos(+$51);
  $53 = +Math_sin(+$51);
  $59 = +Math_sqrt(+($$sroa$12$0$i$i * $$sroa$12$0$i$i + ($$sroa$7$0$i$i * $$sroa$7$0$i$i + $$sroa$0$0$i$i * $$sroa$0$0$i$i)));
  $60 = $59 == 0.0;
  $61 = 1.0 / $59;
  HEAPF32[$4 >> 2] = $53 * ($60 ? 0.0 : $$sroa$0$0$i$i * $61);
  HEAPF32[$4 + 4 >> 2] = $53 * ($60 ? 0.0 : $$sroa$7$0$i$i * $61);
  HEAPF32[$4 + 8 >> 2] = $53 * ($60 ? 0.0 : $$sroa$12$0$i$i * $61);
  HEAPF32[$4 + 12 >> 2] = $52;
  return;
 }
}

function _glmc_quat_for($0, $1, $2, $3) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = $2 | 0;
 $3 = $3 | 0;
 var $$idx$val = 0.0, $$idx4$val = 0.0, $$idx6$val = 0.0, $$idx7$val = 0.0, $$sroa$0$0$i = 0.0, $$sroa$12$0$i = 0.0, $$sroa$7$0$i = 0.0, $$val = 0.0, $$val5 = 0.0, $14 = 0, $16 = 0, $26 = 0.0, $29 = 0.0, $32 = 0.0, $35 = 0.0, $41 = 0.0, $42 = 0, $43 = 0.0, $47 = 0.0, $48 = 0.0, $49 = 0.0, $55 = 0.0, $56 = 0, $57 = 0.0, $8 = 0.0;
 $$val = +HEAPF32[$0 >> 2];
 $$idx$val = +HEAPF32[$0 + 4 >> 2];
 $$idx4$val = +HEAPF32[$0 + 8 >> 2];
 $$val5 = +HEAPF32[$1 >> 2];
 $$idx6$val = +HEAPF32[$1 + 4 >> 2];
 $$idx7$val = +HEAPF32[$1 + 8 >> 2];
 $8 = $$val * $$val5 + $$idx$val * $$idx6$val + $$idx4$val * $$idx7$val;
 if (+Math_abs(+($8 + 1.0)) < 9.999999974752427e-07) {
  $14 = HEAP32[$2 + 4 >> 2] | 0;
  $16 = HEAP32[$2 + 8 >> 2] | 0;
  HEAP32[$3 >> 2] = HEAP32[$2 >> 2];
  HEAP32[$3 + 4 >> 2] = $14;
  HEAP32[$3 + 8 >> 2] = $16;
  HEAPF32[$3 + 12 >> 2] = 3.1415927410125732;
  return;
 }
 if (+Math_abs(+($8 + -1.0)) < 9.999999974752427e-07) {
  HEAP32[$3 >> 2] = 0;
  HEAP32[$3 + 4 >> 2] = 0;
  HEAP32[$3 + 8 >> 2] = 0;
  HEAP32[$3 + 12 >> 2] = 1065353216;
  return;
 } else {
  $26 = +Math_acos(+$8);
  $29 = $$idx4$val * $$idx6$val - $$idx$val * $$idx7$val;
  $32 = $$val * $$idx7$val - $$idx4$val * $$val5;
  $35 = $$idx$val * $$val5 - $$val * $$idx6$val;
  $41 = +Math_sqrt(+($35 * $35 + ($29 * $29 + $32 * $32)));
  $42 = $41 == 0.0;
  $43 = 1.0 / $41;
  $$sroa$12$0$i = $42 ? 0.0 : $35 * $43;
  $$sroa$7$0$i = $42 ? 0.0 : $32 * $43;
  $$sroa$0$0$i = $42 ? 0.0 : $29 * $43;
  $47 = $26 * .5;
  $48 = +Math_cos(+$47);
  $49 = +Math_sin(+$47);
  $55 = +Math_sqrt(+($$sroa$12$0$i * $$sroa$12$0$i + ($$sroa$7$0$i * $$sroa$7$0$i + $$sroa$0$0$i * $$sroa$0$0$i)));
  $56 = $55 == 0.0;
  $57 = 1.0 / $55;
  HEAPF32[$3 >> 2] = $49 * ($56 ? 0.0 : $$sroa$0$0$i * $57);
  HEAPF32[$3 + 4 >> 2] = $49 * ($56 ? 0.0 : $$sroa$7$0$i * $57);
  HEAPF32[$3 + 8 >> 2] = $49 * ($56 ? 0.0 : $$sroa$12$0$i * $57);
  HEAPF32[$3 + 12 >> 2] = $48;
  return;
 }
}

function _glmc_aabb_transform($0, $1, $2) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = $2 | 0;
 var $10 = 0.0, $11 = 0.0, $13 = 0.0, $14 = 0.0, $15 = 0.0, $16 = 0.0, $19 = 0.0, $20 = 0.0, $21 = 0.0, $23 = 0.0, $24 = 0.0, $26 = 0.0, $27 = 0.0, $29 = 0.0, $3 = 0.0, $30 = 0.0, $31 = 0.0, $32 = 0.0, $35 = 0.0, $36 = 0.0, $37 = 0.0, $39 = 0.0, $4 = 0.0, $40 = 0.0, $42 = 0.0, $43 = 0.0, $45 = 0.0, $46 = 0.0, $47 = 0.0, $48 = 0.0, $5 = 0.0, $65 = 0.0, $68 = 0.0, $7 = 0.0, $71 = 0.0, $8 = 0.0;
 $3 = +HEAPF32[$0 >> 2];
 $4 = +HEAPF32[$1 >> 2];
 $5 = $3 * $4;
 $7 = +HEAPF32[$1 + 4 >> 2];
 $8 = $3 * $7;
 $10 = +HEAPF32[$1 + 8 >> 2];
 $11 = $3 * $10;
 $13 = +HEAPF32[$0 + 12 >> 2];
 $14 = $4 * $13;
 $15 = $7 * $13;
 $16 = $10 * $13;
 $19 = +HEAPF32[$0 + 4 >> 2];
 $20 = +HEAPF32[$1 + 16 >> 2];
 $21 = $19 * $20;
 $23 = +HEAPF32[$1 + 20 >> 2];
 $24 = $19 * $23;
 $26 = +HEAPF32[$1 + 24 >> 2];
 $27 = $19 * $26;
 $29 = +HEAPF32[$0 + 16 >> 2];
 $30 = $20 * $29;
 $31 = $23 * $29;
 $32 = $26 * $29;
 $35 = +HEAPF32[$0 + 8 >> 2];
 $36 = +HEAPF32[$1 + 32 >> 2];
 $37 = $35 * $36;
 $39 = +HEAPF32[$1 + 36 >> 2];
 $40 = $35 * $39;
 $42 = +HEAPF32[$1 + 40 >> 2];
 $43 = $35 * $42;
 $45 = +HEAPF32[$0 + 20 >> 2];
 $46 = $36 * $45;
 $47 = $39 * $45;
 $48 = $42 * $45;
 $65 = +HEAPF32[$1 + 48 >> 2];
 $68 = +HEAPF32[$1 + 52 >> 2];
 $71 = +HEAPF32[$1 + 56 >> 2];
 HEAPF32[$2 >> 2] = $65 + (($5 < $14 ? $5 : $14) + ($21 < $30 ? $21 : $30) + ($37 < $46 ? $37 : $46));
 HEAPF32[$2 + 4 >> 2] = $68 + (($8 < $15 ? $8 : $15) + ($24 < $31 ? $24 : $31) + ($40 < $47 ? $40 : $47));
 HEAPF32[$2 + 8 >> 2] = $71 + (($11 < $16 ? $11 : $16) + ($27 < $32 ? $27 : $32) + ($43 < $48 ? $43 : $48));
 HEAPF32[$2 + 12 >> 2] = $65 + (($5 > $14 ? $5 : $14) + ($21 > $30 ? $21 : $30) + ($37 > $46 ? $37 : $46));
 HEAPF32[$2 + 16 >> 2] = $68 + (($8 > $15 ? $8 : $15) + ($24 > $31 ? $24 : $31) + ($40 > $47 ? $40 : $47));
 HEAPF32[$2 + 20 >> 2] = $71 + (($11 > $16 ? $11 : $16) + ($27 > $32 ? $27 : $32) + ($43 > $48 ? $43 : $48));
 return;
}

function _glmc_mul_rot($0, $1, $2) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = $2 | 0;
 var $11 = 0.0, $13 = 0.0, $15 = 0.0, $17 = 0.0, $19 = 0.0, $21 = 0.0, $23 = 0.0, $25 = 0.0, $27 = 0, $29 = 0, $3 = 0.0, $31 = 0, $33 = 0, $34 = 0.0, $36 = 0.0, $38 = 0.0, $40 = 0.0, $42 = 0.0, $44 = 0.0, $46 = 0.0, $48 = 0.0, $5 = 0.0, $50 = 0.0, $7 = 0.0, $9 = 0.0;
 $3 = +HEAPF32[$0 >> 2];
 $5 = +HEAPF32[$0 + 4 >> 2];
 $7 = +HEAPF32[$0 + 8 >> 2];
 $9 = +HEAPF32[$0 + 12 >> 2];
 $11 = +HEAPF32[$0 + 16 >> 2];
 $13 = +HEAPF32[$0 + 20 >> 2];
 $15 = +HEAPF32[$0 + 24 >> 2];
 $17 = +HEAPF32[$0 + 28 >> 2];
 $19 = +HEAPF32[$0 + 32 >> 2];
 $21 = +HEAPF32[$0 + 36 >> 2];
 $23 = +HEAPF32[$0 + 40 >> 2];
 $25 = +HEAPF32[$0 + 44 >> 2];
 $27 = HEAP32[$0 + 48 >> 2] | 0;
 $29 = HEAP32[$0 + 52 >> 2] | 0;
 $31 = HEAP32[$0 + 56 >> 2] | 0;
 $33 = HEAP32[$0 + 60 >> 2] | 0;
 $34 = +HEAPF32[$1 >> 2];
 $36 = +HEAPF32[$1 + 4 >> 2];
 $38 = +HEAPF32[$1 + 8 >> 2];
 $40 = +HEAPF32[$1 + 16 >> 2];
 $42 = +HEAPF32[$1 + 20 >> 2];
 $44 = +HEAPF32[$1 + 24 >> 2];
 $46 = +HEAPF32[$1 + 32 >> 2];
 $48 = +HEAPF32[$1 + 36 >> 2];
 $50 = +HEAPF32[$1 + 40 >> 2];
 HEAPF32[$2 >> 2] = $3 * $34 + $11 * $36 + $19 * $38;
 HEAPF32[$2 + 4 >> 2] = $5 * $34 + $13 * $36 + $21 * $38;
 HEAPF32[$2 + 8 >> 2] = $7 * $34 + $15 * $36 + $23 * $38;
 HEAPF32[$2 + 12 >> 2] = $9 * $34 + $17 * $36 + $25 * $38;
 HEAPF32[$2 + 16 >> 2] = $3 * $40 + $11 * $42 + $19 * $44;
 HEAPF32[$2 + 20 >> 2] = $5 * $40 + $13 * $42 + $21 * $44;
 HEAPF32[$2 + 24 >> 2] = $7 * $40 + $15 * $42 + $23 * $44;
 HEAPF32[$2 + 28 >> 2] = $9 * $40 + $17 * $42 + $25 * $44;
 HEAPF32[$2 + 32 >> 2] = $3 * $46 + $11 * $48 + $19 * $50;
 HEAPF32[$2 + 36 >> 2] = $5 * $46 + $13 * $48 + $21 * $50;
 HEAPF32[$2 + 40 >> 2] = $7 * $46 + $15 * $48 + $23 * $50;
 HEAPF32[$2 + 44 >> 2] = $9 * $46 + $17 * $48 + $25 * $50;
 HEAP32[$2 + 48 >> 2] = $27;
 HEAP32[$2 + 52 >> 2] = $29;
 HEAP32[$2 + 56 >> 2] = $31;
 HEAP32[$2 + 60 >> 2] = $33;
 return;
}

function _glmc_aabb_crop_until($0, $1, $2, $3) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = $2 | 0;
 $3 = $3 | 0;
 var $$$i$i$i = 0.0, $$$i18$i$i = 0.0, $$$i19$i$i = 0.0, $$$i20$i$i = 0.0, $$$i21$i$i = 0.0, $$$i22$i$i = 0.0, $10 = 0.0, $12 = 0, $14 = 0.0, $16 = 0.0, $18 = 0, $20 = 0.0, $22 = 0.0, $24 = 0, $26 = 0.0, $28 = 0.0, $30 = 0, $32 = 0.0, $34 = 0.0, $36 = 0, $37 = 0.0, $4 = 0.0, $40 = 0.0, $43 = 0.0, $46 = 0.0, $49 = 0.0, $5 = 0.0, $52 = 0.0, $8 = 0.0;
 $4 = +HEAPF32[$0 >> 2];
 $5 = +HEAPF32[$1 >> 2];
 $$$i$i$i = $4 > $5 ? $4 : $5;
 HEAPF32[$3 >> 2] = $$$i$i$i;
 $8 = +HEAPF32[$0 + 4 >> 2];
 $10 = +HEAPF32[$1 + 4 >> 2];
 $$$i22$i$i = $8 > $10 ? $8 : $10;
 $12 = $3 + 4 | 0;
 HEAPF32[$12 >> 2] = $$$i22$i$i;
 $14 = +HEAPF32[$0 + 8 >> 2];
 $16 = +HEAPF32[$1 + 8 >> 2];
 $$$i21$i$i = $14 > $16 ? $14 : $16;
 $18 = $3 + 8 | 0;
 HEAPF32[$18 >> 2] = $$$i21$i$i;
 $20 = +HEAPF32[$0 + 12 >> 2];
 $22 = +HEAPF32[$1 + 12 >> 2];
 $$$i20$i$i = $20 < $22 ? $20 : $22;
 $24 = $3 + 12 | 0;
 HEAPF32[$24 >> 2] = $$$i20$i$i;
 $26 = +HEAPF32[$0 + 16 >> 2];
 $28 = +HEAPF32[$1 + 16 >> 2];
 $$$i19$i$i = $26 < $28 ? $26 : $28;
 $30 = $3 + 16 | 0;
 HEAPF32[$30 >> 2] = $$$i19$i$i;
 $32 = +HEAPF32[$0 + 20 >> 2];
 $34 = +HEAPF32[$1 + 20 >> 2];
 $$$i18$i$i = $32 < $34 ? $32 : $34;
 $36 = $3 + 20 | 0;
 HEAPF32[$36 >> 2] = $$$i18$i$i;
 $37 = +HEAPF32[$2 >> 2];
 HEAPF32[$3 >> 2] = $37 < $$$i$i$i ? $37 : $$$i$i$i;
 $40 = +HEAPF32[$2 + 4 >> 2];
 HEAPF32[$12 >> 2] = $40 < $$$i22$i$i ? $40 : $$$i22$i$i;
 $43 = +HEAPF32[$2 + 8 >> 2];
 HEAPF32[$18 >> 2] = $43 < $$$i21$i$i ? $43 : $$$i21$i$i;
 $46 = +HEAPF32[$2 + 12 >> 2];
 HEAPF32[$24 >> 2] = $46 > $$$i20$i$i ? $46 : $$$i20$i$i;
 $49 = +HEAPF32[$2 + 16 >> 2];
 HEAPF32[$30 >> 2] = $49 > $$$i19$i$i ? $49 : $$$i19$i$i;
 $52 = +HEAPF32[$2 + 20 >> 2];
 HEAPF32[$36 >> 2] = $52 > $$$i18$i$i ? $52 : $$$i18$i$i;
 return;
}

function _glmc_quat_look($0, $1, $2) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = $2 | 0;
 var $10 = 0.0, $14 = 0.0, $17 = 0.0, $20 = 0.0, $21 = 0.0, $23 = 0.0, $24 = 0.0, $25 = 0.0, $26 = 0.0, $27 = 0.0, $28 = 0.0, $29 = 0.0, $3 = 0.0, $31 = 0.0, $32 = 0.0, $33 = 0.0, $35 = 0.0, $36 = 0.0, $37 = 0.0, $39 = 0.0, $41 = 0.0, $43 = 0.0, $45 = 0.0, $47 = 0.0, $49 = 0.0, $51 = 0.0, $55 = 0, $57 = 0.0, $59 = 0.0, $6 = 0.0, $61 = 0.0;
 $3 = +HEAPF32[$1 >> 2];
 $6 = +HEAPF32[$1 + 4 >> 2];
 $10 = +HEAPF32[$1 + 8 >> 2];
 $14 = +HEAPF32[$1 + 12 >> 2];
 $17 = +Math_sqrt(+($3 * $3 + $6 * $6 + $10 * $10 + $14 * $14));
 $20 = $17 > 0.0 ? 2.0 / $17 : 0.0;
 $21 = $3 * $20;
 $23 = $6 * $21;
 $24 = $14 * $20;
 $25 = $3 * $24;
 $26 = $6 * $20;
 $27 = $6 * $26;
 $28 = $10 * $26;
 $29 = $6 * $24;
 $31 = $10 * ($10 * $20);
 $32 = $10 * $21;
 $33 = $10 * $24;
 $35 = 1.0 - $27 - $31;
 HEAPF32[$2 >> 2] = $35;
 $36 = 1.0 - $3 * $21;
 $37 = $36 - $31;
 HEAPF32[$2 + 20 >> 2] = $37;
 $39 = $36 - $27;
 HEAPF32[$2 + 40 >> 2] = $39;
 $41 = $23 + $33;
 HEAPF32[$2 + 16 >> 2] = $41;
 $43 = $28 + $25;
 HEAPF32[$2 + 36 >> 2] = $43;
 $45 = $32 + $29;
 HEAPF32[$2 + 8 >> 2] = $45;
 $47 = $23 - $33;
 HEAPF32[$2 + 4 >> 2] = $47;
 $49 = $28 - $25;
 HEAPF32[$2 + 24 >> 2] = $49;
 $51 = $32 - $29;
 HEAPF32[$2 + 32 >> 2] = $51;
 HEAPF32[$2 + 12 >> 2] = 0.0;
 HEAPF32[$2 + 28 >> 2] = 0.0;
 $55 = $2 + 44 | 0;
 HEAP32[$55 >> 2] = 0;
 HEAP32[$55 + 4 >> 2] = 0;
 HEAP32[$55 + 8 >> 2] = 0;
 HEAP32[$55 + 12 >> 2] = 0;
 HEAPF32[$2 + 60 >> 2] = 1.0;
 $57 = +HEAPF32[$0 >> 2];
 $59 = +HEAPF32[$0 + 4 >> 2];
 $61 = +HEAPF32[$0 + 8 >> 2];
 HEAPF32[$2 + 48 >> 2] = -($35 * $57 + $41 * $59 + $51 * $61 + 0.0);
 HEAPF32[$2 + 52 >> 2] = -($47 * $57 + $37 * $59 + $43 * $61 + 0.0);
 HEAPF32[$2 + 56 >> 2] = -($45 * $57 + $49 * $59 + $39 * $61 + 0.0);
 return;
}

function _glmc_frustum_box($0, $1, $2) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = $2 | 0;
 var $$038$i = 0, $11 = 0.0, $13 = 0.0, $15 = 0.0, $17 = 0.0, $19 = 0.0, $21 = 0.0, $23 = 0.0, $25 = 0.0, $27 = 0.0, $3 = 0.0, $30 = 0.0, $34 = 0.0, $38 = 0.0, $40 = 0.0, $47 = 0.0, $5 = 0.0, $54 = 0.0, $56 = 0.0, $58 = 0.0, $60 = 0.0, $62 = 0.0, $64 = 0.0, $66 = 0.0, $7 = 0.0, $9 = 0.0;
 $3 = +HEAPF32[$1 >> 2];
 $5 = +HEAPF32[$1 + 16 >> 2];
 $7 = +HEAPF32[$1 + 32 >> 2];
 $9 = +HEAPF32[$1 + 48 >> 2];
 $11 = +HEAPF32[$1 + 4 >> 2];
 $13 = +HEAPF32[$1 + 20 >> 2];
 $15 = +HEAPF32[$1 + 36 >> 2];
 $17 = +HEAPF32[$1 + 52 >> 2];
 $19 = +HEAPF32[$1 + 8 >> 2];
 $21 = +HEAPF32[$1 + 24 >> 2];
 $23 = +HEAPF32[$1 + 40 >> 2];
 $25 = +HEAPF32[$1 + 56 >> 2];
 $$038$i = 0;
 $56 = 3402823466385288598117041.0e14;
 $58 = 3402823466385288598117041.0e14;
 $60 = 3402823466385288598117041.0e14;
 $62 = -3402823466385288598117041.0e14;
 $64 = -3402823466385288598117041.0e14;
 $66 = -3402823466385288598117041.0e14;
 do {
  $27 = +HEAPF32[$0 + ($$038$i << 4) >> 2];
  $30 = +HEAPF32[$0 + ($$038$i << 4) + 4 >> 2];
  $34 = +HEAPF32[$0 + ($$038$i << 4) + 8 >> 2];
  $38 = +HEAPF32[$0 + ($$038$i << 4) + 12 >> 2];
  $40 = $3 * $27 + $5 * $30 + $7 * $34 + $9 * $38;
  $47 = $11 * $27 + $13 * $30 + $15 * $34 + $17 * $38;
  $54 = $19 * $27 + $21 * $30 + $23 * $34 + $25 * $38;
  $56 = $56 < $40 ? $56 : $40;
  $58 = $58 < $47 ? $58 : $47;
  $60 = $60 < $54 ? $60 : $54;
  $62 = $62 > $40 ? $62 : $40;
  $64 = $64 > $47 ? $64 : $47;
  $66 = $66 > $54 ? $66 : $54;
  $$038$i = $$038$i + 1 | 0;
 } while (($$038$i | 0) != 8);
 HEAPF32[$2 >> 2] = $56;
 HEAPF32[$2 + 4 >> 2] = $58;
 HEAPF32[$2 + 8 >> 2] = $60;
 HEAPF32[$2 + 12 >> 2] = $62;
 HEAPF32[$2 + 16 >> 2] = $64;
 HEAPF32[$2 + 20 >> 2] = $66;
 return;
}

function _glmc_rotate_x($0, $1, $2) {
 $0 = $0 | 0;
 $1 = +$1;
 $2 = $2 | 0;
 var $10 = 0.0, $12 = 0.0, $14 = 0.0, $16 = 0.0, $18 = 0.0, $20 = 0.0, $22 = 0.0, $24 = 0.0, $26 = 0.0, $28 = 0.0, $3 = 0.0, $30 = 0, $32 = 0, $34 = 0, $36 = 0, $4 = 0.0, $5 = 0.0, $56 = 0.0, $6 = 0.0, $62 = 0.0, $68 = 0.0, $74 = 0.0, $8 = 0.0;
 $3 = +Math_cos(+$1);
 $4 = +Math_sin(+$1);
 $5 = -$4;
 $6 = +HEAPF32[$0 >> 2];
 $8 = +HEAPF32[$0 + 4 >> 2];
 $10 = +HEAPF32[$0 + 8 >> 2];
 $12 = +HEAPF32[$0 + 12 >> 2];
 $14 = +HEAPF32[$0 + 16 >> 2];
 $16 = +HEAPF32[$0 + 20 >> 2];
 $18 = +HEAPF32[$0 + 24 >> 2];
 $20 = +HEAPF32[$0 + 28 >> 2];
 $22 = +HEAPF32[$0 + 32 >> 2];
 $24 = +HEAPF32[$0 + 36 >> 2];
 $26 = +HEAPF32[$0 + 40 >> 2];
 $28 = +HEAPF32[$0 + 44 >> 2];
 $30 = HEAP32[$0 + 48 >> 2] | 0;
 $32 = HEAP32[$0 + 52 >> 2] | 0;
 $34 = HEAP32[$0 + 56 >> 2] | 0;
 $36 = HEAP32[$0 + 60 >> 2] | 0;
 HEAPF32[$2 >> 2] = $6 + $14 * 0.0 + $22 * 0.0;
 HEAPF32[$2 + 4 >> 2] = $8 + $16 * 0.0 + $24 * 0.0;
 HEAPF32[$2 + 8 >> 2] = $10 + $18 * 0.0 + $26 * 0.0;
 HEAPF32[$2 + 12 >> 2] = $12 + $20 * 0.0 + $28 * 0.0;
 $56 = $6 * 0.0;
 HEAPF32[$2 + 16 >> 2] = $56 + $3 * $14 + $4 * $22;
 $62 = $8 * 0.0;
 HEAPF32[$2 + 20 >> 2] = $62 + $3 * $16 + $4 * $24;
 $68 = $10 * 0.0;
 HEAPF32[$2 + 24 >> 2] = $68 + $3 * $18 + $4 * $26;
 $74 = $12 * 0.0;
 HEAPF32[$2 + 28 >> 2] = $74 + $3 * $20 + $4 * $28;
 HEAPF32[$2 + 32 >> 2] = $56 + $14 * $5 + $3 * $22;
 HEAPF32[$2 + 36 >> 2] = $62 + $16 * $5 + $3 * $24;
 HEAPF32[$2 + 40 >> 2] = $68 + $18 * $5 + $3 * $26;
 HEAPF32[$2 + 44 >> 2] = $74 + $20 * $5 + $3 * $28;
 HEAP32[$2 + 48 >> 2] = $30;
 HEAP32[$2 + 52 >> 2] = $32;
 HEAP32[$2 + 56 >> 2] = $34;
 HEAP32[$2 + 60 >> 2] = $36;
 return;
}

function _glmc_rotate_z($0, $1, $2) {
 $0 = $0 | 0;
 $1 = +$1;
 $2 = $2 | 0;
 var $10 = 0.0, $12 = 0.0, $14 = 0.0, $16 = 0.0, $18 = 0.0, $20 = 0.0, $22 = 0.0, $24 = 0.0, $26 = 0.0, $28 = 0.0, $3 = 0.0, $30 = 0, $32 = 0, $34 = 0, $36 = 0, $4 = 0.0, $40 = 0.0, $45 = 0.0, $5 = 0.0, $51 = 0.0, $57 = 0.0, $6 = 0.0, $8 = 0.0;
 $3 = +Math_cos(+$1);
 $4 = +Math_sin(+$1);
 $5 = -$4;
 $6 = +HEAPF32[$0 >> 2];
 $8 = +HEAPF32[$0 + 4 >> 2];
 $10 = +HEAPF32[$0 + 8 >> 2];
 $12 = +HEAPF32[$0 + 12 >> 2];
 $14 = +HEAPF32[$0 + 16 >> 2];
 $16 = +HEAPF32[$0 + 20 >> 2];
 $18 = +HEAPF32[$0 + 24 >> 2];
 $20 = +HEAPF32[$0 + 28 >> 2];
 $22 = +HEAPF32[$0 + 32 >> 2];
 $24 = +HEAPF32[$0 + 36 >> 2];
 $26 = +HEAPF32[$0 + 40 >> 2];
 $28 = +HEAPF32[$0 + 44 >> 2];
 $30 = HEAP32[$0 + 48 >> 2] | 0;
 $32 = HEAP32[$0 + 52 >> 2] | 0;
 $34 = HEAP32[$0 + 56 >> 2] | 0;
 $36 = HEAP32[$0 + 60 >> 2] | 0;
 $40 = $22 * 0.0;
 HEAPF32[$2 >> 2] = $3 * $6 + $4 * $14 + $40;
 $45 = $24 * 0.0;
 HEAPF32[$2 + 4 >> 2] = $3 * $8 + $4 * $16 + $45;
 $51 = $26 * 0.0;
 HEAPF32[$2 + 8 >> 2] = $3 * $10 + $4 * $18 + $51;
 $57 = $28 * 0.0;
 HEAPF32[$2 + 12 >> 2] = $3 * $12 + $4 * $20 + $57;
 HEAPF32[$2 + 16 >> 2] = $6 * $5 + $3 * $14 + $40;
 HEAPF32[$2 + 20 >> 2] = $8 * $5 + $3 * $16 + $45;
 HEAPF32[$2 + 24 >> 2] = $10 * $5 + $3 * $18 + $51;
 HEAPF32[$2 + 28 >> 2] = $12 * $5 + $3 * $20 + $57;
 HEAPF32[$2 + 32 >> 2] = $6 * 0.0 + $14 * 0.0 + $22;
 HEAPF32[$2 + 36 >> 2] = $8 * 0.0 + $16 * 0.0 + $24;
 HEAPF32[$2 + 40 >> 2] = $10 * 0.0 + $18 * 0.0 + $26;
 HEAPF32[$2 + 44 >> 2] = $12 * 0.0 + $20 * 0.0 + $28;
 HEAP32[$2 + 48 >> 2] = $30;
 HEAP32[$2 + 52 >> 2] = $32;
 HEAP32[$2 + 56 >> 2] = $34;
 HEAP32[$2 + 60 >> 2] = $36;
 return;
}

function _glmc_rotate_y($0, $1, $2) {
 $0 = $0 | 0;
 $1 = +$1;
 $2 = $2 | 0;
 var $10 = 0.0, $12 = 0.0, $14 = 0.0, $16 = 0.0, $18 = 0.0, $20 = 0.0, $22 = 0.0, $24 = 0.0, $26 = 0.0, $28 = 0.0, $3 = 0.0, $30 = 0, $32 = 0, $34 = 0, $36 = 0, $38 = 0.0, $4 = 0.0, $43 = 0.0, $49 = 0.0, $5 = 0.0, $55 = 0.0, $6 = 0.0, $8 = 0.0;
 $3 = +Math_cos(+$1);
 $4 = +Math_sin(+$1);
 $5 = -$4;
 $6 = +HEAPF32[$0 >> 2];
 $8 = +HEAPF32[$0 + 4 >> 2];
 $10 = +HEAPF32[$0 + 8 >> 2];
 $12 = +HEAPF32[$0 + 12 >> 2];
 $14 = +HEAPF32[$0 + 16 >> 2];
 $16 = +HEAPF32[$0 + 20 >> 2];
 $18 = +HEAPF32[$0 + 24 >> 2];
 $20 = +HEAPF32[$0 + 28 >> 2];
 $22 = +HEAPF32[$0 + 32 >> 2];
 $24 = +HEAPF32[$0 + 36 >> 2];
 $26 = +HEAPF32[$0 + 40 >> 2];
 $28 = +HEAPF32[$0 + 44 >> 2];
 $30 = HEAP32[$0 + 48 >> 2] | 0;
 $32 = HEAP32[$0 + 52 >> 2] | 0;
 $34 = HEAP32[$0 + 56 >> 2] | 0;
 $36 = HEAP32[$0 + 60 >> 2] | 0;
 $38 = $14 * 0.0;
 HEAPF32[$2 >> 2] = $3 * $6 + $38 + $22 * $5;
 $43 = $16 * 0.0;
 HEAPF32[$2 + 4 >> 2] = $3 * $8 + $43 + $24 * $5;
 $49 = $18 * 0.0;
 HEAPF32[$2 + 8 >> 2] = $3 * $10 + $49 + $26 * $5;
 $55 = $20 * 0.0;
 HEAPF32[$2 + 12 >> 2] = $3 * $12 + $55 + $28 * $5;
 HEAPF32[$2 + 16 >> 2] = $6 * 0.0 + $14 + $22 * 0.0;
 HEAPF32[$2 + 20 >> 2] = $8 * 0.0 + $16 + $24 * 0.0;
 HEAPF32[$2 + 24 >> 2] = $10 * 0.0 + $18 + $26 * 0.0;
 HEAPF32[$2 + 28 >> 2] = $12 * 0.0 + $20 + $28 * 0.0;
 HEAPF32[$2 + 32 >> 2] = $4 * $6 + $38 + $3 * $22;
 HEAPF32[$2 + 36 >> 2] = $4 * $8 + $43 + $3 * $24;
 HEAPF32[$2 + 40 >> 2] = $4 * $10 + $49 + $3 * $26;
 HEAPF32[$2 + 44 >> 2] = $4 * $12 + $55 + $3 * $28;
 HEAP32[$2 + 48 >> 2] = $30;
 HEAP32[$2 + 52 >> 2] = $32;
 HEAP32[$2 + 56 >> 2] = $34;
 HEAP32[$2 + 60 >> 2] = $36;
 return;
}

function _glmc_rotate_make($0, $1, $2) {
 $0 = $0 | 0;
 $1 = +$1;
 $2 = $2 | 0;
 var $$idx$val = 0.0, $$idx3$val = 0.0, $$sink$i$i = 0.0, $$sroa$011$0$i = 0.0, $$sroa$9$0$i = 0.0, $$val = 0.0, $10 = 0, $11 = 0.0, $15 = 0.0, $16 = 0.0, $17 = 0.0, $18 = 0.0, $19 = 0.0, $20 = 0.0, $21 = 0.0, $22 = 0.0, $3 = 0.0, $49 = 0, $9 = 0.0;
 $$val = +HEAPF32[$2 >> 2];
 $$idx$val = +HEAPF32[$2 + 4 >> 2];
 $$idx3$val = +HEAPF32[$2 + 8 >> 2];
 $3 = +Math_cos(+$1);
 $9 = +Math_sqrt(+($$val * $$val + $$idx$val * $$idx$val + $$idx3$val * $$idx3$val));
 $10 = $9 == 0.0;
 $11 = 1.0 / $9;
 $$sroa$011$0$i = $10 ? 0.0 : $$val * $11;
 $$sroa$9$0$i = $10 ? 0.0 : $$idx$val * $11;
 $$sink$i$i = $10 ? 0.0 : $$idx3$val * $11;
 $15 = 1.0 - $3;
 $16 = $15 * $$sroa$011$0$i;
 $17 = $15 * $$sroa$9$0$i;
 $18 = $15 * $$sink$i$i;
 $19 = +Math_sin(+$1);
 $20 = $19 * $$sroa$011$0$i;
 $21 = $19 * $$sroa$9$0$i;
 $22 = $19 * $$sink$i$i;
 HEAPF32[$0 >> 2] = $3 + $$sroa$011$0$i * $16;
 HEAPF32[$0 + 16 >> 2] = $$sroa$011$0$i * $17 - $22;
 HEAPF32[$0 + 32 >> 2] = $21 + $$sroa$011$0$i * $18;
 HEAPF32[$0 + 4 >> 2] = $22 + $$sroa$9$0$i * $16;
 HEAPF32[$0 + 20 >> 2] = $3 + $$sroa$9$0$i * $17;
 HEAPF32[$0 + 36 >> 2] = $$sroa$9$0$i * $18 - $20;
 HEAPF32[$0 + 8 >> 2] = $$sink$i$i * $16 - $21;
 HEAPF32[$0 + 24 >> 2] = $20 + $$sink$i$i * $17;
 HEAPF32[$0 + 40 >> 2] = $3 + $$sink$i$i * $18;
 $49 = $0 + 44 | 0;
 HEAPF32[$0 + 28 >> 2] = 0.0;
 HEAPF32[$0 + 12 >> 2] = 0.0;
 HEAP32[$49 >> 2] = 0;
 HEAP32[$49 + 4 >> 2] = 0;
 HEAP32[$49 + 8 >> 2] = 0;
 HEAP32[$49 + 12 >> 2] = 0;
 HEAPF32[$0 + 60 >> 2] = 1.0;
 return;
}

function _glmc_ortho_default_s($0, $1, $2) {
 $0 = +$0;
 $1 = +$1;
 $2 = $2 | 0;
 var $$pn$i = 0.0, $$pn25$i = 0.0, $$pn26$i = 0.0, $$sink19$i = 0.0, $$sink22$in$i = 0.0, $$sink23$in$i = 0.0, $$sink24$in$i = 0.0, $11 = 0.0, $13 = 0.0, $20 = 0.0, $21 = 0.0, $22 = 0.0, $23 = 0.0, $26 = 0.0, $28 = 0.0, $4 = 0.0, $5 = 0.0, $6 = 0.0, $7 = 0.0, $8 = 0.0, dest = 0, stop = 0;
 $4 = -$1;
 if (!($0 >= 1.0)) {
  $20 = $4 / $0;
  $21 = $1 / $0;
  $22 = -100.0 - $1;
  $23 = $1 + 100.0;
  dest = $2 + 4 | 0;
  stop = dest + 44 | 0;
  do {
   HEAP32[dest >> 2] = 0;
   dest = dest + 4 | 0;
  } while ((dest | 0) < (stop | 0));
  $26 = 1.0 / ($1 + $1);
  $28 = 1.0 / ($21 - $20);
  $$pn$i = ($1 - $1) * $26;
  $$pn25$i = ($20 + $21) * $28;
  $$pn26$i = $23 - $22;
  $$sink19$i = $22 + $23;
  $$sink23$in$i = $28;
  $$sink24$in$i = $26;
 } else {
  $5 = $4 * $0;
  $6 = $0 * $1;
  $7 = -100.0 - $1;
  $8 = $1 + 100.0;
  dest = $2 + 4 | 0;
  stop = dest + 44 | 0;
  do {
   HEAP32[dest >> 2] = 0;
   dest = dest + 4 | 0;
  } while ((dest | 0) < (stop | 0));
  $11 = 1.0 / ($6 - $5);
  $13 = 1.0 / ($1 + $1);
  $$pn$i = ($5 + $6) * $11;
  $$pn25$i = ($1 - $1) * $13;
  $$pn26$i = $8 - $7;
  $$sink19$i = $7 + $8;
  $$sink23$in$i = $13;
  $$sink24$in$i = $11;
 }
 $$sink22$in$i = -1.0 / $$pn26$i;
 HEAPF32[$2 >> 2] = $$sink24$in$i * 2.0;
 HEAPF32[$2 + 20 >> 2] = $$sink23$in$i * 2.0;
 HEAPF32[$2 + 40 >> 2] = $$sink22$in$i * 2.0;
 HEAPF32[$2 + 48 >> 2] = -$$pn$i;
 HEAPF32[$2 + 52 >> 2] = -$$pn25$i;
 HEAPF32[$2 + 56 >> 2] = $$sink22$in$i * $$sink19$i;
 HEAPF32[$2 + 60 >> 2] = 1.0;
 return;
}

function _glmc_inv_tr($0) {
 $0 = $0 | 0;
 var $1 = 0.0, $10 = 0, $11 = 0, $12 = 0, $13 = 0, $14 = 0, $15 = 0, $17 = 0.0, $18 = 0, $19 = 0.0, $2 = 0, $22 = 0, $23 = 0.0, $25 = 0.0, $27 = 0, $28 = 0.0, $3 = 0, $30 = 0.0, $34 = 0.0, $37 = 0.0, $39 = 0.0, $4 = 0, $47 = 0.0, $5 = 0, $6 = 0, $7 = 0, $9 = 0.0;
 $1 = +HEAPF32[$0 >> 2];
 $2 = $0 + 16 | 0;
 $3 = HEAP32[$2 >> 2] | 0;
 $4 = $0 + 32 | 0;
 $5 = HEAP32[$4 >> 2] | 0;
 $6 = $0 + 4 | 0;
 $7 = HEAP32[$6 >> 2] | 0;
 $9 = +HEAPF32[$0 + 20 >> 2];
 $10 = $0 + 36 | 0;
 $11 = HEAP32[$10 >> 2] | 0;
 $12 = $0 + 8 | 0;
 $13 = HEAP32[$12 >> 2] | 0;
 $14 = $0 + 24 | 0;
 $15 = HEAP32[$14 >> 2] | 0;
 $17 = +HEAPF32[$0 + 40 >> 2];
 HEAP32[$6 >> 2] = $3;
 HEAP32[$12 >> 2] = $5;
 HEAP32[$2 >> 2] = $7;
 HEAP32[$14 >> 2] = $11;
 HEAP32[$4 >> 2] = $13;
 HEAP32[$10 >> 2] = $15;
 $18 = $0 + 48 | 0;
 $19 = +HEAPF32[$18 >> 2];
 $22 = $0 + 52 | 0;
 $23 = +HEAPF32[$22 >> 2];
 $25 = $1 * $19 + $23 * (HEAP32[tempDoublePtr >> 2] = $7, +HEAPF32[tempDoublePtr >> 2]);
 $27 = $0 + 56 | 0;
 $28 = +HEAPF32[$27 >> 2];
 $30 = $25 + $28 * (HEAP32[tempDoublePtr >> 2] = $13, +HEAPF32[tempDoublePtr >> 2]);
 $34 = $19 * (HEAP32[tempDoublePtr >> 2] = $3, +HEAPF32[tempDoublePtr >> 2]) + $9 * $23;
 $37 = $34 + $28 * (HEAP32[tempDoublePtr >> 2] = $15, +HEAPF32[tempDoublePtr >> 2]);
 $39 = $19 * (HEAP32[tempDoublePtr >> 2] = $5, +HEAPF32[tempDoublePtr >> 2]);
 $47 = -($39 + $23 * (HEAP32[tempDoublePtr >> 2] = $11, +HEAPF32[tempDoublePtr >> 2]) + $17 * $28);
 HEAPF32[$18 >> 2] = -$30;
 HEAPF32[$22 >> 2] = -$37;
 HEAPF32[$27 >> 2] = $47;
 return;
}

function _glmc_quat_rotatev($0, $1, $2) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = $2 | 0;
 var $$idx17$val$i = 0.0, $$idx18$val$i = 0.0, $$sroa$851$0$i = 0.0, $$val16$i = 0.0, $10 = 0.0, $13 = 0, $14 = 0.0, $16 = 0.0, $19 = 0.0, $25 = 0.0, $27 = 0.0, $3 = 0.0, $30 = 0.0, $32 = 0.0, $42 = 0.0, $5 = 0, $58 = 0.0, $6 = 0.0, $9 = 0;
 $3 = +HEAPF32[$0 >> 2];
 $5 = $0 + 4 | 0;
 $6 = +HEAPF32[$5 >> 2];
 $9 = $0 + 8 | 0;
 $10 = +HEAPF32[$9 >> 2];
 $13 = $0 + 12 | 0;
 $14 = +HEAPF32[$13 >> 2];
 $16 = $3 * $3 + $6 * $6 + $10 * $10 + $14 * $14;
 if (!($16 <= 0.0)) {
  $19 = 1.0 / +Math_sqrt(+$16);
  $$sroa$851$0$i = $14 * $19;
  $25 = $3 * $19;
  $27 = $6 * $19;
  $30 = $10 * $19;
 } else {
  HEAP32[$0 >> 2] = 0;
  HEAP32[$5 >> 2] = 0;
  HEAP32[$9 >> 2] = 0;
  HEAP32[$13 >> 2] = 1065353216;
  $$sroa$851$0$i = 0.0;
  $25 = 0.0;
  $27 = 0.0;
  $30 = 0.0;
 }
 $$val16$i = +HEAPF32[$1 >> 2];
 $$idx17$val$i = +HEAPF32[$1 + 4 >> 2];
 $$idx18$val$i = +HEAPF32[$1 + 8 >> 2];
 $32 = ($25 * $$val16$i + $27 * $$idx17$val$i + $30 * $$idx18$val$i) * 2.0;
 $42 = $$sroa$851$0$i * $$sroa$851$0$i - ($25 * $25 + $27 * $27 + $30 * $30);
 $58 = $$sroa$851$0$i * 2.0;
 HEAPF32[$2 >> 2] = $58 * ($27 * $$idx18$val$i - $30 * $$idx17$val$i) + ($$val16$i * $42 + $25 * $32);
 HEAPF32[$2 + 4 >> 2] = $58 * ($30 * $$val16$i - $25 * $$idx18$val$i) + ($42 * $$idx17$val$i + $27 * $32);
 HEAPF32[$2 + 8 >> 2] = $58 * ($25 * $$idx17$val$i - $27 * $$val16$i) + ($42 * $$idx18$val$i + $30 * $32);
 return;
}

function _glmc_vec_rotate_m4($0, $1, $2) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = $2 | 0;
 var $10 = 0.0, $14 = 0.0, $17 = 0.0, $18 = 0, $19 = 0.0, $24 = 0.0, $27 = 0.0, $3 = 0.0, $31 = 0.0, $35 = 0.0, $38 = 0.0, $39 = 0, $40 = 0.0, $45 = 0.0, $48 = 0.0, $52 = 0.0, $56 = 0.0, $59 = 0.0, $6 = 0.0, $60 = 0, $61 = 0.0, $65 = 0.0, $70 = 0.0, $78 = 0.0;
 $3 = +HEAPF32[$0 >> 2];
 $6 = +HEAPF32[$0 + 4 >> 2];
 $10 = +HEAPF32[$0 + 8 >> 2];
 $14 = +HEAPF32[$0 + 12 >> 2];
 $17 = +Math_sqrt(+($3 * $3 + $6 * $6 + $10 * $10 + $14 * $14));
 $18 = $17 == 0.0;
 $19 = 1.0 / $17;
 $24 = +HEAPF32[$0 + 16 >> 2];
 $27 = +HEAPF32[$0 + 20 >> 2];
 $31 = +HEAPF32[$0 + 24 >> 2];
 $35 = +HEAPF32[$0 + 28 >> 2];
 $38 = +Math_sqrt(+($24 * $24 + $27 * $27 + $31 * $31 + $35 * $35));
 $39 = $38 == 0.0;
 $40 = 1.0 / $38;
 $45 = +HEAPF32[$0 + 32 >> 2];
 $48 = +HEAPF32[$0 + 36 >> 2];
 $52 = +HEAPF32[$0 + 40 >> 2];
 $56 = +HEAPF32[$0 + 44 >> 2];
 $59 = +Math_sqrt(+($45 * $45 + $48 * $48 + $52 * $52 + $56 * $56));
 $60 = $59 == 0.0;
 $61 = 1.0 / $59;
 $65 = +HEAPF32[$1 >> 2];
 $70 = +HEAPF32[$1 + 4 >> 2];
 $78 = +HEAPF32[$1 + 8 >> 2];
 HEAPF32[$2 >> 2] = ($18 ? 0.0 : $3 * $19) * $65 + ($39 ? 0.0 : $24 * $40) * $70 + ($60 ? 0.0 : $45 * $61) * $78;
 HEAPF32[$2 + 4 >> 2] = ($18 ? 0.0 : $6 * $19) * $65 + ($39 ? 0.0 : $27 * $40) * $70 + ($60 ? 0.0 : $48 * $61) * $78;
 HEAPF32[$2 + 8 >> 2] = ($18 ? 0.0 : $10 * $19) * $65 + ($39 ? 0.0 : $31 * $40) * $70 + ($60 ? 0.0 : $52 * $61) * $78;
 return;
}

function _glmc_mat3_mul($0, $1, $2) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = $2 | 0;
 var $11 = 0.0, $13 = 0.0, $15 = 0.0, $17 = 0.0, $19 = 0.0, $20 = 0.0, $22 = 0.0, $24 = 0.0, $26 = 0.0, $28 = 0.0, $3 = 0.0, $30 = 0.0, $32 = 0.0, $34 = 0.0, $36 = 0.0, $5 = 0.0, $7 = 0.0, $9 = 0.0;
 $3 = +HEAPF32[$0 >> 2];
 $5 = +HEAPF32[$0 + 4 >> 2];
 $7 = +HEAPF32[$0 + 8 >> 2];
 $9 = +HEAPF32[$0 + 12 >> 2];
 $11 = +HEAPF32[$0 + 16 >> 2];
 $13 = +HEAPF32[$0 + 20 >> 2];
 $15 = +HEAPF32[$0 + 24 >> 2];
 $17 = +HEAPF32[$0 + 28 >> 2];
 $19 = +HEAPF32[$0 + 32 >> 2];
 $20 = +HEAPF32[$1 >> 2];
 $22 = +HEAPF32[$1 + 4 >> 2];
 $24 = +HEAPF32[$1 + 8 >> 2];
 $26 = +HEAPF32[$1 + 12 >> 2];
 $28 = +HEAPF32[$1 + 16 >> 2];
 $30 = +HEAPF32[$1 + 20 >> 2];
 $32 = +HEAPF32[$1 + 24 >> 2];
 $34 = +HEAPF32[$1 + 28 >> 2];
 $36 = +HEAPF32[$1 + 32 >> 2];
 HEAPF32[$2 >> 2] = $3 * $20 + $9 * $22 + $15 * $24;
 HEAPF32[$2 + 4 >> 2] = $5 * $20 + $11 * $22 + $17 * $24;
 HEAPF32[$2 + 8 >> 2] = $7 * $20 + $13 * $22 + $19 * $24;
 HEAPF32[$2 + 12 >> 2] = $3 * $26 + $9 * $28 + $15 * $30;
 HEAPF32[$2 + 16 >> 2] = $5 * $26 + $11 * $28 + $17 * $30;
 HEAPF32[$2 + 20 >> 2] = $7 * $26 + $13 * $28 + $19 * $30;
 HEAPF32[$2 + 24 >> 2] = $3 * $32 + $9 * $34 + $15 * $36;
 HEAPF32[$2 + 28 >> 2] = $5 * $32 + $11 * $34 + $17 * $36;
 HEAPF32[$2 + 32 >> 2] = $7 * $32 + $13 * $34 + $19 * $36;
 return;
}

function _glmc_vec_rotate_m3($0, $1, $2) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = $2 | 0;
 var $11 = 0.0, $13 = 0.0, $15 = 0.0, $17 = 0.0, $19 = 0.0, $26 = 0.0, $27 = 0, $28 = 0.0, $3 = 0.0, $41 = 0.0, $42 = 0, $43 = 0.0, $5 = 0.0, $56 = 0.0, $57 = 0, $58 = 0.0, $65 = 0.0, $7 = 0.0, $70 = 0.0, $78 = 0.0, $9 = 0.0;
 $3 = +HEAPF32[$0 >> 2];
 $5 = +HEAPF32[$0 + 4 >> 2];
 $7 = +HEAPF32[$0 + 8 >> 2];
 $9 = +HEAPF32[$0 + 12 >> 2];
 $11 = +HEAPF32[$0 + 16 >> 2];
 $13 = +HEAPF32[$0 + 20 >> 2];
 $15 = +HEAPF32[$0 + 24 >> 2];
 $17 = +HEAPF32[$0 + 28 >> 2];
 $19 = +HEAPF32[$0 + 32 >> 2];
 $26 = +Math_sqrt(+($3 * $3 + $5 * $5 + $7 * $7 + 0.0));
 $27 = $26 == 0.0;
 $28 = 1.0 / $26;
 $41 = +Math_sqrt(+($9 * $9 + $11 * $11 + $13 * $13 + 0.0));
 $42 = $41 == 0.0;
 $43 = 1.0 / $41;
 $56 = +Math_sqrt(+($15 * $15 + $17 * $17 + $19 * $19 + 0.0));
 $57 = $56 == 0.0;
 $58 = 1.0 / $56;
 $65 = +HEAPF32[$1 >> 2];
 $70 = +HEAPF32[$1 + 4 >> 2];
 $78 = +HEAPF32[$1 + 8 >> 2];
 HEAPF32[$2 >> 2] = ($27 ? 0.0 : $3 * $28) * $65 + ($42 ? 0.0 : $9 * $43) * $70 + ($57 ? 0.0 : $15 * $58) * $78;
 HEAPF32[$2 + 4 >> 2] = ($27 ? 0.0 : $5 * $28) * $65 + ($42 ? 0.0 : $11 * $43) * $70 + ($57 ? 0.0 : $17 * $58) * $78;
 HEAPF32[$2 + 8 >> 2] = ($27 ? 0.0 : $7 * $28) * $65 + ($42 ? 0.0 : $13 * $43) * $70 + ($57 ? 0.0 : $19 * $58) * $78;
 return;
}

function _memset(ptr, value, num) {
 ptr = ptr | 0;
 value = value | 0;
 num = num | 0;
 var end = 0, aligned_end = 0, block_aligned_end = 0, value4 = 0;
 end = ptr + num | 0;
 value = value & 255;
 if ((num | 0) >= 67) {
  while (ptr & 3) {
   HEAP8[ptr >> 0] = value;
   ptr = ptr + 1 | 0;
  }
  aligned_end = end & -4 | 0;
  block_aligned_end = aligned_end - 64 | 0;
  value4 = value | value << 8 | value << 16 | value << 24;
  while ((ptr | 0) <= (block_aligned_end | 0)) {
   HEAP32[ptr >> 2] = value4;
   HEAP32[ptr + 4 >> 2] = value4;
   HEAP32[ptr + 8 >> 2] = value4;
   HEAP32[ptr + 12 >> 2] = value4;
   HEAP32[ptr + 16 >> 2] = value4;
   HEAP32[ptr + 20 >> 2] = value4;
   HEAP32[ptr + 24 >> 2] = value4;
   HEAP32[ptr + 28 >> 2] = value4;
   HEAP32[ptr + 32 >> 2] = value4;
   HEAP32[ptr + 36 >> 2] = value4;
   HEAP32[ptr + 40 >> 2] = value4;
   HEAP32[ptr + 44 >> 2] = value4;
   HEAP32[ptr + 48 >> 2] = value4;
   HEAP32[ptr + 52 >> 2] = value4;
   HEAP32[ptr + 56 >> 2] = value4;
   HEAP32[ptr + 60 >> 2] = value4;
   ptr = ptr + 64 | 0;
  }
  while ((ptr | 0) < (aligned_end | 0)) {
   HEAP32[ptr >> 2] = value4;
   ptr = ptr + 4 | 0;
  }
 }
 while ((ptr | 0) < (end | 0)) {
  HEAP8[ptr >> 0] = value;
  ptr = ptr + 1 | 0;
 }
 return end - num | 0;
}

function _glmc_quat_mat4t($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 var $13 = 0.0, $16 = 0.0, $19 = 0.0, $2 = 0.0, $20 = 0.0, $22 = 0.0, $23 = 0.0, $24 = 0.0, $25 = 0.0, $26 = 0.0, $27 = 0.0, $28 = 0.0, $30 = 0.0, $31 = 0.0, $32 = 0.0, $35 = 0.0, $5 = 0.0, $54 = 0, $9 = 0.0;
 $2 = +HEAPF32[$0 >> 2];
 $5 = +HEAPF32[$0 + 4 >> 2];
 $9 = +HEAPF32[$0 + 8 >> 2];
 $13 = +HEAPF32[$0 + 12 >> 2];
 $16 = +Math_sqrt(+($2 * $2 + $5 * $5 + $9 * $9 + $13 * $13));
 $19 = $16 > 0.0 ? 2.0 / $16 : 0.0;
 $20 = $2 * $19;
 $22 = $5 * $20;
 $23 = $13 * $19;
 $24 = $2 * $23;
 $25 = $5 * $19;
 $26 = $5 * $25;
 $27 = $9 * $25;
 $28 = $5 * $23;
 $30 = $9 * ($9 * $19);
 $31 = $9 * $20;
 $32 = $9 * $23;
 HEAPF32[$1 >> 2] = 1.0 - $26 - $30;
 $35 = 1.0 - $2 * $20;
 HEAPF32[$1 + 20 >> 2] = $35 - $30;
 HEAPF32[$1 + 40 >> 2] = $35 - $26;
 HEAPF32[$1 + 16 >> 2] = $22 + $32;
 HEAPF32[$1 + 36 >> 2] = $27 + $24;
 HEAPF32[$1 + 8 >> 2] = $31 + $28;
 HEAPF32[$1 + 4 >> 2] = $22 - $32;
 HEAPF32[$1 + 24 >> 2] = $27 - $24;
 HEAPF32[$1 + 32 >> 2] = $31 - $28;
 HEAPF32[$1 + 12 >> 2] = 0.0;
 HEAPF32[$1 + 28 >> 2] = 0.0;
 $54 = $1 + 44 | 0;
 HEAP32[$54 >> 2] = 0;
 HEAP32[$54 + 4 >> 2] = 0;
 HEAP32[$54 + 8 >> 2] = 0;
 HEAP32[$54 + 12 >> 2] = 0;
 HEAPF32[$1 + 60 >> 2] = 1.0;
 return;
}

function _glmc_quat_mat4($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 var $13 = 0.0, $16 = 0.0, $19 = 0.0, $2 = 0.0, $20 = 0.0, $22 = 0.0, $23 = 0.0, $24 = 0.0, $25 = 0.0, $26 = 0.0, $27 = 0.0, $28 = 0.0, $30 = 0.0, $31 = 0.0, $32 = 0.0, $35 = 0.0, $5 = 0.0, $54 = 0, $9 = 0.0;
 $2 = +HEAPF32[$0 >> 2];
 $5 = +HEAPF32[$0 + 4 >> 2];
 $9 = +HEAPF32[$0 + 8 >> 2];
 $13 = +HEAPF32[$0 + 12 >> 2];
 $16 = +Math_sqrt(+($2 * $2 + $5 * $5 + $9 * $9 + $13 * $13));
 $19 = $16 > 0.0 ? 2.0 / $16 : 0.0;
 $20 = $2 * $19;
 $22 = $5 * $20;
 $23 = $13 * $19;
 $24 = $2 * $23;
 $25 = $5 * $19;
 $26 = $5 * $25;
 $27 = $9 * $25;
 $28 = $5 * $23;
 $30 = $9 * ($9 * $19);
 $31 = $9 * $20;
 $32 = $9 * $23;
 HEAPF32[$1 >> 2] = 1.0 - $26 - $30;
 $35 = 1.0 - $2 * $20;
 HEAPF32[$1 + 20 >> 2] = $35 - $30;
 HEAPF32[$1 + 40 >> 2] = $35 - $26;
 HEAPF32[$1 + 4 >> 2] = $22 + $32;
 HEAPF32[$1 + 24 >> 2] = $27 + $24;
 HEAPF32[$1 + 32 >> 2] = $31 + $28;
 HEAPF32[$1 + 16 >> 2] = $22 - $32;
 HEAPF32[$1 + 36 >> 2] = $27 - $24;
 HEAPF32[$1 + 8 >> 2] = $31 - $28;
 HEAPF32[$1 + 12 >> 2] = 0.0;
 HEAPF32[$1 + 28 >> 2] = 0.0;
 $54 = $1 + 44 | 0;
 HEAP32[$54 >> 2] = 0;
 HEAP32[$54 + 4 >> 2] = 0;
 HEAP32[$54 + 8 >> 2] = 0;
 HEAP32[$54 + 12 >> 2] = 0;
 HEAPF32[$1 + 60 >> 2] = 1.0;
 return;
}

function _glmc_mat4_scale_p($0, $1) {
 $0 = $0 | 0;
 $1 = +$1;
 var $10 = 0, $13 = 0, $16 = 0, $19 = 0, $22 = 0, $25 = 0, $28 = 0, $31 = 0, $34 = 0, $37 = 0, $4 = 0, $40 = 0, $43 = 0, $46 = 0, $7 = 0;
 HEAPF32[$0 >> 2] = +HEAPF32[$0 >> 2] * $1;
 $4 = $0 + 4 | 0;
 HEAPF32[$4 >> 2] = +HEAPF32[$4 >> 2] * $1;
 $7 = $0 + 8 | 0;
 HEAPF32[$7 >> 2] = +HEAPF32[$7 >> 2] * $1;
 $10 = $0 + 12 | 0;
 HEAPF32[$10 >> 2] = +HEAPF32[$10 >> 2] * $1;
 $13 = $0 + 16 | 0;
 HEAPF32[$13 >> 2] = +HEAPF32[$13 >> 2] * $1;
 $16 = $0 + 20 | 0;
 HEAPF32[$16 >> 2] = +HEAPF32[$16 >> 2] * $1;
 $19 = $0 + 24 | 0;
 HEAPF32[$19 >> 2] = +HEAPF32[$19 >> 2] * $1;
 $22 = $0 + 28 | 0;
 HEAPF32[$22 >> 2] = +HEAPF32[$22 >> 2] * $1;
 $25 = $0 + 32 | 0;
 HEAPF32[$25 >> 2] = +HEAPF32[$25 >> 2] * $1;
 $28 = $0 + 36 | 0;
 HEAPF32[$28 >> 2] = +HEAPF32[$28 >> 2] * $1;
 $31 = $0 + 40 | 0;
 HEAPF32[$31 >> 2] = +HEAPF32[$31 >> 2] * $1;
 $34 = $0 + 44 | 0;
 HEAPF32[$34 >> 2] = +HEAPF32[$34 >> 2] * $1;
 $37 = $0 + 48 | 0;
 HEAPF32[$37 >> 2] = +HEAPF32[$37 >> 2] * $1;
 $40 = $0 + 52 | 0;
 HEAPF32[$40 >> 2] = +HEAPF32[$40 >> 2] * $1;
 $43 = $0 + 56 | 0;
 HEAPF32[$43 >> 2] = +HEAPF32[$43 >> 2] * $1;
 $46 = $0 + 60 | 0;
 HEAPF32[$46 >> 2] = +HEAPF32[$46 >> 2] * $1;
 return;
}

function _glmc_mat4_scale($0, $1) {
 $0 = $0 | 0;
 $1 = +$1;
 var $10 = 0, $13 = 0, $16 = 0, $19 = 0, $22 = 0, $25 = 0, $28 = 0, $31 = 0, $34 = 0, $37 = 0, $4 = 0, $40 = 0, $43 = 0, $46 = 0, $7 = 0;
 HEAPF32[$0 >> 2] = +HEAPF32[$0 >> 2] * $1;
 $4 = $0 + 4 | 0;
 HEAPF32[$4 >> 2] = +HEAPF32[$4 >> 2] * $1;
 $7 = $0 + 8 | 0;
 HEAPF32[$7 >> 2] = +HEAPF32[$7 >> 2] * $1;
 $10 = $0 + 12 | 0;
 HEAPF32[$10 >> 2] = +HEAPF32[$10 >> 2] * $1;
 $13 = $0 + 16 | 0;
 HEAPF32[$13 >> 2] = +HEAPF32[$13 >> 2] * $1;
 $16 = $0 + 20 | 0;
 HEAPF32[$16 >> 2] = +HEAPF32[$16 >> 2] * $1;
 $19 = $0 + 24 | 0;
 HEAPF32[$19 >> 2] = +HEAPF32[$19 >> 2] * $1;
 $22 = $0 + 28 | 0;
 HEAPF32[$22 >> 2] = +HEAPF32[$22 >> 2] * $1;
 $25 = $0 + 32 | 0;
 HEAPF32[$25 >> 2] = +HEAPF32[$25 >> 2] * $1;
 $28 = $0 + 36 | 0;
 HEAPF32[$28 >> 2] = +HEAPF32[$28 >> 2] * $1;
 $31 = $0 + 40 | 0;
 HEAPF32[$31 >> 2] = +HEAPF32[$31 >> 2] * $1;
 $34 = $0 + 44 | 0;
 HEAPF32[$34 >> 2] = +HEAPF32[$34 >> 2] * $1;
 $37 = $0 + 48 | 0;
 HEAPF32[$37 >> 2] = +HEAPF32[$37 >> 2] * $1;
 $40 = $0 + 52 | 0;
 HEAPF32[$40 >> 2] = +HEAPF32[$40 >> 2] * $1;
 $43 = $0 + 56 | 0;
 HEAPF32[$43 >> 2] = +HEAPF32[$43 >> 2] * $1;
 $46 = $0 + 60 | 0;
 HEAPF32[$46 >> 2] = +HEAPF32[$46 >> 2] * $1;
 return;
}

function _glmc_vec_rotate($0, $1, $2) {
 $0 = $0 | 0;
 $1 = +$1;
 $2 = $2 | 0;
 var $$idx$val = 0.0, $$idx3$val = 0.0, $$sink$i$i = 0.0, $$sroa$0$0$i = 0.0, $$sroa$8$0$i = 0.0, $$val = 0.0, $10 = 0.0, $11 = 0, $12 = 0.0, $16 = 0.0, $18 = 0, $19 = 0.0, $21 = 0, $22 = 0.0, $3 = 0.0, $4 = 0.0, $45 = 0.0;
 $$val = +HEAPF32[$2 >> 2];
 $$idx$val = +HEAPF32[$2 + 4 >> 2];
 $$idx3$val = +HEAPF32[$2 + 8 >> 2];
 $3 = +Math_cos(+$1);
 $4 = +Math_sin(+$1);
 $10 = +Math_sqrt(+($$val * $$val + $$idx$val * $$idx$val + $$idx3$val * $$idx3$val));
 $11 = $10 == 0.0;
 $12 = 1.0 / $10;
 $$sroa$8$0$i = $11 ? 0.0 : $$idx$val * $12;
 $$sroa$0$0$i = $11 ? 0.0 : $$val * $12;
 $$sink$i$i = $11 ? 0.0 : $$idx3$val * $12;
 $16 = +HEAPF32[$0 >> 2];
 $18 = $0 + 4 | 0;
 $19 = +HEAPF32[$18 >> 2];
 $21 = $0 + 8 | 0;
 $22 = +HEAPF32[$21 >> 2];
 $45 = (1.0 - $3) * ($$sink$i$i * $22 + ($16 * $$sroa$0$0$i + $19 * $$sroa$8$0$i));
 HEAPF32[$0 >> 2] = $3 * $16 + $4 * ($$sroa$8$0$i * $22 - $19 * $$sink$i$i) + $$sroa$0$0$i * $45;
 HEAPF32[$18 >> 2] = $3 * $19 + $4 * ($16 * $$sink$i$i - $$sroa$0$0$i * $22) + $$sroa$8$0$i * $45;
 HEAPF32[$21 >> 2] = $3 * $22 + $4 * ($19 * $$sroa$0$0$i - $16 * $$sroa$8$0$i) + $$sink$i$i * $45;
 return;
}

function _glmc_mat4_transpose($0) {
 $0 = $0 | 0;
 var $1 = 0, $10 = 0, $12 = 0, $14 = 0, $15 = 0, $16 = 0, $17 = 0, $18 = 0, $2 = 0, $20 = 0, $22 = 0, $24 = 0, $26 = 0, $28 = 0, $30 = 0, $6 = 0, $8 = 0;
 $1 = $0 + 4 | 0;
 $2 = HEAP32[$1 >> 2] | 0;
 $6 = HEAP32[$0 + 20 >> 2] | 0;
 $8 = HEAP32[$0 + 32 >> 2] | 0;
 $10 = HEAP32[$0 + 36 >> 2] | 0;
 $12 = HEAP32[$0 + 48 >> 2] | 0;
 $14 = HEAP32[$0 + 52 >> 2] | 0;
 $15 = $0 + 8 | 0;
 $16 = HEAP32[$15 >> 2] | 0;
 $17 = $0 + 12 | 0;
 $18 = HEAP32[$17 >> 2] | 0;
 $20 = HEAP32[$0 + 24 >> 2] | 0;
 $22 = HEAP32[$0 + 28 >> 2] | 0;
 $24 = HEAP32[$0 + 40 >> 2] | 0;
 $26 = HEAP32[$0 + 44 >> 2] | 0;
 $28 = HEAP32[$0 + 56 >> 2] | 0;
 $30 = HEAP32[$0 + 60 >> 2] | 0;
 HEAP32[$1 >> 2] = HEAP32[$0 + 16 >> 2];
 HEAP32[$15 >> 2] = $8;
 HEAP32[$17 >> 2] = $12;
 HEAP32[$0 + 16 >> 2] = $2;
 HEAP32[$0 + 20 >> 2] = $6;
 HEAP32[$0 + 24 >> 2] = $10;
 HEAP32[$0 + 28 >> 2] = $14;
 HEAP32[$0 + 32 >> 2] = $16;
 HEAP32[$0 + 36 >> 2] = $20;
 HEAP32[$0 + 40 >> 2] = $24;
 HEAP32[$0 + 44 >> 2] = $28;
 HEAP32[$0 + 48 >> 2] = $18;
 HEAP32[$0 + 52 >> 2] = $22;
 HEAP32[$0 + 56 >> 2] = $26;
 HEAP32[$0 + 60 >> 2] = $30;
 return;
}

function _glmc_euler_yzx($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 var $$idx$val = 0.0, $$idx2$val = 0.0, $$val = 0.0, $10 = 0.0, $11 = 0.0, $14 = 0.0, $2 = 0.0, $3 = 0.0, $35 = 0, $4 = 0.0, $5 = 0.0, $6 = 0.0, $7 = 0.0, $8 = 0.0, $9 = 0.0;
 $$val = +HEAPF32[$0 >> 2];
 $$idx$val = +HEAPF32[$0 + 4 >> 2];
 $$idx2$val = +HEAPF32[$0 + 8 >> 2];
 $2 = +Math_sin(+$$val);
 $3 = +Math_cos(+$$val);
 $4 = +Math_sin(+$$idx$val);
 $5 = +Math_cos(+$$idx$val);
 $6 = +Math_sin(+$$idx2$val);
 $7 = +Math_cos(+$$idx2$val);
 $8 = $2 * $4;
 $9 = $3 * $5;
 $10 = $2 * $5;
 $11 = $3 * $4;
 HEAPF32[$1 >> 2] = $5 * $7;
 HEAPF32[$1 + 4 >> 2] = $6;
 $14 = -$7;
 HEAPF32[$1 + 8 >> 2] = $4 * $14;
 HEAPF32[$1 + 16 >> 2] = $8 - $9 * $6;
 HEAPF32[$1 + 20 >> 2] = $3 * $7;
 HEAPF32[$1 + 24 >> 2] = $10 + $11 * $6;
 HEAPF32[$1 + 32 >> 2] = $11 + $10 * $6;
 HEAPF32[$1 + 36 >> 2] = $2 * $14;
 HEAPF32[$1 + 40 >> 2] = $9 - $8 * $6;
 HEAPF32[$1 + 12 >> 2] = 0.0;
 HEAPF32[$1 + 28 >> 2] = 0.0;
 $35 = $1 + 44 | 0;
 HEAP32[$35 >> 2] = 0;
 HEAP32[$35 + 4 >> 2] = 0;
 HEAP32[$35 + 8 >> 2] = 0;
 HEAP32[$35 + 12 >> 2] = 0;
 HEAPF32[$1 + 60 >> 2] = 1.0;
 return;
}

function _glmc_euler_zxy($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 var $$idx$val = 0.0, $$idx2$val = 0.0, $$val = 0.0, $10 = 0.0, $16 = 0.0, $2 = 0.0, $3 = 0.0, $36 = 0, $4 = 0.0, $5 = 0.0, $6 = 0.0, $7 = 0.0, $8 = 0.0, $9 = 0.0;
 $$val = +HEAPF32[$0 >> 2];
 $$idx$val = +HEAPF32[$0 + 4 >> 2];
 $$idx2$val = +HEAPF32[$0 + 8 >> 2];
 $2 = +Math_sin(+$$val);
 $3 = +Math_cos(+$$val);
 $4 = +Math_sin(+$$idx$val);
 $5 = +Math_cos(+$$idx$val);
 $6 = +Math_sin(+$$idx2$val);
 $7 = +Math_cos(+$$idx2$val);
 $8 = $5 * $7;
 $9 = $2 * $4;
 $10 = $5 * $6;
 HEAPF32[$1 >> 2] = $8 - $9 * $6;
 HEAPF32[$1 + 4 >> 2] = $10 + $9 * $7;
 $16 = -$3;
 HEAPF32[$1 + 8 >> 2] = $4 * $16;
 HEAPF32[$1 + 16 >> 2] = $6 * $16;
 HEAPF32[$1 + 20 >> 2] = $3 * $7;
 HEAPF32[$1 + 24 >> 2] = $2;
 HEAPF32[$1 + 32 >> 2] = $4 * $7 + $2 * $10;
 HEAPF32[$1 + 36 >> 2] = $4 * $6 - $2 * $8;
 HEAPF32[$1 + 40 >> 2] = $3 * $5;
 HEAPF32[$1 + 12 >> 2] = 0.0;
 HEAPF32[$1 + 28 >> 2] = 0.0;
 $36 = $1 + 44 | 0;
 HEAP32[$36 >> 2] = 0;
 HEAP32[$36 + 4 >> 2] = 0;
 HEAP32[$36 + 8 >> 2] = 0;
 HEAP32[$36 + 12 >> 2] = 0;
 HEAPF32[$1 + 60 >> 2] = 1.0;
 return;
}

function _glmc_euler_xyz($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 var $$idx$val = 0.0, $$idx2$val = 0.0, $$val = 0.0, $10 = 0.0, $2 = 0.0, $20 = 0.0, $3 = 0.0, $36 = 0, $4 = 0.0, $5 = 0.0, $6 = 0.0, $7 = 0.0, $8 = 0.0, $9 = 0.0;
 $$val = +HEAPF32[$0 >> 2];
 $$idx$val = +HEAPF32[$0 + 4 >> 2];
 $$idx2$val = +HEAPF32[$0 + 8 >> 2];
 $2 = +Math_sin(+$$val);
 $3 = +Math_cos(+$$val);
 $4 = +Math_sin(+$$idx$val);
 $5 = +Math_cos(+$$idx$val);
 $6 = +Math_sin(+$$idx2$val);
 $7 = +Math_cos(+$$idx2$val);
 $8 = $2 * $7;
 $9 = $3 * $7;
 $10 = $4 * $6;
 HEAPF32[$1 >> 2] = $5 * $7;
 HEAPF32[$1 + 4 >> 2] = $3 * $6 + $4 * $8;
 HEAPF32[$1 + 8 >> 2] = $2 * $6 - $4 * $9;
 $20 = -$5;
 HEAPF32[$1 + 16 >> 2] = $6 * $20;
 HEAPF32[$1 + 20 >> 2] = $9 - $2 * $10;
 HEAPF32[$1 + 24 >> 2] = $8 + $3 * $10;
 HEAPF32[$1 + 32 >> 2] = $4;
 HEAPF32[$1 + 36 >> 2] = $2 * $20;
 HEAPF32[$1 + 40 >> 2] = $3 * $5;
 HEAPF32[$1 + 12 >> 2] = 0.0;
 HEAPF32[$1 + 28 >> 2] = 0.0;
 $36 = $1 + 44 | 0;
 HEAP32[$36 >> 2] = 0;
 HEAP32[$36 + 4 >> 2] = 0;
 HEAP32[$36 + 8 >> 2] = 0;
 HEAP32[$36 + 12 >> 2] = 0;
 HEAPF32[$1 + 60 >> 2] = 1.0;
 return;
}

function _glmc_euler($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 var $$idx$val = 0.0, $$idx2$val = 0.0, $$val = 0.0, $10 = 0.0, $2 = 0.0, $20 = 0.0, $3 = 0.0, $36 = 0, $4 = 0.0, $5 = 0.0, $6 = 0.0, $7 = 0.0, $8 = 0.0, $9 = 0.0;
 $$val = +HEAPF32[$0 >> 2];
 $$idx$val = +HEAPF32[$0 + 4 >> 2];
 $$idx2$val = +HEAPF32[$0 + 8 >> 2];
 $2 = +Math_sin(+$$val);
 $3 = +Math_cos(+$$val);
 $4 = +Math_sin(+$$idx$val);
 $5 = +Math_cos(+$$idx$val);
 $6 = +Math_sin(+$$idx2$val);
 $7 = +Math_cos(+$$idx2$val);
 $8 = $2 * $7;
 $9 = $3 * $7;
 $10 = $4 * $6;
 HEAPF32[$1 >> 2] = $5 * $7;
 HEAPF32[$1 + 4 >> 2] = $3 * $6 + $4 * $8;
 HEAPF32[$1 + 8 >> 2] = $2 * $6 - $4 * $9;
 $20 = -$5;
 HEAPF32[$1 + 16 >> 2] = $6 * $20;
 HEAPF32[$1 + 20 >> 2] = $9 - $2 * $10;
 HEAPF32[$1 + 24 >> 2] = $8 + $3 * $10;
 HEAPF32[$1 + 32 >> 2] = $4;
 HEAPF32[$1 + 36 >> 2] = $2 * $20;
 HEAPF32[$1 + 40 >> 2] = $3 * $5;
 HEAPF32[$1 + 12 >> 2] = 0.0;
 HEAPF32[$1 + 28 >> 2] = 0.0;
 $36 = $1 + 44 | 0;
 HEAP32[$36 >> 2] = 0;
 HEAP32[$36 + 4 >> 2] = 0;
 HEAP32[$36 + 8 >> 2] = 0;
 HEAP32[$36 + 12 >> 2] = 0;
 HEAPF32[$1 + 60 >> 2] = 1.0;
 return;
}

function _glmc_euler_yxz($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 var $$idx$val = 0.0, $$idx2$val = 0.0, $$val = 0.0, $10 = 0.0, $11 = 0.0, $2 = 0.0, $3 = 0.0, $35 = 0, $4 = 0.0, $5 = 0.0, $6 = 0.0, $7 = 0.0, $8 = 0.0, $9 = 0.0;
 $$val = +HEAPF32[$0 >> 2];
 $$idx$val = +HEAPF32[$0 + 4 >> 2];
 $$idx2$val = +HEAPF32[$0 + 8 >> 2];
 $2 = +Math_sin(+$$val);
 $3 = +Math_cos(+$$val);
 $4 = +Math_sin(+$$idx$val);
 $5 = +Math_cos(+$$idx$val);
 $6 = +Math_sin(+$$idx2$val);
 $7 = +Math_cos(+$$idx2$val);
 $8 = $5 * $7;
 $9 = $4 * $6;
 $10 = $4 * $7;
 $11 = $5 * $6;
 HEAPF32[$1 >> 2] = $8 + $2 * $9;
 HEAPF32[$1 + 4 >> 2] = $3 * $6;
 HEAPF32[$1 + 8 >> 2] = $2 * $11 - $10;
 HEAPF32[$1 + 16 >> 2] = $2 * $10 - $11;
 HEAPF32[$1 + 20 >> 2] = $3 * $7;
 HEAPF32[$1 + 24 >> 2] = $9 + $2 * $8;
 HEAPF32[$1 + 32 >> 2] = $3 * $4;
 HEAPF32[$1 + 36 >> 2] = -$2;
 HEAPF32[$1 + 40 >> 2] = $3 * $5;
 HEAPF32[$1 + 12 >> 2] = 0.0;
 HEAPF32[$1 + 28 >> 2] = 0.0;
 $35 = $1 + 44 | 0;
 HEAP32[$35 >> 2] = 0;
 HEAP32[$35 + 4 >> 2] = 0;
 HEAP32[$35 + 8 >> 2] = 0;
 HEAP32[$35 + 12 >> 2] = 0;
 HEAPF32[$1 + 60 >> 2] = 1.0;
 return;
}

function _glmc_euler_xzy($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 var $$idx$val = 0.0, $$idx2$val = 0.0, $$val = 0.0, $10 = 0.0, $11 = 0.0, $2 = 0.0, $3 = 0.0, $35 = 0, $4 = 0.0, $5 = 0.0, $6 = 0.0, $7 = 0.0, $8 = 0.0, $9 = 0.0;
 $$val = +HEAPF32[$0 >> 2];
 $$idx$val = +HEAPF32[$0 + 4 >> 2];
 $$idx2$val = +HEAPF32[$0 + 8 >> 2];
 $2 = +Math_sin(+$$val);
 $3 = +Math_cos(+$$val);
 $4 = +Math_sin(+$$idx$val);
 $5 = +Math_cos(+$$idx$val);
 $6 = +Math_sin(+$$idx2$val);
 $7 = +Math_cos(+$$idx2$val);
 $8 = $2 * $4;
 $9 = $2 * $5;
 $10 = $3 * $4;
 $11 = $3 * $5;
 HEAPF32[$1 >> 2] = $5 * $7;
 HEAPF32[$1 + 4 >> 2] = $8 + $11 * $6;
 HEAPF32[$1 + 8 >> 2] = $9 * $6 - $10;
 HEAPF32[$1 + 16 >> 2] = -$6;
 HEAPF32[$1 + 20 >> 2] = $3 * $7;
 HEAPF32[$1 + 24 >> 2] = $2 * $7;
 HEAPF32[$1 + 32 >> 2] = $4 * $7;
 HEAPF32[$1 + 36 >> 2] = $10 * $6 - $9;
 HEAPF32[$1 + 40 >> 2] = $11 + $8 * $6;
 HEAPF32[$1 + 12 >> 2] = 0.0;
 HEAPF32[$1 + 28 >> 2] = 0.0;
 $35 = $1 + 44 | 0;
 HEAP32[$35 >> 2] = 0;
 HEAP32[$35 + 4 >> 2] = 0;
 HEAP32[$35 + 8 >> 2] = 0;
 HEAP32[$35 + 12 >> 2] = 0;
 HEAPF32[$1 + 60 >> 2] = 1.0;
 return;
}

function _glmc_euler_zyx($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 var $$idx$val = 0.0, $$idx2$val = 0.0, $$val = 0.0, $10 = 0.0, $2 = 0.0, $3 = 0.0, $36 = 0, $4 = 0.0, $5 = 0.0, $6 = 0.0, $7 = 0.0, $8 = 0.0, $9 = 0.0;
 $$val = +HEAPF32[$0 >> 2];
 $$idx$val = +HEAPF32[$0 + 4 >> 2];
 $$idx2$val = +HEAPF32[$0 + 8 >> 2];
 $2 = +Math_sin(+$$val);
 $3 = +Math_cos(+$$val);
 $4 = +Math_sin(+$$idx$val);
 $5 = +Math_cos(+$$idx$val);
 $6 = +Math_sin(+$$idx2$val);
 $7 = +Math_cos(+$$idx2$val);
 $8 = $2 * $7;
 $9 = $3 * $7;
 $10 = $4 * $6;
 HEAPF32[$1 >> 2] = $5 * $7;
 HEAPF32[$1 + 4 >> 2] = $5 * $6;
 HEAPF32[$1 + 8 >> 2] = -$4;
 HEAPF32[$1 + 16 >> 2] = $4 * $8 - $3 * $6;
 HEAPF32[$1 + 20 >> 2] = $9 + $2 * $10;
 HEAPF32[$1 + 24 >> 2] = $2 * $5;
 HEAPF32[$1 + 32 >> 2] = $2 * $6 + $4 * $9;
 HEAPF32[$1 + 36 >> 2] = $3 * $10 - $8;
 HEAPF32[$1 + 40 >> 2] = $3 * $5;
 HEAPF32[$1 + 12 >> 2] = 0.0;
 HEAPF32[$1 + 28 >> 2] = 0.0;
 $36 = $1 + 44 | 0;
 HEAP32[$36 >> 2] = 0;
 HEAP32[$36 + 4 >> 2] = 0;
 HEAP32[$36 + 8 >> 2] = 0;
 HEAP32[$36 + 12 >> 2] = 0;
 HEAPF32[$1 + 60 >> 2] = 1.0;
 return;
}

function _glmc_translate($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 var $$idx$val = 0.0, $$idx2$val = 0.0, $$val = 0.0, $30 = 0.0, $33 = 0.0, $36 = 0.0, $37 = 0, $40 = 0, $43 = 0, $46 = 0, $50 = 0.0, $51 = 0.0, $52 = 0.0;
 $$val = +HEAPF32[$1 >> 2];
 $$idx$val = +HEAPF32[$1 + 4 >> 2];
 $$idx2$val = +HEAPF32[$1 + 8 >> 2];
 $30 = $$idx2$val * +HEAPF32[$0 + 36 >> 2];
 $33 = $$idx2$val * +HEAPF32[$0 + 40 >> 2];
 $36 = $$idx2$val * +HEAPF32[$0 + 44 >> 2];
 $37 = $0 + 48 | 0;
 $40 = $0 + 52 | 0;
 $43 = $0 + 56 | 0;
 $46 = $0 + 60 | 0;
 $50 = $$idx$val * +HEAPF32[$0 + 20 >> 2] + ($$val * +HEAPF32[$0 + 4 >> 2] + +HEAPF32[$40 >> 2]);
 $51 = $$idx$val * +HEAPF32[$0 + 24 >> 2] + ($$val * +HEAPF32[$0 + 8 >> 2] + +HEAPF32[$43 >> 2]);
 $52 = $$idx$val * +HEAPF32[$0 + 28 >> 2] + ($$val * +HEAPF32[$0 + 12 >> 2] + +HEAPF32[$46 >> 2]);
 HEAPF32[$37 >> 2] = $$idx2$val * +HEAPF32[$0 + 32 >> 2] + ($$idx$val * +HEAPF32[$0 + 16 >> 2] + ($$val * +HEAPF32[$0 >> 2] + +HEAPF32[$37 >> 2]));
 HEAPF32[$40 >> 2] = $30 + $50;
 HEAPF32[$43 >> 2] = $33 + $51;
 HEAPF32[$46 >> 2] = $36 + $52;
 return;
}

function _glmc_frustum_center($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 var $86 = 0.0, $89 = 0.0, $92 = 0.0;
 $86 = +HEAPF32[$0 + 4 >> 2] + +HEAPF32[$0 + 20 >> 2] + +HEAPF32[$0 + 36 >> 2] + +HEAPF32[$0 + 52 >> 2] + +HEAPF32[$0 + 68 >> 2] + +HEAPF32[$0 + 84 >> 2] + +HEAPF32[$0 + 100 >> 2] + +HEAPF32[$0 + 116 >> 2];
 $89 = +HEAPF32[$0 + 8 >> 2] + +HEAPF32[$0 + 24 >> 2] + +HEAPF32[$0 + 40 >> 2] + +HEAPF32[$0 + 56 >> 2] + +HEAPF32[$0 + 72 >> 2] + +HEAPF32[$0 + 88 >> 2] + +HEAPF32[$0 + 104 >> 2] + +HEAPF32[$0 + 120 >> 2];
 $92 = +HEAPF32[$0 + 12 >> 2] + +HEAPF32[$0 + 28 >> 2] + +HEAPF32[$0 + 44 >> 2] + +HEAPF32[$0 + 60 >> 2] + +HEAPF32[$0 + 76 >> 2] + +HEAPF32[$0 + 92 >> 2] + +HEAPF32[$0 + 108 >> 2] + +HEAPF32[$0 + 124 >> 2];
 HEAPF32[$1 >> 2] = (+HEAPF32[$0 >> 2] + +HEAPF32[$0 + 16 >> 2] + +HEAPF32[$0 + 32 >> 2] + +HEAPF32[$0 + 48 >> 2] + +HEAPF32[$0 + 64 >> 2] + +HEAPF32[$0 + 80 >> 2] + +HEAPF32[$0 + 96 >> 2] + +HEAPF32[$0 + 112 >> 2]) * .125;
 HEAPF32[$1 + 4 >> 2] = $86 * .125;
 HEAPF32[$1 + 8 >> 2] = $89 * .125;
 HEAPF32[$1 + 12 >> 2] = $92 * .125;
 return;
}

function _glmc_mat4_det($0) {
 $0 = $0 | 0;
 var $11 = 0.0, $13 = 0.0, $15 = 0.0, $17 = 0.0, $19 = 0.0, $21 = 0.0, $23 = 0.0, $25 = 0.0, $27 = 0.0, $29 = 0.0, $31 = 0.0, $34 = 0.0, $37 = 0.0, $40 = 0.0, $43 = 0.0, $46 = 0.0, $49 = 0.0, $9 = 0.0;
 $9 = +HEAPF32[$0 + 16 >> 2];
 $11 = +HEAPF32[$0 + 20 >> 2];
 $13 = +HEAPF32[$0 + 24 >> 2];
 $15 = +HEAPF32[$0 + 28 >> 2];
 $17 = +HEAPF32[$0 + 32 >> 2];
 $19 = +HEAPF32[$0 + 36 >> 2];
 $21 = +HEAPF32[$0 + 40 >> 2];
 $23 = +HEAPF32[$0 + 44 >> 2];
 $25 = +HEAPF32[$0 + 48 >> 2];
 $27 = +HEAPF32[$0 + 52 >> 2];
 $29 = +HEAPF32[$0 + 56 >> 2];
 $31 = +HEAPF32[$0 + 60 >> 2];
 $34 = $21 * $31 - $23 * $29;
 $37 = $19 * $31 - $23 * $27;
 $40 = $19 * $29 - $21 * $27;
 $43 = $17 * $31 - $23 * $25;
 $46 = $17 * $29 - $21 * $25;
 $49 = $17 * $27 - $19 * $25;
 return +(+HEAPF32[$0 + 8 >> 2] * ($15 * $49 + ($9 * $37 - $11 * $43)) + (+HEAPF32[$0 >> 2] * ($15 * $40 + ($11 * $34 - $13 * $37)) - +HEAPF32[$0 + 4 >> 2] * ($15 * $46 + ($9 * $34 - $13 * $43))) - +HEAPF32[$0 + 12 >> 2] * ($13 * $49 + ($9 * $40 - $11 * $46)));
}

function _glmc_scale($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 var $11 = 0, $14 = 0, $16 = 0.0, $19 = 0, $2 = 0.0, $22 = 0, $25 = 0, $28 = 0, $30 = 0.0, $33 = 0, $36 = 0, $39 = 0, $5 = 0, $8 = 0;
 $2 = +HEAPF32[$1 >> 2];
 HEAPF32[$0 >> 2] = $2 * +HEAPF32[$0 >> 2];
 $5 = $0 + 4 | 0;
 HEAPF32[$5 >> 2] = $2 * +HEAPF32[$5 >> 2];
 $8 = $0 + 8 | 0;
 HEAPF32[$8 >> 2] = $2 * +HEAPF32[$8 >> 2];
 $11 = $0 + 12 | 0;
 HEAPF32[$11 >> 2] = $2 * +HEAPF32[$11 >> 2];
 $14 = $0 + 16 | 0;
 $16 = +HEAPF32[$1 + 4 >> 2];
 HEAPF32[$14 >> 2] = $16 * +HEAPF32[$14 >> 2];
 $19 = $0 + 20 | 0;
 HEAPF32[$19 >> 2] = $16 * +HEAPF32[$19 >> 2];
 $22 = $0 + 24 | 0;
 HEAPF32[$22 >> 2] = $16 * +HEAPF32[$22 >> 2];
 $25 = $0 + 28 | 0;
 HEAPF32[$25 >> 2] = $16 * +HEAPF32[$25 >> 2];
 $28 = $0 + 32 | 0;
 $30 = +HEAPF32[$1 + 8 >> 2];
 HEAPF32[$28 >> 2] = $30 * +HEAPF32[$28 >> 2];
 $33 = $0 + 36 | 0;
 HEAPF32[$33 >> 2] = $30 * +HEAPF32[$33 >> 2];
 $36 = $0 + 40 | 0;
 HEAPF32[$36 >> 2] = $30 * +HEAPF32[$36 >> 2];
 $39 = $0 + 44 | 0;
 HEAPF32[$39 >> 2] = $30 * +HEAPF32[$39 >> 2];
 return;
}

function _glmc_mat3_inv($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 var $10 = 0.0, $12 = 0.0, $14 = 0.0, $16 = 0.0, $18 = 0.0, $2 = 0.0, $21 = 0.0, $33 = 0.0, $4 = 0.0, $45 = 0.0, $6 = 0.0, $60 = 0.0, $8 = 0.0;
 $2 = +HEAPF32[$0 >> 2];
 $4 = +HEAPF32[$0 + 4 >> 2];
 $6 = +HEAPF32[$0 + 8 >> 2];
 $8 = +HEAPF32[$0 + 12 >> 2];
 $10 = +HEAPF32[$0 + 16 >> 2];
 $12 = +HEAPF32[$0 + 20 >> 2];
 $14 = +HEAPF32[$0 + 24 >> 2];
 $16 = +HEAPF32[$0 + 28 >> 2];
 $18 = +HEAPF32[$0 + 32 >> 2];
 $21 = $10 * $18 - $12 * $16;
 $33 = -($8 * $18 - $12 * $14);
 $45 = $8 * $16 - $10 * $14;
 $60 = 1.0 / ($6 * $45 + ($2 * $21 + $4 * $33));
 HEAPF32[$1 >> 2] = $21 * $60;
 HEAPF32[$1 + 4 >> 2] = -(($4 * $18 - $6 * $16) * $60);
 HEAPF32[$1 + 8 >> 2] = ($4 * $12 - $6 * $10) * $60;
 HEAPF32[$1 + 12 >> 2] = $60 * $33;
 HEAPF32[$1 + 16 >> 2] = ($2 * $18 - $6 * $14) * $60;
 HEAPF32[$1 + 20 >> 2] = -(($2 * $12 - $6 * $8) * $60);
 HEAPF32[$1 + 24 >> 2] = $45 * $60;
 HEAPF32[$1 + 28 >> 2] = -(($2 * $16 - $4 * $14) * $60);
 HEAPF32[$1 + 32 >> 2] = ($2 * $10 - $4 * $8) * $60;
 return;
}

function _glmc_ortho_default($0, $1) {
 $0 = +$0;
 $1 = $1 | 0;
 var $$sink$i = 0.0, $$sink7$i = 0.0, $$sink8$i = 0.0, $$sink9$i = 0.0, $10 = 0.0, $11 = 0.0, $14 = 0.0, $5 = 0.0, dest = 0, stop = 0;
 if (!($0 >= 1.0)) {
  $10 = -1.0 / $0;
  $11 = 1.0 / $0;
  dest = $1 + 4 | 0;
  stop = dest + 44 | 0;
  do {
   HEAP32[dest >> 2] = 0;
   dest = dest + 4 | 0;
  } while ((dest | 0) < (stop | 0));
  $14 = 1.0 / ($11 - $10);
  $$sink$i = -(($10 + $11) * $14);
  $$sink7$i = -0.0;
  $$sink8$i = $14 * 2.0;
  $$sink9$i = 1.0;
 } else {
  dest = $1 + 4 | 0;
  stop = dest + 44 | 0;
  do {
   HEAP32[dest >> 2] = 0;
   dest = dest + 4 | 0;
  } while ((dest | 0) < (stop | 0));
  $5 = 1.0 / ($0 + $0);
  $$sink$i = -0.0;
  $$sink7$i = -(($0 - $0) * $5);
  $$sink8$i = 1.0;
  $$sink9$i = $5 * 2.0;
 }
 HEAPF32[$1 >> 2] = $$sink9$i;
 HEAPF32[$1 + 20 >> 2] = $$sink8$i;
 HEAPF32[$1 + 40 >> 2] = -.009999999776482582;
 HEAPF32[$1 + 48 >> 2] = $$sink7$i;
 HEAPF32[$1 + 52 >> 2] = $$sink$i;
 HEAPF32[$1 + 56 >> 2] = -0.0;
 HEAPF32[$1 + 60 >> 2] = 1.0;
 return;
}

function _glmc_scale_to($0, $1, $2) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = $2 | 0;
 var $20 = 0.0, $3 = 0.0, $38 = 0.0;
 $3 = +HEAPF32[$1 >> 2];
 HEAPF32[$2 >> 2] = $3 * +HEAPF32[$0 >> 2];
 HEAPF32[$2 + 4 >> 2] = $3 * +HEAPF32[$0 + 4 >> 2];
 HEAPF32[$2 + 8 >> 2] = $3 * +HEAPF32[$0 + 8 >> 2];
 HEAPF32[$2 + 12 >> 2] = $3 * +HEAPF32[$0 + 12 >> 2];
 $20 = +HEAPF32[$1 + 4 >> 2];
 HEAPF32[$2 + 16 >> 2] = $20 * +HEAPF32[$0 + 16 >> 2];
 HEAPF32[$2 + 20 >> 2] = $20 * +HEAPF32[$0 + 20 >> 2];
 HEAPF32[$2 + 24 >> 2] = $20 * +HEAPF32[$0 + 24 >> 2];
 HEAPF32[$2 + 28 >> 2] = $20 * +HEAPF32[$0 + 28 >> 2];
 $38 = +HEAPF32[$1 + 8 >> 2];
 HEAPF32[$2 + 32 >> 2] = $38 * +HEAPF32[$0 + 32 >> 2];
 HEAPF32[$2 + 36 >> 2] = $38 * +HEAPF32[$0 + 36 >> 2];
 HEAPF32[$2 + 40 >> 2] = $38 * +HEAPF32[$0 + 40 >> 2];
 HEAPF32[$2 + 44 >> 2] = $38 * +HEAPF32[$0 + 44 >> 2];
 HEAP32[$2 + 48 >> 2] = HEAP32[$0 + 48 >> 2];
 HEAP32[$2 + 52 >> 2] = HEAP32[$0 + 52 >> 2];
 HEAP32[$2 + 56 >> 2] = HEAP32[$0 + 56 >> 2];
 HEAP32[$2 + 60 >> 2] = HEAP32[$0 + 60 >> 2];
 return;
}

function _glmc_quat_mat3t($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 var $13 = 0.0, $16 = 0.0, $19 = 0.0, $2 = 0.0, $20 = 0.0, $22 = 0.0, $23 = 0.0, $24 = 0.0, $25 = 0.0, $26 = 0.0, $27 = 0.0, $28 = 0.0, $30 = 0.0, $31 = 0.0, $32 = 0.0, $35 = 0.0, $5 = 0.0, $9 = 0.0;
 $2 = +HEAPF32[$0 >> 2];
 $5 = +HEAPF32[$0 + 4 >> 2];
 $9 = +HEAPF32[$0 + 8 >> 2];
 $13 = +HEAPF32[$0 + 12 >> 2];
 $16 = +Math_sqrt(+($2 * $2 + $5 * $5 + $9 * $9 + $13 * $13));
 $19 = $16 > 0.0 ? 2.0 / $16 : 0.0;
 $20 = $2 * $19;
 $22 = $5 * $20;
 $23 = $13 * $19;
 $24 = $2 * $23;
 $25 = $5 * $19;
 $26 = $5 * $25;
 $27 = $9 * $25;
 $28 = $5 * $23;
 $30 = $9 * ($9 * $19);
 $31 = $9 * $20;
 $32 = $9 * $23;
 HEAPF32[$1 >> 2] = 1.0 - $26 - $30;
 $35 = 1.0 - $2 * $20;
 HEAPF32[$1 + 16 >> 2] = $35 - $30;
 HEAPF32[$1 + 32 >> 2] = $35 - $26;
 HEAPF32[$1 + 12 >> 2] = $22 + $32;
 HEAPF32[$1 + 28 >> 2] = $27 + $24;
 HEAPF32[$1 + 8 >> 2] = $31 + $28;
 HEAPF32[$1 + 4 >> 2] = $22 - $32;
 HEAPF32[$1 + 20 >> 2] = $27 - $24;
 HEAPF32[$1 + 24 >> 2] = $31 - $28;
 return;
}

function _glmc_quat_mat3($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 var $13 = 0.0, $16 = 0.0, $19 = 0.0, $2 = 0.0, $20 = 0.0, $22 = 0.0, $23 = 0.0, $24 = 0.0, $25 = 0.0, $26 = 0.0, $27 = 0.0, $28 = 0.0, $30 = 0.0, $31 = 0.0, $32 = 0.0, $35 = 0.0, $5 = 0.0, $9 = 0.0;
 $2 = +HEAPF32[$0 >> 2];
 $5 = +HEAPF32[$0 + 4 >> 2];
 $9 = +HEAPF32[$0 + 8 >> 2];
 $13 = +HEAPF32[$0 + 12 >> 2];
 $16 = +Math_sqrt(+($2 * $2 + $5 * $5 + $9 * $9 + $13 * $13));
 $19 = $16 > 0.0 ? 2.0 / $16 : 0.0;
 $20 = $2 * $19;
 $22 = $5 * $20;
 $23 = $13 * $19;
 $24 = $2 * $23;
 $25 = $5 * $19;
 $26 = $5 * $25;
 $27 = $9 * $25;
 $28 = $5 * $23;
 $30 = $9 * ($9 * $19);
 $31 = $9 * $20;
 $32 = $9 * $23;
 HEAPF32[$1 >> 2] = 1.0 - $26 - $30;
 $35 = 1.0 - $2 * $20;
 HEAPF32[$1 + 16 >> 2] = $35 - $30;
 HEAPF32[$1 + 32 >> 2] = $35 - $26;
 HEAPF32[$1 + 4 >> 2] = $22 + $32;
 HEAPF32[$1 + 20 >> 2] = $27 + $24;
 HEAPF32[$1 + 24 >> 2] = $31 + $28;
 HEAPF32[$1 + 12 >> 2] = $22 - $32;
 HEAPF32[$1 + 28 >> 2] = $27 - $24;
 HEAPF32[$1 + 8 >> 2] = $31 - $28;
 return;
}

function _glmc_quat_mul($0, $1, $2) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = $2 | 0;
 var $12 = 0, $14 = 0, $18 = 0, $20 = 0, $3 = 0, $8 = 0;
 $3 = $0 + 12 | 0;
 $8 = $1 + 12 | 0;
 $12 = $0 + 4 | 0;
 $14 = $1 + 8 | 0;
 $18 = $0 + 8 | 0;
 $20 = $1 + 4 | 0;
 HEAPF32[$2 >> 2] = +HEAPF32[$3 >> 2] * +HEAPF32[$1 >> 2] + +HEAPF32[$0 >> 2] * +HEAPF32[$8 >> 2] + +HEAPF32[$12 >> 2] * +HEAPF32[$14 >> 2] - +HEAPF32[$18 >> 2] * +HEAPF32[$20 >> 2];
 HEAPF32[$2 + 4 >> 2] = +HEAPF32[$3 >> 2] * +HEAPF32[$20 >> 2] - +HEAPF32[$0 >> 2] * +HEAPF32[$14 >> 2] + +HEAPF32[$12 >> 2] * +HEAPF32[$8 >> 2] + +HEAPF32[$18 >> 2] * +HEAPF32[$1 >> 2];
 HEAPF32[$2 + 8 >> 2] = +HEAPF32[$3 >> 2] * +HEAPF32[$14 >> 2] + +HEAPF32[$0 >> 2] * +HEAPF32[$20 >> 2] - +HEAPF32[$12 >> 2] * +HEAPF32[$1 >> 2] + +HEAPF32[$18 >> 2] * +HEAPF32[$8 >> 2];
 HEAPF32[$2 + 12 >> 2] = +HEAPF32[$3 >> 2] * +HEAPF32[$8 >> 2] - +HEAPF32[$0 >> 2] * +HEAPF32[$1 >> 2] - +HEAPF32[$12 >> 2] * +HEAPF32[$20 >> 2] - +HEAPF32[$18 >> 2] * +HEAPF32[$14 >> 2];
 return;
}

function _glmc_unprojecti($0, $1, $2, $3) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = $2 | 0;
 $3 = $3 | 0;
 var $10 = 0.0, $18 = 0.0, $20 = 0.0, $76 = 0.0, $78 = 0.0, $79 = 0.0;
 $10 = (+HEAPF32[$0 >> 2] - +HEAPF32[$2 >> 2]) * 2.0 / +HEAPF32[$2 + 8 >> 2] + -1.0;
 $18 = (+HEAPF32[$0 + 4 >> 2] - +HEAPF32[$2 + 4 >> 2]) * 2.0 / +HEAPF32[$2 + 12 >> 2] + -1.0;
 $20 = +HEAPF32[$0 + 8 >> 2] * 2.0 + -1.0;
 $76 = 1.0 / (+HEAPF32[$1 + 60 >> 2] + ($10 * +HEAPF32[$1 + 12 >> 2] + $18 * +HEAPF32[$1 + 28 >> 2] + $20 * +HEAPF32[$1 + 44 >> 2]));
 $78 = (+HEAPF32[$1 + 52 >> 2] + ($10 * +HEAPF32[$1 + 4 >> 2] + $18 * +HEAPF32[$1 + 20 >> 2] + $20 * +HEAPF32[$1 + 36 >> 2])) * $76;
 $79 = (+HEAPF32[$1 + 56 >> 2] + ($10 * +HEAPF32[$1 + 8 >> 2] + $18 * +HEAPF32[$1 + 24 >> 2] + $20 * +HEAPF32[$1 + 40 >> 2])) * $76;
 HEAPF32[$3 >> 2] = (+HEAPF32[$1 + 48 >> 2] + ($20 * +HEAPF32[$1 + 32 >> 2] + (+HEAPF32[$1 >> 2] * $10 + +HEAPF32[$1 + 16 >> 2] * $18))) * $76;
 HEAPF32[$3 + 4 >> 2] = $78;
 HEAPF32[$3 + 8 >> 2] = $79;
 return;
}

function _glmc_uniscaled($0) {
 $0 = $0 | 0;
 var $$idx$val$i$i = 0.0, $$idx11$val$i$i = 0.0, $$idx12$val$i$i = 0.0, $$idx6$val$i$i = 0.0, $$idx8$val$i$i = 0.0, $$idx9$val$i$i = 0.0, $$val$i$i = 0.0, $$val10$i$i = 0.0, $$val7$i$i = 0.0, $13 = 0.0, $6 = 0.0;
 $$val10$i$i = +HEAPF32[$0 >> 2];
 $$idx11$val$i$i = +HEAPF32[$0 + 4 >> 2];
 $$idx12$val$i$i = +HEAPF32[$0 + 8 >> 2];
 $6 = +Math_sqrt(+($$val10$i$i * $$val10$i$i + $$idx11$val$i$i * $$idx11$val$i$i + $$idx12$val$i$i * $$idx12$val$i$i));
 $$val7$i$i = +HEAPF32[$0 + 16 >> 2];
 $$idx8$val$i$i = +HEAPF32[$0 + 20 >> 2];
 $$idx9$val$i$i = +HEAPF32[$0 + 24 >> 2];
 $13 = +Math_sqrt(+($$val7$i$i * $$val7$i$i + $$idx8$val$i$i * $$idx8$val$i$i + $$idx9$val$i$i * $$idx9$val$i$i));
 $$val$i$i = +HEAPF32[$0 + 32 >> 2];
 $$idx$val$i$i = +HEAPF32[$0 + 36 >> 2];
 $$idx6$val$i$i = +HEAPF32[$0 + 40 >> 2];
 return ($6 == $13 ? $6 == +Math_sqrt(+($$val$i$i * $$val$i$i + $$idx$val$i$i * $$idx$val$i$i + $$idx6$val$i$i * $$idx6$val$i$i)) : 0) | 0;
}

function _glmc_quat_axis($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 var $$idx$i$i$i$i = 0, $$idx$val$i$i$i$i = 0.0, $$idx7$i$i$i$i = 0, $$idx7$val$i$i$i$i = 0.0, $$sink$i$i$i$i = 0.0, $$val$i$i$i$i = 0.0, $10 = 0.0, $17 = 0, $7 = 0.0;
 $$val$i$i$i$i = +HEAPF32[$0 >> 2];
 $$idx$i$i$i$i = $0 + 4 | 0;
 $$idx$val$i$i$i$i = +HEAPF32[$$idx$i$i$i$i >> 2];
 $$idx7$i$i$i$i = $0 + 8 | 0;
 $$idx7$val$i$i$i$i = +HEAPF32[$$idx7$i$i$i$i >> 2];
 $7 = +Math_sqrt(+($$val$i$i$i$i * $$val$i$i$i$i + $$idx$val$i$i$i$i * $$idx$val$i$i$i$i + $$idx7$val$i$i$i$i * $$idx7$val$i$i$i$i));
 if ($7 == 0.0) {
  HEAPF32[$1 >> 2] = 0.0;
  HEAPF32[$1 + 4 >> 2] = 0.0;
  $$sink$i$i$i$i = 0.0;
  $17 = $1 + 8 | 0;
  HEAPF32[$17 >> 2] = $$sink$i$i$i$i;
  return;
 } else {
  $10 = 1.0 / $7;
  HEAPF32[$1 >> 2] = $$val$i$i$i$i * $10;
  HEAPF32[$1 + 4 >> 2] = $10 * +HEAPF32[$$idx$i$i$i$i >> 2];
  $$sink$i$i$i$i = $10 * +HEAPF32[$$idx7$i$i$i$i >> 2];
  $17 = $1 + 8 | 0;
  HEAPF32[$17 >> 2] = $$sink$i$i$i$i;
  return;
 }
}

function _glmc_project($0, $1, $2, $3) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = $2 | 0;
 $3 = $3 | 0;
 var $4 = 0.0, $6 = 0.0, $64 = 0.0, $72 = 0.0, $73 = 0.0, $8 = 0.0;
 $4 = +HEAPF32[$0 >> 2];
 $6 = +HEAPF32[$0 + 4 >> 2];
 $8 = +HEAPF32[$0 + 8 >> 2];
 $64 = 1.0 / (+HEAPF32[$1 + 60 >> 2] + ($4 * +HEAPF32[$1 + 12 >> 2] + $6 * +HEAPF32[$1 + 28 >> 2] + $8 * +HEAPF32[$1 + 44 >> 2]));
 $72 = ((+HEAPF32[$1 + 52 >> 2] + ($4 * +HEAPF32[$1 + 4 >> 2] + $6 * +HEAPF32[$1 + 20 >> 2] + $8 * +HEAPF32[$1 + 36 >> 2])) * $64 + 1.0) * .5;
 $73 = ((+HEAPF32[$1 + 56 >> 2] + ($4 * +HEAPF32[$1 + 8 >> 2] + $6 * +HEAPF32[$1 + 24 >> 2] + $8 * +HEAPF32[$1 + 40 >> 2])) * $64 + 1.0) * .5;
 HEAPF32[$3 >> 2] = +HEAPF32[$2 >> 2] + +HEAPF32[$2 + 8 >> 2] * (((+HEAPF32[$1 + 48 >> 2] + ($4 * +HEAPF32[$1 >> 2] + $6 * +HEAPF32[$1 + 16 >> 2] + $8 * +HEAPF32[$1 + 32 >> 2])) * $64 + 1.0) * .5);
 HEAPF32[$3 + 4 >> 2] = +HEAPF32[$2 + 4 >> 2] + +HEAPF32[$2 + 12 >> 2] * $72;
 HEAPF32[$3 + 8 >> 2] = $73;
 return;
}

function _glmc_decompose_scalev($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 var $$idx$val$i = 0.0, $$idx11$val$i = 0.0, $$idx12$val$i = 0.0, $$idx6$val$i = 0.0, $$idx8$val$i = 0.0, $$idx9$val$i = 0.0, $$val$i = 0.0, $$val10$i = 0.0, $$val7$i = 0.0;
 $$val10$i = +HEAPF32[$0 >> 2];
 $$idx11$val$i = +HEAPF32[$0 + 4 >> 2];
 $$idx12$val$i = +HEAPF32[$0 + 8 >> 2];
 HEAPF32[$1 >> 2] = +Math_sqrt(+($$val10$i * $$val10$i + $$idx11$val$i * $$idx11$val$i + $$idx12$val$i * $$idx12$val$i));
 $$val7$i = +HEAPF32[$0 + 16 >> 2];
 $$idx8$val$i = +HEAPF32[$0 + 20 >> 2];
 $$idx9$val$i = +HEAPF32[$0 + 24 >> 2];
 HEAPF32[$1 + 4 >> 2] = +Math_sqrt(+($$val7$i * $$val7$i + $$idx8$val$i * $$idx8$val$i + $$idx9$val$i * $$idx9$val$i));
 $$val$i = +HEAPF32[$0 + 32 >> 2];
 $$idx$val$i = +HEAPF32[$0 + 36 >> 2];
 $$idx6$val$i = +HEAPF32[$0 + 40 >> 2];
 HEAPF32[$1 + 8 >> 2] = +Math_sqrt(+($$val$i * $$val$i + $$idx$val$i * $$idx$val$i + $$idx6$val$i * $$idx6$val$i));
 return;
}

function _glmc_quat_imagn($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 var $$idx$i$i$i = 0, $$idx$val$i$i$i = 0.0, $$idx7$i$i$i = 0, $$idx7$val$i$i$i = 0.0, $$sink$i$i$i = 0.0, $$val$i$i$i = 0.0, $10 = 0.0, $17 = 0, $7 = 0.0;
 $$val$i$i$i = +HEAPF32[$0 >> 2];
 $$idx$i$i$i = $0 + 4 | 0;
 $$idx$val$i$i$i = +HEAPF32[$$idx$i$i$i >> 2];
 $$idx7$i$i$i = $0 + 8 | 0;
 $$idx7$val$i$i$i = +HEAPF32[$$idx7$i$i$i >> 2];
 $7 = +Math_sqrt(+($$val$i$i$i * $$val$i$i$i + $$idx$val$i$i$i * $$idx$val$i$i$i + $$idx7$val$i$i$i * $$idx7$val$i$i$i));
 if ($7 == 0.0) {
  HEAPF32[$1 >> 2] = 0.0;
  HEAPF32[$1 + 4 >> 2] = 0.0;
  $$sink$i$i$i = 0.0;
  $17 = $1 + 8 | 0;
  HEAPF32[$17 >> 2] = $$sink$i$i$i;
  return;
 } else {
  $10 = 1.0 / $7;
  HEAPF32[$1 >> 2] = $$val$i$i$i * $10;
  HEAPF32[$1 + 4 >> 2] = $10 * +HEAPF32[$$idx$i$i$i >> 2];
  $$sink$i$i$i = $10 * +HEAPF32[$$idx7$i$i$i >> 2];
  $17 = $1 + 8 | 0;
  HEAPF32[$17 >> 2] = $$sink$i$i$i;
  return;
 }
}

function _glmc_scale_uni($0, $1) {
 $0 = $0 | 0;
 $1 = +$1;
 var $10 = 0, $13 = 0, $16 = 0, $19 = 0, $22 = 0, $25 = 0, $28 = 0, $31 = 0, $34 = 0, $4 = 0, $7 = 0;
 HEAPF32[$0 >> 2] = +HEAPF32[$0 >> 2] * $1;
 $4 = $0 + 4 | 0;
 HEAPF32[$4 >> 2] = +HEAPF32[$4 >> 2] * $1;
 $7 = $0 + 8 | 0;
 HEAPF32[$7 >> 2] = +HEAPF32[$7 >> 2] * $1;
 $10 = $0 + 12 | 0;
 HEAPF32[$10 >> 2] = +HEAPF32[$10 >> 2] * $1;
 $13 = $0 + 16 | 0;
 HEAPF32[$13 >> 2] = +HEAPF32[$13 >> 2] * $1;
 $16 = $0 + 20 | 0;
 HEAPF32[$16 >> 2] = +HEAPF32[$16 >> 2] * $1;
 $19 = $0 + 24 | 0;
 HEAPF32[$19 >> 2] = +HEAPF32[$19 >> 2] * $1;
 $22 = $0 + 28 | 0;
 HEAPF32[$22 >> 2] = +HEAPF32[$22 >> 2] * $1;
 $25 = $0 + 32 | 0;
 HEAPF32[$25 >> 2] = +HEAPF32[$25 >> 2] * $1;
 $28 = $0 + 36 | 0;
 HEAPF32[$28 >> 2] = +HEAPF32[$28 >> 2] * $1;
 $31 = $0 + 40 | 0;
 HEAPF32[$31 >> 2] = +HEAPF32[$31 >> 2] * $1;
 $34 = $0 + 44 | 0;
 HEAPF32[$34 >> 2] = +HEAPF32[$34 >> 2] * $1;
 return;
}

function _glmc_ortho_aabb_p($0, $1, $2) {
 $0 = $0 | 0;
 $1 = +$1;
 $2 = $2 | 0;
 var $10 = 0.0, $13 = 0.0, $16 = 0.0, $19 = 0.0, $20 = 0.0, $23 = 0.0, $25 = 0.0, $27 = 0.0, $4 = 0.0, $7 = 0.0, dest = 0, stop = 0;
 $4 = +HEAPF32[$0 >> 2] - $1;
 $7 = +HEAPF32[$0 + 12 >> 2] + $1;
 $10 = +HEAPF32[$0 + 4 >> 2] - $1;
 $13 = +HEAPF32[$0 + 16 >> 2] + $1;
 $16 = +HEAPF32[$0 + 20 >> 2] + $1;
 $19 = +HEAPF32[$0 + 8 >> 2] - $1;
 $20 = -$19;
 dest = $2 + 4 | 0;
 stop = dest + 44 | 0;
 do {
  HEAP32[dest >> 2] = 0;
  dest = dest + 4 | 0;
 } while ((dest | 0) < (stop | 0));
 $23 = 1.0 / ($7 - $4);
 $25 = 1.0 / ($13 - $10);
 $27 = -1.0 / ($16 - $19);
 HEAPF32[$2 >> 2] = $23 * 2.0;
 HEAPF32[$2 + 20 >> 2] = $25 * 2.0;
 HEAPF32[$2 + 40 >> 2] = $27 * 2.0;
 HEAPF32[$2 + 48 >> 2] = -(($4 + $7) * $23);
 HEAPF32[$2 + 52 >> 2] = -(($10 + $13) * $25);
 HEAPF32[$2 + 56 >> 2] = ($20 - $16) * $27;
 HEAPF32[$2 + 60 >> 2] = 1.0;
 return;
}

function _glmc_ortho_aabb_pz($0, $1, $2) {
 $0 = $0 | 0;
 $1 = +$1;
 $2 = $2 | 0;
 var $12 = 0.0, $15 = 0.0, $16 = 0.0, $19 = 0.0, $21 = 0.0, $23 = 0.0, $3 = 0.0, $5 = 0.0, $7 = 0.0, $9 = 0.0, dest = 0, stop = 0;
 $3 = +HEAPF32[$0 >> 2];
 $5 = +HEAPF32[$0 + 12 >> 2];
 $7 = +HEAPF32[$0 + 4 >> 2];
 $9 = +HEAPF32[$0 + 16 >> 2];
 $12 = +HEAPF32[$0 + 20 >> 2] + $1;
 $15 = +HEAPF32[$0 + 8 >> 2] - $1;
 $16 = -$15;
 dest = $2 + 4 | 0;
 stop = dest + 44 | 0;
 do {
  HEAP32[dest >> 2] = 0;
  dest = dest + 4 | 0;
 } while ((dest | 0) < (stop | 0));
 $19 = 1.0 / ($5 - $3);
 $21 = 1.0 / ($9 - $7);
 $23 = -1.0 / ($12 - $15);
 HEAPF32[$2 >> 2] = $19 * 2.0;
 HEAPF32[$2 + 20 >> 2] = $21 * 2.0;
 HEAPF32[$2 + 40 >> 2] = $23 * 2.0;
 HEAPF32[$2 + 48 >> 2] = -(($3 + $5) * $19);
 HEAPF32[$2 + 52 >> 2] = -(($7 + $9) * $21);
 HEAPF32[$2 + 56 >> 2] = ($16 - $12) * $23;
 HEAPF32[$2 + 60 >> 2] = 1.0;
 return;
}

function _glmc_mat4_mulv($0, $1, $2) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = $2 | 0;
 var $15 = 0.0, $21 = 0.0, $38 = 0.0, $4 = 0.0, $53 = 0.0, $68 = 0.0, $9 = 0.0;
 $4 = +HEAPF32[$1 >> 2];
 $9 = +HEAPF32[$1 + 4 >> 2];
 $15 = +HEAPF32[$1 + 8 >> 2];
 $21 = +HEAPF32[$1 + 12 >> 2];
 $38 = $4 * +HEAPF32[$0 + 4 >> 2] + $9 * +HEAPF32[$0 + 20 >> 2] + $15 * +HEAPF32[$0 + 36 >> 2] + $21 * +HEAPF32[$0 + 52 >> 2];
 $53 = $4 * +HEAPF32[$0 + 8 >> 2] + $9 * +HEAPF32[$0 + 24 >> 2] + $15 * +HEAPF32[$0 + 40 >> 2] + $21 * +HEAPF32[$0 + 56 >> 2];
 $68 = $4 * +HEAPF32[$0 + 12 >> 2] + $9 * +HEAPF32[$0 + 28 >> 2] + $15 * +HEAPF32[$0 + 44 >> 2] + $21 * +HEAPF32[$0 + 60 >> 2];
 HEAPF32[$2 >> 2] = +HEAPF32[$0 >> 2] * $4 + +HEAPF32[$0 + 16 >> 2] * $9 + +HEAPF32[$0 + 32 >> 2] * $15 + +HEAPF32[$0 + 48 >> 2] * $21;
 HEAPF32[$2 + 4 >> 2] = $38;
 HEAPF32[$2 + 8 >> 2] = $53;
 HEAPF32[$2 + 12 >> 2] = $68;
 return;
}

function _glmc_ortho_aabb($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 var $10 = 0.0, $12 = 0.0, $13 = 0.0, $16 = 0.0, $18 = 0.0, $2 = 0.0, $20 = 0.0, $4 = 0.0, $6 = 0.0, $8 = 0.0, dest = 0, stop = 0;
 $2 = +HEAPF32[$0 >> 2];
 $4 = +HEAPF32[$0 + 12 >> 2];
 $6 = +HEAPF32[$0 + 4 >> 2];
 $8 = +HEAPF32[$0 + 16 >> 2];
 $10 = +HEAPF32[$0 + 20 >> 2];
 $12 = +HEAPF32[$0 + 8 >> 2];
 $13 = -$12;
 dest = $1 + 4 | 0;
 stop = dest + 44 | 0;
 do {
  HEAP32[dest >> 2] = 0;
  dest = dest + 4 | 0;
 } while ((dest | 0) < (stop | 0));
 $16 = 1.0 / ($4 - $2);
 $18 = 1.0 / ($8 - $6);
 $20 = -1.0 / ($10 - $12);
 HEAPF32[$1 >> 2] = $16 * 2.0;
 HEAPF32[$1 + 20 >> 2] = $18 * 2.0;
 HEAPF32[$1 + 40 >> 2] = $20 * 2.0;
 HEAPF32[$1 + 48 >> 2] = -(($2 + $4) * $16);
 HEAPF32[$1 + 52 >> 2] = -(($6 + $8) * $18);
 HEAPF32[$1 + 56 >> 2] = ($13 - $10) * $20;
 HEAPF32[$1 + 60 >> 2] = 1.0;
 return;
}

function _glmc_euler_angles($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 var $$0$i = 0.0, $$043$i = 0.0, $$044$i = 0.0, $10 = 0.0, $12 = 0.0, $14 = 0.0, $2 = 0.0, $4 = 0.0, $6 = 0.0, $8 = 0.0;
 $2 = +HEAPF32[$0 >> 2];
 $4 = +HEAPF32[$0 + 16 >> 2];
 $6 = +HEAPF32[$0 + 32 >> 2];
 $8 = +HEAPF32[$0 + 4 >> 2];
 $10 = +HEAPF32[$0 + 20 >> 2];
 $12 = +HEAPF32[$0 + 36 >> 2];
 $14 = +HEAPF32[$0 + 40 >> 2];
 do if ($6 < 1.0) if ($6 > -1.0) {
  $$0$i = +Math_atan2(+-$4, +$2);
  $$043$i = +Math_asin(+$6);
  $$044$i = +Math_atan2(+-$12, +$14);
  break;
 } else {
  $$0$i = 0.0;
  $$043$i = -1.5707963705062866;
  $$044$i = -+Math_atan2(+$8, +$10);
  break;
 } else {
  $$0$i = 0.0;
  $$043$i = 1.5707963705062866;
  $$044$i = +Math_atan2(+$8, +$10);
 } while (0);
 HEAPF32[$1 >> 2] = $$044$i;
 HEAPF32[$1 + 4 >> 2] = $$043$i;
 HEAPF32[$1 + 8 >> 2] = $$0$i;
 return;
}

function _glmc_aabb_merge($0, $1, $2) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = $2 | 0;
 var $13 = 0.0, $15 = 0.0, $19 = 0.0, $21 = 0.0, $25 = 0.0, $27 = 0.0, $3 = 0.0, $31 = 0.0, $33 = 0.0, $4 = 0.0, $7 = 0.0, $9 = 0.0;
 $3 = +HEAPF32[$0 >> 2];
 $4 = +HEAPF32[$1 >> 2];
 HEAPF32[$2 >> 2] = $3 < $4 ? $3 : $4;
 $7 = +HEAPF32[$0 + 4 >> 2];
 $9 = +HEAPF32[$1 + 4 >> 2];
 HEAPF32[$2 + 4 >> 2] = $7 < $9 ? $7 : $9;
 $13 = +HEAPF32[$0 + 8 >> 2];
 $15 = +HEAPF32[$1 + 8 >> 2];
 HEAPF32[$2 + 8 >> 2] = $13 < $15 ? $13 : $15;
 $19 = +HEAPF32[$0 + 12 >> 2];
 $21 = +HEAPF32[$1 + 12 >> 2];
 HEAPF32[$2 + 12 >> 2] = $19 > $21 ? $19 : $21;
 $25 = +HEAPF32[$0 + 16 >> 2];
 $27 = +HEAPF32[$1 + 16 >> 2];
 HEAPF32[$2 + 16 >> 2] = $25 > $27 ? $25 : $27;
 $31 = +HEAPF32[$0 + 20 >> 2];
 $33 = +HEAPF32[$1 + 20 >> 2];
 HEAPF32[$2 + 20 >> 2] = $31 > $33 ? $31 : $33;
 return;
}

function _glmc_aabb_crop($0, $1, $2) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = $2 | 0;
 var $13 = 0.0, $15 = 0.0, $19 = 0.0, $21 = 0.0, $25 = 0.0, $27 = 0.0, $3 = 0.0, $31 = 0.0, $33 = 0.0, $4 = 0.0, $7 = 0.0, $9 = 0.0;
 $3 = +HEAPF32[$0 >> 2];
 $4 = +HEAPF32[$1 >> 2];
 HEAPF32[$2 >> 2] = $3 > $4 ? $3 : $4;
 $7 = +HEAPF32[$0 + 4 >> 2];
 $9 = +HEAPF32[$1 + 4 >> 2];
 HEAPF32[$2 + 4 >> 2] = $7 > $9 ? $7 : $9;
 $13 = +HEAPF32[$0 + 8 >> 2];
 $15 = +HEAPF32[$1 + 8 >> 2];
 HEAPF32[$2 + 8 >> 2] = $13 > $15 ? $13 : $15;
 $19 = +HEAPF32[$0 + 12 >> 2];
 $21 = +HEAPF32[$1 + 12 >> 2];
 HEAPF32[$2 + 12 >> 2] = $19 < $21 ? $19 : $21;
 $25 = +HEAPF32[$0 + 16 >> 2];
 $27 = +HEAPF32[$1 + 16 >> 2];
 HEAPF32[$2 + 16 >> 2] = $25 < $27 ? $25 : $27;
 $31 = +HEAPF32[$0 + 20 >> 2];
 $33 = +HEAPF32[$1 + 20 >> 2];
 HEAPF32[$2 + 20 >> 2] = $31 < $33 ? $31 : $33;
 return;
}

function _glmc_vec_scale_as($0, $1, $2) {
 $0 = $0 | 0;
 $1 = +$1;
 $2 = $2 | 0;
 var $$idx$i = 0, $$idx$val$i = 0.0, $$idx8$i = 0, $$idx8$val$i = 0.0, $$sink$i = 0.0, $$val$i = 0.0, $11 = 0.0, $18 = 0, $8 = 0.0;
 $$val$i = +HEAPF32[$0 >> 2];
 $$idx$i = $0 + 4 | 0;
 $$idx$val$i = +HEAPF32[$$idx$i >> 2];
 $$idx8$i = $0 + 8 | 0;
 $$idx8$val$i = +HEAPF32[$$idx8$i >> 2];
 $8 = +Math_sqrt(+($$val$i * $$val$i + $$idx$val$i * $$idx$val$i + $$idx8$val$i * $$idx8$val$i));
 if ($8 == 0.0) {
  HEAPF32[$2 >> 2] = 0.0;
  HEAPF32[$2 + 4 >> 2] = 0.0;
  $$sink$i = 0.0;
  $18 = $2 + 8 | 0;
  HEAPF32[$18 >> 2] = $$sink$i;
  return;
 } else {
  $11 = $1 / $8;
  HEAPF32[$2 >> 2] = $$val$i * $11;
  HEAPF32[$2 + 4 >> 2] = $11 * +HEAPF32[$$idx$i >> 2];
  $$sink$i = $11 * +HEAPF32[$$idx8$i >> 2];
  $18 = $2 + 8 | 0;
  HEAPF32[$18 >> 2] = $$sink$i;
  return;
 }
}

function _glmc_vec_normalize_to($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 var $$idx$i = 0, $$idx$val$i = 0.0, $$idx7$i = 0, $$idx7$val$i = 0.0, $$sink$i = 0.0, $$val$i = 0.0, $10 = 0.0, $17 = 0, $7 = 0.0;
 $$val$i = +HEAPF32[$0 >> 2];
 $$idx$i = $0 + 4 | 0;
 $$idx$val$i = +HEAPF32[$$idx$i >> 2];
 $$idx7$i = $0 + 8 | 0;
 $$idx7$val$i = +HEAPF32[$$idx7$i >> 2];
 $7 = +Math_sqrt(+($$val$i * $$val$i + $$idx$val$i * $$idx$val$i + $$idx7$val$i * $$idx7$val$i));
 if ($7 == 0.0) {
  HEAPF32[$1 >> 2] = 0.0;
  HEAPF32[$1 + 4 >> 2] = 0.0;
  $$sink$i = 0.0;
  $17 = $1 + 8 | 0;
  HEAPF32[$17 >> 2] = $$sink$i;
  return;
 } else {
  $10 = 1.0 / $7;
  HEAPF32[$1 >> 2] = $$val$i * $10;
  HEAPF32[$1 + 4 >> 2] = $10 * +HEAPF32[$$idx$i >> 2];
  $$sink$i = $10 * +HEAPF32[$$idx7$i >> 2];
  $17 = $1 + 8 | 0;
  HEAPF32[$17 >> 2] = $$sink$i;
  return;
 }
}

function _glmc_quat_lerp($0, $1, $2, $3) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = +$2;
 $3 = $3 | 0;
 var $$$i$i$i$i = 0.0, $$$i1$i$i$i = 0.0, $11 = 0, $16 = 0, $21 = 0, $25 = 0.0, $26 = 0.0, $27 = 0.0, $7 = 0.0;
 $$$i$i$i$i = $2 > 0.0 ? $2 : 0.0;
 $$$i1$i$i$i = $$$i$i$i$i < 1.0 ? $$$i$i$i$i : 1.0;
 $7 = +HEAPF32[$0 >> 2];
 $11 = $0 + 4 | 0;
 $16 = $0 + 8 | 0;
 $21 = $0 + 12 | 0;
 $25 = $$$i1$i$i$i * (+HEAPF32[$1 + 4 >> 2] - +HEAPF32[$11 >> 2]);
 $26 = $$$i1$i$i$i * (+HEAPF32[$1 + 8 >> 2] - +HEAPF32[$16 >> 2]);
 $27 = $$$i1$i$i$i * (+HEAPF32[$1 + 12 >> 2] - +HEAPF32[$21 >> 2]);
 HEAPF32[$3 >> 2] = $7 + $$$i1$i$i$i * (+HEAPF32[$1 >> 2] - $7);
 HEAPF32[$3 + 4 >> 2] = $25 + +HEAPF32[$11 >> 2];
 HEAPF32[$3 + 8 >> 2] = $26 + +HEAPF32[$16 >> 2];
 HEAPF32[$3 + 12 >> 2] = $27 + +HEAPF32[$21 >> 2];
 return;
}

function _glmc_mat4_transpose_to($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 HEAP32[$1 >> 2] = HEAP32[$0 >> 2];
 HEAP32[$1 + 16 >> 2] = HEAP32[$0 + 4 >> 2];
 HEAP32[$1 + 4 >> 2] = HEAP32[$0 + 16 >> 2];
 HEAP32[$1 + 20 >> 2] = HEAP32[$0 + 20 >> 2];
 HEAP32[$1 + 8 >> 2] = HEAP32[$0 + 32 >> 2];
 HEAP32[$1 + 24 >> 2] = HEAP32[$0 + 36 >> 2];
 HEAP32[$1 + 12 >> 2] = HEAP32[$0 + 48 >> 2];
 HEAP32[$1 + 28 >> 2] = HEAP32[$0 + 52 >> 2];
 HEAP32[$1 + 32 >> 2] = HEAP32[$0 + 8 >> 2];
 HEAP32[$1 + 48 >> 2] = HEAP32[$0 + 12 >> 2];
 HEAP32[$1 + 36 >> 2] = HEAP32[$0 + 24 >> 2];
 HEAP32[$1 + 52 >> 2] = HEAP32[$0 + 28 >> 2];
 HEAP32[$1 + 40 >> 2] = HEAP32[$0 + 40 >> 2];
 HEAP32[$1 + 56 >> 2] = HEAP32[$0 + 44 >> 2];
 HEAP32[$1 + 44 >> 2] = HEAP32[$0 + 56 >> 2];
 HEAP32[$1 + 60 >> 2] = HEAP32[$0 + 60 >> 2];
 return;
}

function _glmc_mat4_swap_row($0, $1, $2) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = $2 | 0;
 var $10 = 0, $11 = 0, $13 = 0, $15 = 0, $17 = 0, $3 = 0, $4 = 0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0;
 $3 = $0 + ($1 << 2) | 0;
 $4 = HEAP32[$3 >> 2] | 0;
 $5 = $0 + 16 + ($1 << 2) | 0;
 $6 = HEAP32[$5 >> 2] | 0;
 $7 = $0 + 32 + ($1 << 2) | 0;
 $8 = HEAP32[$7 >> 2] | 0;
 $9 = $0 + 48 + ($1 << 2) | 0;
 $10 = HEAP32[$9 >> 2] | 0;
 $11 = $0 + ($2 << 2) | 0;
 HEAP32[$3 >> 2] = HEAP32[$11 >> 2];
 $13 = $0 + 16 + ($2 << 2) | 0;
 HEAP32[$5 >> 2] = HEAP32[$13 >> 2];
 $15 = $0 + 32 + ($2 << 2) | 0;
 HEAP32[$7 >> 2] = HEAP32[$15 >> 2];
 $17 = $0 + 48 + ($2 << 2) | 0;
 HEAP32[$9 >> 2] = HEAP32[$17 >> 2];
 HEAP32[$11 >> 2] = $4;
 HEAP32[$13 >> 2] = $6;
 HEAP32[$15 >> 2] = $8;
 HEAP32[$17 >> 2] = $10;
 return;
}

function _glmc_mat4_ucopy($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 HEAP32[$1 >> 2] = HEAP32[$0 >> 2];
 HEAP32[$1 + 4 >> 2] = HEAP32[$0 + 4 >> 2];
 HEAP32[$1 + 8 >> 2] = HEAP32[$0 + 8 >> 2];
 HEAP32[$1 + 12 >> 2] = HEAP32[$0 + 12 >> 2];
 HEAP32[$1 + 16 >> 2] = HEAP32[$0 + 16 >> 2];
 HEAP32[$1 + 20 >> 2] = HEAP32[$0 + 20 >> 2];
 HEAP32[$1 + 24 >> 2] = HEAP32[$0 + 24 >> 2];
 HEAP32[$1 + 28 >> 2] = HEAP32[$0 + 28 >> 2];
 HEAP32[$1 + 32 >> 2] = HEAP32[$0 + 32 >> 2];
 HEAP32[$1 + 36 >> 2] = HEAP32[$0 + 36 >> 2];
 HEAP32[$1 + 40 >> 2] = HEAP32[$0 + 40 >> 2];
 HEAP32[$1 + 44 >> 2] = HEAP32[$0 + 44 >> 2];
 HEAP32[$1 + 48 >> 2] = HEAP32[$0 + 48 >> 2];
 HEAP32[$1 + 52 >> 2] = HEAP32[$0 + 52 >> 2];
 HEAP32[$1 + 56 >> 2] = HEAP32[$0 + 56 >> 2];
 HEAP32[$1 + 60 >> 2] = HEAP32[$0 + 60 >> 2];
 return;
}

function _glmc_mat4_copy($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 HEAP32[$1 >> 2] = HEAP32[$0 >> 2];
 HEAP32[$1 + 4 >> 2] = HEAP32[$0 + 4 >> 2];
 HEAP32[$1 + 8 >> 2] = HEAP32[$0 + 8 >> 2];
 HEAP32[$1 + 12 >> 2] = HEAP32[$0 + 12 >> 2];
 HEAP32[$1 + 16 >> 2] = HEAP32[$0 + 16 >> 2];
 HEAP32[$1 + 20 >> 2] = HEAP32[$0 + 20 >> 2];
 HEAP32[$1 + 24 >> 2] = HEAP32[$0 + 24 >> 2];
 HEAP32[$1 + 28 >> 2] = HEAP32[$0 + 28 >> 2];
 HEAP32[$1 + 32 >> 2] = HEAP32[$0 + 32 >> 2];
 HEAP32[$1 + 36 >> 2] = HEAP32[$0 + 36 >> 2];
 HEAP32[$1 + 40 >> 2] = HEAP32[$0 + 40 >> 2];
 HEAP32[$1 + 44 >> 2] = HEAP32[$0 + 44 >> 2];
 HEAP32[$1 + 48 >> 2] = HEAP32[$0 + 48 >> 2];
 HEAP32[$1 + 52 >> 2] = HEAP32[$0 + 52 >> 2];
 HEAP32[$1 + 56 >> 2] = HEAP32[$0 + 56 >> 2];
 HEAP32[$1 + 60 >> 2] = HEAP32[$0 + 60 >> 2];
 return;
}

function _glmc_mat4_swap_col($0, $1, $2) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = $2 | 0;
 var $10 = 0, $11 = 0, $13 = 0, $15 = 0, $17 = 0, $3 = 0, $4 = 0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0;
 $3 = $0 + ($1 << 4) | 0;
 $4 = HEAP32[$3 >> 2] | 0;
 $5 = $0 + ($1 << 4) + 4 | 0;
 $6 = HEAP32[$5 >> 2] | 0;
 $7 = $0 + ($1 << 4) + 8 | 0;
 $8 = HEAP32[$7 >> 2] | 0;
 $9 = $0 + ($1 << 4) + 12 | 0;
 $10 = HEAP32[$9 >> 2] | 0;
 $11 = $0 + ($2 << 4) | 0;
 HEAP32[$3 >> 2] = HEAP32[$11 >> 2];
 $13 = $0 + ($2 << 4) + 4 | 0;
 HEAP32[$5 >> 2] = HEAP32[$13 >> 2];
 $15 = $0 + ($2 << 4) + 8 | 0;
 HEAP32[$7 >> 2] = HEAP32[$15 >> 2];
 $17 = $0 + ($2 << 4) + 12 | 0;
 HEAP32[$9 >> 2] = HEAP32[$17 >> 2];
 HEAP32[$11 >> 2] = $4;
 HEAP32[$13 >> 2] = $6;
 HEAP32[$15 >> 2] = $8;
 HEAP32[$17 >> 2] = $10;
 return;
}

function _glmc_vec4_lerp($0, $1, $2, $3) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = +$2;
 $3 = $3 | 0;
 var $$$i$i$i = 0.0, $$$i3$i$i = 0.0, $11 = 0, $16 = 0, $21 = 0, $25 = 0.0, $26 = 0.0, $27 = 0.0, $7 = 0.0;
 $$$i$i$i = $2 > 0.0 ? $2 : 0.0;
 $$$i3$i$i = $$$i$i$i < 1.0 ? $$$i$i$i : 1.0;
 $7 = +HEAPF32[$0 >> 2];
 $11 = $0 + 4 | 0;
 $16 = $0 + 8 | 0;
 $21 = $0 + 12 | 0;
 $25 = $$$i3$i$i * (+HEAPF32[$1 + 4 >> 2] - +HEAPF32[$11 >> 2]);
 $26 = $$$i3$i$i * (+HEAPF32[$1 + 8 >> 2] - +HEAPF32[$16 >> 2]);
 $27 = $$$i3$i$i * (+HEAPF32[$1 + 12 >> 2] - +HEAPF32[$21 >> 2]);
 HEAPF32[$3 >> 2] = $7 + $$$i3$i$i * (+HEAPF32[$1 >> 2] - $7);
 HEAPF32[$3 + 4 >> 2] = $25 + +HEAPF32[$11 >> 2];
 HEAPF32[$3 + 8 >> 2] = $26 + +HEAPF32[$16 >> 2];
 HEAPF32[$3 + 12 >> 2] = $27 + +HEAPF32[$21 >> 2];
 return;
}

function _glmc_aabb_isvalid($0) {
 $0 = $0 | 0;
 var $$0$i$i = 0.0, $$0$i6$i = 0.0, $$idx$val$i = 0.0, $$idx2$val$i = 0.0, $$idx4$val$i = 0.0, $$idx5$val$i = 0.0, $$val$i = 0.0, $$val3$i = 0.0, $8 = 0;
 $$val$i = +HEAPF32[$0 >> 2];
 $$idx$val$i = +HEAPF32[$0 + 4 >> 2];
 $$idx2$val$i = +HEAPF32[$0 + 8 >> 2];
 $$0$i$i = $$idx$val$i > $$val$i ? $$idx$val$i : $$val$i;
 if (!(($$0$i$i < $$idx2$val$i ? $$idx2$val$i : $$0$i$i) != 3402823466385288598117041.0e14)) {
  $8 = 0;
  return $8 | 0;
 }
 $$val3$i = +HEAPF32[$0 + 12 >> 2];
 $$idx4$val$i = +HEAPF32[$0 + 16 >> 2];
 $$idx5$val$i = +HEAPF32[$0 + 20 >> 2];
 $$0$i6$i = $$idx4$val$i < $$val3$i ? $$idx4$val$i : $$val3$i;
 $8 = ($$0$i6$i > $$idx5$val$i ? $$idx5$val$i : $$0$i6$i) != -3402823466385288598117041.0e14;
 return $8 | 0;
}

function _glmc_vec4_scale_as($0, $1, $2) {
 $0 = $0 | 0;
 $1 = +$1;
 $2 = $2 | 0;
 var $10 = 0.0, $13 = 0, $14 = 0.0, $17 = 0.0, $19 = 0.0, $3 = 0.0, $5 = 0, $6 = 0.0, $9 = 0;
 $3 = +HEAPF32[$0 >> 2];
 $5 = $0 + 4 | 0;
 $6 = +HEAPF32[$5 >> 2];
 $9 = $0 + 8 | 0;
 $10 = +HEAPF32[$9 >> 2];
 $13 = $0 + 12 | 0;
 $14 = +HEAPF32[$13 >> 2];
 $17 = +Math_sqrt(+($3 * $3 + $6 * $6 + $10 * $10 + $14 * $14));
 if ($17 == 0.0) {
  HEAP32[$2 >> 2] = 0;
  HEAP32[$2 + 4 >> 2] = 0;
  HEAP32[$2 + 8 >> 2] = 0;
  HEAP32[$2 + 12 >> 2] = 0;
  return;
 } else {
  $19 = $1 / $17;
  HEAPF32[$2 >> 2] = $3 * $19;
  HEAPF32[$2 + 4 >> 2] = $19 * +HEAPF32[$5 >> 2];
  HEAPF32[$2 + 8 >> 2] = $19 * +HEAPF32[$9 >> 2];
  HEAPF32[$2 + 12 >> 2] = $19 * +HEAPF32[$13 >> 2];
  return;
 }
}

function _glmc_vec4_normalize_to($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 var $12 = 0, $13 = 0.0, $16 = 0.0, $18 = 0.0, $2 = 0.0, $4 = 0, $5 = 0.0, $8 = 0, $9 = 0.0;
 $2 = +HEAPF32[$0 >> 2];
 $4 = $0 + 4 | 0;
 $5 = +HEAPF32[$4 >> 2];
 $8 = $0 + 8 | 0;
 $9 = +HEAPF32[$8 >> 2];
 $12 = $0 + 12 | 0;
 $13 = +HEAPF32[$12 >> 2];
 $16 = +Math_sqrt(+($2 * $2 + $5 * $5 + $9 * $9 + $13 * $13));
 if ($16 == 0.0) {
  HEAP32[$1 >> 2] = 0;
  HEAP32[$1 + 4 >> 2] = 0;
  HEAP32[$1 + 8 >> 2] = 0;
  HEAP32[$1 + 12 >> 2] = 0;
  return;
 } else {
  $18 = 1.0 / $16;
  HEAPF32[$1 >> 2] = $2 * $18;
  HEAPF32[$1 + 4 >> 2] = $18 * +HEAPF32[$4 >> 2];
  HEAPF32[$1 + 8 >> 2] = $18 * +HEAPF32[$8 >> 2];
  HEAPF32[$1 + 12 >> 2] = $18 * +HEAPF32[$12 >> 2];
  return;
 }
}

function _glmc_quat_normalize_to($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 var $12 = 0, $13 = 0.0, $15 = 0.0, $18 = 0.0, $2 = 0.0, $4 = 0, $5 = 0.0, $8 = 0, $9 = 0.0;
 $2 = +HEAPF32[$0 >> 2];
 $4 = $0 + 4 | 0;
 $5 = +HEAPF32[$4 >> 2];
 $8 = $0 + 8 | 0;
 $9 = +HEAPF32[$8 >> 2];
 $12 = $0 + 12 | 0;
 $13 = +HEAPF32[$12 >> 2];
 $15 = $2 * $2 + $5 * $5 + $9 * $9 + $13 * $13;
 if (!($15 <= 0.0)) {
  $18 = 1.0 / +Math_sqrt(+$15);
  HEAPF32[$1 >> 2] = $2 * $18;
  HEAPF32[$1 + 4 >> 2] = $18 * +HEAPF32[$4 >> 2];
  HEAPF32[$1 + 8 >> 2] = $18 * +HEAPF32[$8 >> 2];
  HEAPF32[$1 + 12 >> 2] = $18 * +HEAPF32[$12 >> 2];
  return;
 } else {
  HEAP32[$0 >> 2] = 0;
  HEAP32[$4 >> 2] = 0;
  HEAP32[$8 >> 2] = 0;
  HEAP32[$12 >> 2] = 1065353216;
  return;
 }
}

function _glmc_vec4_clamp($0, $1, $2) {
 $0 = $0 | 0;
 $1 = +$1;
 $2 = +$2;
 var $$$i$i$i = 0.0, $$$i$i16$i = 0.0, $$$i$i18$i = 0.0, $$$i$i20$i = 0.0, $10 = 0, $11 = 0.0, $14 = 0, $15 = 0.0, $3 = 0.0, $6 = 0, $7 = 0.0;
 $3 = +HEAPF32[$0 >> 2];
 $$$i$i$i = $3 > $1 ? $3 : $1;
 HEAPF32[$0 >> 2] = $$$i$i$i < $2 ? $$$i$i$i : $2;
 $6 = $0 + 4 | 0;
 $7 = +HEAPF32[$6 >> 2];
 $$$i$i20$i = $7 > $1 ? $7 : $1;
 HEAPF32[$6 >> 2] = $$$i$i20$i < $2 ? $$$i$i20$i : $2;
 $10 = $0 + 8 | 0;
 $11 = +HEAPF32[$10 >> 2];
 $$$i$i18$i = $11 > $1 ? $11 : $1;
 HEAPF32[$10 >> 2] = $$$i$i18$i < $2 ? $$$i$i18$i : $2;
 $14 = $0 + 12 | 0;
 $15 = +HEAPF32[$14 >> 2];
 $$$i$i16$i = $15 > $1 ? $15 : $1;
 HEAPF32[$14 >> 2] = $$$i$i16$i < $2 ? $$$i$i16$i : $2;
 return;
}

function _glmc_translate_make($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 var $2 = 0, $4 = 0, $6 = 0;
 $2 = $0 + 4 | 0;
 HEAP32[$0 >> 2] = 1065353216;
 $4 = $0 + 24 | 0;
 HEAP32[$2 >> 2] = 0;
 HEAP32[$2 + 4 >> 2] = 0;
 HEAP32[$2 + 8 >> 2] = 0;
 HEAP32[$2 + 12 >> 2] = 0;
 HEAP32[$0 + 20 >> 2] = 1065353216;
 $6 = $0 + 44 | 0;
 HEAP32[$4 >> 2] = 0;
 HEAP32[$4 + 4 >> 2] = 0;
 HEAP32[$4 + 8 >> 2] = 0;
 HEAP32[$4 + 12 >> 2] = 0;
 HEAP32[$0 + 40 >> 2] = 1065353216;
 HEAP32[$6 >> 2] = 0;
 HEAP32[$6 + 4 >> 2] = 0;
 HEAP32[$6 + 8 >> 2] = 0;
 HEAP32[$6 + 12 >> 2] = 0;
 HEAP32[$0 + 60 >> 2] = 1065353216;
 HEAP32[$0 + 48 >> 2] = HEAP32[$1 >> 2];
 HEAP32[$0 + 52 >> 2] = HEAP32[$1 + 4 >> 2];
 HEAP32[$0 + 56 >> 2] = HEAP32[$1 + 8 >> 2];
 return;
}

function _glmc_scale_make($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 var $2 = 0, $4 = 0, $6 = 0;
 $2 = $0 + 4 | 0;
 HEAP32[$0 >> 2] = 1065353216;
 $4 = $0 + 24 | 0;
 HEAP32[$2 >> 2] = 0;
 HEAP32[$2 + 4 >> 2] = 0;
 HEAP32[$2 + 8 >> 2] = 0;
 HEAP32[$2 + 12 >> 2] = 0;
 HEAP32[$0 + 20 >> 2] = 1065353216;
 $6 = $0 + 44 | 0;
 HEAP32[$4 >> 2] = 0;
 HEAP32[$4 + 4 >> 2] = 0;
 HEAP32[$4 + 8 >> 2] = 0;
 HEAP32[$4 + 12 >> 2] = 0;
 HEAP32[$0 + 40 >> 2] = 1065353216;
 HEAP32[$6 >> 2] = 0;
 HEAP32[$6 + 4 >> 2] = 0;
 HEAP32[$6 + 8 >> 2] = 0;
 HEAP32[$6 + 12 >> 2] = 0;
 HEAP32[$0 + 60 >> 2] = 1065353216;
 HEAP32[$0 >> 2] = HEAP32[$1 >> 2];
 HEAP32[$0 + 20 >> 2] = HEAP32[$1 + 4 >> 2];
 HEAP32[$0 + 40 >> 2] = HEAP32[$1 + 8 >> 2];
 return;
}

function _glmc_mat3_scale($0, $1) {
 $0 = $0 | 0;
 $1 = +$1;
 var $10 = 0, $13 = 0, $16 = 0, $19 = 0, $22 = 0, $25 = 0, $4 = 0, $7 = 0;
 HEAPF32[$0 >> 2] = +HEAPF32[$0 >> 2] * $1;
 $4 = $0 + 4 | 0;
 HEAPF32[$4 >> 2] = +HEAPF32[$4 >> 2] * $1;
 $7 = $0 + 8 | 0;
 HEAPF32[$7 >> 2] = +HEAPF32[$7 >> 2] * $1;
 $10 = $0 + 12 | 0;
 HEAPF32[$10 >> 2] = +HEAPF32[$10 >> 2] * $1;
 $13 = $0 + 16 | 0;
 HEAPF32[$13 >> 2] = +HEAPF32[$13 >> 2] * $1;
 $16 = $0 + 20 | 0;
 HEAPF32[$16 >> 2] = +HEAPF32[$16 >> 2] * $1;
 $19 = $0 + 24 | 0;
 HEAPF32[$19 >> 2] = +HEAPF32[$19 >> 2] * $1;
 $22 = $0 + 28 | 0;
 HEAPF32[$22 >> 2] = +HEAPF32[$22 >> 2] * $1;
 $25 = $0 + 32 | 0;
 HEAPF32[$25 >> 2] = +HEAPF32[$25 >> 2] * $1;
 return;
}

function _glmc_vec_proj($0, $1, $2) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = $2 | 0;
 var $$idx7$i = 0, $$idx7$val$i = 0.0, $$idx8$i = 0, $$idx8$val$i = 0.0, $$val6$i = 0.0, $13 = 0.0;
 $$val6$i = +HEAPF32[$1 >> 2];
 $$idx7$i = $1 + 4 | 0;
 $$idx7$val$i = +HEAPF32[$$idx7$i >> 2];
 $$idx8$i = $1 + 8 | 0;
 $$idx8$val$i = +HEAPF32[$$idx8$i >> 2];
 $13 = (+HEAPF32[$0 >> 2] * $$val6$i + +HEAPF32[$0 + 4 >> 2] * $$idx7$val$i + +HEAPF32[$0 + 8 >> 2] * $$idx8$val$i) / ($$val6$i * $$val6$i + $$idx7$val$i * $$idx7$val$i + $$idx8$val$i * $$idx8$val$i);
 HEAPF32[$2 >> 2] = $$val6$i * $13;
 HEAPF32[$2 + 4 >> 2] = +HEAPF32[$$idx7$i >> 2] * $13;
 HEAPF32[$2 + 8 >> 2] = $13 * +HEAPF32[$$idx8$i >> 2];
 return;
}

function _glmc_frustum($0, $1, $2, $3, $4, $5, $6) {
 $0 = +$0;
 $1 = +$1;
 $2 = +$2;
 $3 = +$3;
 $4 = +$4;
 $5 = +$5;
 $6 = $6 | 0;
 var $11 = 0.0, $13 = 0.0, $14 = 0.0, $9 = 0.0, dest = 0, stop = 0;
 dest = $6 + 4 | 0;
 stop = dest + 60 | 0;
 do {
  HEAP32[dest >> 2] = 0;
  dest = dest + 4 | 0;
 } while ((dest | 0) < (stop | 0));
 $9 = 1.0 / ($1 - $0);
 $11 = 1.0 / ($3 - $2);
 $13 = -1.0 / ($5 - $4);
 $14 = $4 * 2.0;
 HEAPF32[$6 >> 2] = $9 * $14;
 HEAPF32[$6 + 20 >> 2] = $14 * $11;
 HEAPF32[$6 + 32 >> 2] = ($0 + $1) * $9;
 HEAPF32[$6 + 36 >> 2] = ($2 + $3) * $11;
 HEAPF32[$6 + 40 >> 2] = ($4 + $5) * $13;
 HEAPF32[$6 + 44 >> 2] = -1.0;
 HEAPF32[$6 + 56 >> 2] = $14 * $5 * $13;
 return;
}

function _glmc_vec_normalize($0) {
 $0 = $0 | 0;
 var $$idx$i = 0, $$idx$val$i = 0.0, $$idx8$i = 0, $$idx8$val$i = 0.0, $$val$i = 0.0, $6 = 0.0, $8 = 0.0;
 $$val$i = +HEAPF32[$0 >> 2];
 $$idx$i = $0 + 4 | 0;
 $$idx$val$i = +HEAPF32[$$idx$i >> 2];
 $$idx8$i = $0 + 8 | 0;
 $$idx8$val$i = +HEAPF32[$$idx8$i >> 2];
 $6 = +Math_sqrt(+($$val$i * $$val$i + $$idx$val$i * $$idx$val$i + $$idx8$val$i * $$idx8$val$i));
 if ($6 == 0.0) {
  HEAPF32[$$idx8$i >> 2] = 0.0;
  HEAPF32[$$idx$i >> 2] = 0.0;
  HEAPF32[$0 >> 2] = 0.0;
  return;
 } else {
  $8 = 1.0 / $6;
  HEAPF32[$0 >> 2] = $$val$i * $8;
  HEAPF32[$$idx$i >> 2] = $$idx$val$i * $8;
  HEAPF32[$$idx8$i >> 2] = $$idx8$val$i * $8;
  return;
 }
}

function _glmc_persp_decomp($0, $1, $2, $3, $4, $5, $6) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = $2 | 0;
 $3 = $3 | 0;
 $4 = $4 | 0;
 $5 = $5 | 0;
 $6 = $6 | 0;
 var $11 = 0.0, $13 = 0.0, $15 = 0.0, $17 = 0.0, $19 = 0.0, $22 = 0.0, $23 = 0.0;
 $11 = +HEAPF32[$0 + 32 >> 2];
 $13 = +HEAPF32[$0 + 36 >> 2];
 $15 = +HEAPF32[$0 + 40 >> 2];
 $17 = +HEAPF32[$0 + 56 >> 2];
 $19 = $17 / ($15 + -1.0);
 $22 = $19 / +HEAPF32[$0 + 20 >> 2];
 $23 = $19 / +HEAPF32[$0 >> 2];
 HEAPF32[$1 >> 2] = $19;
 HEAPF32[$2 >> 2] = $17 / ($15 + 1.0);
 HEAPF32[$4 >> 2] = ($13 + -1.0) * $22;
 HEAPF32[$3 >> 2] = ($13 + 1.0) * $22;
 HEAPF32[$5 >> 2] = ($11 + -1.0) * $23;
 HEAPF32[$6 >> 2] = ($11 + 1.0) * $23;
 return;
}

function _glmc_mat4_mulv3($0, $1, $2, $3) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = +$2;
 $3 = $3 | 0;
 var $37 = 0.0, $4 = 0.0, $52 = 0.0, $6 = 0.0, $8 = 0.0;
 $4 = +HEAPF32[$1 >> 2];
 $6 = +HEAPF32[$1 + 4 >> 2];
 $8 = +HEAPF32[$1 + 8 >> 2];
 $37 = $4 * +HEAPF32[$0 + 4 >> 2] + $6 * +HEAPF32[$0 + 20 >> 2] + $8 * +HEAPF32[$0 + 36 >> 2] + +HEAPF32[$0 + 52 >> 2] * $2;
 $52 = $4 * +HEAPF32[$0 + 8 >> 2] + $6 * +HEAPF32[$0 + 24 >> 2] + $8 * +HEAPF32[$0 + 40 >> 2] + +HEAPF32[$0 + 56 >> 2] * $2;
 HEAPF32[$3 >> 2] = $4 * +HEAPF32[$0 >> 2] + $6 * +HEAPF32[$0 + 16 >> 2] + $8 * +HEAPF32[$0 + 32 >> 2] + +HEAPF32[$0 + 48 >> 2] * $2;
 HEAPF32[$3 + 4 >> 2] = $37;
 HEAPF32[$3 + 8 >> 2] = $52;
 return;
}

function _glmc_quatv($0, $1, $2) {
 $0 = $0 | 0;
 $1 = +$1;
 $2 = $2 | 0;
 var $$idx$val = 0.0, $$idx3$val = 0.0, $$val = 0.0, $11 = 0.0, $12 = 0, $13 = 0.0, $3 = 0.0, $4 = 0.0, $5 = 0.0;
 $$val = +HEAPF32[$2 >> 2];
 $$idx$val = +HEAPF32[$2 + 4 >> 2];
 $$idx3$val = +HEAPF32[$2 + 8 >> 2];
 $3 = $1 * .5;
 $4 = +Math_cos(+$3);
 $5 = +Math_sin(+$3);
 $11 = +Math_sqrt(+($$val * $$val + $$idx$val * $$idx$val + $$idx3$val * $$idx3$val));
 $12 = $11 == 0.0;
 $13 = 1.0 / $11;
 HEAPF32[$0 >> 2] = $5 * ($12 ? 0.0 : $$val * $13);
 HEAPF32[$0 + 4 >> 2] = $5 * ($12 ? 0.0 : $$idx$val * $13);
 HEAPF32[$0 + 8 >> 2] = $5 * ($12 ? 0.0 : $$idx3$val * $13);
 HEAPF32[$0 + 12 >> 2] = $4;
 return;
}

function _glmc_vec4_normalize($0) {
 $0 = $0 | 0;
 var $1 = 0.0, $11 = 0, $12 = 0.0, $15 = 0.0, $17 = 0.0, $3 = 0, $4 = 0.0, $7 = 0, $8 = 0.0;
 $1 = +HEAPF32[$0 >> 2];
 $3 = $0 + 4 | 0;
 $4 = +HEAPF32[$3 >> 2];
 $7 = $0 + 8 | 0;
 $8 = +HEAPF32[$7 >> 2];
 $11 = $0 + 12 | 0;
 $12 = +HEAPF32[$11 >> 2];
 $15 = +Math_sqrt(+($1 * $1 + $4 * $4 + $8 * $8 + $12 * $12));
 if ($15 == 0.0) {
  HEAP32[$0 >> 2] = 0;
  HEAP32[$0 + 4 >> 2] = 0;
  HEAP32[$0 + 8 >> 2] = 0;
  HEAP32[$0 + 12 >> 2] = 0;
  return;
 } else {
  $17 = 1.0 / $15;
  HEAPF32[$0 >> 2] = $1 * $17;
  HEAPF32[$3 >> 2] = $4 * $17;
  HEAPF32[$7 >> 2] = $8 * $17;
  HEAPF32[$11 >> 2] = $12 * $17;
  return;
 }
}

function _glmc_ortho($0, $1, $2, $3, $4, $5, $6) {
 $0 = +$0;
 $1 = +$1;
 $2 = +$2;
 $3 = +$3;
 $4 = +$4;
 $5 = +$5;
 $6 = $6 | 0;
 var $11 = 0.0, $13 = 0.0, $9 = 0.0, dest = 0, stop = 0;
 dest = $6 + 4 | 0;
 stop = dest + 44 | 0;
 do {
  HEAP32[dest >> 2] = 0;
  dest = dest + 4 | 0;
 } while ((dest | 0) < (stop | 0));
 $9 = 1.0 / ($1 - $0);
 $11 = 1.0 / ($3 - $2);
 $13 = -1.0 / ($5 - $4);
 HEAPF32[$6 >> 2] = $9 * 2.0;
 HEAPF32[$6 + 20 >> 2] = $11 * 2.0;
 HEAPF32[$6 + 40 >> 2] = $13 * 2.0;
 HEAPF32[$6 + 48 >> 2] = -(($0 + $1) * $9);
 HEAPF32[$6 + 52 >> 2] = -(($2 + $3) * $11);
 HEAPF32[$6 + 56 >> 2] = ($4 + $5) * $13;
 HEAPF32[$6 + 60 >> 2] = 1.0;
 return;
}

function _glmc_vec4_inv_to($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 var $10 = 0.0, $2 = 0, $4 = 0, $5 = 0, $7 = 0, $8 = 0;
 $2 = HEAP32[$0 >> 2] | 0;
 HEAP32[$1 >> 2] = $2;
 $4 = HEAP32[$0 + 4 >> 2] | 0;
 $5 = $1 + 4 | 0;
 HEAP32[$5 >> 2] = $4;
 $7 = HEAP32[$0 + 8 >> 2] | 0;
 $8 = $1 + 8 | 0;
 HEAP32[$8 >> 2] = $7;
 $10 = +HEAPF32[$0 + 12 >> 2];
 HEAPF32[$1 >> 2] = -(HEAP32[tempDoublePtr >> 2] = $2, +HEAPF32[tempDoublePtr >> 2]);
 HEAPF32[$5 >> 2] = -(HEAP32[tempDoublePtr >> 2] = $4, +HEAPF32[tempDoublePtr >> 2]);
 HEAPF32[$8 >> 2] = -(HEAP32[tempDoublePtr >> 2] = $7, +HEAPF32[tempDoublePtr >> 2]);
 HEAPF32[$1 + 12 >> 2] = -$10;
 return;
}

function _glmc_vec_angle($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 var $$idx$val = 0.0, $$idx2$val = 0.0, $$idx4$val = 0.0, $$idx5$val = 0.0, $$val = 0.0, $$val3 = 0.0;
 $$val = +HEAPF32[$0 >> 2];
 $$idx$val = +HEAPF32[$0 + 4 >> 2];
 $$idx2$val = +HEAPF32[$0 + 8 >> 2];
 $$val3 = +HEAPF32[$1 >> 2];
 $$idx4$val = +HEAPF32[$1 + 4 >> 2];
 $$idx5$val = +HEAPF32[$1 + 8 >> 2];
 return +(+Math_acos(+(($$val * $$val3 + $$idx$val * $$idx4$val + $$idx2$val * $$idx5$val) * (1.0 / (+Math_sqrt(+($$val * $$val + $$idx$val * $$idx$val + $$idx2$val * $$idx2$val)) * +Math_sqrt(+($$val3 * $$val3 + $$idx4$val * $$idx4$val + $$idx5$val * $$idx5$val)))))));
}

function _glmc_mat3_swap_col($0, $1, $2) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = $2 | 0;
 var $11 = 0, $13 = 0, $3 = 0, $4 = 0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0;
 $3 = $0 + ($1 * 12 | 0) | 0;
 $4 = HEAP32[$3 >> 2] | 0;
 $5 = $0 + ($1 * 12 | 0) + 4 | 0;
 $6 = HEAP32[$5 >> 2] | 0;
 $7 = $0 + ($1 * 12 | 0) + 8 | 0;
 $8 = HEAP32[$7 >> 2] | 0;
 $9 = $0 + ($2 * 12 | 0) | 0;
 HEAP32[$3 >> 2] = HEAP32[$9 >> 2];
 $11 = $0 + ($2 * 12 | 0) + 4 | 0;
 HEAP32[$5 >> 2] = HEAP32[$11 >> 2];
 $13 = $0 + ($2 * 12 | 0) + 8 | 0;
 HEAP32[$7 >> 2] = HEAP32[$13 >> 2];
 HEAP32[$9 >> 2] = $4;
 HEAP32[$11 >> 2] = $6;
 HEAP32[$13 >> 2] = $8;
 return;
}

function _glmc_vec_lerp($0, $1, $2, $3) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = +$2;
 $3 = $3 | 0;
 var $$$i$i$i = 0.0, $$$i3$i$i = 0.0, $11 = 0, $15 = 0.0, $16 = 0.0, $6 = 0.0, $8 = 0;
 $$$i$i$i = $2 > 0.0 ? $2 : 0.0;
 $$$i3$i$i = $$$i$i$i < 1.0 ? $$$i$i$i : 1.0;
 $6 = +HEAPF32[$0 >> 2];
 $8 = $0 + 4 | 0;
 $11 = $0 + 8 | 0;
 $15 = $$$i3$i$i * (+HEAPF32[$1 + 4 >> 2] - +HEAPF32[$8 >> 2]);
 $16 = $$$i3$i$i * (+HEAPF32[$1 + 8 >> 2] - +HEAPF32[$11 >> 2]);
 HEAPF32[$3 >> 2] = $6 + $$$i3$i$i * (+HEAPF32[$1 >> 2] - $6);
 HEAPF32[$3 + 4 >> 2] = $15 + +HEAPF32[$8 >> 2];
 HEAPF32[$3 + 8 >> 2] = $16 + +HEAPF32[$11 >> 2];
 return;
}

function _glmc_persp_decompv($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 var $11 = 0.0, $13 = 0.0, $15 = 0.0, $17 = 0.0, $19 = 0.0, $22 = 0.0, $23 = 0.0;
 $11 = +HEAPF32[$0 + 32 >> 2];
 $13 = +HEAPF32[$0 + 36 >> 2];
 $15 = +HEAPF32[$0 + 40 >> 2];
 $17 = +HEAPF32[$0 + 56 >> 2];
 $19 = $17 / ($15 + -1.0);
 $22 = $19 / +HEAPF32[$0 + 20 >> 2];
 $23 = $19 / +HEAPF32[$0 >> 2];
 HEAPF32[$1 >> 2] = $19;
 HEAPF32[$1 + 4 >> 2] = $17 / ($15 + 1.0);
 HEAPF32[$1 + 12 >> 2] = ($13 + -1.0) * $22;
 HEAPF32[$1 + 8 >> 2] = ($13 + 1.0) * $22;
 HEAPF32[$1 + 16 >> 2] = ($11 + -1.0) * $23;
 HEAPF32[$1 + 20 >> 2] = ($11 + 1.0) * $23;
 return;
}

function _glmc_mat3_mulv($0, $1, $2) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = $2 | 0;
 var $14 = 0, $8 = 0;
 $8 = $1 + 4 | 0;
 $14 = $1 + 8 | 0;
 HEAPF32[$2 >> 2] = +HEAPF32[$0 >> 2] * +HEAPF32[$1 >> 2] + +HEAPF32[$0 + 12 >> 2] * +HEAPF32[$8 >> 2] + +HEAPF32[$0 + 24 >> 2] * +HEAPF32[$14 >> 2];
 HEAPF32[$2 + 4 >> 2] = +HEAPF32[$0 + 4 >> 2] * +HEAPF32[$1 >> 2] + +HEAPF32[$0 + 16 >> 2] * +HEAPF32[$8 >> 2] + +HEAPF32[$0 + 28 >> 2] * +HEAPF32[$14 >> 2];
 HEAPF32[$2 + 8 >> 2] = +HEAPF32[$0 + 8 >> 2] * +HEAPF32[$1 >> 2] + +HEAPF32[$0 + 20 >> 2] * +HEAPF32[$8 >> 2] + +HEAPF32[$0 + 32 >> 2] * +HEAPF32[$14 >> 2];
 return;
}

function _glmc_plane_normalize($0) {
 $0 = $0 | 0;
 var $$idx$i = 0, $$idx$val$i = 0.0, $$idx3$i = 0, $$idx3$val$i = 0.0, $$val$i = 0.0, $11 = 0, $7 = 0.0;
 $$val$i = +HEAPF32[$0 >> 2];
 $$idx$i = $0 + 4 | 0;
 $$idx$val$i = +HEAPF32[$$idx$i >> 2];
 $$idx3$i = $0 + 8 | 0;
 $$idx3$val$i = +HEAPF32[$$idx3$i >> 2];
 $7 = 1.0 / +Math_sqrt(+($$val$i * $$val$i + $$idx$val$i * $$idx$val$i + $$idx3$val$i * $$idx3$val$i));
 HEAPF32[$0 >> 2] = $$val$i * $7;
 HEAPF32[$$idx$i >> 2] = $$idx$val$i * $7;
 HEAPF32[$$idx3$i >> 2] = $$idx3$val$i * $7;
 $11 = $0 + 12 | 0;
 HEAPF32[$11 >> 2] = $7 * +HEAPF32[$11 >> 2];
 return;
}

function _glmc_mat3_swap_row($0, $1, $2) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = $2 | 0;
 var $11 = 0, $13 = 0, $3 = 0, $4 = 0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0;
 $3 = $0 + ($1 << 2) | 0;
 $4 = HEAP32[$3 >> 2] | 0;
 $5 = $0 + 12 + ($1 << 2) | 0;
 $6 = HEAP32[$5 >> 2] | 0;
 $7 = $0 + 24 + ($1 << 2) | 0;
 $8 = HEAP32[$7 >> 2] | 0;
 $9 = $0 + ($2 << 2) | 0;
 HEAP32[$3 >> 2] = HEAP32[$9 >> 2];
 $11 = $0 + 12 + ($2 << 2) | 0;
 HEAP32[$5 >> 2] = HEAP32[$11 >> 2];
 $13 = $0 + 24 + ($2 << 2) | 0;
 HEAP32[$7 >> 2] = HEAP32[$13 >> 2];
 HEAP32[$9 >> 2] = $4;
 HEAP32[$11 >> 2] = $6;
 HEAP32[$13 >> 2] = $8;
 return;
}

function _glmc_vec4_isvalid($0) {
 $0 = $0 | 0;
 var $10 = 0, $14 = 0, $2 = 0, $20 = 0, $6 = 0;
 $2 = HEAP32[$0 >> 2] & 2147483647;
 if ($2 >>> 0 > 2139095040) {
  $20 = 0;
  return $20 | 0;
 }
 $6 = HEAP32[$0 + 4 >> 2] & 2147483647;
 if ($6 >>> 0 > 2139095040) {
  $20 = 0;
  return $20 | 0;
 }
 $10 = HEAP32[$0 + 8 >> 2] & 2147483647;
 if ($10 >>> 0 > 2139095040) {
  $20 = 0;
  return $20 | 0;
 }
 $14 = HEAP32[$0 + 12 >> 2] & 2147483647;
 $20 = ($14 | 0) != 2139095040 & ((($10 | 0) == 2139095040 | (($6 | 0) == 2139095040 | (($2 | 0) == 2139095040 | $14 >>> 0 > 2139095040))) ^ 1);
 return $20 | 0;
}

function _sbrk(increment) {
 increment = increment | 0;
 var oldDynamicTop = 0, newDynamicTop = 0;
 oldDynamicTop = HEAP32[DYNAMICTOP_PTR >> 2] | 0;
 newDynamicTop = oldDynamicTop + increment | 0;
 if ((increment | 0) > 0 & (newDynamicTop | 0) < (oldDynamicTop | 0) | (newDynamicTop | 0) < 0) {
  abortOnCannotGrowMemory() | 0;
  ___setErrNo(12);
  return -1;
 }
 HEAP32[DYNAMICTOP_PTR >> 2] = newDynamicTop;
 if ((newDynamicTop | 0) > (getTotalMemory() | 0)) if (!(enlargeMemory() | 0)) {
  HEAP32[DYNAMICTOP_PTR >> 2] = oldDynamicTop;
  ___setErrNo(12);
  return -1;
 }
 return oldDynamicTop | 0;
}

function _glmc_vec4_minv($0, $1, $2) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = $2 | 0;
 var $13 = 0.0, $15 = 0.0, $19 = 0.0, $21 = 0.0, $3 = 0.0, $4 = 0.0, $7 = 0.0, $9 = 0.0;
 $3 = +HEAPF32[$0 >> 2];
 $4 = +HEAPF32[$1 >> 2];
 HEAPF32[$2 >> 2] = $3 > $4 ? $3 : $4;
 $7 = +HEAPF32[$0 + 4 >> 2];
 $9 = +HEAPF32[$1 + 4 >> 2];
 HEAPF32[$2 + 4 >> 2] = $7 > $9 ? $7 : $9;
 $13 = +HEAPF32[$0 + 8 >> 2];
 $15 = +HEAPF32[$1 + 8 >> 2];
 HEAPF32[$2 + 8 >> 2] = $13 > $15 ? $13 : $15;
 $19 = +HEAPF32[$0 + 12 >> 2];
 $21 = +HEAPF32[$1 + 12 >> 2];
 HEAPF32[$2 + 12 >> 2] = $19 > $21 ? $19 : $21;
 return;
}

function _glmc_vec4_maxv($0, $1, $2) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = $2 | 0;
 var $13 = 0.0, $15 = 0.0, $19 = 0.0, $21 = 0.0, $3 = 0.0, $4 = 0.0, $7 = 0.0, $9 = 0.0;
 $3 = +HEAPF32[$0 >> 2];
 $4 = +HEAPF32[$1 >> 2];
 HEAPF32[$2 >> 2] = $3 < $4 ? $3 : $4;
 $7 = +HEAPF32[$0 + 4 >> 2];
 $9 = +HEAPF32[$1 + 4 >> 2];
 HEAPF32[$2 + 4 >> 2] = $7 < $9 ? $7 : $9;
 $13 = +HEAPF32[$0 + 8 >> 2];
 $15 = +HEAPF32[$1 + 8 >> 2];
 HEAPF32[$2 + 8 >> 2] = $13 < $15 ? $13 : $15;
 $19 = +HEAPF32[$0 + 12 >> 2];
 $21 = +HEAPF32[$1 + 12 >> 2];
 HEAPF32[$2 + 12 >> 2] = $19 < $21 ? $19 : $21;
 return;
}

function _glmc_mat4_identity($0) {
 $0 = $0 | 0;
 var $1 = 0, $3 = 0, $5 = 0;
 $1 = $0 + 4 | 0;
 HEAP32[$0 >> 2] = 1065353216;
 $3 = $0 + 24 | 0;
 HEAP32[$1 >> 2] = 0;
 HEAP32[$1 + 4 >> 2] = 0;
 HEAP32[$1 + 8 >> 2] = 0;
 HEAP32[$1 + 12 >> 2] = 0;
 HEAP32[$0 + 20 >> 2] = 1065353216;
 $5 = $0 + 44 | 0;
 HEAP32[$3 >> 2] = 0;
 HEAP32[$3 + 4 >> 2] = 0;
 HEAP32[$3 + 8 >> 2] = 0;
 HEAP32[$3 + 12 >> 2] = 0;
 HEAP32[$0 + 40 >> 2] = 1065353216;
 HEAP32[$5 >> 2] = 0;
 HEAP32[$5 + 4 >> 2] = 0;
 HEAP32[$5 + 8 >> 2] = 0;
 HEAP32[$5 + 12 >> 2] = 0;
 HEAP32[$0 + 60 >> 2] = 1065353216;
 return;
}

function _glmc_vec4_eqv_eps($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 var $28 = 0;
 if (!(+Math_abs(+(+HEAPF32[$0 >> 2] - +HEAPF32[$1 >> 2])) <= 1.1920928955078125e-07)) {
  $28 = 0;
  return $28 | 0;
 }
 if (!(+Math_abs(+(+HEAPF32[$0 + 4 >> 2] - +HEAPF32[$1 + 4 >> 2])) <= 1.1920928955078125e-07)) {
  $28 = 0;
  return $28 | 0;
 }
 if (!(+Math_abs(+(+HEAPF32[$0 + 8 >> 2] - +HEAPF32[$1 + 8 >> 2])) <= 1.1920928955078125e-07)) {
  $28 = 0;
  return $28 | 0;
 }
 $28 = +Math_abs(+(+HEAPF32[$0 + 12 >> 2] - +HEAPF32[$1 + 12 >> 2])) <= 1.1920928955078125e-07;
 return $28 | 0;
}

function _glmc_persp_sizes($0, $1, $2) {
 $0 = $0 | 0;
 $1 = +$1;
 $2 = $2 | 0;
 var $$idx$val$i = 0.0, $$idx16$val$i = 0.0, $11 = 0.0, $13 = 0.0, $5 = 0.0, $6 = 0.0;
 $5 = +Math_tan(+($1 * .5)) * 2.0;
 $6 = +HEAPF32[$0 + 20 >> 2] / +HEAPF32[$0 >> 2];
 $$idx$val$i = +HEAPF32[$0 + 40 >> 2];
 $$idx16$val$i = +HEAPF32[$0 + 56 >> 2];
 $11 = $5 * ($$idx16$val$i / ($$idx$val$i + -1.0));
 HEAPF32[$2 + 4 >> 2] = $11;
 $13 = $5 * ($$idx16$val$i / ($$idx$val$i + 1.0));
 HEAPF32[$2 + 12 >> 2] = $13;
 HEAPF32[$2 >> 2] = $6 * $11;
 HEAPF32[$2 + 8 >> 2] = $6 * $13;
 return;
}

function _glmc_vec_clamp($0, $1, $2) {
 $0 = $0 | 0;
 $1 = +$1;
 $2 = +$2;
 var $$$i$i$i = 0.0, $$$i$i12$i = 0.0, $$$i$i14$i = 0.0, $10 = 0, $11 = 0.0, $3 = 0.0, $6 = 0, $7 = 0.0;
 $3 = +HEAPF32[$0 >> 2];
 $$$i$i$i = $3 > $1 ? $3 : $1;
 HEAPF32[$0 >> 2] = $$$i$i$i < $2 ? $$$i$i$i : $2;
 $6 = $0 + 4 | 0;
 $7 = +HEAPF32[$6 >> 2];
 $$$i$i14$i = $7 > $1 ? $7 : $1;
 HEAPF32[$6 >> 2] = $$$i$i14$i < $2 ? $$$i$i14$i : $2;
 $10 = $0 + 8 | 0;
 $11 = +HEAPF32[$10 >> 2];
 $$$i$i12$i = $11 > $1 ? $11 : $1;
 HEAPF32[$10 >> 2] = $$$i$i12$i < $2 ? $$$i$i12$i : $2;
 return;
}

function _glmc_mat3_transpose($0) {
 $0 = $0 | 0;
 var $1 = 0, $10 = 0, $11 = 0, $12 = 0, $3 = 0, $4 = 0, $5 = 0, $6 = 0, $7 = 0, $8 = 0, $9 = 0;
 $1 = $0 + 12 | 0;
 $3 = $0 + 24 | 0;
 $4 = HEAP32[$3 >> 2] | 0;
 $5 = $0 + 4 | 0;
 $6 = HEAP32[$5 >> 2] | 0;
 $7 = $0 + 28 | 0;
 $8 = HEAP32[$7 >> 2] | 0;
 $9 = $0 + 8 | 0;
 $10 = HEAP32[$9 >> 2] | 0;
 $11 = $0 + 20 | 0;
 $12 = HEAP32[$11 >> 2] | 0;
 HEAP32[$5 >> 2] = HEAP32[$1 >> 2];
 HEAP32[$9 >> 2] = $4;
 HEAP32[$1 >> 2] = $6;
 HEAP32[$11 >> 2] = $8;
 HEAP32[$3 >> 2] = $10;
 HEAP32[$7 >> 2] = $12;
 return;
}

function _glmc_translate_z($0, $1) {
 $0 = $0 | 0;
 $1 = +$1;
 var $10 = 0.0, $13 = 0.0, $14 = 0, $17 = 0, $20 = 0, $23 = 0, $7 = 0.0;
 $7 = +HEAPF32[$0 + 36 >> 2] * $1;
 $10 = +HEAPF32[$0 + 40 >> 2] * $1;
 $13 = +HEAPF32[$0 + 44 >> 2] * $1;
 $14 = $0 + 48 | 0;
 HEAPF32[$14 >> 2] = +HEAPF32[$0 + 32 >> 2] * $1 + +HEAPF32[$14 >> 2];
 $17 = $0 + 52 | 0;
 HEAPF32[$17 >> 2] = $7 + +HEAPF32[$17 >> 2];
 $20 = $0 + 56 | 0;
 HEAPF32[$20 >> 2] = $10 + +HEAPF32[$20 >> 2];
 $23 = $0 + 60 | 0;
 HEAPF32[$23 >> 2] = $13 + +HEAPF32[$23 >> 2];
 return;
}

function _glmc_translate_y($0, $1) {
 $0 = $0 | 0;
 $1 = +$1;
 var $10 = 0.0, $13 = 0.0, $14 = 0, $17 = 0, $20 = 0, $23 = 0, $7 = 0.0;
 $7 = +HEAPF32[$0 + 20 >> 2] * $1;
 $10 = +HEAPF32[$0 + 24 >> 2] * $1;
 $13 = +HEAPF32[$0 + 28 >> 2] * $1;
 $14 = $0 + 48 | 0;
 HEAPF32[$14 >> 2] = +HEAPF32[$0 + 16 >> 2] * $1 + +HEAPF32[$14 >> 2];
 $17 = $0 + 52 | 0;
 HEAPF32[$17 >> 2] = $7 + +HEAPF32[$17 >> 2];
 $20 = $0 + 56 | 0;
 HEAPF32[$20 >> 2] = $10 + +HEAPF32[$20 >> 2];
 $23 = $0 + 60 | 0;
 HEAPF32[$23 >> 2] = $13 + +HEAPF32[$23 >> 2];
 return;
}

function _glmc_vec4_subadd($0, $1, $2) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = $2 | 0;
 var $13 = 0, $21 = 0, $29 = 0;
 HEAPF32[$2 >> 2] = +HEAPF32[$2 >> 2] + (+HEAPF32[$0 >> 2] - +HEAPF32[$1 >> 2]);
 $13 = $2 + 4 | 0;
 HEAPF32[$13 >> 2] = +HEAPF32[$13 >> 2] + (+HEAPF32[$0 + 4 >> 2] - +HEAPF32[$1 + 4 >> 2]);
 $21 = $2 + 8 | 0;
 HEAPF32[$21 >> 2] = +HEAPF32[$21 >> 2] + (+HEAPF32[$0 + 8 >> 2] - +HEAPF32[$1 + 8 >> 2]);
 $29 = $2 + 12 | 0;
 HEAPF32[$29 >> 2] = +HEAPF32[$29 >> 2] + (+HEAPF32[$0 + 12 >> 2] - +HEAPF32[$1 + 12 >> 2]);
 return;
}

function _glmc_vec4_addadd($0, $1, $2) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = $2 | 0;
 var $13 = 0, $21 = 0, $29 = 0;
 HEAPF32[$2 >> 2] = +HEAPF32[$2 >> 2] + (+HEAPF32[$0 >> 2] + +HEAPF32[$1 >> 2]);
 $13 = $2 + 4 | 0;
 HEAPF32[$13 >> 2] = +HEAPF32[$13 >> 2] + (+HEAPF32[$0 + 4 >> 2] + +HEAPF32[$1 + 4 >> 2]);
 $21 = $2 + 8 | 0;
 HEAPF32[$21 >> 2] = +HEAPF32[$21 >> 2] + (+HEAPF32[$0 + 8 >> 2] + +HEAPF32[$1 + 8 >> 2]);
 $29 = $2 + 12 | 0;
 HEAPF32[$29 >> 2] = +HEAPF32[$29 >> 2] + (+HEAPF32[$0 + 12 >> 2] + +HEAPF32[$1 + 12 >> 2]);
 return;
}

function _glmc_translate_x($0, $1) {
 $0 = $0 | 0;
 $1 = +$1;
 var $12 = 0.0, $13 = 0, $16 = 0, $19 = 0, $22 = 0, $6 = 0.0, $9 = 0.0;
 $6 = +HEAPF32[$0 + 4 >> 2] * $1;
 $9 = +HEAPF32[$0 + 8 >> 2] * $1;
 $12 = +HEAPF32[$0 + 12 >> 2] * $1;
 $13 = $0 + 48 | 0;
 HEAPF32[$13 >> 2] = +HEAPF32[$0 >> 2] * $1 + +HEAPF32[$13 >> 2];
 $16 = $0 + 52 | 0;
 HEAPF32[$16 >> 2] = $6 + +HEAPF32[$16 >> 2];
 $19 = $0 + 56 | 0;
 HEAPF32[$19 >> 2] = $9 + +HEAPF32[$19 >> 2];
 $22 = $0 + 60 | 0;
 HEAPF32[$22 >> 2] = $12 + +HEAPF32[$22 >> 2];
 return;
}

function _glmc_perspective($0, $1, $2, $3, $4) {
 $0 = +$0;
 $1 = +$1;
 $2 = +$2;
 $3 = +$3;
 $4 = $4 | 0;
 var $10 = 0.0, $8 = 0.0, dest = 0, stop = 0;
 dest = $4 + 4 | 0;
 stop = dest + 60 | 0;
 do {
  HEAP32[dest >> 2] = 0;
  dest = dest + 4 | 0;
 } while ((dest | 0) < (stop | 0));
 $8 = 1.0 / +Math_tan(+($0 * .5));
 $10 = 1.0 / ($2 - $3);
 HEAPF32[$4 >> 2] = $8 / $1;
 HEAPF32[$4 + 20 >> 2] = $8;
 HEAPF32[$4 + 40 >> 2] = ($2 + $3) * $10;
 HEAPF32[$4 + 44 >> 2] = -1.0;
 HEAPF32[$4 + 56 >> 2] = $2 * 2.0 * $3 * $10;
 return;
}

function _glmc_vec4_muladd($0, $1, $2) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = $2 | 0;
 var $13 = 0, $21 = 0, $29 = 0;
 HEAPF32[$2 >> 2] = +HEAPF32[$2 >> 2] + +HEAPF32[$0 >> 2] * +HEAPF32[$1 >> 2];
 $13 = $2 + 4 | 0;
 HEAPF32[$13 >> 2] = +HEAPF32[$13 >> 2] + +HEAPF32[$0 + 4 >> 2] * +HEAPF32[$1 + 4 >> 2];
 $21 = $2 + 8 | 0;
 HEAPF32[$21 >> 2] = +HEAPF32[$21 >> 2] + +HEAPF32[$0 + 8 >> 2] * +HEAPF32[$1 + 8 >> 2];
 $29 = $2 + 12 | 0;
 HEAPF32[$29 >> 2] = +HEAPF32[$29 >> 2] + +HEAPF32[$0 + 12 >> 2] * +HEAPF32[$1 + 12 >> 2];
 return;
}

function _glmc_mat3_copy($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 HEAP32[$1 >> 2] = HEAP32[$0 >> 2];
 HEAP32[$1 + 4 >> 2] = HEAP32[$0 + 4 >> 2];
 HEAP32[$1 + 8 >> 2] = HEAP32[$0 + 8 >> 2];
 HEAP32[$1 + 12 >> 2] = HEAP32[$0 + 12 >> 2];
 HEAP32[$1 + 16 >> 2] = HEAP32[$0 + 16 >> 2];
 HEAP32[$1 + 20 >> 2] = HEAP32[$0 + 20 >> 2];
 HEAP32[$1 + 24 >> 2] = HEAP32[$0 + 24 >> 2];
 HEAP32[$1 + 28 >> 2] = HEAP32[$0 + 28 >> 2];
 HEAP32[$1 + 32 >> 2] = HEAP32[$0 + 32 >> 2];
 HEAP32[$1 + 36 >> 2] = HEAP32[$0 + 36 >> 2];
 return;
}

function _glmc_vec_cross($0, $1, $2) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = $2 | 0;
 var $10 = 0, $3 = 0, $5 = 0, $8 = 0;
 $3 = $0 + 4 | 0;
 $5 = $1 + 8 | 0;
 $8 = $0 + 8 | 0;
 $10 = $1 + 4 | 0;
 HEAPF32[$2 >> 2] = +HEAPF32[$3 >> 2] * +HEAPF32[$5 >> 2] - +HEAPF32[$8 >> 2] * +HEAPF32[$10 >> 2];
 HEAPF32[$2 + 4 >> 2] = +HEAPF32[$8 >> 2] * +HEAPF32[$1 >> 2] - +HEAPF32[$0 >> 2] * +HEAPF32[$5 >> 2];
 HEAPF32[$2 + 8 >> 2] = +HEAPF32[$0 >> 2] * +HEAPF32[$10 >> 2] - +HEAPF32[$3 >> 2] * +HEAPF32[$1 >> 2];
 return;
}

function _glmc_quat($0, $1, $2, $3, $4) {
 $0 = $0 | 0;
 $1 = +$1;
 $2 = +$2;
 $3 = +$3;
 $4 = +$4;
 var $13 = 0.0, $14 = 0, $15 = 0.0, $5 = 0.0, $6 = 0.0, $7 = 0.0;
 $5 = $1 * .5;
 $6 = +Math_cos(+$5);
 $7 = +Math_sin(+$5);
 $13 = +Math_sqrt(+($2 * $2 + $3 * $3 + $4 * $4));
 $14 = $13 == 0.0;
 $15 = 1.0 / $13;
 HEAPF32[$0 >> 2] = $7 * ($14 ? 0.0 : $15 * $2);
 HEAPF32[$0 + 4 >> 2] = $7 * ($14 ? 0.0 : $15 * $3);
 HEAPF32[$0 + 8 >> 2] = $7 * ($14 ? 0.0 : $15 * $4);
 HEAPF32[$0 + 12 >> 2] = $6;
 return;
}

function _glmc_vec4_sign($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 var $10 = 0.0, $19 = 0.0, $2 = 0.0, $28 = 0.0;
 $2 = +HEAPF32[$0 >> 2];
 HEAPF32[$1 >> 2] = +(($2 > 0.0 & 1) - ($2 < 0.0 & 1) | 0);
 $10 = +HEAPF32[$0 + 4 >> 2];
 HEAPF32[$1 + 4 >> 2] = +(($10 > 0.0 & 1) - ($10 < 0.0 & 1) | 0);
 $19 = +HEAPF32[$0 + 8 >> 2];
 HEAPF32[$1 + 8 >> 2] = +(($19 > 0.0 & 1) - ($19 < 0.0 & 1) | 0);
 $28 = +HEAPF32[$0 + 12 >> 2];
 HEAPF32[$1 + 12 >> 2] = +(($28 > 0.0 & 1) - ($28 < 0.0 & 1) | 0);
 return;
}

function _glmc_vec4_eq_eps($0, $1) {
 $0 = $0 | 0;
 $1 = +$1;
 var $21 = 0;
 if (!(+Math_abs(+(+HEAPF32[$0 >> 2] - $1)) <= 1.1920928955078125e-07)) {
  $21 = 0;
  return $21 | 0;
 }
 if (!(+Math_abs(+(+HEAPF32[$0 + 4 >> 2] - $1)) <= 1.1920928955078125e-07)) {
  $21 = 0;
  return $21 | 0;
 }
 if (!(+Math_abs(+(+HEAPF32[$0 + 8 >> 2] - $1)) <= 1.1920928955078125e-07)) {
  $21 = 0;
  return $21 | 0;
 }
 $21 = +Math_abs(+(+HEAPF32[$0 + 12 >> 2] - $1)) <= 1.1920928955078125e-07;
 return $21 | 0;
}

function _glmc_mat3_transpose_to($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 HEAP32[$1 >> 2] = HEAP32[$0 >> 2];
 HEAP32[$1 + 4 >> 2] = HEAP32[$0 + 12 >> 2];
 HEAP32[$1 + 8 >> 2] = HEAP32[$0 + 24 >> 2];
 HEAP32[$1 + 12 >> 2] = HEAP32[$0 + 4 >> 2];
 HEAP32[$1 + 16 >> 2] = HEAP32[$0 + 16 >> 2];
 HEAP32[$1 + 20 >> 2] = HEAP32[$0 + 28 >> 2];
 HEAP32[$1 + 24 >> 2] = HEAP32[$0 + 8 >> 2];
 HEAP32[$1 + 28 >> 2] = HEAP32[$0 + 20 >> 2];
 HEAP32[$1 + 32 >> 2] = HEAP32[$0 + 32 >> 2];
 return;
}

function _glmc_mat4_pick3t($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 HEAP32[$1 >> 2] = HEAP32[$0 >> 2];
 HEAP32[$1 + 4 >> 2] = HEAP32[$0 + 16 >> 2];
 HEAP32[$1 + 8 >> 2] = HEAP32[$0 + 32 >> 2];
 HEAP32[$1 + 12 >> 2] = HEAP32[$0 + 4 >> 2];
 HEAP32[$1 + 16 >> 2] = HEAP32[$0 + 20 >> 2];
 HEAP32[$1 + 20 >> 2] = HEAP32[$0 + 36 >> 2];
 HEAP32[$1 + 24 >> 2] = HEAP32[$0 + 8 >> 2];
 HEAP32[$1 + 28 >> 2] = HEAP32[$0 + 24 >> 2];
 HEAP32[$1 + 32 >> 2] = HEAP32[$0 + 40 >> 2];
 return;
}

function _glmc_mat4_pick3($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 HEAP32[$1 >> 2] = HEAP32[$0 >> 2];
 HEAP32[$1 + 4 >> 2] = HEAP32[$0 + 4 >> 2];
 HEAP32[$1 + 8 >> 2] = HEAP32[$0 + 8 >> 2];
 HEAP32[$1 + 12 >> 2] = HEAP32[$0 + 16 >> 2];
 HEAP32[$1 + 16 >> 2] = HEAP32[$0 + 20 >> 2];
 HEAP32[$1 + 20 >> 2] = HEAP32[$0 + 24 >> 2];
 HEAP32[$1 + 24 >> 2] = HEAP32[$0 + 32 >> 2];
 HEAP32[$1 + 28 >> 2] = HEAP32[$0 + 36 >> 2];
 HEAP32[$1 + 32 >> 2] = HEAP32[$0 + 40 >> 2];
 return;
}

function _glmc_mat4_ins3($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 HEAP32[$1 >> 2] = HEAP32[$0 >> 2];
 HEAP32[$1 + 4 >> 2] = HEAP32[$0 + 4 >> 2];
 HEAP32[$1 + 8 >> 2] = HEAP32[$0 + 8 >> 2];
 HEAP32[$1 + 16 >> 2] = HEAP32[$0 + 12 >> 2];
 HEAP32[$1 + 20 >> 2] = HEAP32[$0 + 16 >> 2];
 HEAP32[$1 + 24 >> 2] = HEAP32[$0 + 20 >> 2];
 HEAP32[$1 + 32 >> 2] = HEAP32[$0 + 24 >> 2];
 HEAP32[$1 + 36 >> 2] = HEAP32[$0 + 28 >> 2];
 HEAP32[$1 + 40 >> 2] = HEAP32[$0 + 32 >> 2];
 return;
}

function _glmc_vec_isvalid($0) {
 $0 = $0 | 0;
 var $10 = 0, $15 = 0, $2 = 0, $6 = 0;
 $2 = HEAP32[$0 >> 2] & 2147483647;
 if ($2 >>> 0 > 2139095040) {
  $15 = 0;
  return $15 | 0;
 }
 $6 = HEAP32[$0 + 4 >> 2] & 2147483647;
 if ($6 >>> 0 > 2139095040) {
  $15 = 0;
  return $15 | 0;
 }
 $10 = HEAP32[$0 + 8 >> 2] & 2147483647;
 $15 = ($10 | 0) != 2139095040 & ((($6 | 0) == 2139095040 | (($2 | 0) == 2139095040 | $10 >>> 0 > 2139095040)) ^ 1);
 return $15 | 0;
}

function _glmc_vec4_muladds($0, $1, $2) {
 $0 = $0 | 0;
 $1 = +$1;
 $2 = $2 | 0;
 var $10 = 0, $16 = 0, $22 = 0;
 HEAPF32[$2 >> 2] = +HEAPF32[$2 >> 2] + +HEAPF32[$0 >> 2] * $1;
 $10 = $2 + 4 | 0;
 HEAPF32[$10 >> 2] = +HEAPF32[$10 >> 2] + +HEAPF32[$0 + 4 >> 2] * $1;
 $16 = $2 + 8 | 0;
 HEAPF32[$16 >> 2] = +HEAPF32[$16 >> 2] + +HEAPF32[$0 + 8 >> 2] * $1;
 $22 = $2 + 12 | 0;
 HEAPF32[$22 >> 2] = +HEAPF32[$22 >> 2] + +HEAPF32[$0 + 12 >> 2] * $1;
 return;
}

function _glmc_vec_minv($0, $1, $2) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = $2 | 0;
 var $13 = 0.0, $15 = 0.0, $3 = 0.0, $4 = 0.0, $7 = 0.0, $9 = 0.0;
 $3 = +HEAPF32[$0 >> 2];
 $4 = +HEAPF32[$1 >> 2];
 HEAPF32[$2 >> 2] = $3 > $4 ? $3 : $4;
 $7 = +HEAPF32[$0 + 4 >> 2];
 $9 = +HEAPF32[$1 + 4 >> 2];
 HEAPF32[$2 + 4 >> 2] = $7 > $9 ? $7 : $9;
 $13 = +HEAPF32[$0 + 8 >> 2];
 $15 = +HEAPF32[$1 + 8 >> 2];
 HEAPF32[$2 + 8 >> 2] = $13 > $15 ? $13 : $15;
 return;
}

function _glmc_vec_maxv($0, $1, $2) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = $2 | 0;
 var $13 = 0.0, $15 = 0.0, $3 = 0.0, $4 = 0.0, $7 = 0.0, $9 = 0.0;
 $3 = +HEAPF32[$0 >> 2];
 $4 = +HEAPF32[$1 >> 2];
 HEAPF32[$2 >> 2] = $3 < $4 ? $3 : $4;
 $7 = +HEAPF32[$0 + 4 >> 2];
 $9 = +HEAPF32[$1 + 4 >> 2];
 HEAPF32[$2 + 4 >> 2] = $7 < $9 ? $7 : $9;
 $13 = +HEAPF32[$0 + 8 >> 2];
 $15 = +HEAPF32[$1 + 8 >> 2];
 HEAPF32[$2 + 8 >> 2] = $13 < $15 ? $13 : $15;
 return;
}

function _glmc_vec_center($0, $1, $2) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = $2 | 0;
 var $10 = 0.0, $11 = 0, $16 = 0.0, $5 = 0.0;
 $5 = +HEAPF32[$0 >> 2] + +HEAPF32[$1 >> 2];
 HEAPF32[$2 >> 2] = $5;
 $10 = +HEAPF32[$0 + 4 >> 2] + +HEAPF32[$1 + 4 >> 2];
 $11 = $2 + 4 | 0;
 HEAPF32[$11 >> 2] = $10;
 $16 = +HEAPF32[$0 + 8 >> 2] + +HEAPF32[$1 + 8 >> 2];
 HEAPF32[$2 >> 2] = $5 * .5;
 HEAPF32[$11 >> 2] = $10 * .5;
 HEAPF32[$2 + 8 >> 2] = $16 * .5;
 return;
}

function _glmc_perspective_default($0, $1) {
 $0 = +$0;
 $1 = $1 | 0;
 var dest = 0, stop = 0;
 dest = $1 + 4 | 0;
 stop = dest + 60 | 0;
 do {
  HEAP32[dest >> 2] = 0;
  dest = dest + 4 | 0;
 } while ((dest | 0) < (stop | 0));
 HEAPF32[$1 >> 2] = 2.4142134189605713 / $0;
 HEAPF32[$1 + 20 >> 2] = 2.4142134189605713;
 HEAPF32[$1 + 40 >> 2] = -1.0002000331878662;
 HEAPF32[$1 + 44 >> 2] = -1.0;
 HEAPF32[$1 + 56 >> 2] = -.020002000033855438;
 return;
}

function _glmc_mat3_det($0) {
 $0 = $0 | 0;
 var $11 = 0.0, $15 = 0.0, $17 = 0.0, $3 = 0.0, $5 = 0.0, $9 = 0.0;
 $3 = +HEAPF32[$0 + 4 >> 2];
 $5 = +HEAPF32[$0 + 8 >> 2];
 $9 = +HEAPF32[$0 + 16 >> 2];
 $11 = +HEAPF32[$0 + 20 >> 2];
 $15 = +HEAPF32[$0 + 28 >> 2];
 $17 = +HEAPF32[$0 + 32 >> 2];
 return +(+HEAPF32[$0 + 24 >> 2] * ($3 * $11 - $5 * $9) + (+HEAPF32[$0 >> 2] * ($9 * $17 - $11 * $15) - +HEAPF32[$0 + 12 >> 2] * ($3 * $17 - $5 * $15)));
}

function _glmc_aabb_center($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 var $10 = 0.0, $11 = 0, $16 = 0.0, $5 = 0.0;
 $5 = +HEAPF32[$0 >> 2] + +HEAPF32[$0 + 12 >> 2];
 HEAPF32[$1 >> 2] = $5;
 $10 = +HEAPF32[$0 + 4 >> 2] + +HEAPF32[$0 + 16 >> 2];
 $11 = $1 + 4 | 0;
 HEAPF32[$11 >> 2] = $10;
 $16 = +HEAPF32[$0 + 8 >> 2] + +HEAPF32[$0 + 20 >> 2];
 HEAPF32[$1 >> 2] = $5 * .5;
 HEAPF32[$11 >> 2] = $10 * .5;
 HEAPF32[$1 + 8 >> 2] = $16 * .5;
 return;
}

function _glmc_vec_eqv_eps($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 var $21 = 0;
 if (!(+Math_abs(+(+HEAPF32[$0 >> 2] - +HEAPF32[$1 >> 2])) <= 1.1920928955078125e-07)) {
  $21 = 0;
  return $21 | 0;
 }
 if (!(+Math_abs(+(+HEAPF32[$0 + 4 >> 2] - +HEAPF32[$1 + 4 >> 2])) <= 1.1920928955078125e-07)) {
  $21 = 0;
  return $21 | 0;
 }
 $21 = +Math_abs(+(+HEAPF32[$0 + 8 >> 2] - +HEAPF32[$1 + 8 >> 2])) <= 1.1920928955078125e-07;
 return $21 | 0;
}

function _glmc_vec4_min($0) {
 $0 = $0 | 0;
 var $$0$i$i = 0.0, $$1$i$i = 0.0, $$idx$val$i = 0.0, $$idx6$val$i = 0.0, $$val$i = 0.0, $4 = 0.0;
 $$val$i = +HEAPF32[$0 >> 2];
 $$idx$val$i = +HEAPF32[$0 + 4 >> 2];
 $$idx6$val$i = +HEAPF32[$0 + 8 >> 2];
 $$0$i$i = $$idx$val$i < $$val$i ? $$idx$val$i : $$val$i;
 $$1$i$i = $$0$i$i > $$idx6$val$i ? $$idx6$val$i : $$0$i$i;
 $4 = +HEAPF32[$0 + 12 >> 2];
 return +($4 < $$1$i$i ? $4 : $$1$i$i);
}

function _glmc_vec4_max($0) {
 $0 = $0 | 0;
 var $$0$i$i = 0.0, $$1$i$i = 0.0, $$idx$val$i = 0.0, $$idx6$val$i = 0.0, $$val$i = 0.0, $4 = 0.0;
 $$val$i = +HEAPF32[$0 >> 2];
 $$idx$val$i = +HEAPF32[$0 + 4 >> 2];
 $$idx6$val$i = +HEAPF32[$0 + 8 >> 2];
 $$0$i$i = $$idx$val$i > $$val$i ? $$idx$val$i : $$val$i;
 $$1$i$i = $$0$i$i < $$idx6$val$i ? $$idx6$val$i : $$0$i$i;
 $4 = +HEAPF32[$0 + 12 >> 2];
 return +($4 > $$1$i$i ? $4 : $$1$i$i);
}

function _glmc_quat_inv($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 var $16 = 0.0, $2 = 0.0, $4 = 0.0, $6 = 0.0, $8 = 0.0;
 $2 = +HEAPF32[$0 >> 2];
 $4 = +HEAPF32[$0 + 4 >> 2];
 $6 = +HEAPF32[$0 + 8 >> 2];
 $8 = +HEAPF32[$0 + 12 >> 2];
 $16 = 1.0 / ($2 * $2 + $4 * $4 + $6 * $6 + $8 * $8);
 HEAPF32[$1 >> 2] = -($2 * $16);
 HEAPF32[$1 + 4 >> 2] = -($4 * $16);
 HEAPF32[$1 + 8 >> 2] = -($6 * $16);
 HEAPF32[$1 + 12 >> 2] = $8 * $16;
 return;
}

function _glmc_vec4_isnan($0) {
 $0 = $0 | 0;
 var $16 = 0;
 if ((HEAP32[$0 >> 2] & 2147483647) >>> 0 > 2139095040) {
  $16 = 1;
  return $16 | 0;
 }
 if ((HEAP32[$0 + 4 >> 2] & 2147483647) >>> 0 > 2139095040) {
  $16 = 1;
  return $16 | 0;
 }
 if ((HEAP32[$0 + 8 >> 2] & 2147483647) >>> 0 > 2139095040) {
  $16 = 1;
  return $16 | 0;
 }
 $16 = (HEAP32[$0 + 12 >> 2] & 2147483647) >>> 0 > 2139095040;
 return $16 | 0;
}

function _glmc_vec_subadd($0, $1, $2) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = $2 | 0;
 var $13 = 0, $21 = 0;
 HEAPF32[$2 >> 2] = +HEAPF32[$2 >> 2] + (+HEAPF32[$0 >> 2] - +HEAPF32[$1 >> 2]);
 $13 = $2 + 4 | 0;
 HEAPF32[$13 >> 2] = +HEAPF32[$13 >> 2] + (+HEAPF32[$0 + 4 >> 2] - +HEAPF32[$1 + 4 >> 2]);
 $21 = $2 + 8 | 0;
 HEAPF32[$21 >> 2] = +HEAPF32[$21 >> 2] + (+HEAPF32[$0 + 8 >> 2] - +HEAPF32[$1 + 8 >> 2]);
 return;
}

function _glmc_vec_addadd($0, $1, $2) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = $2 | 0;
 var $13 = 0, $21 = 0;
 HEAPF32[$2 >> 2] = +HEAPF32[$2 >> 2] + (+HEAPF32[$0 >> 2] + +HEAPF32[$1 >> 2]);
 $13 = $2 + 4 | 0;
 HEAPF32[$13 >> 2] = +HEAPF32[$13 >> 2] + (+HEAPF32[$0 + 4 >> 2] + +HEAPF32[$1 + 4 >> 2]);
 $21 = $2 + 8 | 0;
 HEAPF32[$21 >> 2] = +HEAPF32[$21 >> 2] + (+HEAPF32[$0 + 8 >> 2] + +HEAPF32[$1 + 8 >> 2]);
 return;
}

function _glmc_vec4_isinf($0) {
 $0 = $0 | 0;
 var $16 = 0;
 if ((HEAP32[$0 >> 2] & 2147483647 | 0) == 2139095040) {
  $16 = 1;
  return $16 | 0;
 }
 if ((HEAP32[$0 + 4 >> 2] & 2147483647 | 0) == 2139095040) {
  $16 = 1;
  return $16 | 0;
 }
 if ((HEAP32[$0 + 8 >> 2] & 2147483647 | 0) == 2139095040) {
  $16 = 1;
  return $16 | 0;
 }
 $16 = (HEAP32[$0 + 12 >> 2] & 2147483647 | 0) == 2139095040;
 return $16 | 0;
}

function _glmc_vec4_eqv($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 var $20 = 0;
 if (!(+HEAPF32[$0 >> 2] == +HEAPF32[$1 >> 2])) {
  $20 = 0;
  return $20 | 0;
 }
 if (!(+HEAPF32[$0 + 4 >> 2] == +HEAPF32[$1 + 4 >> 2])) {
  $20 = 0;
  return $20 | 0;
 }
 if (!(+HEAPF32[$0 + 8 >> 2] == +HEAPF32[$1 + 8 >> 2])) {
  $20 = 0;
  return $20 | 0;
 }
 $20 = +HEAPF32[$0 + 12 >> 2] == +HEAPF32[$1 + 12 >> 2];
 return $20 | 0;
}

function _glmc_vec_muladd($0, $1, $2) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = $2 | 0;
 var $13 = 0, $21 = 0;
 HEAPF32[$2 >> 2] = +HEAPF32[$2 >> 2] + +HEAPF32[$0 >> 2] * +HEAPF32[$1 >> 2];
 $13 = $2 + 4 | 0;
 HEAPF32[$13 >> 2] = +HEAPF32[$13 >> 2] + +HEAPF32[$0 + 4 >> 2] * +HEAPF32[$1 + 4 >> 2];
 $21 = $2 + 8 | 0;
 HEAPF32[$21 >> 2] = +HEAPF32[$21 >> 2] + +HEAPF32[$0 + 8 >> 2] * +HEAPF32[$1 + 8 >> 2];
 return;
}

function _glmc_aabb_invalidate($0) {
 $0 = $0 | 0;
 HEAPF32[$0 + 8 >> 2] = 3402823466385288598117041.0e14;
 HEAPF32[$0 + 4 >> 2] = 3402823466385288598117041.0e14;
 HEAPF32[$0 >> 2] = 3402823466385288598117041.0e14;
 HEAPF32[$0 + 20 >> 2] = -3402823466385288598117041.0e14;
 HEAPF32[$0 + 16 >> 2] = -3402823466385288598117041.0e14;
 HEAPF32[$0 + 12 >> 2] = -3402823466385288598117041.0e14;
 return;
}

function _glmc_vec4_distance($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 var $10 = 0.0, $17 = 0.0, $24 = 0.0, $4 = 0.0;
 $4 = +HEAPF32[$1 >> 2] - +HEAPF32[$0 >> 2];
 $10 = +HEAPF32[$1 + 4 >> 2] - +HEAPF32[$0 + 4 >> 2];
 $17 = +HEAPF32[$1 + 8 >> 2] - +HEAPF32[$0 + 8 >> 2];
 $24 = +HEAPF32[$1 + 12 >> 2] - +HEAPF32[$0 + 12 >> 2];
 return +(+Math_sqrt(+($4 * $4 + $10 * $10 + $17 * $17 + $24 * $24)));
}

function _glmc_quat_angle($0) {
 $0 = $0 | 0;
 var $$idx2$val$i = 0.0, $$idx3$val$i = 0.0, $$val$i = 0.0, $6 = 0.0;
 $$val$i = +HEAPF32[$0 >> 2];
 $$idx2$val$i = +HEAPF32[$0 + 4 >> 2];
 $$idx3$val$i = +HEAPF32[$0 + 8 >> 2];
 $6 = +Math_sqrt(+($$val$i * $$val$i + $$idx2$val$i * $$idx2$val$i + $$idx3$val$i * $$idx3$val$i));
 return +(+Math_atan2(+$6, +(+HEAPF32[$0 + 12 >> 2])) * 2.0);
}

function _glmc_vec_sign($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 var $10 = 0.0, $19 = 0.0, $2 = 0.0;
 $2 = +HEAPF32[$0 >> 2];
 HEAPF32[$1 >> 2] = +(($2 > 0.0 & 1) - ($2 < 0.0 & 1) | 0);
 $10 = +HEAPF32[$0 + 4 >> 2];
 HEAPF32[$1 + 4 >> 2] = +(($10 > 0.0 & 1) - ($10 < 0.0 & 1) | 0);
 $19 = +HEAPF32[$0 + 8 >> 2];
 HEAPF32[$1 + 8 >> 2] = +(($19 > 0.0 & 1) - ($19 < 0.0 & 1) | 0);
 return;
}

function _glmc_vec_eq_eps($0, $1) {
 $0 = $0 | 0;
 $1 = +$1;
 var $16 = 0;
 if (!(+Math_abs(+(+HEAPF32[$0 >> 2] - $1)) <= 1.1920928955078125e-07)) {
  $16 = 0;
  return $16 | 0;
 }
 if (!(+Math_abs(+(+HEAPF32[$0 + 4 >> 2] - $1)) <= 1.1920928955078125e-07)) {
  $16 = 0;
  return $16 | 0;
 }
 $16 = +Math_abs(+(+HEAPF32[$0 + 8 >> 2] - $1)) <= 1.1920928955078125e-07;
 return $16 | 0;
}

function _glmc_vec4_mulv($0, $1, $2) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = $2 | 0;
 HEAPF32[$2 >> 2] = +HEAPF32[$0 >> 2] * +HEAPF32[$1 >> 2];
 HEAPF32[$2 + 4 >> 2] = +HEAPF32[$0 + 4 >> 2] * +HEAPF32[$1 + 4 >> 2];
 HEAPF32[$2 + 8 >> 2] = +HEAPF32[$0 + 8 >> 2] * +HEAPF32[$1 + 8 >> 2];
 HEAPF32[$2 + 12 >> 2] = +HEAPF32[$0 + 12 >> 2] * +HEAPF32[$1 + 12 >> 2];
 return;
}

function _glmc_vec4_sub($0, $1, $2) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = $2 | 0;
 HEAPF32[$2 >> 2] = +HEAPF32[$0 >> 2] - +HEAPF32[$1 >> 2];
 HEAPF32[$2 + 4 >> 2] = +HEAPF32[$0 + 4 >> 2] - +HEAPF32[$1 + 4 >> 2];
 HEAPF32[$2 + 8 >> 2] = +HEAPF32[$0 + 8 >> 2] - +HEAPF32[$1 + 8 >> 2];
 HEAPF32[$2 + 12 >> 2] = +HEAPF32[$0 + 12 >> 2] - +HEAPF32[$1 + 12 >> 2];
 return;
}

function _glmc_vec4_mul($0, $1, $2) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = $2 | 0;
 HEAPF32[$2 >> 2] = +HEAPF32[$0 >> 2] * +HEAPF32[$1 >> 2];
 HEAPF32[$2 + 4 >> 2] = +HEAPF32[$0 + 4 >> 2] * +HEAPF32[$1 + 4 >> 2];
 HEAPF32[$2 + 8 >> 2] = +HEAPF32[$0 + 8 >> 2] * +HEAPF32[$1 + 8 >> 2];
 HEAPF32[$2 + 12 >> 2] = +HEAPF32[$0 + 12 >> 2] * +HEAPF32[$1 + 12 >> 2];
 return;
}

function _glmc_vec4_div($0, $1, $2) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = $2 | 0;
 HEAPF32[$2 >> 2] = +HEAPF32[$0 >> 2] / +HEAPF32[$1 >> 2];
 HEAPF32[$2 + 4 >> 2] = +HEAPF32[$0 + 4 >> 2] / +HEAPF32[$1 + 4 >> 2];
 HEAPF32[$2 + 8 >> 2] = +HEAPF32[$0 + 8 >> 2] / +HEAPF32[$1 + 8 >> 2];
 HEAPF32[$2 + 12 >> 2] = +HEAPF32[$0 + 12 >> 2] / +HEAPF32[$1 + 12 >> 2];
 return;
}

function _glmc_vec4_add($0, $1, $2) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = $2 | 0;
 HEAPF32[$2 >> 2] = +HEAPF32[$0 >> 2] + +HEAPF32[$1 >> 2];
 HEAPF32[$2 + 4 >> 2] = +HEAPF32[$0 + 4 >> 2] + +HEAPF32[$1 + 4 >> 2];
 HEAPF32[$2 + 8 >> 2] = +HEAPF32[$0 + 8 >> 2] + +HEAPF32[$1 + 8 >> 2];
 HEAPF32[$2 + 12 >> 2] = +HEAPF32[$0 + 12 >> 2] + +HEAPF32[$1 + 12 >> 2];
 return;
}

function _glmc_quat_sub($0, $1, $2) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = $2 | 0;
 HEAPF32[$2 >> 2] = +HEAPF32[$0 >> 2] - +HEAPF32[$1 >> 2];
 HEAPF32[$2 + 4 >> 2] = +HEAPF32[$0 + 4 >> 2] - +HEAPF32[$1 + 4 >> 2];
 HEAPF32[$2 + 8 >> 2] = +HEAPF32[$0 + 8 >> 2] - +HEAPF32[$1 + 8 >> 2];
 HEAPF32[$2 + 12 >> 2] = +HEAPF32[$0 + 12 >> 2] - +HEAPF32[$1 + 12 >> 2];
 return;
}

function _glmc_quat_add($0, $1, $2) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = $2 | 0;
 HEAPF32[$2 >> 2] = +HEAPF32[$0 >> 2] + +HEAPF32[$1 >> 2];
 HEAPF32[$2 + 4 >> 2] = +HEAPF32[$0 + 4 >> 2] + +HEAPF32[$1 + 4 >> 2];
 HEAPF32[$2 + 8 >> 2] = +HEAPF32[$0 + 8 >> 2] + +HEAPF32[$1 + 8 >> 2];
 HEAPF32[$2 + 12 >> 2] = +HEAPF32[$0 + 12 >> 2] + +HEAPF32[$1 + 12 >> 2];
 return;
}

function _glmc_vec_muladds($0, $1, $2) {
 $0 = $0 | 0;
 $1 = +$1;
 $2 = $2 | 0;
 var $10 = 0, $16 = 0;
 HEAPF32[$2 >> 2] = +HEAPF32[$2 >> 2] + +HEAPF32[$0 >> 2] * $1;
 $10 = $2 + 4 | 0;
 HEAPF32[$10 >> 2] = +HEAPF32[$10 >> 2] + +HEAPF32[$0 + 4 >> 2] * $1;
 $16 = $2 + 8 | 0;
 HEAPF32[$16 >> 2] = +HEAPF32[$16 >> 2] + +HEAPF32[$0 + 8 >> 2] * $1;
 return;
}

function _glmc_persp_decomp_y($0, $1, $2) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = $2 | 0;
 var $12 = 0.0, $4 = 0.0, $6 = 0.0;
 $4 = +HEAPF32[$0 + 36 >> 2];
 $6 = +HEAPF32[$0 + 20 >> 2];
 $12 = +HEAPF32[$0 + 56 >> 2] / (+HEAPF32[$0 + 60 >> 2] + -1.0);
 HEAPF32[$2 >> 2] = ($4 + -1.0) * $12 / $6;
 HEAPF32[$1 >> 2] = ($4 + 1.0) * $12 / $6;
 return;
}

function _glmc_persp_decomp_x($0, $1, $2) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = $2 | 0;
 var $11 = 0.0, $3 = 0.0, $5 = 0.0;
 $3 = +HEAPF32[$0 >> 2];
 $5 = +HEAPF32[$0 + 32 >> 2];
 $11 = +HEAPF32[$0 + 56 >> 2] / (+HEAPF32[$0 + 60 >> 2] + -1.0);
 HEAPF32[$1 >> 2] = ($5 + -1.0) * $11 / $3;
 HEAPF32[$2 >> 2] = ($5 + 1.0) * $11 / $3;
 return;
}

function _glmc_vec_isnan($0) {
 $0 = $0 | 0;
 var $12 = 0;
 if ((HEAP32[$0 >> 2] & 2147483647) >>> 0 > 2139095040) {
  $12 = 1;
  return $12 | 0;
 }
 if ((HEAP32[$0 + 4 >> 2] & 2147483647) >>> 0 > 2139095040) {
  $12 = 1;
  return $12 | 0;
 }
 $12 = (HEAP32[$0 + 8 >> 2] & 2147483647) >>> 0 > 2139095040;
 return $12 | 0;
}

function _glmc_mat3_identity($0) {
 $0 = $0 | 0;
 HEAP32[$0 >> 2] = 1065353216;
 HEAP32[$0 + 4 >> 2] = 0;
 HEAP32[$0 + 8 >> 2] = 0;
 HEAP32[$0 + 12 >> 2] = 0;
 HEAP32[$0 + 16 >> 2] = 1065353216;
 HEAP32[$0 + 20 >> 2] = 0;
 HEAP32[$0 + 24 >> 2] = 0;
 HEAP32[$0 + 28 >> 2] = 0;
 HEAP32[$0 + 32 >> 2] = 1065353216;
 return;
}

function _glmc_vec4_divs($0, $1, $2) {
 $0 = $0 | 0;
 $1 = +$1;
 $2 = $2 | 0;
 var $3 = 0.0;
 $3 = 1.0 / $1;
 HEAPF32[$2 >> 2] = $3 * +HEAPF32[$0 >> 2];
 HEAPF32[$2 + 4 >> 2] = $3 * +HEAPF32[$0 + 4 >> 2];
 HEAPF32[$2 + 8 >> 2] = $3 * +HEAPF32[$0 + 8 >> 2];
 HEAPF32[$2 + 12 >> 2] = $3 * +HEAPF32[$0 + 12 >> 2];
 return;
}

function _glmc_vec_isinf($0) {
 $0 = $0 | 0;
 var $12 = 0;
 if ((HEAP32[$0 >> 2] & 2147483647 | 0) == 2139095040) {
  $12 = 1;
  return $12 | 0;
 }
 if ((HEAP32[$0 + 4 >> 2] & 2147483647 | 0) == 2139095040) {
  $12 = 1;
  return $12 | 0;
 }
 $12 = (HEAP32[$0 + 8 >> 2] & 2147483647 | 0) == 2139095040;
 return $12 | 0;
}

function _glmc_vec_eqv($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 var $15 = 0;
 if (!(+HEAPF32[$0 >> 2] == +HEAPF32[$1 >> 2])) {
  $15 = 0;
  return $15 | 0;
 }
 if (!(+HEAPF32[$0 + 4 >> 2] == +HEAPF32[$1 + 4 >> 2])) {
  $15 = 0;
  return $15 | 0;
 }
 $15 = +HEAPF32[$0 + 8 >> 2] == +HEAPF32[$1 + 8 >> 2];
 return $15 | 0;
}

function _glmc_persp_decomp_z($0, $1, $2) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = $2 | 0;
 var $$idx$val = 0.0, $$idx3$val = 0.0;
 $$idx$val = +HEAPF32[$0 + 40 >> 2];
 $$idx3$val = +HEAPF32[$0 + 56 >> 2];
 HEAPF32[$1 >> 2] = $$idx3$val / ($$idx$val + -1.0);
 HEAPF32[$2 >> 2] = $$idx3$val / ($$idx$val + 1.0);
 return;
}

function _glmc_vec_ortho($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 var $2 = 0, $4 = 0;
 $2 = $0 + 4 | 0;
 $4 = $0 + 8 | 0;
 HEAPF32[$1 >> 2] = +HEAPF32[$2 >> 2] - +HEAPF32[$4 >> 2];
 HEAPF32[$1 + 4 >> 2] = +HEAPF32[$4 >> 2] - +HEAPF32[$0 >> 2];
 HEAPF32[$1 + 8 >> 2] = +HEAPF32[$0 >> 2] - +HEAPF32[$2 >> 2];
 return;
}

function _glmc_vec4_sqrt($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 HEAPF32[$1 >> 2] = +Math_sqrt(+(+HEAPF32[$0 >> 2]));
 HEAPF32[$1 + 4 >> 2] = +Math_sqrt(+(+HEAPF32[$0 + 4 >> 2]));
 HEAPF32[$1 + 8 >> 2] = +Math_sqrt(+(+HEAPF32[$0 + 8 >> 2]));
 HEAPF32[$1 + 12 >> 2] = +Math_sqrt(+(+HEAPF32[$0 + 12 >> 2]));
 return;
}

function _glmc_vec_min($0) {
 $0 = $0 | 0;
 var $$0$i = 0.0, $$idx$val = 0.0, $$idx1$val = 0.0, $$val = 0.0;
 $$val = +HEAPF32[$0 >> 2];
 $$idx$val = +HEAPF32[$0 + 4 >> 2];
 $$idx1$val = +HEAPF32[$0 + 8 >> 2];
 $$0$i = $$idx$val < $$val ? $$idx$val : $$val;
 return +($$0$i > $$idx1$val ? $$idx1$val : $$0$i);
}

function _glmc_vec_max($0) {
 $0 = $0 | 0;
 var $$0$i = 0.0, $$idx$val = 0.0, $$idx1$val = 0.0, $$val = 0.0;
 $$val = +HEAPF32[$0 >> 2];
 $$idx$val = +HEAPF32[$0 + 4 >> 2];
 $$idx1$val = +HEAPF32[$0 + 8 >> 2];
 $$0$i = $$idx$val > $$val ? $$idx$val : $$val;
 return +($$0$i < $$idx1$val ? $$idx1$val : $$0$i);
}

function _glmc_vec_distance($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 var $2 = 0.0, $4 = 0.0, $7 = 0.0;
 $2 = +HEAPF32[$1 >> 2] - +HEAPF32[$0 >> 2];
 $4 = +HEAPF32[$1 + 4 >> 2] - +HEAPF32[$0 + 4 >> 2];
 $7 = +HEAPF32[$1 + 8 >> 2] - +HEAPF32[$0 + 8 >> 2];
 return +(+Math_sqrt(+($2 * $2 + $4 * $4 + $7 * $7)));
}

function _glmc_vec4_flipsign($0) {
 $0 = $0 | 0;
 var $3 = 0, $6 = 0, $9 = 0;
 HEAPF32[$0 >> 2] = -+HEAPF32[$0 >> 2];
 $3 = $0 + 4 | 0;
 HEAPF32[$3 >> 2] = -+HEAPF32[$3 >> 2];
 $6 = $0 + 8 | 0;
 HEAPF32[$6 >> 2] = -+HEAPF32[$6 >> 2];
 $9 = $0 + 12 | 0;
 HEAPF32[$9 >> 2] = -+HEAPF32[$9 >> 2];
 return;
}

function _glmc_aabb_radius($0) {
 $0 = $0 | 0;
 var $2 = 0.0, $4 = 0.0, $7 = 0.0;
 $2 = +HEAPF32[$0 + 12 >> 2] - +HEAPF32[$0 >> 2];
 $4 = +HEAPF32[$0 + 16 >> 2] - +HEAPF32[$0 + 4 >> 2];
 $7 = +HEAPF32[$0 + 20 >> 2] - +HEAPF32[$0 + 8 >> 2];
 return +(+Math_sqrt(+($2 * $2 + $4 * $4 + $7 * $7)) * .5);
}

function _glmc_vec4_inv($0) {
 $0 = $0 | 0;
 var $3 = 0, $6 = 0, $9 = 0;
 HEAPF32[$0 >> 2] = -+HEAPF32[$0 >> 2];
 $3 = $0 + 4 | 0;
 HEAPF32[$3 >> 2] = -+HEAPF32[$3 >> 2];
 $6 = $0 + 8 | 0;
 HEAPF32[$6 >> 2] = -+HEAPF32[$6 >> 2];
 $9 = $0 + 12 | 0;
 HEAPF32[$9 >> 2] = -+HEAPF32[$9 >> 2];
 return;
}

function _glmc_quat_imaglen($0) {
 $0 = $0 | 0;
 var $$idx$val = 0.0, $$idx1$val = 0.0, $$val = 0.0;
 $$val = +HEAPF32[$0 >> 2];
 $$idx$val = +HEAPF32[$0 + 4 >> 2];
 $$idx1$val = +HEAPF32[$0 + 8 >> 2];
 return +(+Math_sqrt(+($$val * $$val + $$idx$val * $$idx$val + $$idx1$val * $$idx1$val)));
}

function _glmc_aabb_size($0) {
 $0 = $0 | 0;
 var $2 = 0.0, $4 = 0.0, $7 = 0.0;
 $2 = +HEAPF32[$0 + 12 >> 2] - +HEAPF32[$0 >> 2];
 $4 = +HEAPF32[$0 + 16 >> 2] - +HEAPF32[$0 + 4 >> 2];
 $7 = +HEAPF32[$0 + 20 >> 2] - +HEAPF32[$0 + 8 >> 2];
 return +(+Math_sqrt(+($2 * $2 + $4 * $4 + $7 * $7)));
}

function _glmc_vec4_eq_all($0) {
 $0 = $0 | 0;
 var $1 = 0.0, $11 = 0;
 $1 = +HEAPF32[$0 >> 2];
 if (!($1 == +HEAPF32[$0 + 4 >> 2])) {
  $11 = 0;
  return $11 | 0;
 }
 if (!($1 == +HEAPF32[$0 + 8 >> 2])) {
  $11 = 0;
  return $11 | 0;
 }
 $11 = $1 == +HEAPF32[$0 + 12 >> 2];
 return $11 | 0;
}

function _glmc_vec_mulv($0, $1, $2) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = $2 | 0;
 HEAPF32[$2 >> 2] = +HEAPF32[$0 >> 2] * +HEAPF32[$1 >> 2];
 HEAPF32[$2 + 4 >> 2] = +HEAPF32[$0 + 4 >> 2] * +HEAPF32[$1 + 4 >> 2];
 HEAPF32[$2 + 8 >> 2] = +HEAPF32[$0 + 8 >> 2] * +HEAPF32[$1 + 8 >> 2];
 return;
}

function _glmc_vec4_scale($0, $1, $2) {
 $0 = $0 | 0;
 $1 = +$1;
 $2 = $2 | 0;
 HEAPF32[$2 >> 2] = +HEAPF32[$0 >> 2] * $1;
 HEAPF32[$2 + 4 >> 2] = +HEAPF32[$0 + 4 >> 2] * $1;
 HEAPF32[$2 + 8 >> 2] = +HEAPF32[$0 + 8 >> 2] * $1;
 HEAPF32[$2 + 12 >> 2] = +HEAPF32[$0 + 12 >> 2] * $1;
 return;
}

function _glmc_vec_sub($0, $1, $2) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = $2 | 0;
 HEAPF32[$2 >> 2] = +HEAPF32[$0 >> 2] - +HEAPF32[$1 >> 2];
 HEAPF32[$2 + 4 >> 2] = +HEAPF32[$0 + 4 >> 2] - +HEAPF32[$1 + 4 >> 2];
 HEAPF32[$2 + 8 >> 2] = +HEAPF32[$0 + 8 >> 2] - +HEAPF32[$1 + 8 >> 2];
 return;
}

function _glmc_vec_norm($0) {
 $0 = $0 | 0;
 var $$idx$val = 0.0, $$idx1$val = 0.0, $$val = 0.0;
 $$val = +HEAPF32[$0 >> 2];
 $$idx$val = +HEAPF32[$0 + 4 >> 2];
 $$idx1$val = +HEAPF32[$0 + 8 >> 2];
 return +(+Math_sqrt(+($$val * $$val + $$idx$val * $$idx$val + $$idx1$val * $$idx1$val)));
}

function _glmc_vec_mul($0, $1, $2) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = $2 | 0;
 HEAPF32[$2 >> 2] = +HEAPF32[$0 >> 2] * +HEAPF32[$1 >> 2];
 HEAPF32[$2 + 4 >> 2] = +HEAPF32[$0 + 4 >> 2] * +HEAPF32[$1 + 4 >> 2];
 HEAPF32[$2 + 8 >> 2] = +HEAPF32[$0 + 8 >> 2] * +HEAPF32[$1 + 8 >> 2];
 return;
}

function _glmc_vec_div($0, $1, $2) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = $2 | 0;
 HEAPF32[$2 >> 2] = +HEAPF32[$0 >> 2] / +HEAPF32[$1 >> 2];
 HEAPF32[$2 + 4 >> 2] = +HEAPF32[$0 + 4 >> 2] / +HEAPF32[$1 + 4 >> 2];
 HEAPF32[$2 + 8 >> 2] = +HEAPF32[$0 + 8 >> 2] / +HEAPF32[$1 + 8 >> 2];
 return;
}

function _glmc_vec_add($0, $1, $2) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 $2 = $2 | 0;
 HEAPF32[$2 >> 2] = +HEAPF32[$0 >> 2] + +HEAPF32[$1 >> 2];
 HEAPF32[$2 + 4 >> 2] = +HEAPF32[$0 + 4 >> 2] + +HEAPF32[$1 + 4 >> 2];
 HEAPF32[$2 + 8 >> 2] = +HEAPF32[$0 + 8 >> 2] + +HEAPF32[$1 + 8 >> 2];
 return;
}

function _glmc_vec4_subs($0, $1, $2) {
 $0 = $0 | 0;
 $1 = +$1;
 $2 = $2 | 0;
 HEAPF32[$2 >> 2] = +HEAPF32[$0 >> 2] - $1;
 HEAPF32[$2 + 4 >> 2] = +HEAPF32[$0 + 4 >> 2] - $1;
 HEAPF32[$2 + 8 >> 2] = +HEAPF32[$0 + 8 >> 2] - $1;
 HEAPF32[$2 + 12 >> 2] = +HEAPF32[$0 + 12 >> 2] - $1;
 return;
}

function _glmc_vec4_adds($0, $1, $2) {
 $0 = $0 | 0;
 $1 = +$1;
 $2 = $2 | 0;
 HEAPF32[$2 >> 2] = +HEAPF32[$0 >> 2] + $1;
 HEAPF32[$2 + 4 >> 2] = +HEAPF32[$0 + 4 >> 2] + $1;
 HEAPF32[$2 + 8 >> 2] = +HEAPF32[$0 + 8 >> 2] + $1;
 HEAPF32[$2 + 12 >> 2] = +HEAPF32[$0 + 12 >> 2] + $1;
 return;
}

function _glmc_vec4_eq($0, $1) {
 $0 = $0 | 0;
 $1 = +$1;
 var $13 = 0, $2 = 0.0;
 $2 = +HEAPF32[$0 >> 2];
 if ($2 == $1) if ($2 == +HEAPF32[$0 + 4 >> 2]) if ($2 == +HEAPF32[$0 + 8 >> 2]) $13 = $2 == +HEAPF32[$0 + 12 >> 2]; else $13 = 0; else $13 = 0; else $13 = 0;
 return $13 | 0;
}

function _glmc_vec_eq($0, $1) {
 $0 = $0 | 0;
 $1 = +$1;
 var $10 = 0, $2 = 0.0;
 $2 = +HEAPF32[$0 >> 2];
 if (!($2 == $1)) {
  $10 = 0;
  return $10 | 0;
 }
 if (!($2 == +HEAPF32[$0 + 4 >> 2])) {
  $10 = 0;
  return $10 | 0;
 }
 $10 = $2 == +HEAPF32[$0 + 8 >> 2];
 return $10 | 0;
}

function _glmc_vec_norm2($0) {
 $0 = $0 | 0;
 var $$idx$val = 0.0, $$idx1$val = 0.0, $$val = 0.0;
 $$val = +HEAPF32[$0 >> 2];
 $$idx$val = +HEAPF32[$0 + 4 >> 2];
 $$idx1$val = +HEAPF32[$0 + 8 >> 2];
 return +($$val * $$val + $$idx$val * $$idx$val + $$idx1$val * $$idx1$val);
}

function _glmc_vec4_norm($0) {
 $0 = $0 | 0;
 var $1 = 0.0, $12 = 0.0, $4 = 0.0, $8 = 0.0;
 $1 = +HEAPF32[$0 >> 2];
 $4 = +HEAPF32[$0 + 4 >> 2];
 $8 = +HEAPF32[$0 + 8 >> 2];
 $12 = +HEAPF32[$0 + 12 >> 2];
 return +(+Math_sqrt(+($1 * $1 + $4 * $4 + $8 * $8 + $12 * $12)));
}

function _glmc_quat_norm($0) {
 $0 = $0 | 0;
 var $1 = 0.0, $12 = 0.0, $4 = 0.0, $8 = 0.0;
 $1 = +HEAPF32[$0 >> 2];
 $4 = +HEAPF32[$0 + 4 >> 2];
 $8 = +HEAPF32[$0 + 8 >> 2];
 $12 = +HEAPF32[$0 + 12 >> 2];
 return +(+Math_sqrt(+($1 * $1 + $4 * $4 + $8 * $8 + $12 * $12)));
}

function _glmc_vec4_flipsign_to($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 HEAPF32[$1 >> 2] = -+HEAPF32[$0 >> 2];
 HEAPF32[$1 + 4 >> 2] = -+HEAPF32[$0 + 4 >> 2];
 HEAPF32[$1 + 8 >> 2] = -+HEAPF32[$0 + 8 >> 2];
 HEAPF32[$1 + 12 >> 2] = -+HEAPF32[$0 + 12 >> 2];
 return;
}

function _glmc_quat_conjugate($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 HEAPF32[$1 >> 2] = -+HEAPF32[$0 >> 2];
 HEAPF32[$1 + 4 >> 2] = -+HEAPF32[$0 + 4 >> 2];
 HEAPF32[$1 + 8 >> 2] = -+HEAPF32[$0 + 8 >> 2];
 HEAP32[$1 + 12 >> 2] = HEAP32[$0 + 12 >> 2];
 return;
}

function _glmc_vec4_norm2($0) {
 $0 = $0 | 0;
 var $1 = 0.0, $12 = 0.0, $4 = 0.0, $8 = 0.0;
 $1 = +HEAPF32[$0 >> 2];
 $4 = +HEAPF32[$0 + 4 >> 2];
 $8 = +HEAPF32[$0 + 8 >> 2];
 $12 = +HEAPF32[$0 + 12 >> 2];
 return +($1 * $1 + $4 * $4 + $8 * $8 + $12 * $12);
}

function _glmc_vec4_dot($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 return +(+HEAPF32[$0 >> 2] * +HEAPF32[$1 >> 2] + +HEAPF32[$0 + 4 >> 2] * +HEAPF32[$1 + 4 >> 2] + +HEAPF32[$0 + 8 >> 2] * +HEAPF32[$1 + 8 >> 2] + +HEAPF32[$0 + 12 >> 2] * +HEAPF32[$1 + 12 >> 2]);
}

function _glmc_quat_dot($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 return +(+HEAPF32[$0 >> 2] * +HEAPF32[$1 >> 2] + +HEAPF32[$0 + 4 >> 2] * +HEAPF32[$1 + 4 >> 2] + +HEAPF32[$0 + 8 >> 2] * +HEAPF32[$1 + 8 >> 2] + +HEAPF32[$0 + 12 >> 2] * +HEAPF32[$1 + 12 >> 2]);
}

function _glmc_vec_sqrt($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 HEAPF32[$1 >> 2] = +Math_sqrt(+(+HEAPF32[$0 >> 2]));
 HEAPF32[$1 + 4 >> 2] = +Math_sqrt(+(+HEAPF32[$0 + 4 >> 2]));
 HEAPF32[$1 + 8 >> 2] = +Math_sqrt(+(+HEAPF32[$0 + 8 >> 2]));
 return;
}

function _glmc_vec4_copy($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 HEAP32[$1 >> 2] = HEAP32[$0 >> 2];
 HEAP32[$1 + 4 >> 2] = HEAP32[$0 + 4 >> 2];
 HEAP32[$1 + 8 >> 2] = HEAP32[$0 + 8 >> 2];
 HEAP32[$1 + 12 >> 2] = HEAP32[$0 + 12 >> 2];
 return;
}

function _glmc_quat_copy($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 HEAP32[$1 >> 2] = HEAP32[$0 >> 2];
 HEAP32[$1 + 4 >> 2] = HEAP32[$0 + 4 >> 2];
 HEAP32[$1 + 8 >> 2] = HEAP32[$0 + 8 >> 2];
 HEAP32[$1 + 12 >> 2] = HEAP32[$0 + 12 >> 2];
 return;
}

function _glmc_vec_scale($0, $1, $2) {
 $0 = $0 | 0;
 $1 = +$1;
 $2 = $2 | 0;
 HEAPF32[$2 >> 2] = +HEAPF32[$0 >> 2] * $1;
 HEAPF32[$2 + 4 >> 2] = +HEAPF32[$0 + 4 >> 2] * $1;
 HEAPF32[$2 + 8 >> 2] = +HEAPF32[$0 + 8 >> 2] * $1;
 return;
}

function _glmc_vec4($0, $1, $2) {
 $0 = $0 | 0;
 $1 = +$1;
 $2 = $2 | 0;
 HEAP32[$2 >> 2] = HEAP32[$0 >> 2];
 HEAP32[$2 + 4 >> 2] = HEAP32[$0 + 4 >> 2];
 HEAP32[$2 + 8 >> 2] = HEAP32[$0 + 8 >> 2];
 HEAPF32[$2 + 12 >> 2] = $1;
 return;
}

function _glmc_vec_subs($0, $1, $2) {
 $0 = $0 | 0;
 $1 = +$1;
 $2 = $2 | 0;
 HEAPF32[$2 >> 2] = +HEAPF32[$0 >> 2] - $1;
 HEAPF32[$2 + 4 >> 2] = +HEAPF32[$0 + 4 >> 2] - $1;
 HEAPF32[$2 + 8 >> 2] = +HEAPF32[$0 + 8 >> 2] - $1;
 return;
}

function _glmc_vec_flipsign($0) {
 $0 = $0 | 0;
 var $3 = 0, $6 = 0;
 HEAPF32[$0 >> 2] = -+HEAPF32[$0 >> 2];
 $3 = $0 + 4 | 0;
 HEAPF32[$3 >> 2] = -+HEAPF32[$3 >> 2];
 $6 = $0 + 8 | 0;
 HEAPF32[$6 >> 2] = -+HEAPF32[$6 >> 2];
 return;
}

function _glmc_vec_divs($0, $1, $2) {
 $0 = $0 | 0;
 $1 = +$1;
 $2 = $2 | 0;
 HEAPF32[$2 >> 2] = +HEAPF32[$0 >> 2] / $1;
 HEAPF32[$2 + 4 >> 2] = +HEAPF32[$0 + 4 >> 2] / $1;
 HEAPF32[$2 + 8 >> 2] = +HEAPF32[$0 + 8 >> 2] / $1;
 return;
}

function _glmc_vec_adds($0, $1, $2) {
 $0 = $0 | 0;
 $1 = +$1;
 $2 = $2 | 0;
 HEAPF32[$2 >> 2] = +HEAPF32[$0 >> 2] + $1;
 HEAPF32[$2 + 4 >> 2] = +HEAPF32[$0 + 4 >> 2] + $1;
 HEAPF32[$2 + 8 >> 2] = +HEAPF32[$0 + 8 >> 2] + $1;
 return;
}

function _glmc_vec_inv($0) {
 $0 = $0 | 0;
 var $3 = 0, $6 = 0;
 HEAPF32[$0 >> 2] = -+HEAPF32[$0 >> 2];
 $3 = $0 + 4 | 0;
 HEAPF32[$3 >> 2] = -+HEAPF32[$3 >> 2];
 $6 = $0 + 8 | 0;
 HEAPF32[$6 >> 2] = -+HEAPF32[$6 >> 2];
 return;
}

function _glmc_quat_init($0, $1, $2, $3, $4) {
 $0 = $0 | 0;
 $1 = +$1;
 $2 = +$2;
 $3 = +$3;
 $4 = +$4;
 HEAPF32[$0 >> 2] = $1;
 HEAPF32[$0 + 4 >> 2] = $2;
 HEAPF32[$0 + 8 >> 2] = $3;
 HEAPF32[$0 + 12 >> 2] = $4;
 return;
}

function _glmc_vec_flipsign_to($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 HEAPF32[$1 >> 2] = -+HEAPF32[$0 >> 2];
 HEAPF32[$1 + 4 >> 2] = -+HEAPF32[$0 + 4 >> 2];
 HEAPF32[$1 + 8 >> 2] = -+HEAPF32[$0 + 8 >> 2];
 return;
}

function _glmc_vec_eq_all($0) {
 $0 = $0 | 0;
 var $1 = 0.0, $8 = 0;
 $1 = +HEAPF32[$0 >> 2];
 if (!($1 == +HEAPF32[$0 + 4 >> 2])) {
  $8 = 0;
  return $8 | 0;
 }
 $8 = $1 == +HEAPF32[$0 + 8 >> 2];
 return $8 | 0;
}

function _glmc_vec_inv_to($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 HEAPF32[$1 >> 2] = -+HEAPF32[$0 >> 2];
 HEAPF32[$1 + 4 >> 2] = -+HEAPF32[$0 + 4 >> 2];
 HEAPF32[$1 + 8 >> 2] = -+HEAPF32[$0 + 8 >> 2];
 return;
}

function _glmc_vec_dot($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 return +(+HEAPF32[$0 >> 2] * +HEAPF32[$1 >> 2] + +HEAPF32[$0 + 4 >> 2] * +HEAPF32[$1 + 4 >> 2] + +HEAPF32[$0 + 8 >> 2] * +HEAPF32[$1 + 8 >> 2]);
}

function _glmc_vec4_copy3($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 HEAP32[$1 >> 2] = HEAP32[$0 >> 2];
 HEAP32[$1 + 4 >> 2] = HEAP32[$0 + 4 >> 2];
 HEAP32[$1 + 8 >> 2] = HEAP32[$0 + 8 >> 2];
 return;
}

function _glmc_quat_imag($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 HEAP32[$1 >> 2] = HEAP32[$0 >> 2];
 HEAP32[$1 + 4 >> 2] = HEAP32[$0 + 4 >> 2];
 HEAP32[$1 + 8 >> 2] = HEAP32[$0 + 8 >> 2];
 return;
}

function _glmc_vec_copy($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 HEAP32[$1 >> 2] = HEAP32[$0 >> 2];
 HEAP32[$1 + 4 >> 2] = HEAP32[$0 + 4 >> 2];
 HEAP32[$1 + 8 >> 2] = HEAP32[$0 + 8 >> 2];
 return;
}

function _glmc_vec3($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 HEAP32[$1 >> 2] = HEAP32[$0 >> 2];
 HEAP32[$1 + 4 >> 2] = HEAP32[$0 + 4 >> 2];
 HEAP32[$1 + 8 >> 2] = HEAP32[$0 + 8 >> 2];
 return;
}

function _glmc_vec4_broadcast($0, $1) {
 $0 = +$0;
 $1 = $1 | 0;
 HEAPF32[$1 + 12 >> 2] = $0;
 HEAPF32[$1 + 8 >> 2] = $0;
 HEAPF32[$1 + 4 >> 2] = $0;
 HEAPF32[$1 >> 2] = $0;
 return;
}

function _glmc_quat_identity($0) {
 $0 = $0 | 0;
 HEAP32[$0 >> 2] = 0;
 HEAP32[$0 + 4 >> 2] = 0;
 HEAP32[$0 + 8 >> 2] = 0;
 HEAP32[$0 + 12 >> 2] = 1065353216;
 return;
}

function _glmc_vec4_one($0) {
 $0 = $0 | 0;
 HEAPF32[$0 >> 2] = 1.0;
 HEAPF32[$0 + 4 >> 2] = 1.0;
 HEAPF32[$0 + 8 >> 2] = 1.0;
 HEAPF32[$0 + 12 >> 2] = 1.0;
 return;
}

function _glmc_perspective_resize($0, $1) {
 $0 = +$0;
 $1 = $1 | 0;
 if (+HEAPF32[$1 >> 2] == 0.0) return;
 HEAPF32[$1 >> 2] = +HEAPF32[$1 + 20 >> 2] / $0;
 return;
}
function stackAlloc(size) {
 size = size | 0;
 var ret = 0;
 ret = STACKTOP;
 STACKTOP = STACKTOP + size | 0;
 STACKTOP = STACKTOP + 15 & -16;
 return ret | 0;
}

function _glmc_persp_decomp_near($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 HEAPF32[$1 >> 2] = +HEAPF32[$0 + 56 >> 2] / (+HEAPF32[$0 + 40 >> 2] + -1.0);
 return;
}

function _glmc_persp_decomp_far($0, $1) {
 $0 = $0 | 0;
 $1 = $1 | 0;
 HEAPF32[$1 >> 2] = +HEAPF32[$0 + 56 >> 2] / (+HEAPF32[$0 + 40 >> 2] + 1.0);
 return;
}

function _glmc_vec4_zero($0) {
 $0 = $0 | 0;
 HEAP32[$0 >> 2] = 0;
 HEAP32[$0 + 4 >> 2] = 0;
 HEAP32[$0 + 8 >> 2] = 0;
 HEAP32[$0 + 12 >> 2] = 0;
 return;
}

function _glmc_vec_broadcast($0, $1) {
 $0 = +$0;
 $1 = $1 | 0;
 HEAPF32[$1 + 8 >> 2] = $0;
 HEAPF32[$1 + 4 >> 2] = $0;
 HEAPF32[$1 >> 2] = $0;
 return;
}

function establishStackSpace(stackBase, stackMax) {
 stackBase = stackBase | 0;
 stackMax = stackMax | 0;
 STACKTOP = stackBase;
 STACK_MAX = stackMax;
}

function setThrew(threw, value) {
 threw = threw | 0;
 value = value | 0;
 if (!__THREW__) {
  __THREW__ = threw;
  threwValue = value;
 }
}

function _glmc_vec_zero($0) {
 $0 = $0 | 0;
 HEAPF32[$0 >> 2] = 0.0;
 HEAPF32[$0 + 4 >> 2] = 0.0;
 HEAPF32[$0 + 8 >> 2] = 0.0;
 return;
}

function _glmc_vec_one($0) {
 $0 = $0 | 0;
 HEAPF32[$0 >> 2] = 1.0;
 HEAPF32[$0 + 4 >> 2] = 1.0;
 HEAPF32[$0 + 8 >> 2] = 1.0;
 return;
}

function _glmc_persp_fovy($0) {
 $0 = $0 | 0;
 return +(+Math_atan(+(1.0 / +HEAPF32[$0 + 20 >> 2])) * 2.0);
}

function _glmc_persp_aspect($0) {
 $0 = $0 | 0;
 return +(+HEAPF32[$0 + 20 >> 2] / +HEAPF32[$0 >> 2]);
}

function _glmc_quat_real($0) {
 $0 = $0 | 0;
 return +(+HEAPF32[$0 + 12 >> 2]);
}

function setTempRet0(value) {
 value = value | 0;
 tempRet0 = value;
}

function stackRestore(top) {
 top = top | 0;
 STACKTOP = top;
}

function _glmc_quat_normalize($0) {
 $0 = $0 | 0;
 return;
}

function getTempRet0() {
 return tempRet0 | 0;
}

function stackSave() {
 return STACKTOP | 0;
}

function ___errno_location() {
 return 504;
}

// EMSCRIPTEN_END_FUNCS


  return { ___errno_location: ___errno_location, _free: _free, _glmc_aabb_center: _glmc_aabb_center, _glmc_aabb_crop: _glmc_aabb_crop, _glmc_aabb_crop_until: _glmc_aabb_crop_until, _glmc_aabb_frustum: _glmc_aabb_frustum, _glmc_aabb_invalidate: _glmc_aabb_invalidate, _glmc_aabb_isvalid: _glmc_aabb_isvalid, _glmc_aabb_merge: _glmc_aabb_merge, _glmc_aabb_radius: _glmc_aabb_radius, _glmc_aabb_size: _glmc_aabb_size, _glmc_aabb_transform: _glmc_aabb_transform, _glmc_decompose: _glmc_decompose, _glmc_decompose_rs: _glmc_decompose_rs, _glmc_decompose_scalev: _glmc_decompose_scalev, _glmc_euler: _glmc_euler, _glmc_euler_angles: _glmc_euler_angles, _glmc_euler_by_order: _glmc_euler_by_order, _glmc_euler_xyz: _glmc_euler_xyz, _glmc_euler_xzy: _glmc_euler_xzy, _glmc_euler_yxz: _glmc_euler_yxz, _glmc_euler_yzx: _glmc_euler_yzx, _glmc_euler_zxy: _glmc_euler_zxy, _glmc_euler_zyx: _glmc_euler_zyx, _glmc_frustum: _glmc_frustum, _glmc_frustum_box: _glmc_frustum_box, _glmc_frustum_center: _glmc_frustum_center, _glmc_frustum_corners: _glmc_frustum_corners, _glmc_frustum_corners_at: _glmc_frustum_corners_at, _glmc_frustum_planes: _glmc_frustum_planes, _glmc_inv_tr: _glmc_inv_tr, _glmc_look: _glmc_look, _glmc_look_anyup: _glmc_look_anyup, _glmc_lookat: _glmc_lookat, _glmc_mat3_copy: _glmc_mat3_copy, _glmc_mat3_det: _glmc_mat3_det, _glmc_mat3_identity: _glmc_mat3_identity, _glmc_mat3_inv: _glmc_mat3_inv, _glmc_mat3_mul: _glmc_mat3_mul, _glmc_mat3_mulv: _glmc_mat3_mulv, _glmc_mat3_quat: _glmc_mat3_quat, _glmc_mat3_scale: _glmc_mat3_scale, _glmc_mat3_swap_col: _glmc_mat3_swap_col, _glmc_mat3_swap_row: _glmc_mat3_swap_row, _glmc_mat3_transpose: _glmc_mat3_transpose, _glmc_mat3_transpose_to: _glmc_mat3_transpose_to, _glmc_mat4_copy: _glmc_mat4_copy, _glmc_mat4_det: _glmc_mat4_det, _glmc_mat4_identity: _glmc_mat4_identity, _glmc_mat4_ins3: _glmc_mat4_ins3, _glmc_mat4_inv: _glmc_mat4_inv, _glmc_mat4_inv_fast: _glmc_mat4_inv_fast, _glmc_mat4_inv_precise: _glmc_mat4_inv_precise, _glmc_mat4_mul: _glmc_mat4_mul, _glmc_mat4_mulN: _glmc_mat4_mulN, _glmc_mat4_mulv: _glmc_mat4_mulv, _glmc_mat4_mulv3: _glmc_mat4_mulv3, _glmc_mat4_pick3: _glmc_mat4_pick3, _glmc_mat4_pick3t: _glmc_mat4_pick3t, _glmc_mat4_quat: _glmc_mat4_quat, _glmc_mat4_scale: _glmc_mat4_scale, _glmc_mat4_scale_p: _glmc_mat4_scale_p, _glmc_mat4_swap_col: _glmc_mat4_swap_col, _glmc_mat4_swap_row: _glmc_mat4_swap_row, _glmc_mat4_transpose: _glmc_mat4_transpose, _glmc_mat4_transpose_to: _glmc_mat4_transpose_to, _glmc_mat4_ucopy: _glmc_mat4_ucopy, _glmc_mul: _glmc_mul, _glmc_mul_rot: _glmc_mul_rot, _glmc_ortho: _glmc_ortho, _glmc_ortho_aabb: _glmc_ortho_aabb, _glmc_ortho_aabb_p: _glmc_ortho_aabb_p, _glmc_ortho_aabb_pz: _glmc_ortho_aabb_pz, _glmc_ortho_default: _glmc_ortho_default, _glmc_ortho_default_s: _glmc_ortho_default_s, _glmc_persp_aspect: _glmc_persp_aspect, _glmc_persp_decomp: _glmc_persp_decomp, _glmc_persp_decomp_far: _glmc_persp_decomp_far, _glmc_persp_decomp_near: _glmc_persp_decomp_near, _glmc_persp_decomp_x: _glmc_persp_decomp_x, _glmc_persp_decomp_y: _glmc_persp_decomp_y, _glmc_persp_decomp_z: _glmc_persp_decomp_z, _glmc_persp_decompv: _glmc_persp_decompv, _glmc_persp_fovy: _glmc_persp_fovy, _glmc_persp_sizes: _glmc_persp_sizes, _glmc_perspective: _glmc_perspective, _glmc_perspective_default: _glmc_perspective_default, _glmc_perspective_resize: _glmc_perspective_resize, _glmc_plane_normalize: _glmc_plane_normalize, _glmc_project: _glmc_project, _glmc_quat: _glmc_quat, _glmc_quat_add: _glmc_quat_add, _glmc_quat_angle: _glmc_quat_angle, _glmc_quat_axis: _glmc_quat_axis, _glmc_quat_conjugate: _glmc_quat_conjugate, _glmc_quat_copy: _glmc_quat_copy, _glmc_quat_dot: _glmc_quat_dot, _glmc_quat_for: _glmc_quat_for, _glmc_quat_forp: _glmc_quat_forp, _glmc_quat_identity: _glmc_quat_identity, _glmc_quat_imag: _glmc_quat_imag, _glmc_quat_imaglen: _glmc_quat_imaglen, _glmc_quat_imagn: _glmc_quat_imagn, _glmc_quat_init: _glmc_quat_init, _glmc_quat_inv: _glmc_quat_inv, _glmc_quat_lerp: _glmc_quat_lerp, _glmc_quat_look: _glmc_quat_look, _glmc_quat_mat3: _glmc_quat_mat3, _glmc_quat_mat3t: _glmc_quat_mat3t, _glmc_quat_mat4: _glmc_quat_mat4, _glmc_quat_mat4t: _glmc_quat_mat4t, _glmc_quat_mul: _glmc_quat_mul, _glmc_quat_norm: _glmc_quat_norm, _glmc_quat_normalize: _glmc_quat_normalize, _glmc_quat_normalize_to: _glmc_quat_normalize_to, _glmc_quat_real: _glmc_quat_real, _glmc_quat_rotate: _glmc_quat_rotate, _glmc_quat_rotate_at: _glmc_quat_rotate_at, _glmc_quat_rotate_atm: _glmc_quat_rotate_atm, _glmc_quat_rotatev: _glmc_quat_rotatev, _glmc_quat_slerp: _glmc_quat_slerp, _glmc_quat_sub: _glmc_quat_sub, _glmc_quatv: _glmc_quatv, _glmc_rotate: _glmc_rotate, _glmc_rotate_at: _glmc_rotate_at, _glmc_rotate_atm: _glmc_rotate_atm, _glmc_rotate_make: _glmc_rotate_make, _glmc_rotate_x: _glmc_rotate_x, _glmc_rotate_y: _glmc_rotate_y, _glmc_rotate_z: _glmc_rotate_z, _glmc_scale: _glmc_scale, _glmc_scale_make: _glmc_scale_make, _glmc_scale_to: _glmc_scale_to, _glmc_scale_uni: _glmc_scale_uni, _glmc_translate: _glmc_translate, _glmc_translate_make: _glmc_translate_make, _glmc_translate_to: _glmc_translate_to, _glmc_translate_x: _glmc_translate_x, _glmc_translate_y: _glmc_translate_y, _glmc_translate_z: _glmc_translate_z, _glmc_uniscaled: _glmc_uniscaled, _glmc_unproject: _glmc_unproject, _glmc_unprojecti: _glmc_unprojecti, _glmc_vec3: _glmc_vec3, _glmc_vec4: _glmc_vec4, _glmc_vec4_add: _glmc_vec4_add, _glmc_vec4_addadd: _glmc_vec4_addadd, _glmc_vec4_adds: _glmc_vec4_adds, _glmc_vec4_broadcast: _glmc_vec4_broadcast, _glmc_vec4_clamp: _glmc_vec4_clamp, _glmc_vec4_copy: _glmc_vec4_copy, _glmc_vec4_copy3: _glmc_vec4_copy3, _glmc_vec4_distance: _glmc_vec4_distance, _glmc_vec4_div: _glmc_vec4_div, _glmc_vec4_divs: _glmc_vec4_divs, _glmc_vec4_dot: _glmc_vec4_dot, _glmc_vec4_eq: _glmc_vec4_eq, _glmc_vec4_eq_all: _glmc_vec4_eq_all, _glmc_vec4_eq_eps: _glmc_vec4_eq_eps, _glmc_vec4_eqv: _glmc_vec4_eqv, _glmc_vec4_eqv_eps: _glmc_vec4_eqv_eps, _glmc_vec4_flipsign: _glmc_vec4_flipsign, _glmc_vec4_flipsign_to: _glmc_vec4_flipsign_to, _glmc_vec4_inv: _glmc_vec4_inv, _glmc_vec4_inv_to: _glmc_vec4_inv_to, _glmc_vec4_isinf: _glmc_vec4_isinf, _glmc_vec4_isnan: _glmc_vec4_isnan, _glmc_vec4_isvalid: _glmc_vec4_isvalid, _glmc_vec4_lerp: _glmc_vec4_lerp, _glmc_vec4_max: _glmc_vec4_max, _glmc_vec4_maxv: _glmc_vec4_maxv, _glmc_vec4_min: _glmc_vec4_min, _glmc_vec4_minv: _glmc_vec4_minv, _glmc_vec4_mul: _glmc_vec4_mul, _glmc_vec4_muladd: _glmc_vec4_muladd, _glmc_vec4_muladds: _glmc_vec4_muladds, _glmc_vec4_mulv: _glmc_vec4_mulv, _glmc_vec4_norm: _glmc_vec4_norm, _glmc_vec4_norm2: _glmc_vec4_norm2, _glmc_vec4_normalize: _glmc_vec4_normalize, _glmc_vec4_normalize_to: _glmc_vec4_normalize_to, _glmc_vec4_one: _glmc_vec4_one, _glmc_vec4_scale: _glmc_vec4_scale, _glmc_vec4_scale_as: _glmc_vec4_scale_as, _glmc_vec4_sign: _glmc_vec4_sign, _glmc_vec4_sqrt: _glmc_vec4_sqrt, _glmc_vec4_sub: _glmc_vec4_sub, _glmc_vec4_subadd: _glmc_vec4_subadd, _glmc_vec4_subs: _glmc_vec4_subs, _glmc_vec4_zero: _glmc_vec4_zero, _glmc_vec_add: _glmc_vec_add, _glmc_vec_addadd: _glmc_vec_addadd, _glmc_vec_adds: _glmc_vec_adds, _glmc_vec_angle: _glmc_vec_angle, _glmc_vec_broadcast: _glmc_vec_broadcast, _glmc_vec_center: _glmc_vec_center, _glmc_vec_clamp: _glmc_vec_clamp, _glmc_vec_copy: _glmc_vec_copy, _glmc_vec_cross: _glmc_vec_cross, _glmc_vec_distance: _glmc_vec_distance, _glmc_vec_div: _glmc_vec_div, _glmc_vec_divs: _glmc_vec_divs, _glmc_vec_dot: _glmc_vec_dot, _glmc_vec_eq: _glmc_vec_eq, _glmc_vec_eq_all: _glmc_vec_eq_all, _glmc_vec_eq_eps: _glmc_vec_eq_eps, _glmc_vec_eqv: _glmc_vec_eqv, _glmc_vec_eqv_eps: _glmc_vec_eqv_eps, _glmc_vec_flipsign: _glmc_vec_flipsign, _glmc_vec_flipsign_to: _glmc_vec_flipsign_to, _glmc_vec_inv: _glmc_vec_inv, _glmc_vec_inv_to: _glmc_vec_inv_to, _glmc_vec_isinf: _glmc_vec_isinf, _glmc_vec_isnan: _glmc_vec_isnan, _glmc_vec_isvalid: _glmc_vec_isvalid, _glmc_vec_lerp: _glmc_vec_lerp, _glmc_vec_max: _glmc_vec_max, _glmc_vec_maxv: _glmc_vec_maxv, _glmc_vec_min: _glmc_vec_min, _glmc_vec_minv: _glmc_vec_minv, _glmc_vec_mul: _glmc_vec_mul, _glmc_vec_muladd: _glmc_vec_muladd, _glmc_vec_muladds: _glmc_vec_muladds, _glmc_vec_mulv: _glmc_vec_mulv, _glmc_vec_norm: _glmc_vec_norm, _glmc_vec_norm2: _glmc_vec_norm2, _glmc_vec_normalize: _glmc_vec_normalize, _glmc_vec_normalize_to: _glmc_vec_normalize_to, _glmc_vec_one: _glmc_vec_one, _glmc_vec_ortho: _glmc_vec_ortho, _glmc_vec_proj: _glmc_vec_proj, _glmc_vec_rotate: _glmc_vec_rotate, _glmc_vec_rotate_m3: _glmc_vec_rotate_m3, _glmc_vec_rotate_m4: _glmc_vec_rotate_m4, _glmc_vec_scale: _glmc_vec_scale, _glmc_vec_scale_as: _glmc_vec_scale_as, _glmc_vec_sign: _glmc_vec_sign, _glmc_vec_sqrt: _glmc_vec_sqrt, _glmc_vec_sub: _glmc_vec_sub, _glmc_vec_subadd: _glmc_vec_subadd, _glmc_vec_subs: _glmc_vec_subs, _glmc_vec_zero: _glmc_vec_zero, _malloc: _malloc, _memcpy: _memcpy, _memset: _memset, _sbrk: _sbrk, establishStackSpace: establishStackSpace, getTempRet0: getTempRet0, runPostSets: runPostSets, setTempRet0: setTempRet0, setThrew: setThrew, stackAlloc: stackAlloc, stackRestore: stackRestore, stackSave: stackSave };
})
// EMSCRIPTEN_END_ASM
(Module.asmGlobalArg, Module.asmLibraryArg, buffer);

var ___errno_location = Module["___errno_location"] = asm["___errno_location"];
var _free = Module["_free"] = asm["_free"];
var _glmc_aabb_center = Module["_glmc_aabb_center"] = asm["_glmc_aabb_center"];
var _glmc_aabb_crop = Module["_glmc_aabb_crop"] = asm["_glmc_aabb_crop"];
var _glmc_aabb_crop_until = Module["_glmc_aabb_crop_until"] = asm["_glmc_aabb_crop_until"];
var _glmc_aabb_frustum = Module["_glmc_aabb_frustum"] = asm["_glmc_aabb_frustum"];
var _glmc_aabb_invalidate = Module["_glmc_aabb_invalidate"] = asm["_glmc_aabb_invalidate"];
var _glmc_aabb_isvalid = Module["_glmc_aabb_isvalid"] = asm["_glmc_aabb_isvalid"];
var _glmc_aabb_merge = Module["_glmc_aabb_merge"] = asm["_glmc_aabb_merge"];
var _glmc_aabb_radius = Module["_glmc_aabb_radius"] = asm["_glmc_aabb_radius"];
var _glmc_aabb_size = Module["_glmc_aabb_size"] = asm["_glmc_aabb_size"];
var _glmc_aabb_transform = Module["_glmc_aabb_transform"] = asm["_glmc_aabb_transform"];
var _glmc_decompose = Module["_glmc_decompose"] = asm["_glmc_decompose"];
var _glmc_decompose_rs = Module["_glmc_decompose_rs"] = asm["_glmc_decompose_rs"];
var _glmc_decompose_scalev = Module["_glmc_decompose_scalev"] = asm["_glmc_decompose_scalev"];
var _glmc_euler = Module["_glmc_euler"] = asm["_glmc_euler"];
var _glmc_euler_angles = Module["_glmc_euler_angles"] = asm["_glmc_euler_angles"];
var _glmc_euler_by_order = Module["_glmc_euler_by_order"] = asm["_glmc_euler_by_order"];
var _glmc_euler_xyz = Module["_glmc_euler_xyz"] = asm["_glmc_euler_xyz"];
var _glmc_euler_xzy = Module["_glmc_euler_xzy"] = asm["_glmc_euler_xzy"];
var _glmc_euler_yxz = Module["_glmc_euler_yxz"] = asm["_glmc_euler_yxz"];
var _glmc_euler_yzx = Module["_glmc_euler_yzx"] = asm["_glmc_euler_yzx"];
var _glmc_euler_zxy = Module["_glmc_euler_zxy"] = asm["_glmc_euler_zxy"];
var _glmc_euler_zyx = Module["_glmc_euler_zyx"] = asm["_glmc_euler_zyx"];
var _glmc_frustum = Module["_glmc_frustum"] = asm["_glmc_frustum"];
var _glmc_frustum_box = Module["_glmc_frustum_box"] = asm["_glmc_frustum_box"];
var _glmc_frustum_center = Module["_glmc_frustum_center"] = asm["_glmc_frustum_center"];
var _glmc_frustum_corners = Module["_glmc_frustum_corners"] = asm["_glmc_frustum_corners"];
var _glmc_frustum_corners_at = Module["_glmc_frustum_corners_at"] = asm["_glmc_frustum_corners_at"];
var _glmc_frustum_planes = Module["_glmc_frustum_planes"] = asm["_glmc_frustum_planes"];
var _glmc_inv_tr = Module["_glmc_inv_tr"] = asm["_glmc_inv_tr"];
var _glmc_look = Module["_glmc_look"] = asm["_glmc_look"];
var _glmc_look_anyup = Module["_glmc_look_anyup"] = asm["_glmc_look_anyup"];
var _glmc_lookat = Module["_glmc_lookat"] = asm["_glmc_lookat"];
var _glmc_mat3_copy = Module["_glmc_mat3_copy"] = asm["_glmc_mat3_copy"];
var _glmc_mat3_det = Module["_glmc_mat3_det"] = asm["_glmc_mat3_det"];
var _glmc_mat3_identity = Module["_glmc_mat3_identity"] = asm["_glmc_mat3_identity"];
var _glmc_mat3_inv = Module["_glmc_mat3_inv"] = asm["_glmc_mat3_inv"];
var _glmc_mat3_mul = Module["_glmc_mat3_mul"] = asm["_glmc_mat3_mul"];
var _glmc_mat3_mulv = Module["_glmc_mat3_mulv"] = asm["_glmc_mat3_mulv"];
var _glmc_mat3_quat = Module["_glmc_mat3_quat"] = asm["_glmc_mat3_quat"];
var _glmc_mat3_scale = Module["_glmc_mat3_scale"] = asm["_glmc_mat3_scale"];
var _glmc_mat3_swap_col = Module["_glmc_mat3_swap_col"] = asm["_glmc_mat3_swap_col"];
var _glmc_mat3_swap_row = Module["_glmc_mat3_swap_row"] = asm["_glmc_mat3_swap_row"];
var _glmc_mat3_transpose = Module["_glmc_mat3_transpose"] = asm["_glmc_mat3_transpose"];
var _glmc_mat3_transpose_to = Module["_glmc_mat3_transpose_to"] = asm["_glmc_mat3_transpose_to"];
var _glmc_mat4_copy = Module["_glmc_mat4_copy"] = asm["_glmc_mat4_copy"];
var _glmc_mat4_det = Module["_glmc_mat4_det"] = asm["_glmc_mat4_det"];
var _glmc_mat4_identity = Module["_glmc_mat4_identity"] = asm["_glmc_mat4_identity"];
var _glmc_mat4_ins3 = Module["_glmc_mat4_ins3"] = asm["_glmc_mat4_ins3"];
var _glmc_mat4_inv = Module["_glmc_mat4_inv"] = asm["_glmc_mat4_inv"];
var _glmc_mat4_inv_fast = Module["_glmc_mat4_inv_fast"] = asm["_glmc_mat4_inv_fast"];
var _glmc_mat4_inv_precise = Module["_glmc_mat4_inv_precise"] = asm["_glmc_mat4_inv_precise"];
var _glmc_mat4_mul = Module["_glmc_mat4_mul"] = asm["_glmc_mat4_mul"];
var _glmc_mat4_mulN = Module["_glmc_mat4_mulN"] = asm["_glmc_mat4_mulN"];
var _glmc_mat4_mulv = Module["_glmc_mat4_mulv"] = asm["_glmc_mat4_mulv"];
var _glmc_mat4_mulv3 = Module["_glmc_mat4_mulv3"] = asm["_glmc_mat4_mulv3"];
var _glmc_mat4_pick3 = Module["_glmc_mat4_pick3"] = asm["_glmc_mat4_pick3"];
var _glmc_mat4_pick3t = Module["_glmc_mat4_pick3t"] = asm["_glmc_mat4_pick3t"];
var _glmc_mat4_quat = Module["_glmc_mat4_quat"] = asm["_glmc_mat4_quat"];
var _glmc_mat4_scale = Module["_glmc_mat4_scale"] = asm["_glmc_mat4_scale"];
var _glmc_mat4_scale_p = Module["_glmc_mat4_scale_p"] = asm["_glmc_mat4_scale_p"];
var _glmc_mat4_swap_col = Module["_glmc_mat4_swap_col"] = asm["_glmc_mat4_swap_col"];
var _glmc_mat4_swap_row = Module["_glmc_mat4_swap_row"] = asm["_glmc_mat4_swap_row"];
var _glmc_mat4_transpose = Module["_glmc_mat4_transpose"] = asm["_glmc_mat4_transpose"];
var _glmc_mat4_transpose_to = Module["_glmc_mat4_transpose_to"] = asm["_glmc_mat4_transpose_to"];
var _glmc_mat4_ucopy = Module["_glmc_mat4_ucopy"] = asm["_glmc_mat4_ucopy"];
var _glmc_mul = Module["_glmc_mul"] = asm["_glmc_mul"];
var _glmc_mul_rot = Module["_glmc_mul_rot"] = asm["_glmc_mul_rot"];
var _glmc_ortho = Module["_glmc_ortho"] = asm["_glmc_ortho"];
var _glmc_ortho_aabb = Module["_glmc_ortho_aabb"] = asm["_glmc_ortho_aabb"];
var _glmc_ortho_aabb_p = Module["_glmc_ortho_aabb_p"] = asm["_glmc_ortho_aabb_p"];
var _glmc_ortho_aabb_pz = Module["_glmc_ortho_aabb_pz"] = asm["_glmc_ortho_aabb_pz"];
var _glmc_ortho_default = Module["_glmc_ortho_default"] = asm["_glmc_ortho_default"];
var _glmc_ortho_default_s = Module["_glmc_ortho_default_s"] = asm["_glmc_ortho_default_s"];
var _glmc_persp_aspect = Module["_glmc_persp_aspect"] = asm["_glmc_persp_aspect"];
var _glmc_persp_decomp = Module["_glmc_persp_decomp"] = asm["_glmc_persp_decomp"];
var _glmc_persp_decomp_far = Module["_glmc_persp_decomp_far"] = asm["_glmc_persp_decomp_far"];
var _glmc_persp_decomp_near = Module["_glmc_persp_decomp_near"] = asm["_glmc_persp_decomp_near"];
var _glmc_persp_decomp_x = Module["_glmc_persp_decomp_x"] = asm["_glmc_persp_decomp_x"];
var _glmc_persp_decomp_y = Module["_glmc_persp_decomp_y"] = asm["_glmc_persp_decomp_y"];
var _glmc_persp_decomp_z = Module["_glmc_persp_decomp_z"] = asm["_glmc_persp_decomp_z"];
var _glmc_persp_decompv = Module["_glmc_persp_decompv"] = asm["_glmc_persp_decompv"];
var _glmc_persp_fovy = Module["_glmc_persp_fovy"] = asm["_glmc_persp_fovy"];
var _glmc_persp_sizes = Module["_glmc_persp_sizes"] = asm["_glmc_persp_sizes"];
var _glmc_perspective = Module["_glmc_perspective"] = asm["_glmc_perspective"];
var _glmc_perspective_default = Module["_glmc_perspective_default"] = asm["_glmc_perspective_default"];
var _glmc_perspective_resize = Module["_glmc_perspective_resize"] = asm["_glmc_perspective_resize"];
var _glmc_plane_normalize = Module["_glmc_plane_normalize"] = asm["_glmc_plane_normalize"];
var _glmc_project = Module["_glmc_project"] = asm["_glmc_project"];
var _glmc_quat = Module["_glmc_quat"] = asm["_glmc_quat"];
var _glmc_quat_add = Module["_glmc_quat_add"] = asm["_glmc_quat_add"];
var _glmc_quat_angle = Module["_glmc_quat_angle"] = asm["_glmc_quat_angle"];
var _glmc_quat_axis = Module["_glmc_quat_axis"] = asm["_glmc_quat_axis"];
var _glmc_quat_conjugate = Module["_glmc_quat_conjugate"] = asm["_glmc_quat_conjugate"];
var _glmc_quat_copy = Module["_glmc_quat_copy"] = asm["_glmc_quat_copy"];
var _glmc_quat_dot = Module["_glmc_quat_dot"] = asm["_glmc_quat_dot"];
var _glmc_quat_for = Module["_glmc_quat_for"] = asm["_glmc_quat_for"];
var _glmc_quat_forp = Module["_glmc_quat_forp"] = asm["_glmc_quat_forp"];
var _glmc_quat_identity = Module["_glmc_quat_identity"] = asm["_glmc_quat_identity"];
var _glmc_quat_imag = Module["_glmc_quat_imag"] = asm["_glmc_quat_imag"];
var _glmc_quat_imaglen = Module["_glmc_quat_imaglen"] = asm["_glmc_quat_imaglen"];
var _glmc_quat_imagn = Module["_glmc_quat_imagn"] = asm["_glmc_quat_imagn"];
var _glmc_quat_init = Module["_glmc_quat_init"] = asm["_glmc_quat_init"];
var _glmc_quat_inv = Module["_glmc_quat_inv"] = asm["_glmc_quat_inv"];
var _glmc_quat_lerp = Module["_glmc_quat_lerp"] = asm["_glmc_quat_lerp"];
var _glmc_quat_look = Module["_glmc_quat_look"] = asm["_glmc_quat_look"];
var _glmc_quat_mat3 = Module["_glmc_quat_mat3"] = asm["_glmc_quat_mat3"];
var _glmc_quat_mat3t = Module["_glmc_quat_mat3t"] = asm["_glmc_quat_mat3t"];
var _glmc_quat_mat4 = Module["_glmc_quat_mat4"] = asm["_glmc_quat_mat4"];
var _glmc_quat_mat4t = Module["_glmc_quat_mat4t"] = asm["_glmc_quat_mat4t"];
var _glmc_quat_mul = Module["_glmc_quat_mul"] = asm["_glmc_quat_mul"];
var _glmc_quat_norm = Module["_glmc_quat_norm"] = asm["_glmc_quat_norm"];
var _glmc_quat_normalize = Module["_glmc_quat_normalize"] = asm["_glmc_quat_normalize"];
var _glmc_quat_normalize_to = Module["_glmc_quat_normalize_to"] = asm["_glmc_quat_normalize_to"];
var _glmc_quat_real = Module["_glmc_quat_real"] = asm["_glmc_quat_real"];
var _glmc_quat_rotate = Module["_glmc_quat_rotate"] = asm["_glmc_quat_rotate"];
var _glmc_quat_rotate_at = Module["_glmc_quat_rotate_at"] = asm["_glmc_quat_rotate_at"];
var _glmc_quat_rotate_atm = Module["_glmc_quat_rotate_atm"] = asm["_glmc_quat_rotate_atm"];
var _glmc_quat_rotatev = Module["_glmc_quat_rotatev"] = asm["_glmc_quat_rotatev"];
var _glmc_quat_slerp = Module["_glmc_quat_slerp"] = asm["_glmc_quat_slerp"];
var _glmc_quat_sub = Module["_glmc_quat_sub"] = asm["_glmc_quat_sub"];
var _glmc_quatv = Module["_glmc_quatv"] = asm["_glmc_quatv"];
var _glmc_rotate = Module["_glmc_rotate"] = asm["_glmc_rotate"];
var _glmc_rotate_at = Module["_glmc_rotate_at"] = asm["_glmc_rotate_at"];
var _glmc_rotate_atm = Module["_glmc_rotate_atm"] = asm["_glmc_rotate_atm"];
var _glmc_rotate_make = Module["_glmc_rotate_make"] = asm["_glmc_rotate_make"];
var _glmc_rotate_x = Module["_glmc_rotate_x"] = asm["_glmc_rotate_x"];
var _glmc_rotate_y = Module["_glmc_rotate_y"] = asm["_glmc_rotate_y"];
var _glmc_rotate_z = Module["_glmc_rotate_z"] = asm["_glmc_rotate_z"];
var _glmc_scale = Module["_glmc_scale"] = asm["_glmc_scale"];
var _glmc_scale_make = Module["_glmc_scale_make"] = asm["_glmc_scale_make"];
var _glmc_scale_to = Module["_glmc_scale_to"] = asm["_glmc_scale_to"];
var _glmc_scale_uni = Module["_glmc_scale_uni"] = asm["_glmc_scale_uni"];
var _glmc_translate = Module["_glmc_translate"] = asm["_glmc_translate"];
var _glmc_translate_make = Module["_glmc_translate_make"] = asm["_glmc_translate_make"];
var _glmc_translate_to = Module["_glmc_translate_to"] = asm["_glmc_translate_to"];
var _glmc_translate_x = Module["_glmc_translate_x"] = asm["_glmc_translate_x"];
var _glmc_translate_y = Module["_glmc_translate_y"] = asm["_glmc_translate_y"];
var _glmc_translate_z = Module["_glmc_translate_z"] = asm["_glmc_translate_z"];
var _glmc_uniscaled = Module["_glmc_uniscaled"] = asm["_glmc_uniscaled"];
var _glmc_unproject = Module["_glmc_unproject"] = asm["_glmc_unproject"];
var _glmc_unprojecti = Module["_glmc_unprojecti"] = asm["_glmc_unprojecti"];
var _glmc_vec3 = Module["_glmc_vec3"] = asm["_glmc_vec3"];
var _glmc_vec4 = Module["_glmc_vec4"] = asm["_glmc_vec4"];
var _glmc_vec4_add = Module["_glmc_vec4_add"] = asm["_glmc_vec4_add"];
var _glmc_vec4_addadd = Module["_glmc_vec4_addadd"] = asm["_glmc_vec4_addadd"];
var _glmc_vec4_adds = Module["_glmc_vec4_adds"] = asm["_glmc_vec4_adds"];
var _glmc_vec4_broadcast = Module["_glmc_vec4_broadcast"] = asm["_glmc_vec4_broadcast"];
var _glmc_vec4_clamp = Module["_glmc_vec4_clamp"] = asm["_glmc_vec4_clamp"];
var _glmc_vec4_copy = Module["_glmc_vec4_copy"] = asm["_glmc_vec4_copy"];
var _glmc_vec4_copy3 = Module["_glmc_vec4_copy3"] = asm["_glmc_vec4_copy3"];
var _glmc_vec4_distance = Module["_glmc_vec4_distance"] = asm["_glmc_vec4_distance"];
var _glmc_vec4_div = Module["_glmc_vec4_div"] = asm["_glmc_vec4_div"];
var _glmc_vec4_divs = Module["_glmc_vec4_divs"] = asm["_glmc_vec4_divs"];
var _glmc_vec4_dot = Module["_glmc_vec4_dot"] = asm["_glmc_vec4_dot"];
var _glmc_vec4_eq = Module["_glmc_vec4_eq"] = asm["_glmc_vec4_eq"];
var _glmc_vec4_eq_all = Module["_glmc_vec4_eq_all"] = asm["_glmc_vec4_eq_all"];
var _glmc_vec4_eq_eps = Module["_glmc_vec4_eq_eps"] = asm["_glmc_vec4_eq_eps"];
var _glmc_vec4_eqv = Module["_glmc_vec4_eqv"] = asm["_glmc_vec4_eqv"];
var _glmc_vec4_eqv_eps = Module["_glmc_vec4_eqv_eps"] = asm["_glmc_vec4_eqv_eps"];
var _glmc_vec4_flipsign = Module["_glmc_vec4_flipsign"] = asm["_glmc_vec4_flipsign"];
var _glmc_vec4_flipsign_to = Module["_glmc_vec4_flipsign_to"] = asm["_glmc_vec4_flipsign_to"];
var _glmc_vec4_inv = Module["_glmc_vec4_inv"] = asm["_glmc_vec4_inv"];
var _glmc_vec4_inv_to = Module["_glmc_vec4_inv_to"] = asm["_glmc_vec4_inv_to"];
var _glmc_vec4_isinf = Module["_glmc_vec4_isinf"] = asm["_glmc_vec4_isinf"];
var _glmc_vec4_isnan = Module["_glmc_vec4_isnan"] = asm["_glmc_vec4_isnan"];
var _glmc_vec4_isvalid = Module["_glmc_vec4_isvalid"] = asm["_glmc_vec4_isvalid"];
var _glmc_vec4_lerp = Module["_glmc_vec4_lerp"] = asm["_glmc_vec4_lerp"];
var _glmc_vec4_max = Module["_glmc_vec4_max"] = asm["_glmc_vec4_max"];
var _glmc_vec4_maxv = Module["_glmc_vec4_maxv"] = asm["_glmc_vec4_maxv"];
var _glmc_vec4_min = Module["_glmc_vec4_min"] = asm["_glmc_vec4_min"];
var _glmc_vec4_minv = Module["_glmc_vec4_minv"] = asm["_glmc_vec4_minv"];
var _glmc_vec4_mul = Module["_glmc_vec4_mul"] = asm["_glmc_vec4_mul"];
var _glmc_vec4_muladd = Module["_glmc_vec4_muladd"] = asm["_glmc_vec4_muladd"];
var _glmc_vec4_muladds = Module["_glmc_vec4_muladds"] = asm["_glmc_vec4_muladds"];
var _glmc_vec4_mulv = Module["_glmc_vec4_mulv"] = asm["_glmc_vec4_mulv"];
var _glmc_vec4_norm = Module["_glmc_vec4_norm"] = asm["_glmc_vec4_norm"];
var _glmc_vec4_norm2 = Module["_glmc_vec4_norm2"] = asm["_glmc_vec4_norm2"];
var _glmc_vec4_normalize = Module["_glmc_vec4_normalize"] = asm["_glmc_vec4_normalize"];
var _glmc_vec4_normalize_to = Module["_glmc_vec4_normalize_to"] = asm["_glmc_vec4_normalize_to"];
var _glmc_vec4_one = Module["_glmc_vec4_one"] = asm["_glmc_vec4_one"];
var _glmc_vec4_scale = Module["_glmc_vec4_scale"] = asm["_glmc_vec4_scale"];
var _glmc_vec4_scale_as = Module["_glmc_vec4_scale_as"] = asm["_glmc_vec4_scale_as"];
var _glmc_vec4_sign = Module["_glmc_vec4_sign"] = asm["_glmc_vec4_sign"];
var _glmc_vec4_sqrt = Module["_glmc_vec4_sqrt"] = asm["_glmc_vec4_sqrt"];
var _glmc_vec4_sub = Module["_glmc_vec4_sub"] = asm["_glmc_vec4_sub"];
var _glmc_vec4_subadd = Module["_glmc_vec4_subadd"] = asm["_glmc_vec4_subadd"];
var _glmc_vec4_subs = Module["_glmc_vec4_subs"] = asm["_glmc_vec4_subs"];
var _glmc_vec4_zero = Module["_glmc_vec4_zero"] = asm["_glmc_vec4_zero"];
var _glmc_vec_add = Module["_glmc_vec_add"] = asm["_glmc_vec_add"];
var _glmc_vec_addadd = Module["_glmc_vec_addadd"] = asm["_glmc_vec_addadd"];
var _glmc_vec_adds = Module["_glmc_vec_adds"] = asm["_glmc_vec_adds"];
var _glmc_vec_angle = Module["_glmc_vec_angle"] = asm["_glmc_vec_angle"];
var _glmc_vec_broadcast = Module["_glmc_vec_broadcast"] = asm["_glmc_vec_broadcast"];
var _glmc_vec_center = Module["_glmc_vec_center"] = asm["_glmc_vec_center"];
var _glmc_vec_clamp = Module["_glmc_vec_clamp"] = asm["_glmc_vec_clamp"];
var _glmc_vec_copy = Module["_glmc_vec_copy"] = asm["_glmc_vec_copy"];
var _glmc_vec_cross = Module["_glmc_vec_cross"] = asm["_glmc_vec_cross"];
var _glmc_vec_distance = Module["_glmc_vec_distance"] = asm["_glmc_vec_distance"];
var _glmc_vec_div = Module["_glmc_vec_div"] = asm["_glmc_vec_div"];
var _glmc_vec_divs = Module["_glmc_vec_divs"] = asm["_glmc_vec_divs"];
var _glmc_vec_dot = Module["_glmc_vec_dot"] = asm["_glmc_vec_dot"];
var _glmc_vec_eq = Module["_glmc_vec_eq"] = asm["_glmc_vec_eq"];
var _glmc_vec_eq_all = Module["_glmc_vec_eq_all"] = asm["_glmc_vec_eq_all"];
var _glmc_vec_eq_eps = Module["_glmc_vec_eq_eps"] = asm["_glmc_vec_eq_eps"];
var _glmc_vec_eqv = Module["_glmc_vec_eqv"] = asm["_glmc_vec_eqv"];
var _glmc_vec_eqv_eps = Module["_glmc_vec_eqv_eps"] = asm["_glmc_vec_eqv_eps"];
var _glmc_vec_flipsign = Module["_glmc_vec_flipsign"] = asm["_glmc_vec_flipsign"];
var _glmc_vec_flipsign_to = Module["_glmc_vec_flipsign_to"] = asm["_glmc_vec_flipsign_to"];
var _glmc_vec_inv = Module["_glmc_vec_inv"] = asm["_glmc_vec_inv"];
var _glmc_vec_inv_to = Module["_glmc_vec_inv_to"] = asm["_glmc_vec_inv_to"];
var _glmc_vec_isinf = Module["_glmc_vec_isinf"] = asm["_glmc_vec_isinf"];
var _glmc_vec_isnan = Module["_glmc_vec_isnan"] = asm["_glmc_vec_isnan"];
var _glmc_vec_isvalid = Module["_glmc_vec_isvalid"] = asm["_glmc_vec_isvalid"];
var _glmc_vec_lerp = Module["_glmc_vec_lerp"] = asm["_glmc_vec_lerp"];
var _glmc_vec_max = Module["_glmc_vec_max"] = asm["_glmc_vec_max"];
var _glmc_vec_maxv = Module["_glmc_vec_maxv"] = asm["_glmc_vec_maxv"];
var _glmc_vec_min = Module["_glmc_vec_min"] = asm["_glmc_vec_min"];
var _glmc_vec_minv = Module["_glmc_vec_minv"] = asm["_glmc_vec_minv"];
var _glmc_vec_mul = Module["_glmc_vec_mul"] = asm["_glmc_vec_mul"];
var _glmc_vec_muladd = Module["_glmc_vec_muladd"] = asm["_glmc_vec_muladd"];
var _glmc_vec_muladds = Module["_glmc_vec_muladds"] = asm["_glmc_vec_muladds"];
var _glmc_vec_mulv = Module["_glmc_vec_mulv"] = asm["_glmc_vec_mulv"];
var _glmc_vec_norm = Module["_glmc_vec_norm"] = asm["_glmc_vec_norm"];
var _glmc_vec_norm2 = Module["_glmc_vec_norm2"] = asm["_glmc_vec_norm2"];
var _glmc_vec_normalize = Module["_glmc_vec_normalize"] = asm["_glmc_vec_normalize"];
var _glmc_vec_normalize_to = Module["_glmc_vec_normalize_to"] = asm["_glmc_vec_normalize_to"];
var _glmc_vec_one = Module["_glmc_vec_one"] = asm["_glmc_vec_one"];
var _glmc_vec_ortho = Module["_glmc_vec_ortho"] = asm["_glmc_vec_ortho"];
var _glmc_vec_proj = Module["_glmc_vec_proj"] = asm["_glmc_vec_proj"];
var _glmc_vec_rotate = Module["_glmc_vec_rotate"] = asm["_glmc_vec_rotate"];
var _glmc_vec_rotate_m3 = Module["_glmc_vec_rotate_m3"] = asm["_glmc_vec_rotate_m3"];
var _glmc_vec_rotate_m4 = Module["_glmc_vec_rotate_m4"] = asm["_glmc_vec_rotate_m4"];
var _glmc_vec_scale = Module["_glmc_vec_scale"] = asm["_glmc_vec_scale"];
var _glmc_vec_scale_as = Module["_glmc_vec_scale_as"] = asm["_glmc_vec_scale_as"];
var _glmc_vec_sign = Module["_glmc_vec_sign"] = asm["_glmc_vec_sign"];
var _glmc_vec_sqrt = Module["_glmc_vec_sqrt"] = asm["_glmc_vec_sqrt"];
var _glmc_vec_sub = Module["_glmc_vec_sub"] = asm["_glmc_vec_sub"];
var _glmc_vec_subadd = Module["_glmc_vec_subadd"] = asm["_glmc_vec_subadd"];
var _glmc_vec_subs = Module["_glmc_vec_subs"] = asm["_glmc_vec_subs"];
var _glmc_vec_zero = Module["_glmc_vec_zero"] = asm["_glmc_vec_zero"];
var _malloc = Module["_malloc"] = asm["_malloc"];
var _memcpy = Module["_memcpy"] = asm["_memcpy"];
var _memset = Module["_memset"] = asm["_memset"];
var _sbrk = Module["_sbrk"] = asm["_sbrk"];
var establishStackSpace = Module["establishStackSpace"] = asm["establishStackSpace"];
var getTempRet0 = Module["getTempRet0"] = asm["getTempRet0"];
var runPostSets = Module["runPostSets"] = asm["runPostSets"];
var setTempRet0 = Module["setTempRet0"] = asm["setTempRet0"];
var setThrew = Module["setThrew"] = asm["setThrew"];
var stackAlloc = Module["stackAlloc"] = asm["stackAlloc"];
var stackRestore = Module["stackRestore"] = asm["stackRestore"];
var stackSave = Module["stackSave"] = asm["stackSave"];
;



// === Auto-generated postamble setup entry stuff ===

Module['asm'] = asm;







































































if (memoryInitializer) {
  if (!isDataURI(memoryInitializer)) {
    if (typeof Module['locateFile'] === 'function') {
      memoryInitializer = Module['locateFile'](memoryInitializer);
    } else if (Module['memoryInitializerPrefixURL']) {
      memoryInitializer = Module['memoryInitializerPrefixURL'] + memoryInitializer;
    }
  }
  if (ENVIRONMENT_IS_NODE || ENVIRONMENT_IS_SHELL) {
    var data = Module['readBinary'](memoryInitializer);
    HEAPU8.set(data, GLOBAL_BASE);
  } else {
    addRunDependency('memory initializer');
    var applyMemoryInitializer = function(data) {
      if (data.byteLength) data = new Uint8Array(data);
      HEAPU8.set(data, GLOBAL_BASE);
      // Delete the typed array that contains the large blob of the memory initializer request response so that
      // we won't keep unnecessary memory lying around. However, keep the XHR object itself alive so that e.g.
      // its .status field can still be accessed later.
      if (Module['memoryInitializerRequest']) delete Module['memoryInitializerRequest'].response;
      removeRunDependency('memory initializer');
    }
    function doBrowserLoad() {
      Module['readAsync'](memoryInitializer, applyMemoryInitializer, function() {
        throw 'could not load memory initializer ' + memoryInitializer;
      });
    }
    var memoryInitializerBytes = tryParseAsDataURI(memoryInitializer);
    if (memoryInitializerBytes) {
      applyMemoryInitializer(memoryInitializerBytes.buffer);
    } else
    if (Module['memoryInitializerRequest']) {
      // a network request has already been created, just use that
      function useRequest() {
        var request = Module['memoryInitializerRequest'];
        var response = request.response;
        if (request.status !== 200 && request.status !== 0) {
          var data = tryParseAsDataURI(Module['memoryInitializerRequestURL']);
          if (data) {
            response = data.buffer;
          } else {
            // If you see this warning, the issue may be that you are using locateFile or memoryInitializerPrefixURL, and defining them in JS. That
            // means that the HTML file doesn't know about them, and when it tries to create the mem init request early, does it to the wrong place.
            // Look in your browser's devtools network console to see what's going on.
            console.warn('a problem seems to have happened with Module.memoryInitializerRequest, status: ' + request.status + ', retrying ' + memoryInitializer);
            doBrowserLoad();
            return;
          }
        }
        applyMemoryInitializer(response);
      }
      if (Module['memoryInitializerRequest'].response) {
        setTimeout(useRequest, 0); // it's already here; but, apply it asynchronously
      } else {
        Module['memoryInitializerRequest'].addEventListener('load', useRequest); // wait for it
      }
    } else {
      // fetch it from the network ourselves
      doBrowserLoad();
    }
  }
}



/**
 * @constructor
 * @extends {Error}
 * @this {ExitStatus}
 */
function ExitStatus(status) {
  this.name = "ExitStatus";
  this.message = "Program terminated with exit(" + status + ")";
  this.status = status;
};
ExitStatus.prototype = new Error();
ExitStatus.prototype.constructor = ExitStatus;

var initialStackTop;
var calledMain = false;

dependenciesFulfilled = function runCaller() {
  // If run has never been called, and we should call run (INVOKE_RUN is true, and Module.noInitialRun is not false)
  if (!Module['calledRun']) run();
  if (!Module['calledRun']) dependenciesFulfilled = runCaller; // try this again later, after new deps are fulfilled
}





/** @type {function(Array=)} */
function run(args) {
  args = args || Module['arguments'];

  if (runDependencies > 0) {
    return;
  }


  preRun();

  if (runDependencies > 0) return; // a preRun added a dependency, run will be called later
  if (Module['calledRun']) return; // run may have just been called through dependencies being fulfilled just in this very frame

  function doRun() {
    if (Module['calledRun']) return; // run may have just been called while the async setStatus time below was happening
    Module['calledRun'] = true;

    if (ABORT) return;

    ensureInitRuntime();

    preMain();

    if (Module['onRuntimeInitialized']) Module['onRuntimeInitialized']();


    postRun();
  }

  if (Module['setStatus']) {
    Module['setStatus']('Running...');
    setTimeout(function() {
      setTimeout(function() {
        Module['setStatus']('');
      }, 1);
      doRun();
    }, 1);
  } else {
    doRun();
  }
}
Module['run'] = run;


function exit(status, implicit) {

  // if this is just main exit-ing implicitly, and the status is 0, then we
  // don't need to do anything here and can just leave. if the status is
  // non-zero, though, then we need to report it.
  // (we may have warned about this earlier, if a situation justifies doing so)
  if (implicit && Module['noExitRuntime'] && status === 0) {
    return;
  }

  if (Module['noExitRuntime']) {
  } else {

    ABORT = true;
    EXITSTATUS = status;
    STACKTOP = initialStackTop;

    exitRuntime();

    if (Module['onExit']) Module['onExit'](status);
  }

  if (ENVIRONMENT_IS_NODE) {
    process['exit'](status);
  }
  Module['quit'](status, new ExitStatus(status));
}
Module['exit'] = exit;

var abortDecorators = [];

function abort(what) {
  if (Module['onAbort']) {
    Module['onAbort'](what);
  }

  if (what !== undefined) {
    Module.print(what);
    Module.printErr(what);
    what = JSON.stringify(what)
  } else {
    what = '';
  }

  ABORT = true;
  EXITSTATUS = 1;

  throw 'abort(' + what + '). Build with -s ASSERTIONS=1 for more info.';
}
Module['abort'] = abort;

// {{PRE_RUN_ADDITIONS}}

if (Module['preInit']) {
  if (typeof Module['preInit'] == 'function') Module['preInit'] = [Module['preInit']];
  while (Module['preInit'].length > 0) {
    Module['preInit'].pop()();
  }
}


Module["noExitRuntime"] = true;

run();

// {{POST_RUN_ADDITIONS}}





// {{MODULE_ADDITIONS}}






