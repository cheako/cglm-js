ifelse(`
	First letters:
		r Function has return
		d last param to function is destination, delete this
		v function uses first param as destination, return this
')dnl
deffunc(d:mat4:mat4:mat4, mul, mul, mul, ``this is similar to glm_mat4_mul but specialized to affine transform'')dnl
deffunc(d:mat4:mat4:mat4, mul_rot, mul_rot, mul_rot, ``this is similar to glm_mat4_mul but specialized to affine transform'')dnl
deffunc(v:mat4, inv_tr, inv_tr, inv_tr, ``inverse orthonormal rotation + translation matrix ridig-body)'')dnl
deffunc(v:mat4:vec3, translate, translate, translate, ``translate existing transform matrix by v vector * and stores result in same matrix'')dnl
deffunc(d:mat4:vec3:mat4, translate_to, translate_to, translate_to, ``translate existing transform matrix by v vector * and store result in dest'')dnl
deffunc(v:mat4:float, translate_x, translate_x, translate_x, ``translate existing transform matrix by x factor'')dnl
deffunc(v:mat4:float, translate_y, translate_y, translate_y, ``translate existing transform matrix by y factor'')dnl
deffunc(v:mat4:float, translate_z, translate_z, translate_z, ``translate existing transform matrix by z factor'')dnl
deffunc(v:mat4:vec3, translate_make, translate_make, translate_make, ``creates NEW translate transform matrix by v vector'')dnl
deffunc(d:mat4:vec3:mat4, scale_to, scale_to, scale_to, ``scale existing transform matrix by v vector * and store result in dest'')dnl
deffunc(v:mat4:vec3, scale_make, scale_make, scale_make, ``creates NEW scale matrix by v vector'')dnl
deffunc(v:mat4:vec3, scale, scale, scale, ``scales existing transform matrix by v vector * and stores result in same matrix'')dnl
deffunc(v:mat4:float, scale_uni, scale_uni, scale_uni, ``applies uniform scale to existing transform matrix v = [s, s, s] * and stores result in same matrix'')dnl
deffunc(d:mat4:float:mat4, rotate_x, rotate_x, rotate_x, ``rotate existing transform matrix around X axis by angle * and store result in dest'')dnl
deffunc(d:mat4:float:mat4, rotate_y, rotate_y, rotate_y, ``rotate existing transform matrix around Y axis by angle * and store result in dest'')dnl
deffunc(d:mat4:float:mat4, rotate_z, rotate_z, rotate_z, ``rotate existing transform matrix around Z axis by angle * and store result in dest'')dnl
deffunc(v:mat4:float:vec3, rotate_make, rotate_make, rotate_make, ``creates NEW rotation matrix by angle and axis'')dnl
deffunc(v:mat4:float:vec3, rotate, rotate, rotate, ``rotate existing transform matrix around given axis by angle'')dnl
deffunc(v:mat4:vec3:float:vec3, rotate_at, rotate_at, rotate_at, ``rotate existing transform * around given axis by angle at given pivot point rotation center)'')dnl
deffunc(v:mat4:vec3:float:vec3, rotate_atm, rotate_atm, rotate_atm, ``creates NEW rotation matrix by angle and axis at given point'')dnl
deffunc(v:mat4:vec3, decompose_scalev, decompose_scalev, decompose_scalev, ``decompose scale vector'')dnl
deffunc(r:mat4, uniscaled, uniscaled, uniscaled, ``returns true if matrix is uniform scaled. This is helpful for * creating normal matrix.'')dnl
deffunc(v:mat4:mat4:vec3, decompose_rs, decompose_rs, decompose_rs, ``decompose rotation matrix mat4) and scale vector [Sx, Sy, Sz] * DON'T pass projected matrix here'')dnl
deffunc(v:mat4:vec4:mat4:vec3, decompose, decompose, decompose, ``decompose affine transform, TODO: extract shear factors. * DON'T pass projected matrix here'')dnl
deffunc(d:vec3:mat4:vec3, aabb_transform, aabb_transform, aabb_transform, ``apply transform to Axis-Aligned Bounding Box'')dnl
deffunc(d:vec3:vec3:vec3, aabb_merge, aabb_merge, aabb_merge, ``merges two AABB bounding box and creates new one'')dnl
deffunc(d:vec3:vec3:vec3, aabb_crop, aabb_crop, aabb_crop, ``crops a bounding box with another one.'')dnl
deffunc(d:vec3:vec3:vec3:vec3, aabb_crop_until, aabb_crop_until, aabb_crop_until, ``crops a bounding box with another one.'')dnl
deffunc(r:vec3:vec4, aabb_frustum, aabb_frustum, aabb_frustum, ``check if AABB intersects with frustum planes'')dnl
deffunc(v:vec3, aabb_invalidate, aabb_invalidate, aabb_invalidate, ``invalidate AABB min and max values'')dnl
deffunc(r:vec3, aabb_isvalid, aabb_isvalid, aabb_isvalid, ``check if AABB is valid or not'')dnl
deffunc(r:vec3, aabb_size, aabb_size, aabb_size, ``distance between of min and max'')dnl
deffunc(r:vec3, aabb_radius, aabb_radius, aabb_radius, ``radius of sphere which surrounds AABB'')dnl
deffunc(d:vec3:vec3, aabb_center, aabb_center, aabb_center, ``computes center point of AABB'')dnl
deffunc(d:float:float:float:float:float:float:mat4, frustum, frustum, frustum, ``set up perspective peprojection matrix'')dnl
deffunc(d:float:float:float:float:float:float:mat4, ortho, ortho, ortho, ``set up orthographic projection matrix'')dnl
deffunc(d:vec3:mat4, ortho_aabb, ortho_aabb, ortho_aabb, ``set up orthographic projection matrix using bounding box'')dnl
deffunc(d:vec3:float:mat4, ortho_aabb_p, ortho_aabb_p, ortho_aabb_p, ``set up orthographic projection matrix using bounding box'')dnl
deffunc(d:vec3:float:mat4, ortho_aabb_pz, ortho_aabb_pz, ortho_aabb_pz, ``set up orthographic projection matrix using bounding box'')dnl
deffunc(d:float:mat4, ortho_default, ortho_default, ortho_default, ``set up unit orthographic projection matrix'')dnl
deffunc(d:float:float:mat4, ortho_default_s, ortho_default_s, ortho_default_s, ``set up orthographic projection matrix with given CUBE size'')dnl
deffunc(d:float:float:float:float:mat4, perspective, perspective, perspective, ``set up perspective projection matrix'')dnl
deffunc(d:float:mat4, perspective_default, perspective_default, perspective_default, ``set up perspective projection matrix with default near/far * and angle values'')dnl
deffunc(v:float:mat4, perspective_resize, perspective_resize, perspective_resize, ``resize perspective matrix by aspect ratio width / height ) * this makes very easy to resize proj matrix when window /viewport * reized'')dnl
deffunc(d:vec3:vec3:vec3:mat4, lookat, lookat, lookat, ``set up view matrix'')dnl
deffunc(d:vec3:vec3:vec3:mat4, look, look, look, ``set up view matrix'')dnl
deffunc(d:vec3:vec3:mat4, look_anyup, look_anyup, look_anyup, ``set up view matrix'')dnl
deffunc(v:mat4:float:__restrict:float:__restrict:float:__restrict:float:__restrict:float:__restrict:float:__restrict, persp_decomp, persp_decomp, persp_decomp, ``decomposes frustum values of perspective projection.'')dnl
deffunc(d:mat4:float, persp_decompv, persp_decompv, persp_decompv, ``decomposes frustum values of perspective projection. * this makes easy to get all values at once'')dnl
deffunc(v:mat4:float:__restrict:float:__restrict, persp_decomp_x, persp_decomp_x, persp_decomp_x, ``decomposes left and right values of perspective projection. * x stands for x axis left / right axis)'')dnl
deffunc(v:mat4:float:__restrict:float:__restrict, persp_decomp_y, persp_decomp_y, persp_decomp_y, ``decomposes top and bottom values of perspective projection. * y stands for y axis top / botom axis)'')dnl
deffunc(v:mat4:float:__restrict:float:__restrict, persp_decomp_z, persp_decomp_z, persp_decomp_z, ``decomposes near and far values of perspective projection. * z stands for z axis near / far axis)'')dnl
deffunc(v:mat4:float:__restrict, persp_decomp_far, persp_decomp_far, persp_decomp_far, ``decomposes far value of perspective projection.'')dnl
deffunc(v:mat4:float:__restrict, persp_decomp_near, persp_decomp_near, persp_decomp_near, ``decomposes near value of perspective projection.'')dnl
deffunc(r:mat4, persp_fovy, persp_fovy, persp_fovy, ``returns field of view angle along the Y-axis in radians)'')dnl
deffunc(r:mat4, persp_aspect, persp_aspect, persp_aspect, ``returns aspect ratio of perspective projection'')dnl
deffunc(d:mat4:float:vec4, persp_sizes, persp_sizes, persp_sizes, ``returns sizes of near and far planes of perspective projection'')dnl
deffunc(r:vec3, luminance, luminance, luminance, ``averages the color channels into one value'')dnl
deffunc(r:int, euler_order, euler_order, euler_order, ``averages the color channels into one value'')dnl
deffunc(d:mat4:vec3, euler_angles, euler_angles, euler_angles, ``extract euler angles in radians) using xyz order'')dnl
deffunc(d:vec3:mat4, euler_xyz, euler_xyz, euler_xyz, ``build rotation matrix from euler angles'')dnl
deffunc(d:vec3:mat4, euler, euler, euler, ``build rotation matrix from euler angles'')dnl
deffunc(d:vec3:mat4, euler_xzy, euler_xzy, euler_xzy, ``build rotation matrix from euler angles'')dnl
deffunc(d:vec3:mat4, euler_yxz, euler_yxz, euler_yxz, ``build rotation matrix from euler angles'')dnl
deffunc(d:vec3:mat4, euler_yzx, euler_yzx, euler_yzx, ``build rotation matrix from euler angles'')dnl
deffunc(d:vec3:mat4, euler_zxy, euler_zxy, euler_zxy, ``build rotation matrix from euler angles'')dnl
deffunc(d:vec3:mat4, euler_zyx, euler_zyx, euler_zyx, ``build rotation matrix from euler angles'')dnl
deffunc(d:vec3:glm_euler_sq:mat4, euler_by_order, euler_by_order, euler_by_order, ``build rotation matrix from euler angles'')dnl
deffunc(d:mat4:vec4, frustum_planes, frustum_planes, frustum_planes, ``extracts view frustum planes'')dnl
deffunc(d:mat4:vec4, frustum_corners, frustum_corners, frustum_corners, ``extracts view frustum corners using clip-space coordinates'')dnl
deffunc(d:vec4:vec4, frustum_center, frustum_center, frustum_center, ``finds center of view frustum'')dnl
deffunc(v:vec4:mat4:vec3, frustum_box, frustum_box, frustum_box, ``finds bounding box of frustum relative to given matrix e.g. view mat'')dnl
deffunc(v:vec4:float:float:vec4, frustum_corners_at, frustum_corners_at, frustum_corners_at, ``finds planes corners which is between near and far planes parallel)'')dnl
deffunc(v:mat4:FILE:__restrict, mat4_print, mat4_print, print, ``finds planes corners which is between near and far planes parallel)'')dnl
deffunc(v:mat3:FILE:__restrict, mat3_print, mat3_print, print, ``finds planes corners which is between near and far planes parallel)'')dnl
deffunc(v:vec4:FILE:__restrict, vec4_print, vec4_print, print, ``finds planes corners which is between near and far planes parallel)'')dnl
deffunc(v:vec3:FILE:__restrict, vec3_print, vec3_print, vec3_print, ``finds planes corners which is between near and far planes parallel)'')dnl
deffunc(v:ivec3:FILE:__restrict, ivec3_print, ivec3_print, print, ``finds planes corners which is between near and far planes parallel)'')dnl
deffunc(v:versor:FILE:__restrict, versor_print, versor_print, print, ``finds planes corners which is between near and far planes parallel)'')dnl
deffunc(v:vec3:const:*:tag,:FILE:__restrict, aabb_print, aabb_print, aabb_print, ``finds planes corners which is between near and far planes parallel)'')dnl
deffunc(d:mat3:mat3, mat3_copy, mat3_copy, copy, ``copy all members of [mat] to [dest]'')dnl
deffunc(v:mat3, mat3_identity, mat3_identity, identity, ``make given matrix identity. It is identical with below, * but it is more easy to do that with this func especially for members * e.g. glm_mat3_identity aStruct->aMatrix);'')dnl
deffunc(d:mat3:mat3:mat3, mat3_mul, mat3_mul, mul, ``multiply m1 and m2 to dest'')dnl
deffunc(d:mat3:mat3, mat3_transpose_to, mat3_transpose_to, transpose_to, ``transpose mat3 and store in dest'')dnl
deffunc(v:mat3, mat3_transpose, mat3_transpose, transpose, ``tranpose mat3 and store result in same matrix'')dnl
deffunc(d:mat3:vec3:vec3, mat3_mulv, mat3_mulv, mulv, ``multiply mat3 with vec3 column vector) and store in dest vector'')dnl
deffunc(d:mat3:versor, mat3_quat, mat3_quat, quat, ``convert mat3 to quaternion'')dnl
deffunc(v:mat3:float, mat3_scale, mat3_scale, scale, ``scale multiply with scalar) matrix'')dnl
deffunc(r:mat3, mat3_det, mat3_det, det, ``mat3 determinant'')dnl
deffunc(d:mat3:mat3, mat3_inv, mat3_inv, inv, ``inverse mat3 and store in dest'')dnl
deffunc(v:mat3:int:int, mat3_swap_col, mat3_swap_col, swap_col, ``swap two matrix columns'')dnl
deffunc(v:mat3:int:int, mat3_swap_row, mat3_swap_row, swap_row, ``swap two matrix rows'')dnl
deffunc(d:mat4:mat4, mat4_ucopy, mat4_ucopy, ucopy, ``copy all members of [mat] to [dest]'')dnl
deffunc(d:mat4:mat4, mat4_copy, mat4_copy, copy, ``copy all members of [mat] to [dest]'')dnl
deffunc(v:mat4, mat4_identity, mat4_identity, identity, ``make given matrix identity. It is identical with below,  * but it is more easy to do that with this func especially for members * e.g. glm_mat4_identity aStruct->aMatrix);'')dnl
deffunc(d:mat4:mat3, mat4_pick3, mat4_pick3, pick3, ``copy upper-left of mat4 to mat3'')dnl
deffunc(d:mat4:mat3, mat4_pick3t, mat4_pick3t, pick3t, ``copy upper-left of mat4 to mat3 transposed)'')dnl
deffunc(d:mat3:mat4, mat4_ins3, mat4_ins3, mat4_ins3, ``copy mat3 to mat4's upper-left'')dnl
deffunc(d:mat4:mat4:mat4, mat4_mul, mat4_mul, mul_extra, ``multiply m1 and m2 to dest'')dnl
deffunc(d:mat4:__restrict:uint32_t:mat4, mat4_mulN, mat4_mulN, mulN, ``mupliply N mat4 matrices and store result in dest'')dnl
deffunc(d:mat4:vec4:vec4, mat4_mulv, mat4_mulv, mulv, ``multiply mat4 with vec4 column vector) and store in dest vector'')dnl
deffunc(d:mat4:versor, mat4_quat, mat4_quat, quat, ``convert mat4's rotation part to quaternion'')dnl
deffunc(d:mat4:vec3:float:vec3, mat4_mulv3, mat4_mulv3, mulv3, ``multiply vector with mat4'')dnl
deffunc(d:mat4:mat4, mat4_transpose_to, mat4_transpose_to, transpose_to, ``transpose mat4 and store in dest'')dnl
deffunc(v:mat4, mat4_transpose, mat4_transpose, transpose, ``tranpose mat4 and store result in same matrix'')dnl
deffunc(v:mat4:float, mat4_scale_p, mat4_scale_p, scale_p, ``scale multiply with scalar) matrix without simd optimization'')dnl
deffunc(v:mat4:float, mat4_scale, mat4_scale, scale_extra, ``scale multiply with scalar) matrix'')dnl
deffunc(r:mat4, mat4_det, mat4_det, det, ``mat4 determinant'')dnl
deffunc(d:mat4:mat4, mat4_inv, mat4_inv, inv, ``inverse mat4 and store in dest'')dnl
deffunc(d:mat4:mat4, mat4_inv_fast, mat4_inv_fast, inv_fast, ``inverse mat4 and store in dest'')dnl
deffunc(v:mat4:int:int, mat4_swap_col, mat4_swap_col, swap_col, ``swap two matrix columns'')dnl
deffunc(v:mat4:int:int, mat4_swap_row, mat4_swap_row, swap_row, ``swap two matrix rows'')dnl
deffunc(v:vec4, plane_normalize, plane_normalize, plane_normalize, ``normalizes a plane'')dnl
deffunc(d:vec3:mat4:vec4:vec3, unprojecti, unprojecti, unprojecti, ``maps the specified viewport coordinates into specified space [1] * the matrix should contain projection matrix.'')dnl
deffunc(d:vec3:mat4:vec4:vec3, unproject, unproject, unproject, ``maps the specified viewport coordinates into specified space [1] * the matrix should contain projection matrix.'')dnl
deffunc(d:vec3:mat4:vec4:vec3, project, project, project, ``map object coordinates to window coordinates'')dnl
deffunc(v:versor, quat_identity, quat_identity, quat_identity, ``makes given quat to identity'')dnl
deffunc(v:versor:float:float:float:float, quat_init, quat_init, quat_init, ``inits quaterion with raw values'')dnl
deffunc(v:versor:float:vec3, quatv, quatv, quatv, ``creates NEW quaternion with axis vector'')dnl
deffunc(v:versor:float:float:float:float, quat, quat, quat, ``creates NEW quaternion with individual axis components'')dnl
deffunc(d:versor:versor, quat_copy, quat_copy, quat_copy, ``copy quaternion to another one'')dnl
deffunc(r:versor, quat_norm, quat_norm, quat_norm, ``returns norm magnitude) of quaternion'')dnl
deffunc(d:versor:versor, quat_normalize_to, quat_normalize_to, quat_normalize_to, ``normalize quaternion and store result in dest'')dnl
deffunc(v:versor, quat_normalize, quat_normalize, quat_normalize, ``normalize quaternion'')dnl
deffunc(r:versor:versor, quat_dot, quat_dot, quat_dot, ``dot product of two quaternion'')dnl
deffunc(d:versor:versor, quat_conjugate, quat_conjugate, quat_conjugate, ``conjugate of quaternion'')dnl
deffunc(d:versor:versor, quat_inv, quat_inv, quat_inv, ``inverse of non-zero quaternion'')dnl
deffunc(d:versor:versor:versor, quat_add, quat_add, quat_add, ``add componentwise) two quaternions and store result in dest'')dnl
deffunc(d:versor:versor:versor, quat_sub, quat_sub, quat_sub, ``subtract componentwise) two quaternions and store result in dest'')dnl
deffunc(r:versor, quat_real, quat_real, quat_real, ``returns real part of quaternion'')dnl
deffunc(d:versor:vec3, quat_imag, quat_imag, quat_imag, ``returns imaginary part of quaternion'')dnl
deffunc(d:versor:vec3, quat_imagn, quat_imagn, quat_imagn, ``returns normalized imaginary part of quaternion'')dnl
deffunc(r:versor, quat_imaglen, quat_imaglen, quat_imaglen, ``returns length of imaginary part of quaternion'')dnl
deffunc(r:versor, quat_angle, quat_angle, quat_angle, ``returns angle of quaternion'')dnl
deffunc(d:versor:versor, quat_axis, quat_axis, quat_axis, ``axis of quaternion'')dnl
deffunc(d:versor:versor:versor, quat_mul, quat_mul, quat_mul, ``multiplies two quaternion and stores result in dest * this is also called Hamilton Product'')dnl
deffunc(d:versor:mat4, quat_mat4, quat_mat4, quat_mat4, ``convert quaternion to mat4'')dnl
deffunc(d:versor:mat4, quat_mat4t, quat_mat4t, quat_mat4t, ``convert quaternion to mat4 transposed)'')dnl
deffunc(d:versor:mat3, quat_mat3, quat_mat3, quat_mat3, ``convert quaternion to mat3'')dnl
deffunc(d:versor:mat3, quat_mat3t, quat_mat3t, quat_mat3t, ``convert quaternion to mat3 transposed)'')dnl
deffunc(d:versor:versor:float:versor, quat_lerp, quat_lerp, quat_lerp, ``interpolates between two quaternions * using linear interpolation LERP)'')dnl
deffunc(d:versor:versor:float:versor, quat_slerp, quat_slerp, quat_slerp, ``interpolates between two quaternions * using spherical linear interpolation SLERP)'')dnl
deffunc(d:vec3:versor:mat4, quat_look, quat_look, quat_look, ``creates view matrix using quaternion as camera orientation'')dnl
deffunc(d:vec3:vec3:vec3:versor, quat_for, quat_for, quat_for, ``creates look rotation quaternion'')dnl
deffunc(d:vec3:vec3:vec3:vec3:versor, quat_forp, quat_forp, quat_forp, ``creates look rotation quaternion using source and * destination positions p suffix stands for position'')dnl
deffunc(d:versor:vec3:vec3, quat_rotatev, quat_rotatev, quat_rotatev, ``rotate vector using using quaternion'')dnl
deffunc(d:mat4:versor:mat4, quat_rotate, quat_rotate, quat_rotate, ``rotate existing transform matrix using quaternion'')dnl
deffunc(v:mat4:versor:vec3, quat_rotate_at, quat_rotate_at, quat_rotate_at, ``rotate existing transform matrix using quaternion at pivot point'')dnl
deffunc(v:mat4:versor:vec3, quat_rotate_atm, quat_rotate_atm, quat_rotate_atm, ``rotate NEW transform matrix using quaternion at pivot point'')dnl
deffunc(r:int, sign, sign, sign, ``get sign of 32 bit integer as +1, -1, 0'')dnl
deffunc(r:float, signf, signf, signf, ``get sign of 32 bit float as +1, -1, 0'')dnl
deffunc(r:float, rad, rad, rad, ``convert degree to radians'')dnl
deffunc(r:float, deg, deg, deg, ``convert radians to degree'')dnl
deffunc(v:float, make_rad, make_rad, make_rad, ``convert exsisting degree to radians. this will override degrees value'')dnl
deffunc(v:float, make_deg, make_deg, make_deg, ``convert exsisting radians to degree. this will override radians value'')dnl
deffunc(r:float, pow2, pow2, pow2, ``multiplies given parameter with itself = x * x or powf x, 2)'')dnl
deffunc(r:float:float, min, min, min, ``find minimum of given two values'')dnl
deffunc(r:float:float, max, max, max, ``find maximum of given two values'')dnl
deffunc(r:float:float:float, clamp, clamp, clamp, ``clamp a number between min and max'')dnl
deffunc(r:float:float:float, lerp, lerp, lerp, ``linear interpolation between two number'')dnl
deffunc(v:vec3:vec3:vec3, vec_mulv, vec_mulv, mulv, ``DEPRECATED! use glm_vec_mul'')dnl
deffunc(v:float:vec3, vec_broadcast, vec_broadcast, vec_broadcast, ``fill a vector with specified value'')dnl
deffunc(r:vec3:float, vec_eq, vec_eq, eq, ``check if vector is equal to value without epsilon)'')dnl
deffunc(r:vec3:float, vec_eq_eps, vec_eq_eps, eq_eps, ``check if vector is equal to value with epsilon)'')dnl
deffunc(r:vec3, vec_eq_all, vec_eq_all, eq_all, ``check if vectors members are equal without epsilon)'')dnl
deffunc(r:vec3:vec3, vec_eqv, vec_eqv, eqv, ``check if vector is equal to another without epsilon)'')dnl
deffunc(r:vec3:vec3, vec_eqv_eps, vec_eqv_eps, eqv_eps, ``check if vector is equal to another with epsilon)'')dnl
deffunc(r:vec3, vec_max, vec_max, max, ``max value of vector'')dnl
deffunc(r:vec3, vec_min, vec_min, min, ``min value of vector'')dnl
deffunc(r:vec3, vec_isnan, vec_isnan, isnan, ``check if all items are NaN not a number) * you should only use this in DEBUG mode or very critical asserts'')dnl
deffunc(r:vec3, vec_isinf, vec_isinf, isinf, ``check if all items are INFINITY * you should only use this in DEBUG mode or very critical asserts'')dnl
deffunc(r:vec3, vec_isvalid, vec_isvalid, isvalid, ``check if all items are valid number * you should only use this in DEBUG mode or very critical asserts'')dnl
deffunc(d:vec3:vec3, vec_sign, vec_sign, sign, ``get sign of 32 bit float as +1, -1, 0'')dnl
deffunc(d:vec3:vec3, vec_sqrt, vec_sqrt, sqrt, ``square root of each vector item'')dnl
deffunc(d:vec4:vec3, vec3, vec3, vec3, ``init vec3 using vec4'')dnl
deffunc(d:vec3:vec3, vec_copy, vec_copy, copy, ``copy all members of [a] to [dest]'')dnl
deffunc(v:vec3, vec_zero, vec_zero, zero, ``make vector zero'')dnl
deffunc(v:vec3, vec_one, vec_one, one, ``make vector one'')dnl
deffunc(r:vec3:vec3, vec_dot, vec_dot, dot, ``vec3 dot product'')dnl
deffunc(v:vec3:vec3:vec3, vec_cross, vec_cross, cross, ``vec3 cross product'')dnl
deffunc(r:vec3, vec_norm2, vec_norm2, norm2, ``norm * norm magnitude) of vec'')dnl
deffunc(r:vec3, vec_norm, vec_norm, norm, ``norm magnitude) of vec3'')dnl
deffunc(d:vec3:vec3:vec3, vec_add, vec_add, add, ``add a vector to b vector store result in dest'')dnl
deffunc(d:vec3:float:vec3, vec_adds, vec_adds, adds, ``add scalar to v vector store result in dest d = v + s)'')dnl
deffunc(d:vec3:vec3:vec3, vec_sub, vec_sub, sub, ``subtract v2 vector from v1 vector store result in dest'')dnl
deffunc(d:vec3:float:vec3, vec_subs, vec_subs, subs, ``subtract scalar from v vector store result in dest d = v - s)'')dnl
deffunc(v:vec3:vec3:vec3, vec_mul, vec_mul, mul, ``multiply two vector component-wise multiplication)'')dnl
deffunc(d:vec3:float:vec3, vec_scale, vec_scale, scale, ``multiply/scale vec3 vector with scalar: result = v * s'')dnl
deffunc(d:vec3:float:vec3, vec_scale_as, vec_scale_as, scale_as, ``make vec3 vector scale as specified: result = unit v) * s'')dnl
deffunc(d:vec3:vec3:vec3, vec_div, vec_div, div, ``div vector with another component-wise division: d = a / b'')dnl
deffunc(d:vec3:float:vec3, vec_divs, vec_divs, divs, ``div vector with scalar: d = v / s'')dnl
deffunc(d:vec3:vec3:vec3, vec_addadd, vec_addadd, addadd, ``add two vectors and add result to sum'')dnl
deffunc(d:vec3:vec3:vec3, vec_subadd, vec_subadd, subadd, ``sub two vectors and add result to dest'')dnl
deffunc(d:vec3:vec3:vec3, vec_muladd, vec_muladd, muladd, ``mul two vectors and add result to dest'')dnl
deffunc(d:vec3:float:vec3, vec_muladds, vec_muladds, muladds, ``mul vector with scalar and add result to sum'')dnl
deffunc(v:vec3, vec_flipsign, vec_flipsign, flipsign, ``flip sign of all vec3 members'')dnl
deffunc(d:vec3:vec3, vec_flipsign_to, vec_flipsign_to, flipsign_to, ``flip sign of all vec3 members and store result in dest'')dnl
deffunc(v:vec3, vec_inv, vec_inv, inv, ``make vector as inverse/opposite of itself'')dnl
deffunc(d:vec3:vec3, vec_inv_to, vec_inv_to, inv_to, ``inverse/opposite vector'')dnl
deffunc(v:vec3, vec_normalize, vec_normalize, normalize, ``normalize vec3 and store result in same vec'')dnl
deffunc(d:vec3:vec3, vec_normalize_to, vec_normalize_to, normalize_to, ``normalize vec3 to dest'')dnl
deffunc(r:vec3:vec3, vec_angle, vec_angle, angle, ``angle betwen two vector'')dnl
deffunc(v:vec3:float:vec3, vec_rotate, vec_rotate, rotate, ``rotate vec3 around axis by angle using Rodrigues' rotation formula'')dnl
deffunc(d:mat4:vec3:vec3, vec_rotate_m4, vec_rotate_m4, vec_rotate_m4, ``apply rotation matrix to vector'')dnl
deffunc(d:mat3:vec3:vec3, vec_rotate_m3, vec_rotate_m3, vec_rotate_m3, ``apply rotation matrix to vector'')dnl
deffunc(d:vec3:vec3:vec3, vec_proj, vec_proj, proj, ``project a vector onto b vector'')dnl
deffunc(d:vec3:vec3:vec3, vec_center, vec_center, center, ``find center point of two vector'')dnl
deffunc(r:vec3:vec3, vec_distance, vec_distance, distance, ``distance between two vectors'')dnl
deffunc(d:vec3:vec3:vec3, vec_maxv, vec_maxv, maxv, ``max values of vectors'')dnl
deffunc(d:vec3:vec3:vec3, vec_minv, vec_minv, minv, ``min values of vectors'')dnl
deffunc(d:vec3:vec3, vec_ortho, vec_ortho, ortho, ``possible orthogonal/perpendicular vector'')dnl
deffunc(v:vec3:float:float, vec_clamp, vec_clamp, clamp, ``clamp vector's individual members between min and max values'')dnl
deffunc(d:vec3:vec3:float:vec3, vec_lerp, vec_lerp, lerp, ``linear interpolation between two vector'')dnl
deffunc(v:vec3:vec3:vec3, cross, cross, cross_extra, ``vec3 cross product'')dnl
deffunc(r:vec3:vec3, dot, dot, dot_extra, ``vec3 dot product'')dnl
deffunc(v:vec3, normalize, normalize, normalize_extra, ``normalize vec3 and store result in same vec'')dnl
deffunc(d:vec3:vec3, normalize_to, normalize_to, normalize_to_extra, ``normalize vec3 to dest'')dnl
deffunc(v:vec4:vec4:vec4, vec4_mulv, vec4_mulv, mulv, ``DEPRECATED! use glm_vec4_mul'')dnl
deffunc(v:float:vec4, vec4_broadcast, vec4_broadcast, vec4_broadcast, ``fill a vector with specified value'')dnl
deffunc(r:vec4:float, vec4_eq, vec4_eq, eq, ``check if vector is equal to value without epsilon)'')dnl
deffunc(r:vec4:float, vec4_eq_eps, vec4_eq_eps, eq_eps, ``check if vector is equal to value with epsilon)'')dnl
deffunc(r:vec4, vec4_eq_all, vec4_eq_all, eq_all, ``check if vectors members are equal without epsilon)'')dnl
deffunc(r:vec4:vec4, vec4_eqv, vec4_eqv, eqv, ``check if vector is equal to another without epsilon)'')dnl
deffunc(r:vec4:vec4, vec4_eqv_eps, vec4_eqv_eps, eqv_eps, ``check if vector is equal to another with epsilon)'')dnl
deffunc(r:vec4, vec4_max, vec4_max, max, ``max value of vector'')dnl
deffunc(r:vec4, vec4_min, vec4_min, min, ``min value of vector'')dnl
deffunc(r:vec4, vec4_isnan, vec4_isnan, isnan, ``check if one of items is NaN not a number) * you should only use this in DEBUG mode or very critical asserts'')dnl
deffunc(r:vec4, vec4_isinf, vec4_isinf, isinf, ``check if one of items is INFINITY * you should only use this in DEBUG mode or very critical asserts'')dnl
deffunc(r:vec4, vec4_isvalid, vec4_isvalid, isvalid, ``check if all items are valid number * you should only use this in DEBUG mode or very critical asserts'')dnl
deffunc(d:vec4:vec4, vec4_sign, vec4_sign, sign, ``get sign of 32 bit float as +1, -1, 0'')dnl
deffunc(d:vec4:vec4, vec4_sqrt, vec4_sqrt, sqrt, ``square root of each vector item'')dnl
deffunc(d:vec3:float:vec4, vec4, vec4, vec4, ``init vec4 using vec3'')dnl
deffunc(d:vec4:vec3, vec4_copy3, vec4_copy3, copy3, ``copy first 3 members of [a] to [dest]'')dnl
deffunc(d:vec4:vec4, vec4_copy, vec4_copy, copy, ``copy all members of [a] to [dest]'')dnl
deffunc(v:vec4, vec4_zero, vec4_zero, zero, ``make vector zero'')dnl
deffunc(v:vec4, vec4_one, vec4_one, one, ``make vector one'')dnl
deffunc(r:vec4:vec4, vec4_dot, vec4_dot, dot, ``vec4 dot product'')dnl
deffunc(r:vec4, vec4_norm2, vec4_norm2, norm2, ``norm * norm magnitude) of vec'')dnl
deffunc(r:vec4, vec4_norm, vec4_norm, norm, ``norm magnitude) of vec4'')dnl
deffunc(d:vec4:vec4:vec4, vec4_add, vec4_add, add, ``add v2 vector to v1 vector store result in dest'')dnl
deffunc(d:vec4:float:vec4, vec4_adds, vec4_adds, adds, ``add scalar to v vector store result in dest d = v + vec s))'')dnl
deffunc(d:vec4:vec4:vec4, vec4_sub, vec4_sub, sub, ``subtract b vector from a vector store result in dest d = v1 - v2)'')dnl
deffunc(d:vec4:float:vec4, vec4_subs, vec4_subs, subs, ``subtract scalar from v vector store result in dest d = v - vec s))'')dnl
deffunc(v:vec4:vec4:vec4, vec4_mul, vec4_mul, mul, ``multiply two vector component-wise multiplication)'')dnl
deffunc(d:vec4:float:vec4, vec4_scale, vec4_scale, scale, ``multiply/scale vec4 vector with scalar: result = v * s'')dnl
deffunc(d:vec4:float:vec4, vec4_scale_as, vec4_scale_as, scale_as, ``make vec4 vector scale as specified: result = unit v) * s'')dnl
deffunc(d:vec4:vec4:vec4, vec4_div, vec4_div, div, ``div vector with another component-wise division: d = v1 / v2'')dnl
deffunc(d:vec4:float:vec4, vec4_divs, vec4_divs, divs, ``div vec4 vector with scalar: d = v / s'')dnl
deffunc(d:vec4:vec4:vec4, vec4_addadd, vec4_addadd, addadd, ``add two vectors and add result to sum'')dnl
deffunc(d:vec4:vec4:vec4, vec4_subadd, vec4_subadd, subadd, ``sub two vectors and add result to dest'')dnl
deffunc(d:vec4:vec4:vec4, vec4_muladd, vec4_muladd, muladd, ``mul two vectors and add result to dest'')dnl
deffunc(d:vec4:float:vec4, vec4_muladds, vec4_muladds, muladds, ``mul vector with scalar and add result to sum'')dnl
deffunc(v:vec4, vec4_flipsign, vec4_flipsign, flipsign, ``flip sign of all vec4 members'')dnl
deffunc(d:vec4:vec4, vec4_flipsign_to, vec4_flipsign_to, flipsign_to, ``flip sign of all vec4 members and store result in dest'')dnl
deffunc(v:vec4, vec4_inv, vec4_inv, inv, ``make vector as inverse/opposite of itself'')dnl
deffunc(d:vec4:vec4, vec4_inv_to, vec4_inv_to, inv_to, ``inverse/opposite vector'')dnl
deffunc(d:vec4:vec4, vec4_normalize_to, vec4_normalize_to, normalize_to, ``normalize vec4 to dest'')dnl
deffunc(v:vec4, vec4_normalize, vec4_normalize, normalize, ``normalize vec4 and store result in same vec'')dnl
deffunc(r:vec4:vec4, vec4_distance, vec4_distance, distance, ``distance between two vectors'')dnl
deffunc(d:vec4:vec4:vec4, vec4_maxv, vec4_maxv, maxv, ``max values of vectors'')dnl
deffunc(d:vec4:vec4:vec4, vec4_minv, vec4_minv, minv, ``min values of vectors'')dnl
deffunc(v:vec4:float:float, vec4_clamp, vec4_clamp, clamp, ``clamp vector's individual members between min and max values'')dnl
deffunc(d:vec4:vec4:float:vec4, vec4_lerp, vec4_lerp, lerp, ``linear interpolation between two vector'')dnl
